# Class Assignment 4 - Report (Part 2) </br> Containers with Docker

## A - Docker

Docker is a software platform that allows us to build, test, and deploy applications quickly. 

Docker provides the ability to package and run an application in a loosely isolated environment called a container. The isolation and security allows you to run many containers simultaneously on a given host.

Using Docker, we can quickly deploy and scale applications into any environment and know our code will run.


### A.1- Docker architecture

Docker uses a client-server architecture. 

The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. 

The Docker client and daemon can run on the same system, or we can connect a Docker client to a remote Docker daemon. 

The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface. 

<b>Another Docker client is Docker Compose, that lets you work with applications consisting of a set of containers.</b>

![](./Part-2/images/architecture.jpg)


### A.2- Compose application model

The Compose specification allows one to define a platform-agnostic container based application. 

Such an application is designed as a set of containers which have to both run together with adequate shared resources and communication channels.

Computing components of an application are defined as Services. 

A Service is an abstract concept implemented on platforms by running the same container image (and configuration) one or more times.

Services communicate with each other through Networks. 

In this specification, a Network is a platform capability abstraction to establish an IP route between containers within services connected together.

Services store and share persistent data into Volumes. 

The specification describes such a persistent data as a high-level filesystem mount with global options. 

Actual platform-specific implementation details are grouped into the Volumes definition and may be partially implemented on some platforms.

<b>The class assignment 4 application split into a frontend web application and a backend service.</b>



Both services communicate with each other on an isolated back-tier network.

    
                  +--------------------+
                  |  frontend service  |...ro...<HTTP configuration>
                  |      "webapp"      |
                  +--------------------+
                            |
                        [backend network]
                            |
                  +--------------------+
                  |  backend service   |  r+w   ___________________
                  |     "database"     |=======( persistent volume )
                  +--------------------+        \_________________/


--------------------------------------

## B- Implementation

##### 1- Create new directory 'docker-compose' in personal repo '~/Documents/git/repos/devops-21-22-atb-1211757/CA4'.

<br>
##### 2- Create new directories 'db' and 'web into 'docker-compose'.

<br>
##### 3- Create a file named 'Dockerfile' (with no file extension) into folder 'db' with the following content:

    FROM ubuntu

    RUN apt-get update && \
    apt-get install -y openjdk-8-jdk-headless && \
    apt-get install unzip -y && \
    apt-get install wget -y

    RUN mkdir -p /usr/src/app

    WORKDIR /usr/src/app/

    RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

    EXPOSE 8082
    EXPOSE 9092

    CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

<br>
##### 4- Create a file named 'Dockerfile' (with no file extension) into folder 'web' with the following content:

    FROM tomcat:8-jdk8-temurin

    RUN apt-get update -y 

    RUN apt-get install sudo nano git nodejs npm -f -y

    RUN apt-get clean && rm -rf /var/lib/apt/lists/*

    RUN mkdir -p /tmp/build

    WORKDIR /tmp/build

    RUN git clone https://danielSSantosTorres@bitbucket.org/danielssantostorres/devops-21-22-atb-1211757.git

    WORKDIR /tmp/build/devops-21-22-atb-1211757/CA2/Part-2/react-and-spring-data-rest-basic

    RUN chmod u+x gradlew

    RUN ./gradlew clean build 

    RUN cp build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

    EXPOSE 8080

<br>
##### 5- Create a file named 'docker-compose' into '~/Documents/git/repos/devops-21-22-atb-1211757/CA4/docker-compose', with the following content:

    version: '3'

    services:

        web:
            build: web
            ports:
                - "8080:8080"
            networks:
                default:
                    ipv4_address: 192.168.56.10
            depends_on:
                - "db"

        db:
            build: db
            ports:
                - "8082:8082"
                - "9092:9092"
            volumes:
                - ./data:/usr/src/data
            networks:
                default:
                    ipv4_address: 192.168.56.11

    networks:
        default:
            ipam:
                driver: default
                config:
                  - subnet: 192.168.56.0/24

<br>
##### 6- From Git Bash command line, run 'docker-compose build'

    docker-compose build


<br>
##### 7- Check if the images were properly created. From Git Bash command line, run 'docker-images'

    docker images

<br>
##### 8- From Git Bash command line, run 'docker-compose up'

    docker-compose up

<br>
##### 9- Everything went well, and the application worked as expected.

From host, it's possible access the H2 console:

![](./Part-2/images/5-result-browser-h2-localhost8082-login.jpg)

![](./Part-2/images/5-result-browser-h2-localhost8082.jpg)

It's also possible open the spring web application.

![](./Part-2/images/5-result-browser-web-localhost8080.jpg)

##### 10- Tag and publish the images ('db' and 'web') to Docker Hub.

    docker tag docker-compose_web 1211757/devops:dockerComposeWeb

    docker tag docker-compose_web 1211757/devops:dockerComposeDB

    docker push 1211757/devops:dockerComposeWeb

    docker push 1211757/devops:dockerComposeDB

<br>
##### 11- Access to Docker Desktop to confirm that the images are available on the remote repository.

![](./Part-2/images/10-check-remote-repos-docker-hub.jpg)

<br>
##### 11- Remove old tags.

    docker rmi docker-compose_web:latest

    docker rmi docker-compose_db:latest


<br>
##### 12- Use a volume with the db container to get a copy of the database file.

    winpty docker exec -it 3c6a31a59321 bash

    cp ./jpadb.mv.db /usr/src/data


File <b>jpadb.mv.db</b> is now in the ~/Documents/git/repos/devops-21-22-atb-1211757/CA4/Part-2/docker-compose/<b>data</b> folder, as expected.

![](./Part-2/images/check-file.jpg)


--------------------------------------


## B- Kubernetes

As applications grow to span multiple containers deployed across multiple servers, <b>operating them becomes more complex</b>. 

While Docker provides an <b>open standard for packaging and distributing containerized apps</b>, the potential complexities can add up fast. 

Kubernetes provides the potential to <b>orchestrate and manage all your container resources </b> from a single control plane. 

It helps with <b>networking, load-balancing, security, and scaling </b> across all Kubernetes nodes which runs your containers.

The difference between Kubernetes and Docker is more easily understood when framed as a “both-and” question. Kubernetes and Docker are fundamentally different technologies that <b>work well together for building, delivering, and scaling containerized apps</b>.

Combining DevOps practises with containers and Kubernetes further enables a baseline of microservices architecture that promotes <b>fast delivery and scalable orchestration of cloud-native applications</b>.


### B.1- Kubernetes as alternative to Docker Compose

Kubernetes runs containers over a number of computers, virtual or real. Docker Compose runs containers on a single host machine. 

![](./Part-2/images/k8s-vs-compose-fig-01.jpeg)

One of the attractive features of Docker Compose is that it allows us to run many containers on a single host as distinct services, and each service can be configured to run on one or many services on a particular Docker Compose network. 

Kubernetes, on the other hand, is intended to run containers as services over one or many machines, virtual or real.

The important thing to understand about the difference between Docker Compose is that while under Docker Compose, a container and its corresponding service and network run on a single machine, under Kubernetes containers are organized according to an abstraction called a pod. A pod can be used by one or many services and pods associated with a single service are usually distributed over a number of machines.

### B.2- Kubernetes as alternative - Theorethical Implementation

According to the literature, to implement this alternative the main steps would be:

1- Instal kubernetes and minikube.

2- Deploy 'db' and 'web' docker images from DockerHub.

3- Install kompose to convert from docker-compose.yaml to kubernetes objects.

4- Run 'kompose convert' command. This commands should generate multiple kubernetes objects.

5- Set docker image tags and networking configuration in yaml deployment files (should exist an yaml deployment file foreach container) .

6- Run 'minikube start' command to start minikube. This command will create a docker based virtual box.

7- Start the services to run the aplication.