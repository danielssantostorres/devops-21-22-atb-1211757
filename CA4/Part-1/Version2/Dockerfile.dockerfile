
# "Vanilla" builds of OpenJDK (an open-source implementation of the Java Platform, Standard Edition)
FROM openjdk:11

# Change workdir
WORKDIR /root

# Copy jar of local host
COPY basic_demo-0.1.0.jar .

# To identify all ports exposed
EXPOSE 59001

# Run server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001