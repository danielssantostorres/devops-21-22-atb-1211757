
# Ubuntu Docker Official Image
FROM ubuntu

# Noninteractive mode is useful when we need zero interaction while installing or upgrading the system via apt. 
ARG DEBIAN_FRONTEND=noninteractive

# Configure environment
RUN apt-get update -y && apt-get install -y git \
openjdk-8-jdk-headless \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

# Change workdir
WORKDIR /tmp-build

# Clone personal repo
RUN git clone https://danielSSantosTorres@bitbucket.org/danielssantostorres/devops-21-22-atb-1211757.git

# Change workdir to gradle folder
WORKDIR /tmp-build/devops-21-22-atb-1211757/CA4/Part-1/gradle_basic_demo

# Made the file executable for user
RUN chmod u+x gradlew

RUN ./gradlew clean build 

# To identify all ports exposed
EXPOSE 59001

# Run server
CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001