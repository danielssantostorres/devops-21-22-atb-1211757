# Class Assignment 4 - Part 1
## Containers with ![](./Part-1/Version1/images/Docker-build-900x551.jpg)

### 1- Brief introduction to Docker

Docker is a software platform that allows us to build, test, and deploy applications quickly. 

Docker provides the ability to package and run an application in a loosely isolated environment called a container. The isolation and security allows you to run many containers simultaneously on a given host.

Using Docker, we can quickly deploy and scale applications into any environment and know our code will run.

![](./Part-1/Version1/images/how-docker-works.jpg)

### 3- Procedure

1- Create new directory 'CA4' in personal repo '~/Documents/git/repos/devops-21-22-atb-1211757'.

    mkdir CA4

2- Change the current working directory to 'CA4' and create new directory 'Part-1'.

    cd CA4/

    mkdir Part-1

3- Change the current working directory to 'Part-1' and clone the chat application.

    cd Part-1/

    git clone https://bitbucket.org/luisnogueira/gradle_basic_demo/

#### 3.1- Build the chat server *inside* the Dockerfile

1- Create new directory 'Version1' in '~/Documents/git/repos/devops-21-22-atb-1211757/Part-1'.

    mkdir Version1

2- Change the current working directory to 'Version1'.

    cd Version1/

3- Create dockerfile named 'Dockerfile'.

    # Ubuntu Docker Official Image
    FROM ubuntu

    # Noninteractive mode is useful when we need zero interaction while installing or upgrading the system via apt. 
    ARG DEBIAN_FRONTEND=noninteractive
    
    # Configure environment
    RUN apt-get update -y && apt-get install -y git \
    openjdk-8-jdk-headless \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

    # Change workdir
    WORKDIR /tmp-build

    # Clone personal repo
    RUN git clone https://danielSSantosTorres@bitbucket.org/danielssantostorres/devops-21-22-atb-1211757.git

    # Change workdir to gradle folder
    WORKDIR /tmp-build/devops-21-22-atb-1211757/CA4/Part-1/gradle_basic_demo

    # Made the file executable for user
    RUN chmod u+x gradlew

    RUN ./gradlew clean build 

    # To identify all ports exposed
    EXPOSE 59001

    # Run server
    CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

4- Build the image from dockerfile.

    docker build . -t my_first_image -f Dockerfile.dockerfile
 
- Although the default path for docker build is *'PATH/Dockerfile'*, I have to add option <b>-f</b> to specify dockerfile name and extension.
- Add option <b>-t</b> to name the image.

5- Run process in a isolated container.

    docker run -p59001:59001 --name my_first_container my_first_image

- Add option <b>-p</b> to make the exposed port (59001) accessible on the host. The port will be available to any client that can reach the host.
- Add option <b>--name</b> to name the container.

6- Change the current working directory to '~/devops-21-22-atb-1211757/CA4/Part-1/gradle_basic_demo' and run:

    ./gradlew clean build 

    ./gradlew runClient

The chat window pop-out.

![](./Part-1/Version1/images/1-dockerfile-build-and-run/5-host-clean-build-runClient-result.jpg)


7- Login to a docker registry.

    docker login -u 1211757

- Option <b>-u</b> to specify the username.

8- To change the image tag, run docker tag. This step is not necessary. It was kind of an experience.

    docker tag my_first_image:latest my_first_image:ca1pt1

9- Tag local image 'my_first_image' into the repository '1211757/devops' with tag 'chatServer'.

    docker tag my_first_image:ca4pt1 1211757/devops:chatServer2

10- Push image to dockerhub.

    docker push 1211757/devops:chatServer

![](./Part-1/Version1/images/2-tag-and-push/4-dockerhub-repo.jpg)



#### 3.2- Build the chat server in host computer and copy the jar file *into* the Dockerfile

1- Create new directory 'Version2' in '~/devops-21-22-atb-1211757/CA4/Part-1/'.

    mkdir Version2

2- Change the current working directory to 'Version2'.

    cd Version2/

3- Copy jar file 'basic_demo-0.1.0.jar' from '~/devops-21-22-atb-1211757/CA4/Part-1/gradle_basic_demo/build/libs/'.

    cp /c/Users/danie/Documents/git/repos/devops-21-22-atb-1211757/CA4/Part-1/gradle_basic_demo/build/libs/basic_demo-0.1.0.jar /c/Users/danie/Documents/git/repos/devops-21-22-atb-1211757/CA4/Part-1/Version2

4- Create dockerfile named 'Dockerfile'.

    # "Vanilla" builds of OpenJDK (an open-source implementation of the Java Platform, Standard Edition)
    FROM openjdk:11

    # Change workdir
    WORKDIR /root

    # Copy jar of local host
    COPY basic_demo-0.1.0.jar .

    # To identify all ports exposed
    EXPOSE 59001

    # Run server
    CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

5- Build the image from dockerfile.

    docker build . -t ca4_pt1_version2 -f Dockerfile.dockerfile
 
- Although the default path for docker build is *'PATH/Dockerfile'*, I have to add option <b>-f</b> to specify dockerfile name and extension.
- Add option <b>-t</b> to name the image.

6- Execute docker run to processes in a isolated container.

    docker run -p59001:59001 --name ca4_pt1_version2 ca4_pt1_version2

- Add option <b>-p</b> to make the exposed port (59001) accessible on the host. The port will be available to any client that can reach the host.
- Add option <b>--name</b> to name the container.

7- Change the current working directory to '~/devops-21-22-atb-1211757/CA4/Part-1/gradle_basic_demo' and run:

    ./gradlew clean build 

    ./gradlew runClient

The chat window pop-out.

![](./Part-1/Version2/images/4-docker-runClient.jpg)

8- Login to a docker registry.

    docker login -u 1211757

- Option <b>-u</b> to specify the username.

9- Tag local image 'ca4_pt1_version2' into the repository '1211757/devops' with tag 'chatServer2'.

    docker tag ca4_pt1_version2 1211757/devops:chatServer2

10- Push image to dockerhub.

    docker push 1211757/devops:chatServer2

![](./Part-1/Version2/images/6-check-dockerhub.jpg)