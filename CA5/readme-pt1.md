# Class Assignment 5 - Part 1

## CI / CD Pipelines with Jenkins

### Procedure

#### 1- Install Jenkins

To install Jenkins on Windows machine, I used the <b>Jenkins Windows MSI installer</b>, avaiable on https://www.jenkins.io/doc/book/installing/windows/.

The Jenkins Windows MSI installer runs Jenkins as a <b>windows service on port 8080</b> by default. 

![](./Part-1/Images/3-successful-install.jpg)
<br>

##### 1.1 - Troubleshooting Windows installation: Invalid service logon credentials

When installing Jenkins on <b>*Windows 11 Home Version 21H2*</b>, I had an error related to <b>"Invalid Logon"</b>.

![](./Part-1/Images/1-invalid-windows-credentials.jpg)

After some research, I realized that when installing a service to run under a domain user account, the account must have the right to logon as a service. 

To solve the problem, at first I had to <b>enable Local Security Policy</b> (secpol.msc).

Then, I <b>edited the Local Security Policy</b> to define the ‘logon as a service’ permission (add the user "danie")

![](./Part-1/Images/2-secpol-solution.jpg)


#### 2- Post-installation setup

Browse to http://localhost:8080/ (default port).

<b>The post-installation setup wizard begins</b>.

The first step was unlock Jenkins.

![](./Part-1/Images/4-post-setup.jpg)

<b>Completing the setup wizard, I had access to the jenkins dashboard.</b> 

![](./Part-1/Images/4-post-setup-result.jpg)

#### 3- Create a Jenkinsfile

Then, I created a Jenkinsfile in <b>CA2/Part-1/gradle_basic_demo</b> directory.

    pipeline {
        agent any
        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'danielssantostorres-devops-21-22-atb-1211757',
                    url:'https://bitbucket.org/danielssantostorres/devops-21-22-atb-1211757'
                }
            }
            stage('Assemble') {
                steps {
                    echo 'Assembling...'
                    dir('CA2/Part-1/gradle_basic_demo') {
                        script {
                            if (isUnix()) {
                                sh 'chmod +x gradlew'
                                sh './gradlew assemble'
                            }
                            else {
                                bat './gradlew assemble'
                            }
                        }
                    }
                }
            }
            stage('Test') {
                steps {
                    echo 'Testing...'
                    dir('CA2/Part-1/gradle_basic_demo') {
                        script {
                            if (isUnix()) {
                                sh 'chmod +x gradlew'
                                sh './gradlew test'
                                junit 'build/test-results/test/*.xml'
                            }
                            else {
                                bat './gradlew test'
                                junit 'build/test-results/test/*.xml'
                            }
                        }
                    }
                }
            }
            stage('Archiving') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'CA2/Part-1/gradle_basic_demo/build/distributions/*'
                }
            }
        }
    }

#### 4- Add new Job to Jenkins dashboard

I have finally made a new Jenkins job named "gradle_basic_demo". 

The pipeline configuration includes:
* <b>Repository URL:</b> https://bitbucket.org/danielssantostorres/devops-21-22-atb-1211757
* <b>Credentials:</b> (according Bitbucket credentials)
* <b>Script Path:</b> CA2/Part-1/gradle_basic_demo/Jenkinsfile
</br>

![](./Part-1/Images/5-create-job.jpg)

#### 5- Build

<b>After all the work described above, I have manually started the build and everything went as expected. Build successfully! </b>

![](./Part-1/Images/6-final.jpg)
