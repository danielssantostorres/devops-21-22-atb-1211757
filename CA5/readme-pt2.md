# Class Assignment 5 - Report (Part 2) </br> CI / CD Pipelines with Jenkins

## Introduction
In Chapter A, I'll present and briefly compare two Continuous Integration and Delivery tools (CI/CD): Jenkins and Buddy (BDY Sp. z o.o. Sp. k.).

Chapter B intends to be a guide to execute the part 2 of class assignment 5, using Jenkins.

Right away, chapter C intends to be a guide to execute the part 2 of class assignment 5, using Buddy.

The final chapter contains few personal considerations.

## A- Analysis of the alternatives

![](./Part-2/Images/jenkins-buddy.jpg)

Jenkins is a self-contained, open source automation server which can be used to automate all sorts of tasks related to building, testing, and delivering or deploying software.

Jenkins can be installed through native system packages, Docker, or even run standalone by any machine with a Java Runtime Environment (JRE) installed.

Jenkins is the leading open source automation server and provides hundreds of plugins to support building, deploying and automating any project.

Buddy is a cloud-based CI/CD and DevOps automation platform which removes the chores of configuring and managing Jenkins with a smart UI/UX that makes it very easy to build, test, and deploy software. 

With Buddy, pipelines are created with over 100 ready-to-use actions, that can be arranged in any way.

The table below presents a brief summary of the main differences between this tools:

| Category | Buddy | Jenkins|
| ---------| ------|--------|
|Installation|No installation needed|Requires installation|
|Maintenane|No maintenance needed|Requires regular patching, tuning and cleaning|
|Integrations|Pre-made and tested integrations|Open source plugins|
|Pipelines|Scripted and UI Driven|Scripted and UI Driven|
|Pricing|Subscription based|Free|
|Customisation|Many highly tested options to choose from, but less control of new integrations | If a new technology came out next week, a Jenkins plugin would likely pop up quickly. It would be provided by a vast open source community.
|Specialist Knowledge|It is possible to go from 0 to running with Buddy in minutes|Requires a good understanding of Jenkins|

## B- Jenkins Implementation

### 1- Add a new Job in Jenkins 

For this class assignement, add a new Job in Jenkins. Configure:
* <b>Repository URL:</b> https://bitbucket.org/danielssantostorres/devops-21-22-atb-1211757
* <b>Credentials:</b> (according Bitbucket credentials)
* <b>Script Path:</b> CA2/Part-2/react-and-spring-data-rest-basic/Jenkinsfile
</br>

![](./Part-2/Images/4-job-creation-jenkins.jpg)

### 2- Install Jenkins Plugins
Plugins are the primary means of enhancing the functionality of a Jenkins environment to suit user-specific needs.

The simplest and most common way of installing plugins is through the <b>Manage Jenkins > Manage Plugins</b> view, available to administrators of a Jenkins environment.

![](./Part-2/Images/1-install-plugins.jpg)

#### 2.1- HTML Publisher Plugin

To publish HTML reports (javadoc), install HTML Publisher plugin.

![](./Part-2/Images/3-publisher-plugin.jpg)

#### 2.2- Docker Pipeline Plugin

To build and use Docker containers from pipelines, install Docker Pipeline plugin:

![](./Part-2/Images/3-docker-plugin.jpg)

### 3- Add Docker Hub credentials to Jenkins

Docker Hub is a 3rd-party application that interact with Jenkins, to build and push a Docker image. 

Add Docker Hub credentials in Jenkins, to allow them to be used by pipeline to interact with Docker Hub.

To add new global credentials, from the Jenkins Dashboard click <b>Manage Jenkins > Manage Credentials</b>.
</br>

![](./Part-2/Images/2-manage-credentials.jpg)

</br>
<p>Then, create an <b>internal unique ID</b>  by which the credentials are identified from the pipeline. </p>

![](./Part-2/Images/2-manage-credentials-input.jpg)

<p>Finally, in Jenkins Dashboard, validate that Docker Hub credentials was successfully created. </p>
</br>

![](./Part-2/Images/2-manage-credentials-result.jpg)

### 4- Create Dockerfile

Add a Dockerfile in <b>CA2/Part-2/react-and-spring-data-rest-basic</b> directory, to copy the WAR file to the deployment directory of the Tomcat server.

    FROM tomcat:8-jdk8-temurin

    COPY build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

    EXPOSE 8080

### 5- Create Jenkinsfile

Add Jenkinsfile in <b>CA2/Part-2/react-and-spring-data-rest-basic</b> directory.
    
    pipeline {
        agent any
        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'danielssantostorres-devops-21-22-atb-1211757',
                    url:'https://bitbucket.org/danielssantostorres/devops-21-22-atb-1211757'
                }
            }
            stage('Assemble') {
                steps {
                    echo 'Assembling...'
                    dir('CA2/Part-2/react-and-spring-data-rest-basic') {
                        script {
                            if (isUnix()) {
                                sh 'chmod +x gradlew'
                                sh './gradlew assemble'
                            }
                            else {
                                bat './gradlew assemble'
                            }
                        }
                    }
                }
            }
            stage('Test') {
                steps {
                    echo 'Testing...'
                    dir('CA2/Part-2/react-and-spring-data-rest-basic') {
                        script {
                            if (isUnix()) {
                                sh 'chmod +x gradlew'
                                sh './gradlew test'
                                junit 'build/test-results/test/*.xml'
                            }
                            else {
                                bat './gradlew test'
                                junit 'build/test-results/test/*.xml'
                            }
                        }
                    }
                }
            }
            stage('Archiving') {
                steps {
                    echo 'Archiving...'
                    dir('CA2/Part-2/react-and-spring-data-rest-basic') {
                        archiveArtifacts 'build/libs/*.war'
                    }
                }
            }
            stage('Javadoc') {
                steps {
                    echo 'Javadocking...'
                    dir ("CA2/Part-2/react-and-spring-data-rest-basic"){
                        javadoc javadocDir: 'build/reports/tests/test',
                        keepAll: false
                    }
                }
            }
            stage('HTML') {
                steps {
                    echo 'Publishing HTML...'
                    dir ("CA2/Part-2/react-and-spring-data-rest-basic"){
                        publishHTML([
                        allowMissing: false, 
                        alwaysLinkToLastBuild: false, 
                        keepAll: false, 
                        reportDir: 'build/reports/tests/test', 
                        reportFiles: 'index.html', 
                        reportName: 'HTML Report', 
                        reportTitles: 'Docs Loadtest Dashboard'
                        ])
                    }
                }
            }
            stage('Build and Push Docker Image') {
                steps {
                    echo 'DockerImaging...'
                    script {
                        docker.withRegistry('', '1211757-dockerhub') {
                            app = docker.build("1211757/devops:appimage-${env.BUILD_ID}",
                                'CA2/Part-2/react-and-spring-data-rest-basic')
                            app.push()
                        }
                    }
                }
            }
        }
    }


#### 6- Build

After a few tries, build successful!

![](./Part-2/Images/5-build-status.jpg)

<p>Confirm that image was published im DockerHub.</p>

![](./Part-2/Images/6-docker-hub-desktop-img.jpg)


## C- Buddy Implementation

#### 1- Create new Bitbucket repository with CA2-Part2 content

I decided to create another Bitbucket repository designated by '1211757-devops-ca2pt2'.

![](./Part-2/Images/buddy/0-bitbucket-repo.jpg)

Then, I copy-pasted the contents of class assignment 2 part 2 from my personal repository and push to remote.

![](./Part-2/Images/buddy/2-bitbucket-repo.jpg)

#### 1- Sign up Buddy

On the first attempt to register on Buddy with student email account (1211757@isep.ipp.pt), I got an unexpected error ('Your account has been flagged').

![](./Part-2/Images/buddy/0-account-flagged.jpg)

I did contact Buddy support to have my account status approved, but in the meantime I made another register with my personal email (danielsousatorres-pro@outlook.pt). This time everything went smooth and the account was quickly approved. The registration process is very simple and fast. Unfortunately, it didn't go well with the student email.

![](./Part-2/Images/buddy/1-account-buddy-daniel.jpg)

#### 2- Create new Buddy project

On Buddy Dashboard, I created new project designated by '1211757-devops-ca2pt2'.

Then, I set the git provider (Bitbucket) and selected the repository that I created for this class.

![](./Part-2/Images/buddy/3-budy-new-project.jpg)

Project was successfully created.

![](./Part-2/Images/buddy/3-budy-new-project-1.jpg)

#### 3- Create new pipeline

Inside the previously created project, I created a pipeline designated by 'ca5pt2_buddy'.

![](./Part-2/Images/buddy/4-create-new-pipeline.jpg)

#### 4- Add action: Assemble

![](./Part-2/Images/buddy/assemble/assemble-0.jpg)

#### 5- Add action: Test

![](./Part-2/Images/buddy/test/test.jpg)

#### 6- Add action: Javadoc

![](./Part-2/Images/buddy/javadoc/javadoc.jpg)

#### 7- Add action: Build Docker Image

![](./Part-2/Images/buddy/docker/docker-build.jpg)

#### 8- Add action: Push Docker Image 

![](./Part-2/Images/buddy/docker/docker-push.jpg)

#### 9- Run

The whole process was very simple and fast, and quickly I was ready to run the pipeline. 

When I executed the "run" command, all actions passed. 

![](./Part-2/Images/buddy/run.jpg)

I verified that Unit Tests were executed and published in Buddy.

![](./Part-2/Images/buddy/6-confirm-tests.jpg)

I verified that the war file was generated and archived by Buddy.

![](./Part-2/Images/buddy/7-confirm-war.jpg)

I verified that javadoc of the project was generated and published in Buddy.

![](./Part-2/Images/buddy/8-confirm-javadoc.jpg)

I veified that Docker image was published on DockerHub.

![](./Part-2/Images/buddy/9-confirm-dockerhub.jpg)

## D- Final considerations / Conclusion

This class assignement was one of my favorite, because we apply all the knowledge from previous classes. I felt like I was actually doing a little of the work of a devops engineer.

Between the two aforementioned tools - Jenkins and Buddy -, I found it a greater challenge to use jenkins. I had already used jenkins previously, but without understanding how it really works *under the hood*.

If I had to choose between Jenkins and Buddy, I would choose to use Jenkins because:
- Jenkins is free and open-source;
- Jenkins is the leading open source automation server, that has been in development since 2004 and is one of the most popular tools of its kind. This means that its technology is very mature and there is a lot of documentation and resources available for it;
- Jenkins provides hundreds of plugins, wich supports most of the technological stacks;
- Jenkins has a large and helpful community, which welcomes new users and provides a great number of tutorials.

Despite that, I really enjoyed using Buddy. The ease to setup custom pipelines are amazing, can easily various settings quickly and then be ready to deploy. 

Buddy entirely fulfil is commitment to make it very easy to build, test, and deploy software with a smart UI/UX. It's really possible to go from zero to running with Buddy in minutes.


