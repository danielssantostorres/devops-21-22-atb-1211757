# Class Assignment 1 <br> Build Tools with Gradle

## Implementation description

<b>1- Create Bitbucket issues</b>

![](./Images/1-CreateBitbucketIssues.jpg)

<b>2- Clone the example application available at https://bitbucket.org/luisnogueira/gradle_basic_demo/ (delete .git folder)</b>

![](./Images/2-Clone-repo-and-delete-git-folder.jpg)

<b>3- Commit and push to remote, resolving issue 5.</b>

![](./Images/3-Commit-and-push.jpg)

<b>4- Read the instructions available in the readme file.</b>

![](./Images/4-Read-Instructions.jpg)

<b>5- Experiment with the application.</b>

![](./Images/5-Experiment-the-app.jpg)

<b>6- Add a new task to execute the server.</b>

![](./Images/6-Add-new-task-to-build-gradle.jpg)

<b>6.1- Commit and push to remote, resolving issue 6.</b>

![](./Images/6.1-Add-new-task-to-build-gradle-commit.jpg)

<b>7- Add a simple unit test and update the gradle script so that it is able to execute the
test.</b>

![](./Images/7-Add%20-simple-unit-test-and-update-gradle-script.jpg)

<b>7.1- Running tests from terminal. </b>

![](./Images/7.0-Run-tests.jpg)

<b>7.2- Commit and push to remote, resolving issue 7.</b>

![](./Images/7.1-Add%20-simple-unit-test-and-update-gradle-script-commit.jpg)

<b>8- Add a new task of type Copy to be used to make a backup of the sources of the application. It should copy the contents of the src folder to a new backup folder.</b>

![](./Images/8-Add-task-to-copy-files.jpg)

<b>8.1- Commit and push to remote, resolving issue 8.</b>

![](./Images/8.1-Add-task-to-copy-files-commit.jpg)

<b>9- Add a new task of type Zip to be used to make an archive (i.e., zip file) of the
sources of the application. It should copy the contents of the src folder to a new zip file.</b>

![](./Images/9-Add-task-to-make-an-archive-of-src-zip.jpg)

<b>9.1- Commit and push to remote, resolving issue 9.</b>

![](./Images/9.1-Add-task-to-make-an-archive-of-src-zip-commit.jpg)

<b>10- At the end of the part 1 of this assignment mark your repository with the tag
ca2-part1.</b>

![](./Images/10-Add-final-tag.jpg)

<b>11- Check all issues at Bitbucket. </b>

![](./Images/11-All-issues-resolved.jpg)

<b>12- Check commits at Bitbucket. </b>

![](./Images/12-All-commits.jpg)

## Extra

<b>13- Create Script Plugin for «copyFiles» and «createZip» tasks. </b>

![](./Images/13-Extra-Create-Script-Plugin.jpg)

<b>13.1- Commit and push to remote, resolving issue 10.</b>

![](./Images/13.1-Extra-Create-Script-Plugin-Commit.jpg)
