import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class EmployeeTest {

    @Test
    public void createEmployeeWithValid() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act
        Employee expected = new Employee(firstName, lastName, description,
                jobTitle, jobYears, email);

        //Assert
        assertEquals(firstName, expected.getFirstName());
    }

    @Test
    public void createEmployeeFailsBecauseInvalidFirstName() {
        //Arrange
        String firstName = "aa";
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

    @Test
    public void createEmployeeFailsBecauseFirstNameIsNull() {
        //Arrange
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee(null, lastName, description, jobTitle, jobYears,
                    email);
        });
    }

    @Test
    public void createEmployeeFailsBecauseInvalidLastName() {
        //Arrange
        String firstName = "John";
        String lastName = "aa";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

    @Test
    public void createEmployeeFailsBecauseLastNameIsNull() {
        //Arrange
        String firstName = "John";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee(firstName, null, description, jobTitle, jobYears,
                    email);
        });
    }

    @Test
    public void createEmployeeFailsBecauseEmailIsNull() {
        //Arrange
        String firstName = "John";
        String lastName = "Teewt";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = null;

        //Act && Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee(firstName, null, description, jobTitle, jobYears,
                    email);
        });
    }

    @Test
    public void createEmployeeFailsBecauseInvalidDescription() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String description = "pp";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

    @Test
    public void createEmployeeFailsBecauseDescriptionIsNull() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee(firstName, lastName, null, jobTitle, jobYears, email);
        });
    }

    @Test
    public void createEmployeeFailsBecauseInvalidJobTitle() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "t";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

    @Test
    public void createEmployeeFailsBecauseInvalidJobYears() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "t";
        int jobYears = -2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

    @Test
    public void createEmployeeFailsBecauseInvalidEmail() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "t";
        int jobYears = -2;
        String email = "1000000isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

}
