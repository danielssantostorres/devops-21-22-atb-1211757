# Class Assignment 2 - Report (Part 2) </br> Build Tools with Gradle


## Introduction
In Chapter A, I'll present and compare three of the predominant build tools: Apache Ant, Maven and Gradle.

Chapter B intends to be a guide to convert the basic version of the tutorial application to Gradle, and how to add Gradle tasks in *build.gradle* file.

Additionally, I'll present two alternative technological solutions not based on Gradle, that are:
- Apache Ant with Ivy (Chapter C.1);
- Maven (Chapter C.2).

The final chapter contains few personal considerations about the aforementioned technological solutions for the build automation tool, in particular, compare how the build tool can be extended with new functionality.


![](./Images/other/vs.jpg)

## A- Analysis of the alternatives

### Apache Ant with Ivy (2000)
Apache Ant is a Java-based command line tool that uses XML files to define build scripts. 
It’s predominantly used for Java builds but it can also be used for C/C++ development. 
Built-in tasks provide ways to compile, assemble, test and run software applications. 
Users can also create their own “antlibs” to enhance the functionality of Ant. 
<b>Apache Ivy is a dependency management tool that integrates easily with Ant to provide a more robust ecosystem.</b> 

<b>Pros</b>
- Better control over the overall build process;
- Flexible enough to work with any work process.

<b>Cons</b>
- XML based build files can grow large and unmaintainable;
- Lots of time and resources are necessary to maintain the build scripts;
- IDE integration is difficult to achieve.
</br>

### Maven (2004)
Maven was developed to resolve the problems faced with Ant-based scripting. 
It kept the XML files but took a different approach to organization. In Ant, developers have to create all the tasks. <b>Maven decreases the task creation by implementing stronger standards for organizing code. </b>As a result, it’s easier to get started on standard projects.
<b>It also introduced dependency downloads which made the development easier</b>. Before the introduction of Ivy in Ant, users had to manage dependencies locally. Maven adopted the dependency management philosophy first.
<b>However, Mavens strict standards make it difficult to write custom build scripts. </b>
The tool is easy to work with as long as the project follow the strict standards.

<b>Pros</b>
- Automatic dependency downloads;
- All dependencies are automatically recorded in source control as part of the Maven scripts;
- Standardizes and simplifies the build process;
- Easily integrates with IDEs and CI/CD systems.

<b>Cons</b>
- Not flexible in creating custom workflows;
- Steep learning curve and the process is difficult for novices to understand;
- Time-consuming to solve build problems and new library integrations;
- Not good with multiple versions of the same dependency.
</br>

### Gradle (2008)
Gradle combines the power of Ant and Maven. 
<b>Instead of XML, Gradle provides a domain specific language, or DSL, for describing builds. As a result, build scripts in Gradle are easier to write and read.  </b>
This build language is available in Groovy and Kotlin. A Groovy build script can contain any Groovy language element. A Kotlin build script can contain any Kotlin language element. Gradle assumes that each build script is encoded using UTF-8.
It was initially using Ivy for dependency management, but <b>it is using its own dependency engine now</b>.

<b>Pros</b>
- Provides standardization while staying flexible;
- Easy to read and write build scripts;
- Better at handling multiple versions of dependencies;
- Able to handle multiple programming languages and technologies;
- Active community helping develop the tool;
- Gradle DSL (Domain-Specific Language) makes it simple configuration structure;
- Gradle provides performance improvements using incrementally, build cache and the Gradle Daemon.

<b>Cons</b>
- IDE integration not as good as Maven.

-------------------------------------------

## B- Implementation with Gradle

1- Create a folder named "Part-2" in personal repository, and starts *markdown* class assignment report.

![](./Images/1-Create-folder-ca2-pt2.jpg)

2- Create a new *branch* called "tut-basic-gradle".

![](./Images/2-Create-and-checkout-branch-tut-basic-gradle.jpg)

3- As instructed in the "readme" file of the tutorial, use https://startspring.io to
start a new gradle spring boot project with the following dependencies: 
- *Rest Repositories*; 
- *Thymeleaf*; 
- *JPA*; 
- *H2*.

![](./Images/3-spring-initializr.jpg)

4- Extract the generated *zip* file inside the folder "CA2/Part2/" of your repository.

![](./Images/4-Add-project-to-repo.jpg)

5- Check the available gradle tasks by executing *./gradlew tasks*.

![](./Images/5-run-gradle-wrapper-tasks.jpg)

6- Delete the *src* folder.

![](./Images/6-Delete-src-folder-and-commit.jpg)

7- Copy the *src* folder (and all its subfolders) from the *basic* folder of the tutorial into this new folder.
- Copy also the files *webpack.config.js* and *package.json*;
- Delete the folder src/main/resources/static/built/ since this folder should be generated from the javascrit by the webpack tool.

![](./Images/7-Copy-src-folder.jpg)

8- Experiment the application by using *./gradlew bootRun*.
Notice that the web page http://localhost:8080 is empty! This is because gradle is missing the plugin for dealing with the frontend code!

![](./Images/8-First-test-with-gradlew-bootRun.jpg)

9- Add the gradle plugin *org.siouan.frontend* to the project so that gradle is also able to manage the frontend. Configure plugin and update the scripts section in *package.json* to configure the execution of *webpack*.

![](./Images/9-Configure-plugin.jpg)

10- Execute *./gradlew build*. The tasks related to the frontend are also executed and the frontend code is generated.

![](./Images/10-Run-gradlew-build.jpg)

11- Execute the application by using *./gradlew bootRun*.

![](./Images/11-Run-gradlew-bootRun.jpg)

12- Add a task to gradle to copy the generated jar to a folder named "dist" located a the project root folder level.

![](./Images/12-Add-a-task-to-copy-libs.jpg)

13- Add a task to gradle to delete all the files generated by *webpack* (usually located at src/resources/main/static/built/). This new task should be executed automatically by gradle before the task clean.

![](./Images/13-Add-a-task-to-delete-built-folder-before-clean.jpg)

14- Merge with the master branch.

![](./Images/14-Merge-with-master-branch.jpg)

15- Push to remote.

![](./Images/14-Merge-with-master-branch-push-to-remote.jpg)

16- Verify that  all issues are resolved.

![](./Images/15-All-Issues-Resolved.jpg)

------------------------------------------

## C.1 - Alternative implementation with Apache Ant + Ivy

1- Download *Apache Ant* from ant.apache.org. 

![](./Images/Ant/1-Download-Apache-Ant.jpg)

2- Install *Apache Ant*.

![](./Images/Ant/2-Install-Apache-Ant-C.jpg)

3- Add envyronment variable *ANT_HOME*.

![](./Images/Ant/3-Add-environment-variable-ANT_HOME.jpg)

4- Update PATH.

![](./Images/Ant/4-Update-PATH-variable.jpg)

5- Check if *Apache Ant* is properly installed.

![](./Images/Ant/5-Check-Ant-Is-Installed.jpg)

6- Download *Apache Ivy* from ant.apache.org.

![](./Images/Ant/6-Download-Apache-Ivy.jpg)

7- Copy *apache-ivy-2.5.0.jar* (downloaded in the previous point) into Ant lib directory.

![](./Images/Ant/7-Copy-ivy-2-5-0-jar-to-ant-lib-folder.jpg)

8- Create simple build.XML file to check if *Apache Ivy* is properly installed.

![](./Images/Ant/8.1-Verifying-Apache-Ivy-Installation-Create-Build-File.jpg)

9- Call *ant* command; if Ivy is properly installed, build should be successful.

![](./Images/Ant/8-Verifying-Apache-Ivy-Installation-Build-Successful.jpg) 

10- Copy the *src* folder (and all its subfolders) from the *basic* folder of the tutorial.

![](./Images/Ant/9-Copy-tut-react-and-spring-data-rest-main-basic-src-folder-to-repo.jpg)

11- Create build.XML and ivy.XML files.

![](./Images/Ant/10-Create-build-and-ivy-XML-files.jpg)

![](./Images/Ant/10.1-Create-build-XML-file.jpg)

![](./Images/Ant/10.2-Create-ivy-XML-file.jpg)

12- Call *ant compile*.

![](./Images/Ant/11-ant-compile.jpg)

13- Call *ant build -lib lib/compile*.

![](./Images/Ant/12-ant-build.jpg)

14- Expected result to previous command is: *Build successful*.

![](./Images/Ant/12.1-ant-build-successful.jpg)

15- Run *myapp.jar* from command line.

![](./Images/Ant/13-Execute-jar.jpg)

16- Add a task to copy the generated jar to a folder named "dist" located a the project root folder level. Add a task to delete all the files located at src/resources/main/static/built/. This new task should be executed automatically before the task clean.

![](./Images/Ant/14-Add-tasks-to-build-xml.jpg)

17- Call *copyJar* task.

![](./Images/Ant/14.1-run-copyJAR-task.jpg)

18- Call *deleteBuilt* task.

![](./Images/Ant/14.1-run-delete-files-task.jpg)

------------------------------------------

## C.2 - Alternative implementation with Maven

1- Copy the application tutorial into a local folder.

![](./Images/Maven/1-Copy-tut-react-and-spring-data-rest-project.jpg)

2- Move to the "basic" folder. Execute the application from the command line (call *./mvnw spring-boot:run*)

![](./Images/Maven/2-basic-mvnw-spring-boot-run.jpg)

3- Enter the following url in your browser: http://localhost:8080/

![](./Images/Maven/2.1-project-running.jpg)

4- Call *./mvnw install*, to build the project described by Maven POM file, and installs the resulting artifact (JAR) into local Maven repository.

![](./Images/Maven/3-Run-mvnw-install-to-generate-jar.jpg)

5- Check if JAR file is successfuly created.

![](./Images/Maven/3.1-Verifying-jar-was-generated.jpg)

6- Add a plugin to copy the generated JAR to a folder named "dist", located at the project root folder level. Select the build lifecycle phase. 
According Maven Apache documentation, if you are uncertain what is most appropriate phase, the preferred phase to call is <b>verify</b>. 

![](./Images/Maven/4-Add-plugin-to-copy-jar-to-folder-dist.jpg)

7- Call *./mvnw verify*. The previous created plugin will be executed.

![](./Images/Maven/4-Add-plugin-to-copy-jar-to-folder-dist-Run-with-mvnw-verify.jpg)

8- Verify that a folder named "dist" was created at project root folder level, with the JAR file inside.

![](./Images/Maven/4-Add-plugin-to-copy-jar-to-folder-dist-Verifying-Sucess.jpg)

9- Add a plugin to delete all the files located at src/resources/main/static/built/. This new task should be executed automatically before the task clean. So, select the build lifecycle phase: <b>pre-clean</b> (execute processes needed prior to the actual project cleaning).

![](./Images/Maven/5-Add-plugin-to-delete-built-content.jpg)

10- Call *./mvnw clean*.

![](./Images/Maven/5-Add-plugin-to-delete-built-content-Run-with-mvnw-clean.jpg)

11- Verify that all the files located at src/resources/main/static/built/ was deleted.

![](./Images/Maven/5-Add-plugin-to-delete-built-content-Verifying-result.jpg)


-----------------------------------

## Final considerations | Conclusion

With Gradle, task customizations are very simple. You can write them directly into the build script. 
In Maven, plugins are less intuitive and more complex than Gradle, despite there are a lot of available plugins. 
Ant does not provide plugins, and scripts are more complex than other tools. 

Gradle have much better documentation and community support.
Maven and Ant do not compare on either level, although a long history and large community of current/past users can be a valuable resource.

Overall, in my personal experience, I prefer Gradle to Ant and Maven, based on Gradle extreme simplicity, readability, customizability and documentation.
