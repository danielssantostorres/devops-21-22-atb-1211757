# Class Assignment 3 - Report (Part 2) </br> Virtualization with Vagrant


## Introduction
In Chapter A, I'll present and briefly compare two hypervisor solutions: Windows Hyper-V and Oracle VM VirtualBox.

Chapter B intends to be a guide to execute the part 2 of class assignment 3, using Vagrant with VirtualBox provider.

Right away, chapter C intends to be a guide to execute the part 2 of class assignment 3, using Vagrant with Hyper-V provider.

The final chapter contains few personal considerations.


## A- Analysis of the alternatives

![](./Part-2/images/hyper-v-vs-vb.jpg)

Hypervisor is a software that allows you to run one or multiple virtual machines with their own operating systems (guest operating systems) on a physical computer, which is called a host machine. 

There are two types of hypervisors – type 1 and type 2.

Hyper-V is a type 1 hypervisor that is also called a bare metal hypervisor, and runs directly on a computer’s hardware. When a physical computer (a host) starts, a Hyper-V hypervisor takes control from BIOS or UEFI. Then, Hyper-V starts the management operating system, which can be Hyper-V Server, Windows, or Windows Server. Virtual machines can be started manually by user or automatically, depending on its settings.

VirtualBox is a type 2 hypervisor that is sometimes called a hosted hypervisor. A type 2 hypervisor is an application that runs on the operating system (OS) and is already installed on a host. When a physical computer starts, the operating system installed on the host loads and takes control. A user starts the hypervisor application (VirtualBox in this case) and then starts the needed virtual machines. VM hosted processes are created.

![](./Part-2/images/Type-1-and-type-2-hypervisor-1024x584.jpeg)

Hyper-V is on if the host is powered on, while the VirtualBox can be started and closed by a user on demand.

Hyper-V is a Windows-only application, whereas VirtualBox is a cross-platform application.

Clustering technologies like as failover clustering and load balancing are supported by Hyper-V. 

For firms that currently use a Windows-based environment, Microsoft's virtualization solution would be a suitable fit. 

Oracle VirtualBox is a free virtualization software that supports a large range of guest and host operating systems, making it ideal for multiplatform situations.

-------------------------------------------

## B- Virtualization with Vagrant | Vagrant VirtualBox provider

<b>1- Get Vagrantfile</b>

Get vagrantfile from https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ as an initial solution.

    git clone https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/
    cd vagrant-multi-spring-tut-demo
    rm -rf .git

<b>2- Update the Vagrantfile configuration</b>

 2.1- Add the following command to clone personal repo and use pretended version of the application:

    git clone https://danielSSantosTorres@bitbucket.org/danielssantostorres/devops-21-22-atb-1211757.git
    cd devops-21-22-atb-1211757/CA2/Part-2/react-and-spring-data-rest-basic


2.2- Add the following command deploy the war file to tomacat8:

    sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

2.3- Set networking configurations:

    db.vm.network "private_network", ip: "192.168.56.11"
    web.vm.network "private_network", ip: "192.168.56.10"

<b>3- Update the build.gradle file </b>

3.1- Add the org.siouan.frontend-jdk8 plugin to build Javascript applications with Node, NPM, Yarn 1 (Classic), Yarn 2+ (Berry): distribution management, configurable tasks (build, test, publish), additional types, NPX support.

    plugins {
        id "org.siouan.frontend-jdk8" version "6.0.0"
        ...
    }

3.2- Add the War plugin to extends the Java plugin to add support for assembling web application WAR files. It disables the default JAR archive generation of the Java plugin and adds a default WAR archive task.

    plugins {
        id 'war'
        ...
    }

3.3- Update gradle frontend plugin and add webpack run script.

    frontend{
        nodeVersion = "14.17.3"
        assembleScript = "run webpack"
    }

3.4- Source compatibility concerns translating Java source code into class files including whether or not code still compiles at all. As VM's os don't support jdk11, remove/ comment the following line:

    sourceCompatibility = '11'
    
3.5- To build a war file that is both executable and deployable into an external container, mark the embedded container dependencies as belonging to a configuration named "providedRuntime":

    dependencies {
        providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
        ...
    }


<b>4- Create ServletInitializer class </b>

(in ./src/main/java/com/greglturnquist/payroll)

This class extends the SpringBootServletInitializer and overrides the configure() method. That method uses SpringApplicationBuilder to simply register our class as a configuration class of the application:

    package com.greglturnquist.payroll;

    import org.springframework.boot.builder.SpringApplicationBuilder;
    import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

    public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }

    }

Now, if we package our application as a WAR, we'll be able to deploy it on any web container in a traditional way, which will also execute the logic we added in the configure() method.

<b>5- Update package.json </b>

Add webpack run script:

    "scripts"{
        "watch":webpack --watch -d", 
        "webpack": "webpack
    }

<b>6- Update application.properties file</b>

6.1- Change Spring Boot context path:

    server.servlet.context-path=/react-and-spring-data-rest-basic

6.2- Add the following line so that spring will no drop de database on every execution:

    spring.jpa.hibernate.ddl-auto=update

6.3- Add support for h2 console:

    spring.datasource.driverClassName=org.h2.Driver
    spring.datasource.username=sa
    spring.datasource.password=
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    spring.h2.console.enabled=true
    spring.h2.console.path=/h2-console

6.4- Add webAllowOthers to h2:

     spring.h2.console.settings.web-allow-others=true

6.5- Add settings for remote h2 database:

    spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE

<b>7- Update app.js file</b>

7.1- Update application context path:

    componentDidMount() { // <2>

		client({method: 'GET', path: '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});

	}

<b>8- Update index.html</b>

8.1- Fixes ref to css:

    <link rel="stylesheet" href="main.css" />

<b>9- Commit and push to remote.</b>

<b>10- Create and configure guest machines according to Vagrantfile. Run from command line:</b>

    vagrant up

<b>11- In the host, open the spring web application using http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/</b>

![](./Part-2/images/1-vagrant%2Boracle-virtualBox/8-vagrant-up-result/result-browser.jpg)

<b>12- Open the H2 console, using http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console</b>


A login window is displayed:

![](./Part-2/images/1-vagrant%2Boracle-virtualBox/8-vagrant-up-result/result-browser-h2-console.jpg)

Enter the following credentials to access h2 console:
    
    JDBC URL: jbdc:h2:tcp://192.168.56.11:9092/./jpadb 
    Username: sa
    Password: 

Successful access.

![](./Part-2/images/1-vagrant%2Boracle-virtualBox/8-vagrant-up-result/result-browser-h2-console-manager.jpg)



-------------------------

## C- Virtualization with Vagrant | Vagrant Hyper-V provider

The Vagrant Hyper-V provider works in almost every way like the VirtualBox, but has some limitations that are inherent to Hyper-V itself.

<b>Vagrant does not yet know how to create and configure new networks for Hyper-V.</b> When launching a machine with Hyper-V, Vagrant will prompt you asking what virtual switch you want to connect the virtual machine to.

A result of this is that <b>networking configurations in the Vagrantfile are completely ignored with Hyper-V.</b> Vagrant cannot enforce a static IP or automatically configure a NAT.

However, the IP address of the machine will be reported as part of the vagrant up, and you can use that IP address as if it were a host only network.


###Procedure:

<b>1- Get Vagrantfile</b>

Get vagrantfile from https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ as an initial solution.

    git clone https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/
    cd vagrant-multi-spring-tut-demo
    rm -rf .git

<b>2- Update the Vagrantfile configuration</b>

2.1- Set vagrant box config to "generic/ubuntu1604". This Vagrant box makes avaiable hyperv provider.

    Vagrant.configure("2") do |config|
        config.vm.box = "generic/ubuntu1604"
        ...

         # Configurations specific to the database VM
        config.vm.define "db" do |db|
            db.vm.box = "generic/ubuntu1604"
            ...
        end

        # Configurations specific to the webserver VM
        config.vm.define "web" do |web|
            web.vm.box = "generic/ubuntu1604"
            ...
        end
    end

2.1- Add the following command to clone personal repo and use pretended version of the application:

    git clone https://danielSSantosTorres@bitbucket.org/danielssantostorres/devops-21-22-atb-1211757.git
    cd devops-21-22-atb-1211757/CA2/Part-2/react-and-spring-data-rest-basic


2.2- Add the following command deploy the war file to tomacat8:

    sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps


---------------------------------

<b>Note: </b>
Resources <span style = "color:blue">build.gradle, ServletInitializer.java, package.json, application.properties, app.js, index.html</span> were modified in the exercise with VirtualBox provider.
For further information about the configuration of that files, see topics 3 to 9 of "Vagrant with Oracle VirtualBox provider" chapter.

---------------------------------


<b>4- Create and configure guest machines according to Vagrantfile. Run from command line (force use hyperv provider): </b>

    vagrant up --provider-hyperv

<b>5- Get the configuration of db virtual machine network interface. Run from command line:</b>

    vagrant ssh db

    ifconfig -a 

    vagrant exit


<b>6- Shut down the running machines. Run from command line: </b>

    vagrant halt


<b>7- Update application.properties file</b>

Add settings for remote h2 database (update db ip according to the result of procedure item 5):

    spring.datasource.url=jdbc:h2:tcp://172.17.53.79:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE

<b>8- In the host, open the spring web application using http://172.17.61.29:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/</b>

![](./Part-2/images/2-vagrant%2Bwindows-hyper-v/browser.jpg)

<b>9- Open the H2 console using http://172.17.61.29:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console</b>

Enter the following credentials in login window:

    JDBC URL: jbdc:h2:tcp://172.17.53.79:9092/./jpadb 
    Username: sa
    Password: 

![](./Part-2/images/2-vagrant%2Bwindows-hyper-v/browser4.jpg)

Successful access.
</br>
![](./Part-2/images/2-vagrant%2Bwindows-hyper-v/browser3.jpg)

--------------------------------------

## D- Final considerations / Conclusion

The Vagrant Hyper-V provider works in almost every way like the VirtualBox, but has some limitations that are inherent to Hyper-V itself. 

One of the main limitations is described in chapter C, and conditioned my work in this class assignment. Vagrant does not yet know how to create and configure new networks for Hyper-V. 

Additionally, restoring snapshot VMs sometimes raise errors when mounting SMB shared folders, however these mounts will still work inside the guest.

For these limitations, I prefer the VirtualBox provider.