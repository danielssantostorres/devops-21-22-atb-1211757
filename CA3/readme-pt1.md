# Class Assignment 3 - Part 1
## Virtualization with Oracle VM VirtualBox
### 1- Brief introduction to Oracle VM VirtualBox

Oracle VM VirtualBox is one of the most popular free and open source <b>cross-platform virtualization</b> software. 

<b>Cross-platform</b> means it works across different hardware and software platforms. You can install VirtualBox on all your existing Intel or AMD-based x86 computers, whether they’re running Windows, macOS, Linux, or Oracle Solaris operating systems (OSs).

<b>Virtualization</b> means you can create an installation of one OS inside another OS’s operating environment. VirtualBox extends the capabilities of your existing computer so that it can run multiple OSs, inside multiple virtual machines, at the same time. As an example, you can run Windows and Linux on your Mac, run Linux on your Windows PC, and so on, all alongside your existing applications. You can install and run as many virtual machines as you like. The only practical limits are disk space and memory.

It's simple yet very powerful. It can run everywhere from small embedded systems or desktop class machines all the way up to data center deployments and cloud environments.

VirtualBox enables users to set up multiplatform virtual machine environments for <b>software development, testing, and general-purpose operating system (OS) virtualization, with optional runtime encryption</b>. 

Software engineers can develop for cloud native environments from within VirtualBox VMs directly on their Windows, macOS, Linux, and Oracle Solaris machines, making it easier to create multitier applications with just a standard laptop.

### 2- Some key terms

Here are some key terms that will be popping up:

• <b>Host operating system (host OS)</b>: This is the operating system of the physical computer on which VirtualBox is installed. VirtualBox is available for windows, macOS, Linux, and Oracle Solaris operating systems.

• <b>Guest operating system (guest OS)</b>: This is the operating system that runs inside a virtual machine. Theoretically, VirtualBox can run any x86 operating system inside a virtual machine.

• <b>Virtual machine (VM)</b>: This is a process, running on the host operating system, that shows the virtual machine as a window on your computer’s desktop.

• <b>Graphical user interface (GUI)</b>: This is a user interface that consists of a graphical environment with icons and windows, such as Windows or macOS. Users interact with it using a pointing device such as a mouse.

• <b>Command-line interface (CLI)</b>: This type of interface is a textbased prompt where users type commands using a keyboard.

### 3- Procedure

1 - Create Bitbucket issues.

![](./Part-1/images/1-create-bitbucket-issues.jpg)


2 - Create VM as described in the lecture

![](./Part-1/images/2-create-VM-properties.jpg)

![](./Part-1/images/3-check-VM-ubuntu-version-command-line.jpg)


3 - Install available upgrades of all packages currently installed on the system from the sources configured via sources.

![](./Part-1/images/4-apt-get-upgrade.jpg)

4 - Download package information from all configured sources

![](./Part-1/images/5-apt-update.jpg)

5 - Install net-tools package. The Net-tools package is a collection of programs for controlling the network subsystem of the Linux kernel.

![](./Part-1/images/6-install-net-tools.jpg)

6 - Edit the network configuration file to setup the IP.

![](./Part-1/images/7-edit-network-config-file-to-setup-ip.jpg)

7 - Apply the new changes.

![](./Part-1/images/8-sudo-net-plan-apply.jpg)

8 - Install OpenSSH Daemon (or server). It's the daemon program for ssh client.  
ssh replaces insecure rlogin and rsh, and provide secure encrypted communications between two untrusted hosts over an insecure network such as the Internet.

![](./Part-1/images/9-sudo-instal-openssh-server.jpg)

9 - Enable password authentication for ssh.

![](./Part-1/images/10-enable-pass-autentication-for-ssh-uncomment-line.jpg)

10 - Restart sshd daemon.

![](./Part-1/images/11-sudo-service-ssh-restart.jpg)

11 - Install VSFTPd. It's GNU General Public License FTP server for UNIX systems, including Linux.

![](./Part-1/images/12-sudo-apt-install-vsftpd.jpg)

12 - Configure VSFTPd.

![](./Part-1/images/13-enable-write-access-for-vsftpd-uncomment-line.jpg)

13 - Restart VSFTPd.

![](./Part-1/images/14-sudo-service-vsftpd-restart.jpg)

14 - Install git.

![](./Part-1/images/15-sudo-apt-install-git.jpg)

15 - Install JDK11 (implementation of version 11 of the Java SE Platform).

![](./Part-1/images/16-sudo-apt-install-jdk-11.jpg)

16 - Login to the VM via SSH, from Host OS.

![](./Part-1/images/17-use-ssh-to-connect-VM-in-host.jpg)

17 - Clone individual repository inside the VM.

![](./Part-1/images/18-Clone-repository-from-host-ssh.jpg)

18 - Check if repository was successfully created on VM.

![](./Part-1/images/19-Check-Repository-In-VM-Command-Line.jpg)

19 - When attempting to run gradlew from command line, returned "permission denied". To solve this problem, I made the file executable for user (chmod u+x).

![](./Part-1/images/20-try-gradlew-error-permissions.jpg)

20 - When attempting to run gradlew from command line, returned "Could not find or load main class org.gradle.wrapper.GradleWrapperMain".

![](./Part-1/images/21-try-gradlew-error-missing-jar.jpg)

21 - To solve the error presented above (20), I restored the file gradle-wrapper.jar into gradle folder by FileZilla. 

![](./Part-1/images/22-fix-gradle-wrapper-file-using-filezilla.jpg)

22 - Build project with Gradle Wrapper.

![](./Part-1/images/23-gradlew-build-successful.jpg)

23 - Run task "runServer" with Gradle Wrapper.

![](./Part-1/images/24-runServer-is-running.jpg)

24 - Try to run task "runClient" with Gradle Wrapper. It fails, with error message *"No X11 display variable was set, but this program performed an operation which requires it"*. 

![](./Part-1/images/25-open-another-window-runClient-fail-no-graphics.jpg)

25 - X11 or X Window is a graphical system for most flavors of UNIX. Guest OS dont't have Graphical User Interface (GUI), but this program performed an operation which requires it.
To solve this problem, I created a task runClientOnVM on Host OS, that launches a chat client that connects to a server on host 192.168.56.5.

![](./Part-1/images/26-Create-new-task-to-runClient-on-host.jpg)

26 - Run task "runClientOnVM" with Gradle Wrapper from Host OS.

![](./Part-1/images/27-Call-gradlew-bat--runClientOnVM.jpg)

27 - With task "runServer" running on VM, and "runClientOnVM" running on Host, the chat window appears.

![](./Part-1/images/28-Chat-Window-On-Host.jpg)

28 - After close chat window, we can achieve that build was successful.

![](./Part-1/images/29-After-Close-Chat-Build-Successful-Msg.jpg)

29 - When attempting to call mvnw from command line, returned "permission denied". To solve this problem, I made the file executable for user (chmod u+x).

![](./Part-1/images/30-Maven-CA1-change-mvnw-permissions.jpg)

30 - Run project with Maven Wrapper.

![](./Part-1/images/31-mvnw-spring-boot-run.jpg)

31 - Check if application is running.

![](./Part-1/images/32-spring-app-running.jpg)