package test;

import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void createEmployeeWithValid() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act
        Employee expected = new Employee(firstName, lastName, description,
                jobTitle, jobYears, email);

        //Assert
        assertEquals(firstName, expected.getFirstName());
    }

    @Test
    void createEmployeeFailsBecauseInvalidFirstName() {
        //Arrange
        String firstName = "aa";
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

    @Test
    void createEmployeeFailsBecauseFirstNameIsNull() {
        //Arrange
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee(null, lastName, description, jobTitle, jobYears,
                    email);
        });
    }

    @Test
    void createEmployeeFailsBecauseInvalidLastName() {
        //Arrange
        String firstName = "John";
        String lastName = "aa";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

    @Test
    void createEmployeeFailsBecauseLastNameIsNull() {
        //Arrange
        String firstName = "John";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee(firstName, null, description, jobTitle, jobYears,
                    email);
        });
    }

    @Test
    void createEmployeeFailsBecauseEmailIsNull() {
        //Arrange
        String firstName = "John";
        String lastName = "Teewt";
        String description = "Person";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = null;

        //Act && Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee(firstName, null, description, jobTitle, jobYears,
                    email);
        });
    }

    @Test
    void createEmployeeFailsBecauseInvalidDescription() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String description = "pp";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

    @Test
    void createEmployeeFailsBecauseDescriptionIsNull() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String jobTitle = "Teacher";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee(firstName, lastName, null, jobTitle, jobYears, email);
        });
    }

    @Test
    void createEmployeeFailsBecauseInvalidJobTitle() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "t";
        int jobYears = 2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

    @Test
    void createEmployeeFailsBecauseInvalidJobYears() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "t";
        int jobYears = -2;
        String email = "1000000@isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

    @Test
    void createEmployeeFailsBecauseInvalidEmail() {
        //Arrange
        String firstName = "John";
        String lastName = "Tweet";
        String description = "Person";
        String jobTitle = "t";
        int jobYears = -2;
        String email = "1000000isep.ipp.pt";

        //Act && Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(firstName, lastName, description, jobTitle, jobYears
                    , email);
        });
    }

}
