/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id @GeneratedValue Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobTitle;
    private int jobYears;
    private String email;

    protected Employee() {
    }

    public Employee(String firstNameInput, String lastNameInput,
                    String descriptionInput, String jobTitleInput,
                    int jobYearsInput, String emailInput) {

        String regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)" +
                "*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)" +
                "*(\\.[A-Za-z]{2,})$";
        Pattern regexPattern = Pattern.compile(regex);
        Matcher matcherEmail = regexPattern.matcher(emailInput);

        if (firstNameInput == null || lastNameInput == null ||
                descriptionInput == null || jobTitleInput == null ||
                emailInput == null) {
            throw new NullPointerException();

        } else if (firstNameInput.length() < 3 || lastNameInput.length() < 3
                || descriptionInput.length() < 3 || jobTitleInput.length() < 3
                || jobYearsInput < 0 || !matcherEmail.matches()) {
            throw new IllegalArgumentException();

        } else {
            this.firstName = firstNameInput;
            this.lastName = lastNameInput;
            this.description = descriptionInput;
            this.jobTitle = jobTitleInput;
            this.jobYears = jobYearsInput;
            this.email = emailInput;
        }
    }

    @Override public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Employee)) {
            return false;
        }
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id);
    }

    @Override public int hashCode() {
        return Objects.hash(id);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getJobYears() {
        return jobYears;
    }

    public void setJobYears(int jobYears) {
        this.jobYears = jobYears;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", jobYears=" + jobYears +
                '}';
    }
}
// end::code[]
