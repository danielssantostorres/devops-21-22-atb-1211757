# Class Assignment 1 <br> Version Control with Git

## A. Analysis, Design and Implementation

### A.1 First class (17-mar-2022)

<b>0- Create first Bitbucket issue</b>

![repoSettings](./work-17032022/Images/CreateIssue-RepoSettings.jpg)

![createIssue1](./work-17032022/Images/CreateIssue1.jpg)

<b>1- Copy the code of the Tutorial React.js and Spring Data REST Application into a new folder named CA1</b>

![image1](./work-17032022/Images/1.jpg)

<b>2- Commit the changes (and push them)</b>

![image2](./work-17032022/Images/2.jpg)

<b>3- Tag the initial version as v1.1.0. Push it to the server</b>

![image3](./work-17032022/Images/3.jpg)

<b>4- Develop a new feature to add a new field to the application. Add a new field to record the years of the employee in the company (e.g., jobYears)</b>
  
![image4](./work-17032022/Images/4.jpg)
![imageResolvingIssue](./work-17032022/Images/CreateIssue-ResolvingIssue1.jpg)

Add unit tests for testing the creation of Employees and the validation of their attributes (for instance, no null/empty values). For the new field, only integer values should be allowed.

![image4.2](./work-17032022/Images/4.2.jpg)
![imageResolvingIssue2](./work-17032022/Images/CreateIssue-ResolvingIssue2.jpg)

Debug the server and client parts of the solution. When the new feature is completed (and tested) the code should be committed and pushed and a new tag should be created (e.g, v1.2.0).

![image4.3](./work-17032022/Images/4.3.jpg)


</br>
### 1.2 Second class (24-mar-2022)

<b>0- Create a new branch called «email-field».</b>

![image1](./work-24032022/Images/1.jpg)

<b>1- Create new Bitbucket Issue #3.</b>

![image3](./work-24032022/Images/3.jpg)

<b>2- Add support for the email field, with unit tests (for testing the creation of Employees and the validation of their attributes). Debug the server and client parts of the solution.</b>

![image2](./work-24032022/Images/2.jpg)

<b>3- Commit and push «email-field» branch to origin. </b>

![image4](./work-24032022/Images/4.jpg)

![image5](./work-24032022/Images/5.jpg)

<b>4- When the new feature is completed (and tested) the code should be merged with the
master and a new tag should be created (e.g, v1.3.0).</b>

![image6](./work-24032022/Images/6.jpg)

![image7](./work-24032022/Images/7.jpg)

<b>5- Create new Bitbucket Issue #4.</b>

![image8](./work-24032022/Images/8.jpg)

<b>6- Commit and push «fix-invalid-email» branch to origin. </b>

![image9](./work-24032022/Images/9.jpg)

![image10](./work-24032022/Images/10.jpg)

![image11](./work-24032022/Images/11.jpg)

<b>7- When the fix is completed (and tested) the code should be merged into master and a new tag should be created (with a change in the minor number, e.g., v1.3.0 -> v1.3.1)</b>

![image12](./work-24032022/Images/12.jpg)

<b>8- At the end of the assignment mark repository with the tag ca1-part2.</b>

![image13](./work-24032022/Images/13.jpg)

![image14](./work-24032022/Images/14.jpg)


************************************************************************************************************
## 2. Analysis of an Alternative - Mercurial

Mercurial and Git both are two quite similar and most popular <b>distributed version control systems</b>. 

Today, Git has more than 31 million users and is owned by Microsoft. Since the last decade, the Git has become the standard for most development projects.

Mercurial still has a handful tool of large development organizations. Some software development giants like Facebook, Mozilla, and World Wide Web Consortium are using it. But it only has approx 2 % of the VCS market share. Comparatively, Git has covered more than 80% market share.

###2.1. Usability
In general, if your team is <b>less experienced</b>, or if you have <b>non-technical people on your team</b>, Mercurial might be the better solution.
Once you get over the learning curve, Git offers teams <b>more flexibility</b>. 

<b>Git is more complex, with more commands</b>
Git is more complex, and it requires your team to know it inside and out before using it safely and effectively. Documentation is also harder to understand.

<b>Mercurial is simpler</b>
Mercurial’s syntax is simpler, and the documentation is easier to understand. Furthermore, it works the way a tool should — you don’t think about it while using it. Conversely, with Git, you might end up spending time figuring out finicky behavior and pouring over forums for help.

###2.2. Security
<b>Mercurial is safer for less experienced users</b>
By default, Mercurial doesn’t allow you to change history. However, Git allows all involved developers to change the version history. Obviously, this can have disastrous consequences. With basic Mercurial, you can only change your last commit with “hg commit – amend”.

Git also stores every change made for 30 days in reflog. For example, you can modify the commit message of the latest commit, and revert it for 30 days. However, the changes can only be made locally, as these changes are not pushed to the remote repository by default. After 30 days, the changes are collected, meaning you can no longer revert.

<b>Git is better for experienced users</b>
Mercurial may be safer for less experienced users, but Git can offer ways to enhance safety (once you know what you are doing).

###2.3. Branching

<b>Git’s branching model is more effective</b>
One of the main reasons developers swear by Git is its effective branching model. In Git, branches are only references to a certain commit. This makes them lightweight yet powerful.

Git allows you to create, delete, and change a branch anytime, without affecting the commits. If you need to test a new feature or find a bug — make a branch, do the changes, and then delete the branch.

Git supports the idea of a staging area, which is also known as the index file.

Staging is the practice of adding files for your next commit. It allows you to choose which files to commit next. This is useful when you don't want to commit all changed files together.

<b>Mercurial’s branching model can cause confusion</b>
Branching in Mercurial doesn't share the same meaning. This can make it more cumbersome. In Mercurial, branches refer to a linear line of consecutive changesets. Changesets (csets) refer to a complete set of changes made to a file in a repository.

Mercurial embeds the branches in the commits, where they are stored forever. This means that branches cannot be removed because that would alter the history. However, you can refer to certain commits with bookmarks, and use them in a similar manner to Git’s branches.

Lastly, it requires you take added care not to push code to the wrong branch, especially if you haven’t clearly named them. You can easily avoid this in Git.

In Mercurial, there is no index or staging area before the commit. Changes are committed as they are in the working directory. This might be an adequate and simple solution for many people. But if you want the option to choose which parts of the working directory you commit, you can use a Mercurial extension, such as DirState or Mercurial Queues.

<b>Bookmarks can be used as an alternative to NamedBranches for tracking multiple lines of development.</b> 

Bookmarks are references to commits that can be automatically updated when new commits are made. If you run hg bookmark feature, the feature bookmark refers to the current changeset. As you work and commit changes the bookmark will move forward with every commit you do. The bookmark will always point to the latest revision in your line of work.

<b>Mercurial's bookmark feature is analogous to Git's branching scheme.</b>

************************************************************************************************************
## 3. Implementation of the Alternative
<b>1- Register in Helix</b>

![imagehg1](./work-17032022/Images/Mercurial/1_Register_Helix_Code_Repo.jpg)

<b>2- Create Helix project named "Mercurial"</b>

![imagehg2](./work-17032022/Images/Mercurial/2_Create_Project_Mercurial.jpg)


<b>3- Create Helix mercurial repository named devops-21-22-atb-1211757-hg</b>

![imagehg3](./work-17032022/Images/Mercurial/3_Create_Repo_devops-21-22-atb-1211757-hg.jpg)

<b>4- Initialize devops-21-22-atb-1211757-hg directory as a mercurial repository</b>

![imagehg4](./work-17032022/Images/Mercurial/4_hg_init.jpg)

<b>5- Push an initial commit</b>

![imagehg5](./work-17032022/Images/Mercurial/4_initial_commit.jpg)

<b>6- Create and set configuration file hgrc</b>

![imagehg6.1](./work-17032022/Images/Mercurial/5_Create_Configuration_File_hgrc.jpg)

![imagehg6.2](./work-17032022/Images/Mercurial/6_Set_Configuration_Files_hgrc.jpg)

![imagehg6.3](./work-17032022/Images/Mercurial/7_Hg_push_remote.jpg)

![imagehg6.4](./work-17032022/Images/Mercurial/7_Hg_push_remote_Helix.jpg)

<b>7- Copy the code of the Tutorial React.js and Spring Data REST Application into a new folder named CA1</b>

![imagehg7](./work-17032022/Images/Mercurial/8_Copy_Tutorial.jpg)

<b>8- Commit the changes. Tag the initial version as v1.1.0. Push it to the server</b>

![imagehg8.1](./work-17032022/Images/Mercurial/9_Commit_And_Add_Tag.jpg)

![imagehg8.2](./work-17032022/Images/Mercurial/9.1_Commit_And_Add_Tag.jpg)

<b>9- Create Helix Issue</b>

![imagehg9](./work-17032022/Images/Mercurial/10_Create_Issue_Helix.jpg)

<b>10- Push commit and update Helix issue as "Closed"</b>

![imagehg10.1](./work-17032022/Images/Mercurial/11_Reference_Issue_In_Commit_And_Close_Helix.jpg)

![imagehg10.2](./work-17032022/Images/Mercurial/11.1_Reference_Issue_In_Commit_And_Close_Helix.jpg)

<b>11- Push final tag</b>

![imagehg11.1](./work-17032022/Images/Mercurial/12_Final_tag.jpg)

![imagehg11.2](./work-17032022/Images/Mercurial/12.1_Final_tag.jpg)

</br>
### 1.2 Second class (24-mar-2022)

#### Working with Mercurial's bookmark feature
Mercurial's bookmark feature is analogous to Git's branching scheme.

0- Start with a basic listing of available bookmarks:.

![imagehg1](./work-24032022/Images/Mercurial/1.jpg)

1- Create a bookmark on the current tip of the repository called «my-tip», and another bookmark called «email-field». Notice how the current bookmark is preceded by "*".

![imagehg2](./work-24032022/Images/Mercurial/2.jpg)

3- Update to «my-tip» bookmark and add a text file with the reference (link) to Bookmarks Mercurial Tutorial. Commit and push to remote.

![imagehg3](./work-24032022/Images/Mercurial/3.jpg)

4- Update to «email-field» bookmark and add a text file called «email-field», to simulate the operation of support for the email field with tests and debug. Commit (will generate new head)"

![imagehg4](./work-24032022/Images/Mercurial/4.jpg)

5- Update to «my-tip» bookmark and merge with «email-field». Add tag v1.3.0

![imagehg5](./work-24032022/Images/Mercurial/5.jpg)

![imagehg6](./work-24032022/Images/Mercurial/6.jpg)


************************************************************************************************************

##<span style="color:blue">APPENDIX 1 - GIT COMMANDS</span>

<span style="color:gray">Note:
In Git, "origin" is a shorthand name for the remote repository that a project was originally cloned from. More precisely, it is used instead of that original repository's URL - and thereby makes referencing much easier</span>

###SETUP
<b>git config --global user.name “[firstname lastname]”</b>
Set a name that is identifiable for credit when review version history
<b>git config --global user.email “[valid-email]”</b>
Set an email address that will be associated with each history marker
<b>git config --global -e</b>
Adding variable and properties inside Git config global

###SETUP & INIT
<b>git init</b>
Initialize an existing directory as a Git repository
<b>git clone [url]</b>
Retrieve an entire repository from a hosted location via URL

###STAGE & SNAPSHOT
<b>git status</b>
Show modified files in working directory, staged for next commit
<b>git add [file]</b>
Add a file as it looks now to next commit (stage)
<b>git add .</b>
Add all changes not staged to next commit
<b>git commit -m “[descriptive message]”</b>
Commit staged content as a new commit snapshot|
<b>git commit -m "[descriptive message *resolving issue #1*]"</b>
Commit and update <b>Bitbucket Issue</b> status as "Resolved"
<b>git reset --hard HEAD</b>
Clear staging area, rewrite working tree from last commit
<b>git reset --hard [commit]</b>
Clear staging area, rewrite working tree from specified commit
<b>git checkout [commit code]</b>
Switchs to a specific commit.

###SHARE & UPDATE
<b>git push</b>
Upload local repository content to a remote repository|
<b>git pull</b>
Fetch and merge any commits from the tracking remote branch

###TEMPORARY COMMITS
<b>git stash</b>
Save modified and staged changes|

###INSPECT & COMPARE
<b>git log</b>
Show the commit history for the currently active branch

###TAGS
<b>git tag -a [tagname] -m "[descriptive message]"</b>
Creates an annotated tag object with a tag message
<b>git tag</b>
List the existing tags|
<b>git push origin [tagname]</b>
By default, the git push command doesn’t transfer tags to remote servers. It's mandatory to explicitly push tags to a shared server after creating them
<b>git push origin --tags</b>
To push multiple tags at once

###BRANCH
<b>git branch</b>
List all of the branches in repository
<b>git branch [branchname]</b>
Create a new branch called [branchname]. This does not check out the new branch
<b>git branch -d [branchname]</b>
Delete the specified branch. This is a “safe” operation in that Git prevents you from deleting the branch if it has unmerged changes
<b>git checkout [branchname]</b>
Switch branches or restore working tree files.
<b>git merge [branchname]</b>
Join two or more development histories together
<b>git push -u origin [branchname]</b>
Push local content to remote; '-u' flag is upstream, which is equivalent to '-set-upstream' 
