# Project Management System - DevOps
Apply DevOps to the Project Management System

## Introduction

- [task1](task1/README.md)
- [task2](task2/README.md)
- [task3](task3/README.md)
- [task4](task4/README.md)
- [task5](task5/README)
- [task6](task6/README.md)
- [task7](task7/README.md)
- [task8](task8/README.md)
- [task9](task9/README.md)
- [task10](task10/README.md)

To carry out this work it was necessary to fork the repository of our projectg5. This task was performed by Professor
Alexandre Bragança.
After that, and in agreement with the whole group, we decided to split up into pairs, each with two similar tracks, 
changing only the Build tool (Maven or Gradle).
As the project was already configured in Maven, there was a need to migrate it to Gradle. This conversion is demonstrated 
in the section below - **Implementation**.
After that, it was time to adapt and implement our project according to the task assigned to us.
In the following section, we will briefly introduce the tools used, already known to us both in theory and in practice,
except for Ansible, whose practice had not yet been implemented, and it was indeed a challenge to do so for the project.

**Build Tools**

We use both Maven and Gradle to comply and test our code. They both did their job. But we considered Gradle easier to
work with, since is very flexible and is based on a script and is much more simple.

**Ansible**

We use Ansible to deploy our application locally to VMs using Vagrant. With this, we can control and provision the Vms 
with a couple of files. We used 3 VMs, one with Ansible and Jenkins (control), one with the application 
(frontend and backend) and one with the H2 database. 

An alternative was to use Ansible to control de deployment oy our application to virtual servers provide by DEI (ISEP Cloud).
In this case, we provide ansible to the URL of the servers we create in this cloud. 
Those who were responsible for working in the ISEP cloud had to create another virtual machine - one for 
control the VMs machine, using Vagrant, another for DB, Web and Backend, these three created on the DEI server.
With the cloud, we have to be connected to an ISEP VPN to access the servers.

Both these options were interesting, since they allowed us to explore the potential of Ansible, since we were only 
aware of the theoretical part.

**Docker**

Docker is a software platform that allows developers to build, test, and deploy containerized applications quickly.
Another option was to use Docker containers to deploy the app locally in our machines.
In these tracks, we also deployed the application to cloud servers, this time manually. We used Ubuntu VSs with Tomcat,
and H2 Servers for the database.
Docker has not being easly automated like with Ansible.  We considered Ansible easer to work.

**PostgreSQL and H2 Database**
PostgreSQL is considered one of the most advanced open source relational database, while the H2 database it is still a quite new product.

In the course of the work, we found it easier and more intuitive to work with the H2, perhaps because we already 
had knowledge and use of the latter.


## IMPLEMENTATION
### Apply the _Gradle_ build tool to the project

Our project management system was developed with the maven build tool being one of the first tasks necessary
was to apply the Gradle build tool to it.

Our group did the conversion using the _gradle init_ command. For this it is necessary to have gradle installed on the
machine that will do the conversion.
After installing gradle, we open the console in the root directory of the project's pom.xml (maven) and run the
_gradle init_ command.

Console output:

![](images/gradleInit.png)

New files were created, such as:
- .gradle (folder)
- gradle (folder)
- build.gradle
- gradlew
- gradlew.bat
- settings.gradle

![](images/newFiles.png)

Gradle automatically read the dependencies and plugins that were in pom.xml and adopted them
to build.gradle.
Even so, it was necessary to place some dependencies and plugins manually in order to be able to
give an answer to the various tasks that we will develop from now on.


## Final considerations 

In conclusion, we learned a lot about how to build and deploy a web application.

The fact of implementing our application with the proposed alternatives allowed us to better understand how each one
works and the differences between each one of them.

Working in pairs undoubtedly helped in understanding the task as well as making the work easier, however, it should be 
noted that the whole group helped each other, as it happens from the beginning, which made the work even more pleasurable.

