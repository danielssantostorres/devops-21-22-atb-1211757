/* groovylint-disable DuplicateStringLiteral */

pipeline {
    agent any

    stages {
        stage('Check out') {
            steps {
                echo 'CHECKING OUT...'
                git(credentialsId: 'devops-task1',
                    url: 'https://bitbucket.org/switch-2021/projectg5-devops/')
            }
        }
        stage('Change application.properties file') {
            steps {
                echo 'CHANGING APPLICATION PROPERTIES FILE...'
                sh 'sudo rm src/main/resources/application.properties'
                sh 'sudo cp DEVOPS/task1/application.properties src/main/resources/'
            }
        }
        stage('Change URL_API file') {
            steps {
                echo 'CHANGING URL_API FILE...'
                sh 'sudo rm webapp/src/services/URL_API.js'
                sh 'sudo cp DEVOPS/task1/URL_API.js webapp/src/services'
            }
        }
        stage('Setup servers') {
            steps {
                echo 'SETTING UP SERVERS...'
                ansiblePlaybook(
                    credentialsId: 'control',
                    disableHostKeyChecking: true,
                    inventory: 'DEVOPS/task1/hosts',
                    playbook: 'DEVOPS/task1/playbook-config.yaml')
            }
        }
        stage('Assemble') {
            steps {
                echo 'ASSEMBLING...'
                sh './mvnw package -Dmaven.test.skip=true'
            }
        }
        stage('Test') {
            steps {
                echo 'TESTING...'
                dir('./') {
                    sh './mvnw test'
                    junit 'target/surefire-reports/*.xml'
                }
            }
        }
        stage('Javadoc') {
            steps {
                echo 'JAVADOCKING...'
                dir('./') {
                    sh './mvnw javadoc:javadoc'
                    echo 'PUBLISHING HTML...'
                    publishHTML(
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: false,
                        reportDir: 'target/site/',
                        reportFiles: 'index-all.html',
                        reportName: 'HTML Report')
                }
            }
        }
        stage('Archive') {
            steps {
                echo 'ARCHIVING...'
                archiveArtifacts 'target/*.war'
            }
        }
        stage('Build') {
            steps {
                echo 'BUILDING FRONTEND...'
                dir('webapp') {
                    sh 'npm install'
                    sh 'sudo npm run build'
                }
            }
        }
        stage('Deploy') {
            steps {
                echo 'DEPLOYING...'
                ansiblePlaybook(
                    credentialsId: 'control',
                    disableHostKeyChecking: true,
                    inventory: 'DEVOPS/task1/hosts',
                    playbook: 'DEVOPS/task1/playbook-run.yaml')
            }
        }
    }
}
