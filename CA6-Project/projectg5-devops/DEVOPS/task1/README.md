# Track 1 - Maven :: Local deployment of Web + DB (*H2*) to Vagrant virtual machines with Ansible

## INTRODUCTION

Track 1 aims to build our web application with <b>Apache Maven</b> and deploy localy with <b>Ansible</b> to a multi-machine environment, created and managed with <b>Vagrant</b>. 

Tasks should be automated with <b>Jenkins</b>.

Vagrant multi-machine environment must have three hosts:
- <b>WEB</b> - the Project Management System, with <b>Apache Tomcat</b> web server environment.
- <b>DB</b> - the web application database, with <b>H2 Database</b>.
- <b>CONTROL</b> - with <b>Ansible</b> to configure other hosts (WEB and DB) and deploy the web application, and <b>Jenkins</b> to automate tasks.

## IMPLEMENTATION

### 1- Build virtual machines with Vagrant

##### 1.1- Create Vagrantfile

First, we looked for a box with <b>Ubuntu 20.04 (Focal Fossa)</b> with Oracle VirtualBox provider, in HashiCorp Vagrant Cloud.

Ubuntu 20.04 (Focal Fossa) is the <b>latest long-term support (LTS) version of Ubuntu</b> at the moment. 

Ubuntu LTS releases are usually more stable and polished in comparison to regular releases. This stability and robustness allow for <b>better integration and compatibility with other software</b>.

After some research, we selected the box <b>'bento/ubuntu-20.04'</b> (https://app.vagrantup.com/bento/boxes/ubuntu-20.04).


We install, by default, on the three hosts (WEB, DB, CONTROL):

- <b>iputils-ping</b>: tools to test the reachability of network hosts. The ping command sends packets to a host in order to test if the host is reachable via the network.

- <b>JDK11</b>: our Java project SDK is Oracle Java SE Development Kit (JDK) 11.

- <b>python3</b>: most Ansible modules are written in Python, including the ones central to letting Ansible work. By default, Ansible assumes it can find a /usr/bin/python on our remote system that is either Python2 (version >= 2.6) or Python3 (version >= 3.5).

Hosts WEB and DB don't need more software at Vagrant up.

On CONTROL, we additionally install <b>Node.js</b>, <b>Ansible</b> and <b>Jenkins</b>, and other utilities.

The Vagrantfile presented bellow was placed in DEVOPS/task1 folder.


    Vagrant.configure("2") do |config|
        config.vm.box = "bento/ubuntu-20.04"
        config.vm.provision "shell", inline: <<-SHELL
            sudo apt-get update -y
            sudo apt-get install iputils-ping -y
            sudo apt-get install python3 --yes
            sudo apt-get install openjdk-11-jdk-headless -y
        SHELL

        config.vm.define "web" do |web|
            web.vm.box = "bento/ubuntu-20.04"
            web.vm.hostname = "web"
            web.vm.network "private_network", ip: "192.168.33.10"
            web.vm.provider "virtualbox" do |v|
                v.memory = 1024
            end

            # To access Tomcat from port 8080
            web.vm.network "forwarded_port", guest: 8080, host: 8080

            web.vm.provision "shell", inline: <<-SHELL, privileged: false
            SHELL
        end

        config.vm.define "db" do |db|
            db.vm.box = "bento/ubuntu-20.04"
            db.vm.hostname = "db"
            db.vm.network "private_network", ip: "192.168.33.11"
            db.vm.network "forwarded_port", guest: 8082, host: 8082
            db.vm.network "forwarded_port", guest: 9092, host: 9092
        end

        config.vm.define "control" do |control|
            control.vm.box = "bento/ubutu-20.04"
            control.vm.hostname = "control"
            control.vm.network "private_network", ip: "192.168.33.12"
            control.vm.provider "virtualbox" do |v2|
                v2.memory = 2048
            end
            
            # To run ansible from jenkins
            control.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=775,fmode=600"]
            
            # To access jenkins from port 8081
            control.vm.network "forwarded_port", guest: 8080, host: 8081

            control.vm.provision "shell", inline: <<-SHELL
                sudo apt-get update -y
                sudo apt-get install -y curl
                sudo apt-get install apt-transport-https
                sudo apt-get install -y nano git
                sudo apt-get install -y --no-install-recommends apt-utils
                sudo apt-get install software-properties-common --yes
      
                curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
                sudo bash nodesource_setup.sh
                sudo apt install nodejs

                sudo apt-add-repository --yes --u ppa:ansible/ansible
                sudo apt-get install ansible --yes

                sudo apt-get install -y avahi-daemon libnss-mdns
                sudo apt-get install -y unzip
                wget https://mirrors.jenkins.io/war-stable/latest/jenkins.war
            SHELL

            control.vm.provision "shell", :run => 'always', inline: <<-SHELL
                sudo java -jar jenkins.war &
            SHELL
        end
    end

<span style="color: blue"><b>Important:</b> to install Jenkins, we got the jenkins.war file and execute it in Provision. This way, we install Jenkins as a *super user* and prevent *'Host key verification failed'* error in future.</span>

![](./media/images/1-jenkins-install.png)

<p>Finally, we ran the command <b>'vagrant up'</b> from the command line, in the directory where the Vagrantfile is located.</p>

![](./media/images/1-vms.jpg)


### 2- Configure Jenkins

#### 2.1- Post-installation setup

After downloading, installing and running Jenkins, the post-installation setup wizard begins.

##### 2.1.1- Unlock Jenkins

To ensure Jenkins is securely set up by the admnistrator, admin password is required.

![](./media/images/2-post-installation-wizard.jpg)

#### 2.2- Customize Jenkins with plugins

After unlock Jenkins, we had to customize Jenkins with base plugins.

![](./media/images/3-jenkins-plugins.jpg)

We chose <b>"Install suggested plugins"</b> option, to install plugins the Jenkins community finds most useful.

![](./media/images/4-jenkins-plugins.png)

#### 2.3- Create user *'devops'*

<span style="color: blue"><b>This step is not mandatory.</b></span> We could skip and continue with 'admin' user.

![](./media/images/5-create-user.jpg)

Although we could skip and continue as 'admin', we created a first admin user:

    Username: devops

    Full name: devops

    E-mail address: 1211757@isep.ipp.pt

#### 2.4 - Instance configuration

At this stage, we set Jenkins URL to http://192.168.33.12:8080/, as expected.

![](./media/images/5-jenkins-instance.jpg)

#### 2.5- Create the Jenkinsfile

Before creating the Jenkins job, we created the Jenkinsfile locally. The file presented bellow was placed in DEVOPS/task1 folder.

    /* groovylint-disable DuplicateStringLiteral */

    pipeline {
        agent any

        stages {
            stage('Check out') {
                steps {
                    echo 'CHECKING OUT...'
                    git (credentialsId: 'devops-task1', 
                        url: 'https://bitbucket.org/switch-2021/projectg5-devops/')
                }
            }
            stage('Change application.properties file') {
                steps {
                    echo 'CHANGING APPLICATION PROPERTIES FILE...'
                    sh 'sudo rm src/main/resources/application.properties'
                    sh 'sudo cp DEVOPS/task1/application.properties src/main/resources/'
                }
            }
            stage('Change URL_API file') {
                steps {
                    echo 'CHANGING URL_API FILE...'
                    sh 'sudo rm webapp/src/services/URL_API.js'
                    sh 'sudo cp DEVOPS/task1/URL_API.js webapp/src/services'
                }
            }
            stage('Setup servers') {
                steps {
                    echo 'SETTING UP SERVERS...'
                    ansiblePlaybook(
                        credentialsId: 'control',
                        disableHostKeyChecking: true,
                        inventory: 'DEVOPS/task1/hosts',
                        playbook: 'DEVOPS/task1/playbook-config.yaml')
                }
            }
            stage('Assemble') {
                steps {
                    echo 'ASSEMBLING...'
                    sh './mvnw package -Dmaven.test.skip=true'
                }
            }
            stage('Test') {
                steps {
                    echo 'TESTING...'
                    dir('./') {
                        sh './mvnw test'
                        junit 'target/surefire-reports/*.xml'
                    }
                }
            }
            stage('Javadoc') {
                steps {
                    echo 'JAVADOCKING...'
                    dir('./') {
                        sh './mvnw javadoc:javadoc'
                        echo 'PUBLISHING HTML...'
                        publishHTML(
                            allowMissing: false,
                            alwaysLinkToLastBuild: false,
                            keepAll: false,
                            reportDir: 'target/site/',
                            reportFiles: 'index-all.html',
                            reportName: 'HTML Report')
                    }
                }
            }
            stage('Archive') {
                steps {
                    echo 'ARCHIVING...'
                    archiveArtifacts 'target/*.war'
                }
            }
            stage('Build frontend') {
                steps {
                    echo 'BUILDING FRONTEND...'
                    dir('webapp') {
                        sh 'npm install'
                        sh 'sudo npm run build'
                    }
                }
            }
            stage('Deploy') {
                steps {
                    echo 'DEPLOYING...'
                    ansiblePlaybook(
                        credentialsId: 'control',
                        disableHostKeyChecking: true,
                        inventory: 'DEVOPS/task1/hosts',
                        playbook: 'DEVOPS/task1/playbook-run.yaml')
                }
            }
        }
    }

#### 2.6- Create job *'test'*

Now, we were able to create the Jenkins job.

![](./media/images/6-create-job.jpg)

We created a job designated by <b>'test'</b>, and configure the pipeline.

![](./media/images/8-create-pipeline.jpg)

<span style="color: blue"><b>Important:</b> We configured Jenkins Global Credencials with Bitbucket Username and App Password. </span>

First, we created a app password in Bitbucket.

![](./media/images/9-bitbucket-app-pw.png)

Then, we could configure Jenkins Global Credencials.

![](./media/images/10-bitbucket-app-pw.jpg)

#### 2.7- Install plugins

We installed the following plugins:

- HTML Plubisher plugin 1.30;
- Ansible plugin 1.1.

![](./media/images/17-jenkins-plugins.jpg)

### 3- Configure hosts WEB + DB and deploy web application with Ansible

#### 3.1- Create configuration source file (ansible.cfg)

Ansible supports several sources for configuring its behavior, including an ini file named ansible.cfg.

The default Ansible configuration file is located under /etc/ansible/ansible.cfg. Most of Ansible’s settings can be modified using this configuration file to meet the needs of our environment, but the default configurations should satisfy most use cases.

    [defaults]
    inventory = /vagrant/hosts
    remote_user = vagrant

#### 3.2- Create inventory file (hosts)

Ansible works against multiple managed hosts in our infrastructure at the same time, using a inventory. Once our inventory is defined, we use patterns to select the hosts or groups we want Ansible to run against.

The default location for inventory is a file called /etc/ansible/hosts.

    [servers]
    web ansible_ssh_host=192.168.33.10 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/web/virtualbox/private_key
    db ansible_ssh_host=192.168.33.11 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/db/virtualbox/private_key

The heading in brackets is group name, which is used in classifying hosts and deciding what hosts we are controlling at what times and for what purpose.

#### 3.3- Create playbooks

Ansible Playbooks offer a repeatable, re-usable, simple configuration management and multi-machine deployment system.

If we need to execute a task with Ansible more than once, we could write a playbook and put it under source control. 

Then we can use the playbook to push out new configuration or confirm the configuration of remote systems. 

Playbooks can:
- declare configurations;
- orchestrate steps of any manual ordered process, on multiple sets of machines, in a defined order;
- launch tasks synchronously or asynchronously.

We decided to create two distinct playbooks:
- one playbook to install all dependencies and software that virtual machines should have to support our needs - including H2 Database in DB, and Node.js 12, Apache and Tomcat 9 in WEB;
- one playbook to deploy Web and DB to Vagrant virtual machines and automate task with Jenkins.

##### 3.3.1- Configure virtual machines: playbook-config.yaml

    ---
    - hosts: servers
      become: yes
      tasks:
        - name: Update cache
          apt: update_cache=yes
        - name: Remove Apache
          apt: name=apache2 state=absent
        - name: Update cache
          apt: update_cache=yes

    - hosts: db
      become: yes
      tasks:
        - name: Install git
          apt: name=git state=present
        - name: Install H2
          get_url:      
            url: https://search.maven.org/remotecontent?filepath=com/h2database/h2/2.1.214/h2-2.1.214.jar
            dest: ./
        - name: Initialize H2
          ansible.builtin.shell:    
            java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
          args:
            chdir: ./

    - hosts: web
      become: yes
      tasks:
        - name: Install Node.js 12.18.3
          ansible.builtin.shell: |
            curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
            sudo bash nodesource_setup.sh
            sudo apt install nodejs
        - name: Install Tomcat 9
          apt: name=tomcat9 state=present
        - name: Install Apache
          apt: name=apache2 state=present
        - name: Clean Tomcat
          ansible.builtin.file:
            path: /var/lib/tomcat9/webapps/*
            state: absent

<span style="color: blue"><b>Important:</b> Different versions of Apache Tomcat are available for different versions of the specifications. Apache Tomcat 9.0. x requires Java 8 or later - we use Java 11. :</span>

![](./media/images/12-tomcat.png)

(font: https://tomcat.apache.org/whichversion.html)


##### 3.3.2- Deploy Web and DB: playbook-run.yaml

    ---
    - hosts: servers
      become: yes
      tasks:
        - name: Update cache
          apt: update_cache=yes

    - hosts: web
      become: yes
      tasks:
        - name: Stop Tomcat Service
        ansible.builtin.shell: sudo service tomcat9 stop
        - name: Copy war file from localhost
          copy:
            src: ~/.jenkins/workspace/test/target/switchproject-1.0-SNAPSHOT.war
            dest: /var/lib/tomcat9/webapps/
        - name: Start Tomcat Service
          ansible.builtin.shell: sudo service tomcat9 start   
        - name: Stop Apache Service
          ansible.builtin.shell: sudo service apache2 stop
        - name: Clean frontend
          ansible.builtin.file:
            path: /var/www/html/*
            state: absent
        - name: Copy frontend build files
          copy: 
            src: ~/.jenkins/workspace/test/webapp/build/ 
            dest: /var/www/html/
        - name: Start Apache Service
          ansible.builtin.shell: sudo service apache2 start

### 4- Configure H2 Database to connect with DB server

#### 4.1- Setup application.properties in our Java project

    spring.datasource.driverClassName=org.h2.Driver

    spring.datasource.username=sa

    spring.datasource.password=password

    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect

    spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;CACHE_SIZE=8192;DB_CLOSE_ON_EXIT=FALSE
        
    spring.jpa.hibernate.ddl-auto=create-drop
    
    load.bootstrapData=true

### 5- Connect Frontend to Backend using Services (URL_API.js file)

#### 5.1 - Setup URL_API.js file 

    export const URL_API = 'http://localhost:8080/switchproject-1.0-SNAPSHOT'


### 6- Build with Jenkins

With servers WEB, DB and CONTROL running, we were able to run Jenkins.

![](./media/images/13-pipeline-success.jpg)

After a few tries, we achieve build successful.

![](./media/images/7-pipeline-success.jpg)

At this time, we could check our frontend from 192.168.33.10 (WEB).

![](./media/images/14-build-successful.jpg)

<b>Check video:</b> ./media/video/screen-recorder-mon-jun-27-2022-19-01-58.webm

### 7- Analysis of the alternatives

#### 7.1- Apache Maven *vs* Gradle Comparison

The following is a summary of the pros and cons of Apache Maven and Gradle:

|Build tool|Pros|Cons|
|--|--|--|
|<b>Apache Maven</b>|- Automatic dependency downloads;<br>- All dependencies are automatically recorded in source control as part of the Maven scripts;<br>- Standardizes and simplifies the build process;<br>- Easily integrates with IDEs and CI/CD systems.|- Provides standardization while staying flexible;<br>- Easy to read and write build scripts;<br>- Better at handling multiple versions of dependencies;<br>- Able to handle multiple programming languages and technologies;<br>- Active community helping develop the tool;<br>- Gradle DSL (Domain-Specific Language) makes it simple configuration structure;<br>- Gradle provides performance improvements using incrementally, build cache and the Gradle Daemon.|
|<b>Gradle</b>|- Not flexible in creating custom workflows;<br>- Steep learning curve and the process is difficult for novices to understand;<br>- Time-consuming to solve build problems and new library integrations;<br>- Not good with multiple versions of the same dependency.|- IDE integration not as good as Maven.|

Overall, I prefer Gradle to Maven, based on Gradle extreme simplicity, readability, customizability and documentation.

#### 7.2- Containers *vs* Virtual Machines

The key differentiator between containers and virtual machines is that <b>virtual machines virtualize an entire machine down to the hardware layers</b> and <b>containers only virtualize software layers above the operating system level</b>.

![](./media/images/vms-containers.jpg)

In this DEVOPS project we had to use both virtual machines and containers to deploy the web application.

The following is a summary of the pros and cons of virtual machines and containers:

||Pros|Cons|
|--|--|--|
|<b>Virtual Machines</b>|- <b>Full isolation security</b>: virtual machines run in isolation as  a fully standalone system. This means that virtual machines are immune to any exploits or interference from other virtual machines on a shared host.<br>- <b>Interactive development</b>: containers are usually static definitions of the expected dependencies and configuration needed to run the container. Virtual machines are more dynamic and can be interactively developed. |- <b>Iteration speed</b>: virtual machines are time consuming to build and regenerate because they encompass a full stack system. Any modifications to a virtual machine snapshot can take significant time to regenerate and validate they behave as expected.<br>- <b>Storage size cost</b>: virtual machines can take up a lot of storage space. They can quickly grow to several gigabytes in size.|
|<b>Containers</b>|- <b>Iteration speed</b>: because containers are lightweight and only include high level software, they are very fast to modify and iterate on. <br>- <b>Robust ecosystem</b>: most container runtime systems offer a hosted public repository of pre-made containers. These container repositories contain many popular software applications, saving time for development teams.|- <b>Shared host exploits</b>: containers all share the same underlying hardware system below the operating system layer, it is possible that an exploit in one container could break out of the container and affect the shared hardware. There is a security risk in using one of these public images as they may contain exploits or may be vulnerable to being hijacked by nefarious actors.|

In our project context, I prefer containers to virtual machines, because containers are very fast to modify and iterate on. Virtual machines are time consuming to build and regenerate, and often shut down or crash.

#### 7.3- PostgreSQL *vs* H2 Database 

<b>PostgreSQL</b> is a powerful, open source object-relational database system that uses and extends the SQL language combined with many features that safely store and scale the most complicated data workloads.

PostgreSQL is considered one of the most advanced open source relational database,  with over 30 years of active development that has earned it a strong reputation for <b>reliability, feature robustness, and performance</b>.

<b>H2 Database</b> is an open-source lightweight Java database. We can easily embed this database to our application by using JDBC, or run in the client-server mode. 

H2 Database it is still a quite new product, so there are probably still bugs that have not yet been found. 

![](./media/images/h2-reliability.jpg)

<b>H2 Database is mostly used for development and testing - it is not commonly used for production development.</b>

For learning purposes, I believe that the H2 Database fits better, because it can be configured to run as inmemory database. For a web application in production, I would choose PostgreSQL.

## Final considerations / Conclusion

It was very interesting to develop this project and integrate different technologies, including build tools with Maven and Gradle, virtualization with Vagrant, containerization using Docker, continuous integration with Jenkins and Red Hat Ansible for automate our infrastructure management and configuration tasks.

In this particular track, we had a first initial challenge which was learning Red Hat Ansible. We hadn't used it yet. 

However, Ansible leverages a very simple language (yaml) to define playbooks in a human-readable data format that is really easy to understand from day one. Even more, Ansible doesn’t require the installation of any extra agents on the managed nodes so it’s simple to start using it.

There were many challenges along this track that we had to overcome, mainly related to hosts configuration.

It's very satisfying to reach the final solution. It's a win for the team, which worked in an well organized and solidary way.