import React, {useContext, useEffect, useState} from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import "react-datepicker/dist/react-datepicker.css";
import Button from "@mui/material/Button";
import {Link} from "react-router-dom";
import AppContext from "../../context/AppContext";
import {URL_API} from "../../services/URL_API";
import {addProjectStarted, addProjectToDB, fetchCustomers, fetchTypologies, fetchUsers} from "../../context/Actions";
import {Alert, InputLabel} from "@mui/material";
import Snackbar from '@mui/material/Snackbar';
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import {format, parseISO} from "date-fns";

function ProjectCreationTable() {

    const {state, dispatch} = useContext(AppContext);

    // Perform typologies fetching -> Typologies dropdown

    const {typologies} = state;
    const fetchTypologiesData = typologies.data;
    useEffect(() => {
        let url = `${URL_API}/api/typologies`;
        const request = {method: 'GET'};
        fetchTypologies(url, request, dispatch);
    }, []);


    // Perform customers fetching -> Customers dropdown

    const {customers} = state;
    const fetchCustomersData = customers.data;
    useEffect(() => {
        let url = `${URL_API}/api/customers`;
        const request = {method: 'GET'};
        fetchCustomers(url, request, dispatch);
    }, []);


    // Perform users fetching -> Users dropdown

    const {users} = state;
    const fetchUsersData = users.data;
    useEffect(() => {
        let url = `${URL_API}/users?email=`;
        const request = {method: 'GET'};
        fetchUsers(url, request, dispatch);
    }, []);

    // Create project

    const {addProjects} = state;
    const createProjectData = addProjects.data;
    const createProjectError = addProjects.error;

    const initialState = {
        projectCode: '',
        projectName: '',
        projectDescription: '',
        projectBusinessSector: '',
        projectNumberOfPlannedSprints: '',
        projectSprintDuration: '',
        projectBudget: '',
        startDate: '',
        endDate: '',
        typologyDescription: '',
        customerName: '',
        userEmail: '',
        costPerHour: '',
        percentageOfAllocation: ''
    }

    const [project, setProject] = useState(initialState)

    useEffect(() => {
        dispatch(addProjectStarted())
    }, [])

    let creationMessage = <div></div>

    const [open, setOpen] = React.useState(false);

    // Start date control - Today

    function getToday() {
        let d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }


    if (createProjectError !== null) {
        creationMessage = (<div>
            <Snackbar
                open={open}
                autoHideDuration={1500}
                onClose={() => setOpen(false)}>
                <Alert variant="filled" severity="warning">Something went wrong...</Alert>
            </Snackbar>
        </div>);
    } else if (!Array.isArray(createProjectData)) {
        creationMessage = (<Snackbar
            open={open}
            autoHideDuration={1500}
            onClose={() => {
                setOpen(false);
                window.open('http://localhost:3000/projects/' + createProjectData.projectCode, "_self")
            }}>

            <Alert variant="filled" severity="success">Project {createProjectData.projectCode} created</Alert>
        </Snackbar>);
    }

    function createNewProject() {
        let url = `${URL_API}/api/projects`;
        addProjectToDB(url, dispatch, project);
        setOpen(true)
    }


    return (<div>
        {creationMessage}
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': {m: 2, width: '50ch'}
            }}
            noValidate
            autoComplete="off"

        >
            <TextField
                id="project-code"
                label={"Project code"}
                value={project.projectCode}
                onChange={(e) => setProject({...project, projectCode: e.target.value})}
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <p/>
            <TextField
                id="project-name"
                label="Project Name"
                value={project.projectName}
                onChange={(e) => setProject({...project, projectName: e.target.value})}
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <p/>
            <TextField
                id="project-description"
                label="Project Description"
                value={project.projectDescription}
                onChange={(e) => setProject({...project, projectDescription: e.target.value})}
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <p/>
            <TextField
                id="project-business-sector"
                label="Business Sector"
                value={project.projectBusinessSector}
                onChange={(e) => setProject({...project, projectBusinessSector: e.target.value})}
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <p/>
            <TextField
                type='number'
                id="project-budget"
                label="Budget"
                value={project.projectBudget}
                onChange={(e) => setProject({...project, projectBudget: e.target.value})}
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <p/>
            <FormControl style={{width: '50ch',}}>
                <InputLabel id="demo-simple-select-standard-label">Project Typology </InputLabel>
                <Select
                    labelId="project-typologies-label"
                    id="customers-label"
                    value={project.typologyDescription}
                    onChange={(e) => setProject({...project, typologyDescription: e.target.value})}
                    label="Project Typology">
                    {fetchTypologiesData.map((dataItem) => (<MenuItem value={dataItem.description}>{dataItem.description}</MenuItem>))}
                </Select>
            </FormControl>
            <p/>
            <TextField
                type="date"
                id="project-start-Date"
                label="Start Date"
                value={null}
                InputProps={{inputProps: {min: getToday()}}}
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={
                    (e) => {
                        setProject({...project, startDate: format(parseISO(e.target.value), "dd/MM/yyyy")});
                    }}
            />

            <p/>
            <TextField
                type="date"
                id="project-end-Date"
                label="End Date"
                value={null}
                InputProps={{inputProps: {min: getToday()}}}
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={(e) => {
                    setProject({...project, endDate: format(parseISO(e.target.value), "dd/MM/yyyy")});
                }}
            />
            <p/>
            <TextField
                type='number'
                id="project-number-of-planned-sprints"
                label="Number of Planned Sprints"
                value={project.projectNumberOfPlannedSprints}
                onChange={(e) => setProject({...project, projectNumberOfPlannedSprints: e.target.value})}
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <p/>
            <TextField
                type='number'
                id="project-sprint-duration"
                label="Project Sprint Duration"
                value={project.projectSprintDuration}
                onChange={(e) => setProject({...project, projectSprintDuration: e.target.value})}
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <p/>
            <FormControl style={{width: '50ch'}}>
                <InputLabel id="demo-simple-select-standard-label">Customer </InputLabel>
                <Select
                    labelId="project-customers-label"
                    id="customers-label"
                    value={project.customerName}
                    onChange={(e) => setProject({...project, customerName: e.target.value})}
                    label="Customer">
                    {fetchCustomersData.map((dataItem) => (<MenuItem value={dataItem.customerName}>{dataItem.customerName}</MenuItem>))}
                </Select>
            </FormControl>

            <p></p>
            <p></p>
            <br></br>
            <InputLabel>
                Project Manager
            </InputLabel>

            <FormControl style={{m: 10, width: '50ch'}}>
                <InputLabel id="demo-simple-select-standard-label">Project Manager Email</InputLabel>
                <Select
                    value={project.userEmail}
                    onChange={(e) => setProject({...project, userEmail: e.target.value})}
                    label="Project Manager Email">
                    {fetchUsersData.map((dataItem) => (<MenuItem value={dataItem.email}>{dataItem.email}</MenuItem>))}
                </Select>
            </FormControl>
            <p/>
            <TextField
                type='number'
                id="costPerHour"
                label="Cost Per Hour"
                value={project.costPerHour}
                onChange={(e) => setProject({...project, costPerHour: e.target.value})}
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <p/>
            <TextField
                type='number'
                id="percentageOfAllocation"
                label="Percentage Of Allocation"
                value={project.percentageOfAllocation}
                onChange={(e) => setProject({...project, percentageOfAllocation: e.target.value})}
                InputLabelProps={{
                    shrink: true,
                }}
            />

            <p></p>

            <div>
                <Button variant="contained" onClick={() => createNewProject()}> Create New Project </Button>
            </div>

            <p></p>

            <Link to="/projects">
                <Button variant="outlined">Cancel</Button>
            </Link>


        </Box>
    </div>);

}

export default ProjectCreationTable;
