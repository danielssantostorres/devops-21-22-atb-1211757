import  * as React from 'react';
import {useContext, useEffect} from "react";
import AppContext from "../../context/AppContext";
import {fetchActivities} from "../../context/Actions";
import DataTable from "../DataTable";

const Activities = () => {
    const {state, dispatch,statusOfActivitiesHeaders} = useContext(AppContext);
    const {activities, selectedProject} = state;
    const {loading, error} = activities;
    const fetchActivitiesData = activities.data;

    const projectData = selectedProject.data;

    useEffect(() => {
        let url = (projectData._links['activities'].href)
        const request = { method: 'GET'};
        fetchActivities(url,request,dispatch);
    },[])

    if (loading === true) {
        return(
            <div>
                <center>
                    <h1>Loading ....</h1>
                </center>
            </div>
        );
    } else {
        if (error !== null) {
            return (<div>
                <center>
                    <h3>
                        Error ....
                    </h3>
                </center>
            </div>)
        } else {
            if (fetchActivitiesData.length > 0) {
                return (
                    <div>
                        <center>
                            <div style={{height: '60%', width: '80%'}}>
                            <DataTable tableData={fetchActivitiesData} headers={statusOfActivitiesHeaders} id={"activityCode"}
                                       url={"/activities/" + projectData.projectCode}
                                height={450}/>
                            </div>
                        </center>
                    </div>
                );
            } else {
               return (<h3> No data ....</h3>);
            }
        }
    }
 };

 export default Activities;


