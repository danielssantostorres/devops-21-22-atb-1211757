import * as React from 'react';
import { useState, useContext, useEffect } from "react";
import Box from '@mui/material/Box';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Switch from '@mui/material/Switch';
import { Button } from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import AppContext from "../../context/AppContext";
import { moveUserStory } from '../../context/Actions';
import { URL_API } from '../../services/URL_API';

const style = {
    modal: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        height: 400,
        bgcolor: 'background.paper',
        border: '1px solid #000',
        // boxShadow: 24,
        p: 4,
    },
    cursorPointer: {
        cursor: 'pointer',
        color: 'black'
    }
};

const DialogUserStory = ({ codeUS }) => {

    const { state, dispatch, projectHeaders } = useContext(AppContext);

    // START: configurações da 'Dialog'
    const [open, setOpen] = useState(false);
    const [fullWidth, setFullWidth] = useState(true);
    const [maxWidth, setMaxWidth] = useState('sm');

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleMaxWidthChange = (event) => {
        setMaxWidth(
            // @ts-expect-error autofill of arbitrary value is not handled.
            event.target.value,
        );
    };

    const handleFullWidthChange = (event) => {
        setFullWidth(event.target.checked);
    };
};

export default DialogUserStory;