import * as React from 'react';
import { useContext, useEffect } from 'react';
import AppContext from "../../context/AppContext";
import { fetchUserStories, setSelectedUserStory } from "../../context/Actions";
import DataTable from "../DataTable";
import { Typography } from "@mui/material";

const ProductBacklog = () => {
    const { state, dispatch, userStoryHeaders } = useContext(AppContext);
    const { userStories, addUserStory, selectedProject } = state;
    const { loading, error, data } = userStories;

    const projectData = selectedProject.data;

    useEffect(() => {
        let url = (projectData._links['productBacklog'].href);
        const request = { method: 'GET' };
        fetchUserStories(url, request, dispatch);
    }, [addUserStory.data])

    if (loading === true) {
        return (
            <div>
                <center>
                    <h1>Loading ....</h1>
                </center>
            </div>);
    } else {
        if (error !== null) {
            return (<div>
                <center>
                    <h3>Error ....</h3>
                </center>
            </div>);
        } else {
            if (data.length > 0) {
                return (
                    <center>
                        <Typography variant="h5">Project {projectData.projectCode} Product Backlog</Typography>
                        <br />
                        <div style={{ height: '60%', width: '80%' }}>
                            <DataTable
                                tableData={data}
                                headers={userStoryHeaders}
                                id={"code"}
                                url={"/productBacklog/" + projectData.projectCode}
                                onClickNavigate={true}
                                onClickFunction={setSelectedUserStory}
                            />
                        </div>
                    </center>
                );
            } else {
                return (
                    <div>
                        <Typography variant="h5">Project {projectData.projectCode} Product Backlog</Typography>
                        <br />
                        <h3>No UserStories found ...</h3>
                    </div>
                );
            }
        }
    }
};

export default ProductBacklog;
