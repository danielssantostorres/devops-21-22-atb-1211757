import { useContext, useEffect, useState } from "react";
import { URL_API } from "../../services/URL_API";
import Button from "@mui/material/Button";
import FormControl from '@mui/material/FormControl';

import {
    fetchAllSprintLinks,
    fetchSprintsDataStarted,
    moveUserStory,
    setSelectedUserStory
} from "../../context/Actions";
import * as React from "react";
import AppContext from "../../context/AppContext";
import TextField from "@mui/material/TextField";
import { InputLabel, MenuItem, Select } from "@mui/material";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import { Alert } from "@mui/material";
import Snackbar from "@mui/material/Snackbar";
import { Typography } from "@mui/material";

function UserStoryDetails() {

    let navigate = useNavigate();

    const { state, dispatch } = useContext(AppContext);
    const { selectedUserStory, sprintLinks, selectedProject, moveUSToSprint } = state;
    const { data } = selectedUserStory;
    const links = sprintLinks.data;
    const projectData = selectedProject.data
    const [sprint, setSprint] = useState(0);
    const [open, setOpen] = React.useState(false);


    useEffect(() => {
        dispatch(fetchSprintsDataStarted())

        let url = (projectData._links['sprints'].href)
        const request = { method: 'GET' };
        fetchAllSprintLinks(url, request, dispatch)

        return () => {
            dispatch(setSelectedUserStory([]))
        }
    }, [])

    let effortField = <div></div>
    if (data.effort != null) {
        effortField = (<TextField
            id="user-story-effort"
            label={"Effort"}
            value={data.effort}
            disabled={true}
            InputLabelProps={{
                shrink: true,
            }}
        />)
    }

    let parentUSCodeField;
    if (data.parentUserStoryCode != null) {
        parentUSCodeField = (<TextField
            id="user-story-parent-code"
            label={"Parent User Story"}
            value={data.parentUserStoryCode}
            disabled={true}
            InputLabelProps={{
                shrink: true,
            }}
        />)
    }

    const [moveToSprint, setMoveToSprint] = useState(false)

    function openMoveToSprintForm() {
        if (!moveToSprint) {
            setMoveToSprint(true)
        } else {
            setMoveToSprint(false)
        }
    }

    let showMoveButton;
    if (moveToSprint === false) {
        showMoveButton = <Button variant="contained" onClick={() => openMoveToSprintForm()}> Move to Sprint </Button>
    }

    let moveForm = <div></div>

    function moveToSprintConfirm() {
        let url = `${URL_API}` + "/projects/" +
            projectData.projectCode + "/productBacklog/" + data.code;

        const request = {
            method: "PATCH",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ sprintID: `${sprint}` })
        };
        moveUserStory(url, request, dispatch);
        setOpen(true)
        console.log(links)
    }

    let updateMessage = <div></div>
    if (moveUSToSprint.error !== null) {
        updateMessage = (
            <div>
                <Snackbar
                    open={open}
                    autoHideDuration={3000}
                    onClose={() => setOpen(false)}>
                    <Alert variant="filled" severity="warning">Something went wrong...</Alert>
                </Snackbar>
            </div>);
    } else if (Object.keys(moveUSToSprint.data).length > 0) {
        updateMessage = (
            <Snackbar
                open={open}
                autoHideDuration={3000}
                onClose={() => {
                    setOpen(false)
                }}>
                <Alert variant="filled" severity="success">User Story {moveUSToSprint.data.code} moved to
                    sprint {moveUSToSprint.data.sprintID}</Alert>
            </Snackbar>
        );
    }

    let SPRINT_LINKS_NUMBER = Object.keys(links).length;
    if (moveToSprint) {
        if (SPRINT_LINKS_NUMBER > 0) {
            moveForm = (<div>
                <FormControl sx={{ m: 1, minWidth: 120 }}>
                    <InputLabel id="demo-simple-select-standard-label"> Sprint </InputLabel>
                    <Select
                        label="Sprint"
                        onChange={(e) => setSprint(e.target.value)}
                        style={{ width: '25ch' }}>
                        {
                            Object.keys(links._links).map((sprintNumber) => (
                                <MenuItem value={sprintNumber}>{sprintNumber}</MenuItem>
                            ))
                        }
                    </Select>
                </FormControl>
                <br></br>
                <Button variant="contained" onClick={() => moveToSprintConfirm()}> Confirm </Button>
            </div>)
        } else {
            const linkToProductBacklog = () => {
                const productBacklogURL = '/productBacklog/' + projectData.projectCode
                navigate(productBacklogURL, "_self")
            }
            const linkToListSprints = () => {
                const sprintsURL = '/sprints/projectCode/' + projectData.projectCode
                navigate(sprintsURL, "_self")
                console.log(projectData)
            }
            moveForm = (
                <div>
                    <h3>No Sprints were found ...</h3>
                    <Button variant="contained" onClick={linkToListSprints}> Project Sprints </Button>
                    <p />
                    <Button variant="outlined" onClick={linkToProductBacklog}> Cancel </Button>
                </div>)
        }
    }

    return (
        <div>
            {updateMessage}
            <center>
                <Typography variant="h5">User Story {data.code}</Typography>
                <br />
                <Box
                    component="form"
                    sx={{
                        '& .MuiTextField-root': { m: 2, width: '50ch' },
                    }}
                    noValidate
                    autoComplete="off"
                >
                    <TextField
                        id="user-story-code"
                        label={"User Story Code"}
                        value={data.code}
                        disabled={true}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <TextField
                        id="user-story-description"
                        label={"Description"}
                        value={data.description}
                        disabled={true}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    {effortField}
                    <TextField
                        id="user-story-priority"
                        label={"Priority"}
                        value={data.priority}
                        disabled={true}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    {parentUSCodeField}
                    <TextField
                        id="user-story-status"
                        label={"Status"}
                        value={data.status}
                        disabled={true}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Box>
                {showMoveButton}
                {moveForm}
            </center>
        </div>
    );
}

export default UserStoryDetails;