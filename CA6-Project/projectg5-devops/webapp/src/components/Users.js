import * as React from 'react';
import {useContext, useState} from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import RefreshIcon from '@mui/icons-material/Refresh';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import DataTable from './DataTable';
import AppContext from "../context/AppContext";
import {URL_API} from "../services/URL_API";
import {fetchUsers} from "../context/Actions";

export default function Users() {

    const {state, dispatch, usersHeaders} = useContext(AppContext);

    const {users} = state;

    const {data} = users;

    const [search, setSearch] = React.useState('');

    const [radioButton, setradioButton] = React.useState('');

    const [alreadySearched, setAlreadySearched] = React.useState(0);

    const searchHandleChange = (event) => {
        setSearch(event.target.value);
    };

    const radioHandleChange = (event) => {
        setradioButton(event.target.value);
    }

    const [tableData, setTableData] = useState([]);

    const [mess, setMess] = React.useState('');

    const [error, setError] = React.useState('')

    const searchUsers = () => {

        if (radioButton === 'email') {
            URL = URL_API + '/users?email=' + search
        } else {
            URL = URL_API + '/users/' + radioButton + '/' + search;
        }

        const request = {method: 'GET'};

        fetchUsers(URL, request, dispatch)

        console.log(users.data);
        console.log(data);
        console.log(URL);
        console.log(users.data)
        console.log(users.error)
        console.log(tableData);

        if (radioButton === '') {
            setMess('Please choose a type of search.')
        } else {
        }

        if (users.data.length === 0 && radioButton !== '') {
            setError('Users not found.')
            setMess('');
        }

        setAlreadySearched(1);
    }


    return (
        <Paper sx={{maxWidth: 936, margin: 'auto', overflow: 'hidden'}}>
            <AppBar
                position="static"
                color="default"
                elevation={0}
                sx={{borderBottom: '1px solid rgba(0, 0, 0, 0.12)'}}
            >
                <Toolbar>
                    <Grid container spacing={2} alignItems="center">
                        <Grid item>
                            <SearchIcon color="inherit" sx={{display: 'block'}}/>
                        </Grid>
                        <Grid item xs>
                            <TextField
                                fullWidth
                                placeholder="Search by email address or user profile"
                                InputProps={{
                                    disableUnderline: true,
                                    sx: {fontSize: 'default'},
                                }}
                                variant="standard"
                                onChange={searchHandleChange}
                            />
                        </Grid>
                        <Grid item>
                            <Button variant="contained" sx={{mr: 1}}
                                    onClick={searchUsers}>>
                                Search user
                            </Button>

                            <Tooltip title="Reload">
                                <IconButton>
                                    <RefreshIcon color="inherit" sx={{display: 'block'}}/>
                                </IconButton>
                            </Tooltip>

                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>


            <RadioGroup
                row
                aria-labelledby="demo-row-radio-buttons-group-label"
                name="row-radio-buttons-group"
            >
                <FormControlLabel value="email"
                                  onChange={radioHandleChange}
                                  control={<Radio/>} label="email"/>
                <FormControlLabel value="profiles"
                                  onChange={radioHandleChange}
                                  control={<Radio/>} label="profiles"/>
            </RadioGroup>
            {/*<Typography sx={{ my: 5, mx: 2 }} color="text.secondary" align="center">*/}
            {/*  No users for this project yet*/}
            {/*</Typography>*/}

            {
                users.data.length !== 0 ? (
                    <DataTable
                        tableData={users.data}
                        headers={usersHeaders}
                        id={"email"}
                        url={'/users'}
                        onClickNavigate={true}
                    />
                ) : (

                    <h3>  {error}
                        {mess}
                    </h3>

                )
            }

        </Paper>


    );
}
