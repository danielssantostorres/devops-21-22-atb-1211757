import React, { useContext, useEffect, useState } from "react";
import AppContext from "../../context/AppContext";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { URL_API } from "../../services/URL_API";
import Box from "@mui/material/Box";
import { addProfileStarted, addTypologyStarted, addTypologyToDB } from "../../context/Actions";

function TypologyForm() {

    const { state, dispatch } = useContext(AppContext)
    const { addTypology } = state;
    const { error, data } = addTypology;
    const initialState = {
        description: '',
    }
    const [typology, setTypology] = useState(initialState)

    useEffect(() => {
        dispatch(addTypologyStarted());
    }, [])

    const createNewTypology = () => {
        let url = `${URL_API}/api/typologies`;
        const req = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(typology)
        }
        addTypologyToDB(url, req, dispatch)
    }

    let submission = <div></div>

    if (error !== null) {
        submission = (<div>
            <center>
                <h4>Something went wrong</h4>
            </center>
        </div>);
    } else {
        if (data.length > 0) {
            submission = (
                <div>
                    <h4>Typology {data[0].typologyDescription} - successfully created.</h4>
                </div>
            );
        }
    }

    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': { m: 2, width: '50ch' },
            }}
            noValidate
            autoComplete="off"
        >
            <TextField
                id="typology-description"
                label="New typology description"
                onChange={(e) => setTypology({ ...typology, typologyDescription: e.target.value })}
            />

            <p></p>
            <Button variant="contained" onClick={() => createNewTypology()}> Submit</Button>
            <p></p>

            {submission}
        </Box>
    );
}

export default TypologyForm;