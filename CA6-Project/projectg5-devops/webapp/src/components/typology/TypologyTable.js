import React, {useContext, useEffect} from "react";
import AppContext from "../../context/AppContext";
import {URL_API} from '../../services/URL_API';
import {fetchTypologies} from '../../context/Actions';
import DataTable from "../DataTable";

const AppTypologies = () => {

    const {state, dispatch, typologyHeader} = useContext(AppContext);

    const {typologies} = state;

    const fetchTypologiesLoading = typologies.loading;
    const fetchTypologiesError = typologies.error;
    const fetchTypologiesData = typologies.data;

    let addTypologyStateSpy = fetchTypologiesData

    useEffect(() => {
        let url = `${URL_API}/api/typologies`;
        const request = {method: 'GET'};
        fetchTypologies(url, request, dispatch);
    }, [addTypologyStateSpy])

    if (fetchTypologiesLoading === true) {
        return (
            <div>
                <center>
                    <h1>Loading ...</h1>
                </center>
            </div>);
    } else {
        if (fetchTypologiesError !== null) {
            return (<div>
                <center>
                    <h3>Error ...</h3>
                </center>
            </div>);
        } else {
            if (fetchTypologiesData.length > 0) {
                return (
                    <center>
                        <div style={{height: '60%', width: '40%'}}>
                            <DataTable
                                tableData={fetchTypologiesData}
                                headers={typologyHeader}
                                id={"description"}
                                onClickNavigate={false}
                            />
                            <p></p>
                        </div>
                    </center>

                );
            } else {
                return (<div>
                    <h3>
                        No data ...
                    </h3>
                </div>);
            }
        }
    }
};

export default AppTypologies;
