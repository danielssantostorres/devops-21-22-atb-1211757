import * as React from "react";
import { useContext } from "react";
import AppContext from "../../context/AppContext";
import DataTable from "../DataTable";
import Button from "@mui/material/Button";

const ResourceTable = () => {

    const { state, resourceHeaders } = useContext(AppContext);
    const { resources } = state;
    const fetchResourcesData = resources.data;


    const getProjectCode = () => {
        const url = (window.location.href)
        return (url.split("/").pop());
    }

    const linkToCreateResources = () => {
        const projectCode = (window.location.href).split("/").pop()

        const resourcesURL = 'http://localhost:3000/resources/projectCode/' + projectCode
        window.open(resourcesURL, "_self")

    }

    if (fetchResourcesData.length > 0) {
        return (
            <div>
                <center>
                    <div style={{ height: '60%', width: '80%' }}>
                        <DataTable
                            tableData={fetchResourcesData}
                            headers={resourceHeaders}
                            id={"resourceID"}
                            onClickNavigate={false}
                        />
                        <Button variant="contained" onClick={linkToCreateResources}> Create Resource </Button>
                        <p></p>
                    </div>
                </center>
            </div>);
    } else {
        return (<div>
            <div>No data...</div>
            <Button variant="contained" onClick={linkToCreateResources}> Create Resource </Button>
        </div>);

    }
}

export default ResourceTable;
