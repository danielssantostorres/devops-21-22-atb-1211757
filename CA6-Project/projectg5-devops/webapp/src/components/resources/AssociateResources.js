/*
Search input
projectCode, dates, costPerHour, allocation, role
save - pegar no state do resource e enviar através do fetch para a API com o url
redirect
 */
import * as React from "react";
import {useContext, useEffect, useState} from "react";
import {Alert, MenuItem, Select} from "@mui/material";
import TextField from "@mui/material/TextField";
import AppContext from "../../context/AppContext";
import {URL_API} from "../../services/URL_API";
import {addResourceToDB, createResourceStarted, fetchUsers} from "../../context/Actions";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import FormHelperText from "@mui/material/FormHelperText";
import SendIcon from '@mui/icons-material/Send';
import Snackbar from "@mui/material/Snackbar";
import {format, parseISO} from "date-fns";


export default function AssociateResources() {


    const getProjectCode = () => {
        const url = (window.location.href)
        return (url.split("/").pop());
    }
    const initialState = {
        userIdDto: '',
        projectCodeDto: getProjectCode(),
        startDateDto: '',
        endDateDto: '',
        costPerHourDto: '',
        percentageOfAllocationDto: '',
    }
    const {state, dispatch} = useContext(AppContext);

    const {resources} = state;
    const {error, data} = resources;
    const [resource, setResource] = useState(initialState)
    const eachResourcesData = resources.data;

    // Start date control - Today

    function getToday() {
        let d = new Date(), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    const createNewResource = () => {

        let roleURL;

        if (role === "Team Member") {
            roleURL = "teamMember"
        } else if (role === "Product Owner") {
            roleURL = "productOwner"
        } else if (role === "Scrum Master") {
            roleURL = "scrumMaster"
        }

        let url = `${URL_API}/api/resources/` + roleURL
        addResourceToDB(url, dispatch, resource);
        setOpen2(true)

    }

    const {users} = state;
    const fetchUsersData = users.data;
    useEffect(() => {
        let url = `${URL_API}/users?email=`;
        const request = {method: 'GET'};
        fetchUsers(url, request, dispatch);
    }, []);


    const [role, setRole] = React.useState('');

    const handleChange = (role) => {
        setRole(role.target.value);
    };

    useEffect(() => {
        dispatch(createResourceStarted());
    }, [])

    let creationMessage = <div></div>

    const [open2, setOpen2] = React.useState(false);

    if (error !== null) {
        creationMessage = (<div>
            <Snackbar
                open={open2}
                autoHideDuration={1500}
                onClose={() => setOpen2(false)}>
                <Alert variant="filled" severity="error" color="error">Resource associated failed</Alert>
            </Snackbar>
        </div>);
    } else if (!Array.isArray(data)) {
        creationMessage = (<Snackbar
            open={open2}
            autoHideDuration={1500}
            onClose={() => {
                setOpen2(false);
                window.open('http://localhost:3000/resources/' + getProjectCode(), "_self")
            }}>

            <Alert variant="filled" severity="success" color="sucess">Resource {data.userIdDto} associated</Alert>
        </Snackbar>);
    }

    return (<div>
            <div>
                <center>
                    <div>
                        <h2 className="MuiTypography-root MuiTypography-h3 css-q4657b-MuiTypography-root">Associate
                            Resource
                        </h2>
                        <h4 className=".MuiTypography-h5">Define your team roles
                        </h4>

                        <p><FormControl style={{width: '100ch'}}>
                            {creationMessage}
                            <InputLabel id="demo-simple-select-standard-label">Select User Email </InputLabel>
                            <Select
                                labelId="resource-user-label"
                                id="user-label"
                                value={eachResourcesData.userIdDto}
                                onChange={(e) => setResource({...resource, userIdDto: e.target.value})}
                                label="Project Manager">
                                {fetchUsersData.map((dataItem) => (
                                    <MenuItem value={dataItem.email}>{dataItem.email}</MenuItem>))}
                            </Select>
                        </FormControl></p>
                        <p><FormControl style={{width: '100ch'}}>
                            <TextField
                                type="date"
                                id="project-start-Date"
                                label="Start Date"
                                value={null}
                                InputProps={{inputProps: {min: getToday()}}}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={(e) => {
                                    setResource({
                                        ...resource, startDate: format(parseISO(e.target.value), "dd/MM/yyyy")
                                    });
                                }}
                            /></FormControl></p>
                        <p><FormControl style={{width: '100ch'}}>

                            <TextField
                                type="date"
                                id="project-end-Date"
                                label="End Date"
                                value={null}
                                InputProps={{inputProps: {min: getToday()}}}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={(e) => {
                                    setResource({...resource, endDate: format(parseISO(e.target.value), "dd/MM/yyyy")});
                                }}
                            />

                        </FormControl></p>
                        <p><FormControl style={{width: '100ch'}}>

                            <TextField
                                required
                                labelID="end-date-resources-label"
                                type='number'
                                required
                                id="cost-per-hour"
                                label="Cost Per Hour"
                                value={eachResourcesData.costPerHourDto}
                                onChange={(e) => setResource({...resource, costPerHourDto: e.target.value})}
                            /></FormControl></p>
                        <p><FormControl style={{width: '100ch'}}>
                            <TextField
                                required
                                labelID="end-date-resources-label"
                                type='number'
                                required
                                id="percentage-of-allocation"
                                label="Allocation (%)"
                                value={eachResourcesData.percentageOfAllocationDto}
                                onChange={(e) => setResource({
                                    ...resource, percentageOfAllocationDto: e.target.value
                                })}
                            /></FormControl></p>


                        <FormControl style={{width: '100ch'}}>
                            <InputLabel id="role">Select Role</InputLabel>
                            <Select
                                required
                                labelId="role-id"
                                id="role"
                                value={eachResourcesData.role}
                                label="Role"
                                onChange={handleChange}
                                labelID="end-date-resources-label"


                            >
                                <MenuItem value={"Team Member"}>Team Member</MenuItem>
                                <MenuItem value={"Product Owner"}>Product Owner</MenuItem>
                                <MenuItem value={"Scrum Master"}>Scrum Master</MenuItem>

                            </Select>
                            <FormHelperText>Role In Project</FormHelperText>
                        </FormControl>

                        <Box sx={{
                            mx: 'auto',
                            width: 200,
                            p: 1,
                            m: 1,
                            textAlign: 'center',
                            fontSize: '0.875rem',
                            fontWeight: '700',
                        }}><Button variant="contained" endIcon={<SendIcon/>} padding="left"
                                   onClick={() => createNewResource()}> ASSOCIATE</Button></Box>
                        <p></p>
                        {creationMessage}

                    </div>
                </center>
            </div>
        </div>)

}

