import React, { useContext, useState } from "react";
import AppContext from "../../context/AppContext";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { URL_API } from "../../services/URL_API";
import { addSprintToDB } from "../../context/Actions";
import Box from "@mui/material/Box";
import { Alert } from "@mui/material";
import Snackbar from "@mui/material/Snackbar";
import { useNavigate } from "react-router-dom";
import { Typography } from "@mui/material";
import {format, parseISO} from "date-fns";


function SprintForm() {
    const navigate = useNavigate();

    const { state, dispatch } = useContext(AppContext);

    const { addSprints } = state;

    const { error, data } = addSprints;

    const { selectedProject } = state;
    const projectData = selectedProject.data;

    const initialState = {
        description: '',
        startDate: '',
        endDate: '',
    }

    const [sprint, setSprint] = useState(initialState)

    const createNewSprint = () => {
        let url = `${URL_API}/api/sprints/projectCode/` + projectData.projectCode;
        addSprintToDB(url, dispatch, sprint);
        setOpen(true)
    }

    function getToday() {
        let d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    let updateMessage = <div></div>
    const [open, setOpen] = React.useState(false);
    if (error !== null) {
        updateMessage = (
            <div>
                <Snackbar
                    open={open}
                    autoHideDuration={1500}
                    onClose={() => setOpen(false)}>
                    <Alert variant="filled" severity="warning">Something went wrong...</Alert>
                </Snackbar>
            </div>);
    } else if (data.length > 0) {
        updateMessage = (
            <Snackbar
                open={open}
                autoHideDuration={1500}
                onClose={() => {
                    setOpen(false)
                    navigate('/sprints/projectCode/' + projectData.projectCode)
                }}>

                <Alert variant="filled" severity="success">Sprint {data.description} created</Alert>
            </Snackbar>
        );
    }

    return (<div>
        {updateMessage}

        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': { m: 2, width: '50ch' },
            }}
            noValidate
            autoComplete="off"
        >
            <Typography variant="h5">Create Sprint</Typography>
            <br />
            <TextField
                id="description"
                label="Description"
                onChange={(e) => setSprint({ ...sprint, description: e.target.value })}
            />

            <TextField
                type="date"
                id="project-start-Date"
                label="Start Date"
                value={null}
                InputProps={{inputProps: {min: getToday()}}}
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={
                    (e) => {
                        setSprint({...sprint, startDate: format(parseISO(e.target.value), "dd/MM/yyyy")});
                    }}
            />
            <TextField
                type="date"
                id="project-end-Date"
                label="End Date"
                value={null}
                InputProps={{inputProps: {min: getToday()}}}
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={
                    (e) => {
                        setSprint({...sprint, endDate: format(parseISO(e.target.value), "dd/MM/yyyy")});
                    }}
            />
            <p><Button variant="contained" onClick={() => createNewSprint()}>Create</Button></p>
        </Box>

    </div>
    );
}

export default SprintForm;
