import React, {useContext, useEffect} from "react";
import AppContext from "../../context/AppContext";
import {URL_API} from '../../services/URL_API';
import {fetchCustomers} from '../../context/Actions';
import DataTable from "../DataTable";

const AppCustomers = () => {

    const {state, dispatch, customerHeaders} = useContext(AppContext);

    const {customers} = state;

    const fetchCustomersLoading = customers.loading;
    const fetchCustomersError = customers.error;
    const fetchCustomersData = customers.data;

    let addCustomerStateSpy = fetchCustomersData

    useEffect(() => {
        let url = `${URL_API}/api/customers`;
        const request = {method: 'GET'};
        fetchCustomers(url, request, dispatch);
    }, [addCustomerStateSpy])

    if (fetchCustomersLoading === true) {
        return (
            <div>
                <center>
                    <h1>Loading ....</h1>
                </center>
            </div>);
    } else {
        if (fetchCustomersError !== null) {
            return (<div>
                <center>
                    <h3>Error ....</h3>
                </center>
            </div>);
        } else {
            if (fetchCustomersData.length > 0) {
                return (
                    <center>
                        <div style={{height: '60%', width: '40%'}}>
                            <DataTable tableData={fetchCustomersData}
                                       headers={customerHeaders}
                                       id={"customerName"}
                                       onClickNavigate={false}/>
                            <p></p>
                        </div>
                    </center>
                );
            } else {
                return (<div>
                    <h3>
                        No data ....
                    </h3>
                </div>);
            }
        }
    }
};

export default AppCustomers;
