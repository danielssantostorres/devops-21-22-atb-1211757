import React, { useContext, useState } from "react";
import AppContext from "../../context/AppContext";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { URL_API } from "../../services/URL_API";
import { addCustomerToDB } from "../../context/Actions";
import Box from "@mui/material/Box";

function CustomerForm() {

    const { state, dispatch } = useContext(AppContext);

    const { addCustomer } = state;

    const { error, data } = addCustomer;

    const initialState = {
        customerName: '',
    }

    const [customer, setCustomer] = useState(initialState)

    const createNewCustomer = () => {
        let url = `${URL_API}/api/customers`;
        addCustomerToDB(url, dispatch, customer)
    }

    let outputMessage = <div></div>

    if (error !== null) {
        outputMessage = (<div>
            <center>
                <h4>Something went wrong ....</h4>
            </center>
        </div>);
    } else {
        if (data.length > 0) {
            outputMessage = (
                <div>
                    <h4>Customer {data[0].customerName} - successfully created.</h4>
                </div>
            );
        }
    }

    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': { m: 2, width: '50ch' },
            }}
            noValidate
            autoComplete="off"
        >
            <TextField
                id="customer-name"
                label="New customer name"
                onChange={(e) => setCustomer({ ...customer, customerName: e.target.value })}
            />

            <p></p>
            <Button variant="contained" onClick={() => createNewCustomer()}> Submit</Button>

            <p></p>

            {outputMessage}

        </Box>
    );
}

export default CustomerForm;