import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Button from "@mui/material/Button";
import {URL_API} from "../../services/URL_API";
import {useContext, useEffect} from "react";
import AppContext from "../../context/AppContext";
import {addProfileToDB, fetchProfileDataList, fetchProfileLinks, fetchProfiles, fetchUser} from "../../context/Actions";
import FormLabel from "@mui/material/FormLabel";

export default function AddProfile(Email) {
    const {state, dispatch} = useContext(AppContext);
    const {profileLinks, addProfiles, applicationCollections} = state;
    const {loading, data, error} = profileLinks

    const addProfileError = addProfiles.error
    const addProfileData = addProfiles.data

    const collectionLinks = applicationCollections.data

    useEffect(() => {
        let url = collectionLinks._links['profiles'].href;
        const request = {method: 'GET'};
        fetchProfileLinks(url, request, dispatch);
    }, []);

    const [profile, setProfile] = React.useState('');

    const handleChange = (event) => {
        setProfile(event.target.value);
    };

    const addProfile = () => {
        let addProfileURL = `${URL_API}/profiles/` + profile + '/updateuser/' + Email.Email;
        const request = {method: 'POST'};
        addProfileToDB(addProfileURL, request, dispatch)
    }

    let message = <div></div>
    if (addProfileError !== null) {
        message = <div><p>Fail: User already has that profile. </p></div>
    } else if (addProfileData.length > 0) {
        message = <div><p> Profile succesfully added. </p></div>

    }

    let dataItems
    if (!Array.isArray(data)) {
        dataItems = Object.keys(data._links)

        return (
            <div style={{"width": "50%", "float": "right"}}>
                <FormControl>
                    <FormLabel id="demo-radio-buttons-group-label">Add profile: </FormLabel>
                    <FormControl variant="standard" sx={{m: 1, minWidth: 120}}>


                        <InputLabel id="demo-simple-select-standard-label">Choose a new profile</InputLabel>
                        <Select
                            labelId="demo-simple-select-standard-label"
                            id="demo-simple-select-standard"
                            value={profile}
                            onChange={handleChange}
                            label="Profile"
                        >
                            {
                                dataItems.map((dataItem) => (
                                    <MenuItem value={dataItem}>{dataItem}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>


                    <Button variant="contained" onClick={addProfile}>
                        Add Profile
                    </Button>
                    {message}
                </FormControl>
            </div>
        )
    }

}
