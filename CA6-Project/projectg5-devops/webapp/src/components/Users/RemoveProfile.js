import * as React from 'react';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import MenuItem from "@mui/material/MenuItem";
import {useContext, useEffect, useState} from "react";
import AppContext from "../../context/AppContext";
import {URL_API} from "../../services/URL_API";
import Button from "@mui/material/Button";
import {fetchProfiles, fetchUser} from "../../context/Actions";

export default function RemoveProfile(Email) {

    let message = <div>as </div>

    const {state, dispatch} = useContext(AppContext);

    const {addProfiles, user} = state;

    const {loading, data, error} = user;

    const [profile, setProfile] = React.useState([])

    const [mess, setMess] = React.useState('');

    // Update table if a new Profile was added or current profile removed
    const [userProfileList, setUserProfileList] = useState(data.userProfileList)
    const addProfileData = addProfiles.data

    useEffect(() => {
        const user = Email.Email;
        let url = `${URL_API}/users/email/` + user;
        const request = {method: 'GET'};
        fetchUser(url, request, dispatch)
    }, [addProfileData,userProfileList])

    const removeProfile = () => {
        let removeProfileURL = `${URL_API}/profiles/` + profile + '/updateuser/' + Email.Email;

        if (data.userProfileList.length <= 1) {
            setMess("Fail: User needs at least one profile.");
        } else if (data.userProfileList.length > 1) {

            fetch(removeProfileURL, {method: 'DELETE'})
            setMess('Profile successfully removed.');
            setUserProfileList(data.userProfileList)
        }

    }

    const handleChangeProfile = (event) => {
        setProfile(event.target.value);
    };


    return (<div style={{"width": "50%", "float": "left"}}>
            <FormControl>
                <FormLabel id="demo-radio-buttons-group-label">User's profile list:</FormLabel>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue="female"
                    name="radio-buttons-group"
                >
                    {
                        data.userProfileList.map((dataItem) => (
                            <FormControlLabel onChange={handleChangeProfile} value={dataItem} control={<Radio/>}
                                              label={dataItem}/>
                        ))

                    }

                </RadioGroup>

                {<Button variant="contained" onClick={removeProfile}>
                    Remove Profile
                </Button>}

            </FormControl>

            <p></p>

            {mess}
        </div>
    );
}