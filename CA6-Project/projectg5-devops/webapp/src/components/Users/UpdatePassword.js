import React, {useContext, useState} from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from "@mui/material/Button";
import {URL_API} from "../../services/URL_API";
import {changeUserPassword, changeUserPasswordStarted} from "../../context/Actions";
import AppContext from "../../context/AppContext";
import {Alert} from "@mui/material";
import VisibilityIcon from '@mui/icons-material/Visibility';
import InputPassword from '../password/InputPassword';

const UpdatePassword = ({userEmail}) => {
    const {state, dispatch} = useContext(AppContext);
    const {changePassword} = state;

    // flag for password icon
    const [ passwordType, setPasswordType ] = useState("password");

    const initialState = {
        email: userEmail,
        actualPassword: '',
        newPassword: ''
    }
    const [userToChangePassword, setUserToChangePassword] = useState(initialState);

    function changePasswordFunction() {
        dispatch(changeUserPasswordStarted())
        let url = `${URL_API}/users/password`;
        const changePasswordJSON = {
            email: userToChangePassword.email,
            oldPassword: userToChangePassword.actualPassword,
            newPassword: userToChangePassword.newPassword
        }
        const request = {
            method: "PATCH",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(changePasswordJSON)
        }
        changeUserPassword(url, request, dispatch);
    }

    let submission = <div></div>
    if (changePassword.error !== null) {
        submission = (<div>
            <Alert variant="filled" severity="warning">Something went wrong...</Alert>
        </div>);
    } else {
        if (!Array.isArray(changePassword.data)) {
            if (changePassword.data.email.length > 0) {
                submission = (
                    <div>
                        <Alert variant="filled" severity="success">Password changed successfully</Alert>
                    </div>
                );
            }
        }
    }

    const changeActualAndOldPassword = (eventValue, valueField) => {

        if(valueField == "actualPassword") {

            setUserToChangePassword({
                ...userToChangePassword, 
                actualPassword: eventValue
            })
        } else {
            setUserToChangePassword({
                ...userToChangePassword, 
                newPassword: eventValue
            })
        }
    };

    return (
        <div>
            <center>
                <Box component="form"
                     sx={{
                         '& .MuiTextField-root': {m: 2, width: '50ch'},
                     }}
                     noValidate
                     autoComplete="off">

                    {/* <TextField
                        type="password"
                        id="actualPassword"
                        label="Actual Password"
                        value={userToChangePassword.actualPassword}
                        onChange={(e) => setUserToChangePassword({...userToChangePassword, actualPassword: e.target.value})}
                    /> */}
                    {/* <TextField
                        type="password"
                        id="newPassword"
                        label="New Password"
                        value={userToChangePassword.newPassword}
                        onChange={(e) => setUserToChangePassword({...userToChangePassword, newPassword: e.target.value})}
                    /> */}

                    <Box style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginBottom: '20px'
                    }}>
                        <InputPassword parentToChildFunc={changeActualAndOldPassword} 
                            valueField={"actualPassword"} 
                            labelPassword={"Actual Password"}
                        />
                        <InputPassword parentToChildFunc={changeActualAndOldPassword} 
                            valueField={"newPassword"} 
                            labelPassword={"New Password"}
                        />
                    </Box>
                </Box>
            </center>
            <Button variant="contained" onClick={() => changePasswordFunction()}>
                Change Password
            </Button>

            <p></p>
            {submission}
        </div>
    );

}

export default UpdatePassword;