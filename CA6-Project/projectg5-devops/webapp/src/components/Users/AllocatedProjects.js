import DataTable from "../DataTable";
import AppContext from "../../context/AppContext";
import React, {useContext, useEffect} from "react";
import {URL_API} from "../../services/URL_API";
import {
    fetchAllocatedProjects,
    fetchAllocatedProjectsStarted
} from "../../context/Actions";
import {CircularProgress} from "@mui/material";

function AllocatedProjects() {

    const {state, dispatch, allocatedProjectsHeaders} = useContext(AppContext);
    const {allocatedProjects} = state;
    const {loading, error, data} = allocatedProjects;

    useEffect(() => {
        dispatch(fetchAllocatedProjectsStarted())

        let url = `${URL_API}/users/projects/` + getEmail();
        const request = {method: 'GET'};
        fetchAllocatedProjects(url, request, dispatch);

        return () => {
            dispatch(fetchAllocatedProjectsStarted())
        }
    }, [])

    const getEmail = () => {
        const url = (window.location.href)
        return (url.split("/").pop());
    }

    if (loading === true) {
        return (
            <div>
                <center>
                    <h3>Loading .... <CircularProgress/></h3>
                </center>
            </div>
        )
    } else {
        if (error !== null) {
            return (
                <div>
                    <center>
                        <h3>
                            Error in fetching allocated projects...
                        </h3>
                    </center>
                </div>
            )
        } else {
            if (data.length > 0) {
                return (
                    <div>
                        <h1 className="MuiTypography-root MuiTypography-h5 css-q4657b-MuiTypography-root">
                            Allocated Projects
                        </h1>

                        <br></br>

                        <center>
                            <div style={{height: '60%', width: '80%'}}>
                                <DataTable
                                    tableData={data}
                                    headers={allocatedProjectsHeaders}
                                    id={"projectName"}
                                    onClickNavigate={false}
                                >
                                </DataTable>
                            </div>
                        </center>

                    </div>
                )
            }
        }
    }
}

export default AllocatedProjects;
