import React, { useContext, useEffect } from "react";
import AppContext from "../context/AppContext";
import { fetchUser, fetchUserStarted } from "../context/Actions";
import { URL_API } from "../services/URL_API";
import UpdateUserTable from "../components/Users/UpdateUserTable";
import UpdatePassword from "../components/Users/UpdatePassword";
import AddProfile from "../components/Users/AddProfile";
import { CircularProgress } from "@mui/material";
import RemoveProfile from "../components/Users/RemoveProfile.js";
import AllocatedProjects from "../components/Users/AllocatedProjects";
import { Typography } from "@mui/material";

function UserUpdate() {

    const { state, dispatch } = useContext(AppContext);
    const { user } = state;
    const { loading, error, data } = user;

    const getCurrentUserEmail = () => {
        const url = (window.location.href)
        return (url.split("/").pop());
    }

    useEffect(() => {
        dispatch(fetchUserStarted())
        let url = `${URL_API}/users/email/` + getCurrentUserEmail();
        const request = { method: 'GET' };
        fetchUser(url, request, dispatch);
    }, [])

    if (loading === true) {
        return (
            <div>
                <center>
                    <h3>Loading .... <CircularProgress /></h3>
                </center>
            </div>);
    } else {
        if (error !== null) {
            return (<div>
                <center>
                    <h3>Server connection failed</h3>
                </center>
            </div>);
        } else {
            if (data.email.length > 0) {
                return (
                    <div>
                        <Typography variant="h5">{data.userName} Account Information</Typography>
                        <br />
                        <UpdateUserTable userEmail={data.email} userName={data.userName} userFunction={data.function} />

                        <br></br>
                        <br></br>
                        <br></br>

                        <UpdatePassword userEmail={data.email} />
                        <br></br>
                        <br></br>
                        <br></br>

                        <div>
                            <AllocatedProjects />
                        </div>

                        <br></br>
                        <br></br>
                        <br></br>

                        <center>
                            <div style={{ width: '50%' }}>
                                <AddProfile Email={data.email}> </AddProfile>
                                <RemoveProfile Email={data.email}> </RemoveProfile>
                            </div>
                        </center>

                    </div>
                );
            } else {
                return (<div>
                    <h3>No such email in store</h3>
                </div>);

            }

        }

    }

}

export default UserUpdate;