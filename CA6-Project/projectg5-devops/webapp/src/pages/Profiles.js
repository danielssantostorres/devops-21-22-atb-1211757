import React, {useContext, useEffect} from "react";
import Button from "@mui/material/Button";
import ProfileCreationForm from "../components/profiles/ProfileCreationForm";
import ProfileTable from "../components/profiles/ProfileTable";
import {setOpenForm} from "../context/Actions";
import AppContext from "../context/AppContext";

function Profiles() {

    const {state,dispatch} = useContext(AppContext)
    const {openForm} = state

    useEffect(() => {
        dispatch(setOpenForm(false))
    },  [])

    let showFormState;
    if (openForm === true) {
        showFormState = <ProfileCreationForm/>
    }

    return (
        <div>
            <ProfileTable/>

            <Button onClick={() => {
                if (openForm === false) {
                    dispatch(setOpenForm(true));
                }
            }} variant="contained">
                Create New Profile
            </Button>

            {showFormState}
        </div>
    );
}

export default Profiles;
