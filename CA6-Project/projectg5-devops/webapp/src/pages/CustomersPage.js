import React, { useState } from "react";
import CustomersTable from "../components/customer/CustomersTable"
import CustomerForm from "../components/customer/CustomerForm"
import Button from "@mui/material/Button";

function Customers() {

    const [customerForm, isCustomerFormOn] = useState(false);

    const [buttonLabel, setButtonLabel] = useState("Show Creation Form");

    let creationPanel;

    if (customerForm === true) {
        creationPanel = <CustomerForm />
    } else {
        creationPanel = <div />
    }

    return (
        <div>

            <div>
                <CustomersTable />
            </div>

            <Button variant="contained" onClick={() => {
                if (customerForm === false) {
                    isCustomerFormOn(true);
                    setButtonLabel("Hide Creation Form")
                } else {
                    isCustomerFormOn(false);
                    setButtonLabel("Create New Customer")
                }
            }}>
                {buttonLabel}
            </Button>

            {creationPanel}

        </div>
    )
}

export default Customers;