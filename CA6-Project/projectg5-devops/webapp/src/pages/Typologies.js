import React, { useState } from "react";
import TypologyTable from "../components/typology/TypologyTable"
import TypologyForm from "../components/typology/TypologyForm"
import Button from "@mui/material/Button";

function Typologies() {

    const [typologyForm, isTypologyFormOn] = useState(false);

    const [buttonLabel, setButtonLabel] = useState("Show Creation Form");

    let creationPanel;

    if (typologyForm === true) {
        creationPanel = <TypologyForm />
    } else {
        creationPanel = <div />
    }

    return (
        <div>
            <div>
                <TypologyTable />
            </div>

            <Button variant="contained" onClick={() => {
                if (typologyForm === false) {
                    isTypologyFormOn(true);
                    setButtonLabel("Cancel New Typology Creation")
                } else {
                    isTypologyFormOn(false);
                    setButtonLabel("Create New Typology")
                }
            }}>
                {buttonLabel}
            </Button>

            {creationPanel}

        </div>
    )
}

export default Typologies;