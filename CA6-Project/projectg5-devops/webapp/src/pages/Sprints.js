import React from "react"
import SprintTable from "../components/sprints/SprintTable";
import {Link} from "react-router-dom";
import Button from "@mui/material/Button";

export const Sprints = () => {
    return (
        <center>
            <div style={{height: '60%', width: '80%'}}>
                <SprintTable/>
                <Link style={{textDecoration: 'none'}} to="newSprint">
                    <Button variant="contained">Create Sprint</Button>
                </Link>
            </div>
        </center>
    )
}
