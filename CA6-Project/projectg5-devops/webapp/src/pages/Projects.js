import React from "react"
import ProjectsTable from "../components/projects/ProjectsTable"
import { Link } from "react-router-dom";
import Button from "@mui/material/Button";

export const Projects = () => {
    return (
        <center>
            <div style={{ height: '60%', width: '80%' }}>
                <ProjectsTable />
                <Link style={{ textDecoration: 'none' }} to="/projects/newProject">
                    <Button variant="contained">Create Project</Button>
                </Link>

            </div>
        </center>

    )
}