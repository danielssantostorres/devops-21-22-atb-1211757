import React, {useReducer} from 'react';
import PropTypes from "prop-types";
import {Provider} from './AppContext';
import reducer from './Reducer';
import DialogUserStory from '../components/userStories/DialogUserStory';

const initialState = {

    projectLinks: {
        loading: false,
        error: null,
        data: []
    },
    projectDataList: {
        loading: false,
        error: null,
        data: []
    },
    selectedProject: {
        loading: false,
        error: null,
        data: []
    },
    addProjects: {
        loading: true,
        error: null,
        data: []
    },
    updatedUser: {
        loading: true,
        error: null,
        data: []
    },
    sprintLinks: {
        loading: false,
        error: null,
        data: []
    },
    sprintDataList: {
        loading: false,
        error: null,
        data: []
    },
    selectedSprint: {
        loading: false,
        error: null,
        data: []
    },
    addSprints: {
        loading: true,
        error: null,
        data: []
    },

    addUserStory: {
        loading: false,
        error: null,
        data: []
    },
    customers: {
        loading: true,
        error: null,
        data: []
    },
    addCustomer: {
        loading: true,
        error: null,
        data: []
    },
    profileLinks: {
        loading: false,
        error: null,
        data: []
    },
    profileDataList: {
        loading: false,
        error: null,
        data: []
    },
    addProfiles: {
        loading: true,
        error: null,
        data: []
    },
    removeProfile: {
        loading: true,
        error: null,
        data: []
    },
    userStories: {
        loading: true,
        error: null,
        data: []
    },
    selectedUserStory: {
        loading: false,
        error: null,
        data: []
    },
    resources: {
        loading: true,
        error: null,
        data: []
    },
    addResources: {
        loading: true,
        error: null,
        data: []
    },
    user: {
        loading: true,
        error: null,
        data: []
    },
    changePassword: {
        loading: true,
        error: null,
        data: []
    },
    addTypology: {
        loading: true,
        error: null,
        data: []
    },

    typologies: {
        loading: true,
        error: null,
        data: []
    },

    allocatedProjects: {
        loading: true,
        error: null,
        data: []
    },
    users: {
        loading: false,
        error: null,
        data: []
    },

    activities: {
        loading: false,
        error: null,
        data: []
    },
    moveUSToSprint: {
        loading: false,
        error: null,
        data: []
    },
    applicationCollections:{
        loading: false,
        error: null,
        data: []
    },
    openForm: true
};

const projectHeaders = [
    {field: 'projectCode', headerName: 'Project Code', minWidth: 100},
    {field: 'projectName', headerName: 'Name', minWidth: 100, flex: 1},
    {field: 'projectDescription', headerName: 'Description', minWidth: 100, flex: 1},
    {field: 'projectBusinessSector', headerName: 'Business Sector', minWidth: 100, flex: 1},
    {field: 'projectNumberOfPlannedSprints', headerName: 'Planned Sprints', minWidth: 100},
    {field: 'projectSprintDuration', headerName: 'Sprint Duration', minWidth: 100},
    {field: 'projectBudget', headerName: 'Budget', minWidth: 100},
    {field: 'startDate', headerName: 'Start Date', minWidth: 100},
    {field: 'endDate', headerName: 'End Date', minWidth: 100},
    {field: 'typologyDescription', headerName: 'Typology', minWidth: 100, flex: 1},
    {field: 'customerName', headerName: 'Customer', minWidth: 100, flex: 1},
    {field: 'status', headerName: 'Status', minWidth: 100, flex: 1},
];

const customerHeaders = [
    {field: 'customerName', headerName: 'Customer Name', minWidth: 400},
];

const profileHeaders = [
    {field: 'designation', headerName: 'Profile Description', minWidth: 400, flex: 1}
]

const userStoryHeaders = [
    {field: 'code', headerName: 'Code', minWidth: 100},
    {field: 'effort', headerName: 'Estimated Effort', minWidth: 100},
    {field: 'priority', headerName: 'Priority', minWidth: 100},
    {field: 'description', headerName: 'Description', minWidth: 100, flex: 1},
    {field: 'status', headerName: 'Status', minWidth: 100},
    {field: 'parentUserStoryCode', headerName: 'Parent', minWidth: 100},
    {
        field: 'button',
        headerName: '',
        minWidth: 100,
        renderCell: (cellValues) => {
            return (
                <DialogUserStory codeUS={cellValues.row.code}/>
            );
        }
    },
];

const resourceHeaders = [
    {field: 'resourceID', headerName: 'Resource ID', minWidth: 100},
    {field: 'email', headerName: 'Email', minWidth: 100},
    {field: 'projectCode', headerName: 'Code', minWidth: 100},
    {field: 'startDate', headerName: 'Start Date', minWidth: 100},
    {field: 'endDate', headerName: 'End Date', minWidth: 100},
    {field: 'costPerHour', headerName: 'Cost Per Hour', minWidth: 100},
    {field: 'percentageOfAllocation', headerName: 'Percentage Of Allocation', minWidth: 100},
    {field: 'role', headerName: 'Role', minWidth: 100},
];

const sprintHeaders = [
    {field: 'sprintNumber', headerName: 'Sprint Number', minWidth: 100},
    {field: 'startDate', headerName: 'Start Date', minWidth: 100},
    {field: 'endDate', headerName: 'End Date', minWidth: 100},
    {field: 'description', headerName: 'Sprint Description', minWidth: 100},
    {field: 'status', headerName: 'Sprint Status', minWidth: 100},
];

const usersHeaders = [
    {field: 'userName', headerName: 'Name', minWidth: 100, flex: 1},
    {field: 'email', headerName: 'Email', minWidth: 100, flex: 1},
    {field: 'userProfileList', headerName: 'Profile List', minWidth: 100},
    {field: 'activation', headerName: 'Activation', minWidth: 100},
    {field: 'function', headerName: 'Function', minWidth: 100, flex: 1},
];

const typologyHeader = [
    {field: 'description', headerName: 'Typology Description', minWidth: 100},
];

const allocatedProjectsHeaders = [
    {field: 'projectName', headerName: 'Project Name', minWidth: 100, flex: 1},
    {field: 'role', headerName: 'Role', minWidth: 100, flex: 1}

]

const statusOfActivitiesHeaders = [
    {field: 'typeOfActivity', headerName: 'Type Of Activity', minWidth: 100, flex: 1},
    {field: 'activityCode', headerName: 'Activity Code', minWidth: 100, flex: 1},
    {field: 'activityStatus', headerName: 'Activity Status', minWidth: 100, flex: 1}

]

const AppProvider = (props) => {

    const [state, dispatch] = useReducer(reducer, initialState);

    return (
        <Provider value={{
            state,
            dispatch,
            projectHeaders,
            customerHeaders,
            profileHeaders,
            typologyHeader,
            userStoryHeaders,
            resourceHeaders,
            sprintHeaders,
            allocatedProjectsHeaders,
            usersHeaders,
            statusOfActivitiesHeaders,
        }}>
            {props.children}
        </Provider>
    );
};

AppProvider.propTypes = {children: PropTypes.node};

export default AppProvider;

