package switchfive.project.ainfrastructure.persistence.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CustomerJPATest {

    @Test
    void createCustomerJpa() {
        //Arrange
        String name = "Ferrari";

        //Act
        CustomerJPA customerJPA = CustomerJPA.createCustomerJpa(name);
        String actual = customerJPA.getName();

        //Assert
        Assertions.assertEquals(name,actual);
    }
}
