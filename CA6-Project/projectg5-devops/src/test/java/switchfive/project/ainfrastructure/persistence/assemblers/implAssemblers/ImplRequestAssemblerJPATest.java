package switchfive.project.ainfrastructure.persistence.assemblers.implAssemblers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import switchfive.project.ainfrastructure.persistence.data.RequestJPA;
import switchfive.project.ddomain.aggregates.request.Request;
import switchfive.project.ddomain.factories.iFactories.RequestFactory;
import switchfive.project.ddomain.shared.valueObjects.Email;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;
import switchfive.project.ddomain.shared.valueObjects.RequestID;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class ImplRequestAssemblerJPATest {

    @Mock
    RequestFactory requestFactory;

    @InjectMocks
    ImplRequestAssemblerJPA implRequestAssemblerJPA;

    @Test
    void toDomain() {
        // Arrange
        RequestJPA requestJPA = new RequestJPA();
        requestJPA.setApproved(false);
        requestJPA.setCreationDate("2010-09-29");
        requestJPA.setProfileDescription("Visitor");
        requestJPA.setIdentity("123e4567-e89b-12d3-a456-426614174000");
        requestJPA.setUserID("abilio@isep.ipp.pt");

        Request expected = mock(Request.class);
        RequestID requestID = RequestID.createRequestID(requestJPA.getIdentity());
        Email userID = Email.createEmail(requestJPA.getUserID());
        ProfileDescription profileID = ProfileDescription
                .createProfileDescription(requestJPA.getProfileDescription());
        LocalDate creationDate = java.time.LocalDate.parse(requestJPA.getCreationDate());


        doReturn(expected).when(requestFactory)
                .createNewRequest(requestID,
                        userID,
                        profileID,
                        creationDate,
                        requestJPA.isApproved());

        // Act
        Request result = implRequestAssemblerJPA.toDomain(requestJPA);

        // Assert
        assertEquals(expected, result);
    }

    @Test
    void toData() {
        // Arrange
        RequestID requestID = RequestID.createRequestID();
        Email userID = Email.createEmail("abilio@isep.ipp.pt");
        ProfileDescription profileID = ProfileDescription
                .createProfileDescription("Visitor");
        LocalDate creationDate = java.time.LocalDate.parse("2010-09-29");
        Request request = new Request(requestID, userID, profileID, creationDate,
                true);


        // Act
        RequestJPA result = implRequestAssemblerJPA.toData(request);

        // Assert
        assertEquals(request.getIdentity(), result.getIdentity());
        assertEquals("Visitor",result.getProfileDescription());
        assertEquals("abilio@isep.ipp.pt",result.getUserID());
        assertEquals("2010-09-29",result.getCreationDate());
        assertTrue(result.isApproved());
    }
}
