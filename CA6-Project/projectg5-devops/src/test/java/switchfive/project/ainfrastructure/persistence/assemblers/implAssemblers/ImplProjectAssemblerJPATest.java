package switchfive.project.ainfrastructure.persistence.assemblers.implAssemblers;

import org.junit.jupiter.api.Test;
import switchfive.project.ainfrastructure.persistence.data.ProjectJPA;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.factories.implFactories.ImplProjectBuilder;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ImplProjectAssemblerJPATest {

    @Test
    void getTypologyDescriptionFromProjectJPA() {
        //Arrange
        TypologyDescription expected = TypologyDescription.create("testing");

        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                new ImplProjectAssemblerJPA(projectBuilderMock);

        ProjectJPA projectJPAMock = mock(ProjectJPA.class);

        when(projectJPAMock.getTypologyDescription()).thenReturn("testing");

        //Act
        TypologyDescription actual =
                projectAssemblerJPA.getTypologyDescriptionFromProjectJPA(
                        projectJPAMock);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getCustomerNameFromProjectJPA() {
        //Arrange
        CustomerName expected = CustomerName.create("Ferrari");

        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                new ImplProjectAssemblerJPA(projectBuilderMock);

        ProjectJPA projectJPAMock = mock(ProjectJPA.class);

        when(projectJPAMock.getCustomerName()).thenReturn("Ferrari");

        //Act
        CustomerName actual =
                projectAssemblerJPA.getCustomerNameFromProjectJPA(
                        projectJPAMock);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getProjectCodeFromProjectJPA() {
        //Arrange
        ProjectCode expected = ProjectCode.createProjectCode("ISEP1");

        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                new ImplProjectAssemblerJPA(projectBuilderMock);

        ProjectJPA projectJPAMock = mock(ProjectJPA.class);

        when(projectJPAMock.getCode()).thenReturn("ISEP1");

        //Act
        ProjectCode actual =
                projectAssemblerJPA.getProjectCodeFromProjectJPA(
                        projectJPAMock);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getProjectNameFromProjectJPA() {
        //Arrange
        ProjectName expected = ProjectName.create("SWITCH");

        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                new ImplProjectAssemblerJPA(projectBuilderMock);

        ProjectJPA projectJPAMock = mock(ProjectJPA.class);

        when(projectJPAMock.getName()).thenReturn("SWITCH");

        //Act
        ProjectName actual =
                projectAssemblerJPA.getProjectNameFromProjectJPA(
                        projectJPAMock);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getDescriptionFromProjectJPA() {
        //Arrange
        ProjectDescription expected = ProjectDescription.create("Testing");

        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                new ImplProjectAssemblerJPA(projectBuilderMock);

        ProjectJPA projectJPAMock = mock(ProjectJPA.class);

        when(projectJPAMock.getDescription()).thenReturn("Testing");

        //Act
        ProjectDescription actual =
                projectAssemblerJPA.getDescriptionFromProjectJPA(
                        projectJPAMock);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getBusinessSectorFromProjectJPA() {
        //Arrange
        ProjectBusinessSector expected = ProjectBusinessSector.create(
                "Education");

        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                new ImplProjectAssemblerJPA(projectBuilderMock);

        ProjectJPA projectJPAMock = mock(ProjectJPA.class);

        when(projectJPAMock.getBusinessSector()).thenReturn("Education");

        //Act
        ProjectBusinessSector actual =
                projectAssemblerJPA.getBusinessSectorFromProjectJPA(
                        projectJPAMock);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getDatesFromProjectJPA() throws ParseException {
        //Arrange
        Time expected = Time.create("17/05/2023", "17/08/2024");

        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                new ImplProjectAssemblerJPA(projectBuilderMock);

        ProjectJPA projectJPAMock = mock(ProjectJPA.class);

        when(projectJPAMock.getStartDate()).thenReturn("17/05/2023");
        when(projectJPAMock.getEndDate()).thenReturn("17/08/2024");

        //Act
        Time actual = projectAssemblerJPA.getDatesFromProjectJPA(
                projectJPAMock);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getNumberOfSprintsFromProjectJPA() {
        //Arrange
        ProjectNumberOfPlannedSprints expected =
                ProjectNumberOfPlannedSprints.create(
                        1);

        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                new ImplProjectAssemblerJPA(projectBuilderMock);

        ProjectJPA projectJPAMock = mock(ProjectJPA.class);

        when(projectJPAMock.getNumberOfPlannedSprints()).thenReturn(1);

        //Act
        ProjectNumberOfPlannedSprints actual =
                projectAssemblerJPA.getNumberOfSprintsFromProjectJPA(
                        projectJPAMock);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getBudgetFromProjectJPA() {
        //Arrange
        ProjectBudget expected = ProjectBudget.create(1.0);

        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                new ImplProjectAssemblerJPA(projectBuilderMock);

        ProjectJPA projectJPAMock = mock(ProjectJPA.class);

        when(projectJPAMock.getBudget()).thenReturn(1.0);

        //Act
        ProjectBudget actual = projectAssemblerJPA.getBudgetFromProjectJPA(
                projectJPAMock);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getSprintDurationFromProjectJPA() {
        //Arrange
        ProjectSprintDuration expected = ProjectSprintDuration.create(1);

        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                new ImplProjectAssemblerJPA(projectBuilderMock);

        ProjectJPA projectJPAMock = mock(ProjectJPA.class);

        when(projectJPAMock.getSprintDuration()).thenReturn(1);

        //Act
        ProjectSprintDuration actual =
                projectAssemblerJPA.getSprintDurationFromProjectJPA(
                        projectJPAMock);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void projectToProjectJPA() throws ParseException {
        //Arrange
        ImplProjectBuilder projectBuilderMock = mock(ImplProjectBuilder.class);
        ImplProjectAssemblerJPA projectAssemblerJPA =
                spy(new ImplProjectAssemblerJPA(projectBuilderMock));

        Project projectMock = mock(Project.class);

        ProjectJPA projectJPA = new ProjectJPA();
        projectJPA.setCode("Monza");
        projectJPA.setName("Ferrari");
        projectJPA.setDescription("Sports");
        projectJPA.setCustomerName("Maranello");
        projectJPA.setTypologyDescription("Fast and furious");
        projectJPA.setStartDate("18/05/2023");
        projectJPA.setEndDate("18/05/2024");
        projectJPA.setSprintDuration(1);
        projectJPA.setNumberOfPlannedSprints(1);
        projectJPA.setBudget(0.0);
        projectJPA.setBusinessSector("Automotive");
        projectJPA.setStatus("Planned");

        doReturn(ProjectCode.createProjectCode("Monza")).when(
                projectAssemblerJPA).getProjectCodeFromProjectJPA(
                any(ProjectJPA.class));
        doReturn(ProjectName.create("Ferrari")).when(
                projectAssemblerJPA).getProjectNameFromProjectJPA(
                any(ProjectJPA.class));
        doReturn(ProjectDescription.create("Sports")).when(
                projectAssemblerJPA).getDescriptionFromProjectJPA(
                any(ProjectJPA.class));
        doReturn(CustomerName.create("Maranello")).when(
                projectAssemblerJPA).getCustomerNameFromProjectJPA(
                any(ProjectJPA.class));
        doReturn(TypologyDescription.create("Fast and furious")).when(
                projectAssemblerJPA).getTypologyDescriptionFromProjectJPA(
                any(ProjectJPA.class));
        doReturn(Time.create("18/05/2023", "18/05/2024")).when(
                projectAssemblerJPA).getDatesFromProjectJPA(
                any(ProjectJPA.class));
        doReturn(ProjectSprintDuration.create(1)).when(
                projectAssemblerJPA).getSprintDurationFromProjectJPA(
                any(ProjectJPA.class));
        doReturn(ProjectNumberOfPlannedSprints.create(1)).when(
                projectAssemblerJPA).getNumberOfSprintsFromProjectJPA(
                any(ProjectJPA.class));
        doReturn(ProjectBudget.create(0.0)).when(
                projectAssemblerJPA).getBudgetFromProjectJPA(
                any(ProjectJPA.class));
        doReturn(ProjectBusinessSector.create("Automotive")).when(
                projectAssemblerJPA).getBusinessSectorFromProjectJPA(
                any(ProjectJPA.class));
        doReturn(ProjectStatus.valueOf("Planned")).when(projectAssemblerJPA).
                getProjectStatusFromProjectJPA(any(ProjectJPA.class));

        doNothing().when(projectBuilderMock).addCodeNameDescriptionCustomerStatus(
                any(ProjectCode.class), any(ProjectName.class),
                any(ProjectDescription.class), any(CustomerName.class),any(ProjectStatus.class));
        doNothing().when(projectBuilderMock)
                .addTypologyDescription(any(TypologyDescription.class));
        doNothing().when(projectBuilderMock).addTime(any(Time.class));
        doNothing().when(projectBuilderMock)
                .addSprintDuration(any(ProjectSprintDuration.class));
        doNothing().when(projectBuilderMock).addNumberOfPlannedSprints(
                any(ProjectNumberOfPlannedSprints.class));
        doNothing().when(projectBuilderMock)
                .addBudget(any(ProjectBudget.class));
        doNothing().when(projectBuilderMock)
                .addBusinessSector(any(ProjectBusinessSector.class));

        when(projectBuilderMock.getProject()).thenReturn(projectMock);

        //Act
        Project actual = projectAssemblerJPA.toDomain(projectJPA);

        //Assert
        assertEquals(projectMock, actual);

        verify(projectBuilderMock, times(1)).addCodeNameDescriptionCustomerStatus(
                any(ProjectCode.class), any(ProjectName.class),
                any(ProjectDescription.class), any(CustomerName.class),any(ProjectStatus.class));
        verify(projectBuilderMock, times(1)).addTypologyDescription(
                any(TypologyDescription.class));
        verify(projectBuilderMock, times(1)).addTime(any(Time.class));
        verify(projectBuilderMock, times(1)).addSprintDuration(
                any(ProjectSprintDuration.class));
        verify(projectBuilderMock, times(1)).addNumberOfPlannedSprints(
                any(ProjectNumberOfPlannedSprints.class));
        verify(projectBuilderMock, times(1)).addBudget(
                any(ProjectBudget.class));
        verify(projectBuilderMock, times(1)).addBusinessSector(
                any(ProjectBusinessSector.class));
    }

}
