package switchfive.project.ainfrastructure.persistence.assemblers.implAssemblers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import switchfive.project.ainfrastructure.persistence.data.ResourceJPA;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.factories.iFactories.IResourceFactory;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ImplResourceAssemblerJPATest {

    @Mock
    IResourceFactory iResourceFactory;

    @InjectMocks
    private ImplResourceAssemblerJPA implResourceAssemblerJPA;

    @Test
    void toDomainWithEmail() throws ParseException {
        //Arrange
        ResourceJPA resourceJPA = new ResourceJPA();

        resourceJPA.setResourceID("123e4567-e89b-12d3-a456-426614174000");
        ResourceID expectedResourceID = ResourceID.createResourceID("123e4567-e89b-12d3-a456-426614174000");

        resourceJPA.setEmail("benfica@tetra.pt");
        Email expectedEmail = Email.createEmail("benfica@tetra.pt");

        resourceJPA.setProjectCode("ISEP1");
        ProjectCode expectedCode = ProjectCode.createProjectCode("ISEP1");

        resourceJPA.setStartDate("27/08/2025");
        resourceJPA.setEndDate("27/05/2026");

        Time expectedDates = Time.create("27/08/2025", "27/05/2026");

        resourceJPA.setCostPerHour(50.0);
        ResourceCostPerHour expectedCostPerHour = ResourceCostPerHour.create(50.0);

        resourceJPA.setPercentageOfAllocation(50.0);
        ResourcePercentageOfAllocation expectedAllocation = ResourcePercentageOfAllocation.create(50.0);

        resourceJPA.setRole("ProductOwner");
        Role expectedRole = Role.valueOf("ProductOwner");

        Resource expected = new Resource(expectedResourceID, expectedEmail, expectedCode, expectedDates, expectedCostPerHour, expectedAllocation, expectedRole);

        when(iResourceFactory.createResourceWithEmail(expectedResourceID, expectedEmail, expectedCode, expectedDates, expectedCostPerHour, expectedAllocation, expectedRole)).thenReturn(expected);

        //Act

        Resource result = implResourceAssemblerJPA.toDomainWithEmail(resourceJPA);

        // Assert

        assertEquals(expected, result);
    }

    @Test
    void toDomain() throws ParseException {
        //Arrange
        ResourceJPA resourceJPA = new ResourceJPA();

        resourceJPA.setResourceID("123e4567-e89b-12d3-a456-426614174000");
        ResourceID expectedResourceID = ResourceID.createResourceID("123e4567-e89b-12d3-a456-426614174000");

        resourceJPA.setUserID("benfica@tetra.pt");
        Email expectedEmail = Email.createEmail("benfica@tetra.pt");

        resourceJPA.setProjectCode("ISEP1");
        ProjectCode expectedCode = ProjectCode.createProjectCode("ISEP1");

        resourceJPA.setStartDate("27/08/2025");
        resourceJPA.setEndDate("27/05/2026");

        Time expectedDates = Time.create("27/08/2025", "27/05/2026");

        resourceJPA.setCostPerHour(50.0);
        ResourceCostPerHour expectedCostPerHour = ResourceCostPerHour.create(50.0);

        resourceJPA.setPercentageOfAllocation(50.0);
        ResourcePercentageOfAllocation expectedAllocation = ResourcePercentageOfAllocation.create(50.0);

        resourceJPA.setRole("ProductOwner");
        Role expectedRole = Role.valueOf("ProductOwner");

        Resource expected = new Resource(expectedResourceID, expectedEmail, expectedCode, expectedDates, expectedCostPerHour, expectedAllocation, expectedRole);

        when(iResourceFactory.createResource(expectedResourceID, expectedEmail, expectedCode, expectedDates, expectedCostPerHour, expectedAllocation, expectedRole)).thenReturn(expected);

        //Act

        Resource result = implResourceAssemblerJPA.toDomain(resourceJPA);

        // Assert

        assertEquals(expected, result);
    }

    @Test
    void toData() throws ParseException {

        ResourceID expectedResourceID = ResourceID.createResourceID();
        Email expectedEmail = Email.createEmail("benfica@tetra.pt");
        ProjectCode expectedCode = ProjectCode.createProjectCode("ISEP1");
        Time expectedDates = Time.create("27/08/2025", "27/05/2026");
        ResourceCostPerHour expectedCostPerHour = ResourceCostPerHour.create(50.0);
        ResourcePercentageOfAllocation expectedAllocation = ResourcePercentageOfAllocation.create(50.0);
        Role expectedRole = Role.valueOf("ProductOwner");

        Resource resource = new Resource(expectedResourceID, expectedEmail, expectedCode, expectedDates, expectedCostPerHour, expectedAllocation, expectedRole);
        ResourceJPA result = implResourceAssemblerJPA.toData(resource);

        assertEquals(resource.getResourceID(), result.getResourceID());
        assertEquals("benfica@tetra.pt", result.getEmail());
        assertEquals("ISEP1", result.getProjectCode());
        assertEquals("27/08/2025", result.getStartDate());
        assertEquals("27/05/2026", result.getEndDate());
        assertEquals(50.0, result.getCostPerHour());
        assertEquals(50.0, result.getPercentageOfAllocation());
        assertEquals("ProductOwner", result.getRole());

    }
}