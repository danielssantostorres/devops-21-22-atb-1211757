package switchfive.project.ddomain.aggregates.resource;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ResourceTest {


    ResourceID resourceID = ResourceID.createResourceID("123e4567-e89b-12d3-a456-426614174000");
    Email userID = Email.createEmail("benfica@campeao.pt");
    ProjectCode code = ProjectCode.createProjectCode("ISEP1");
    Time dates = Time.create("26/04/2023", "27/04/2024");
    ResourceCostPerHour costPerHour = ResourceCostPerHour.create(50);
    ResourcePercentageOfAllocation allocation = ResourcePercentageOfAllocation.create(10);
    Role role = Role.ProjectManager;
    Resource newResource;

    ResourceTest() throws ParseException {
    }

    @BeforeEach
    void setUp() {
        newResource = new Resource(resourceID, userID, code, dates, costPerHour, allocation, role);
    }

    @Test
    void getAllocation() {
        //Arrange
        double allocation = 10;

        //Act
        boolean actual = Objects.equals(newResource.getAllocation(), allocation);

        //Assert
        Assertions.assertTrue(actual);
    }

    @Test
    void getCostPerHour() {
        //Arrange
        double costPerHour = 50;

        //Act
        boolean actual = Objects.equals(newResource.getCostPerHour(), costPerHour);

        //Assert
        Assertions.assertTrue(actual);
    }

    @Test
    void getUserID() {
        //Arrange
        Email userIDInput = Email.createEmail("benfica@campeao.pt");

        //Act
        boolean actual = Objects.equals(newResource.getUserID(), userIDInput);

        //Assert
        Assertions.assertTrue(actual);
    }

    @Test
    void getCode() {
        //Arrange
        String code = "ISEP1";

        //Act
        boolean actual = Objects.equals(newResource.projectCode(), code);

        //Assert
        Assertions.assertTrue(actual);
    }

    @Test
    void getRole() {
        //Arrange
        //Act
        boolean actual = Objects.equals(newResource.getRole(), role.toString());

        //Assert
        Assertions.assertTrue(actual);
    }

    @Test
    void thisResourceIsTheSameAsOther() {
        //Arrange
        Resource clone = newResource;

        //Act
        boolean actual = newResource.equals(clone);

        //Assert
        Assertions.assertTrue(actual);
    }

    @Test
    void thisResourceIsNotEqualsToOther() {
        //Arrange
        ResourceID resourceIDClone =
                ResourceID.createResourceID("123e4667-e89b-12d3-a456-426614174000");

        Resource clone = new Resource(resourceIDClone, userID, code, dates, costPerHour, allocation, role);

        //Act
        boolean actual = newResource.equals(clone);

        //Assert
        Assertions.assertFalse(actual);
    }

    @Test
    @DisplayName("Two compared instances are equals.")
    void comparedResourcesAreEquals() throws ParseException {
        //Arrange
        ResourceID resourceID = ResourceID.createResourceID();
        Email email = Email.createEmail("valid@gmail.com");
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(5);
        ResourcePercentageOfAllocation allocation =
                ResourcePercentageOfAllocation.create(50);
        Role role = Role.TeamMember;

        Resource newResource = new Resource(resourceID, email, projectCode,
                dates, costPerHour, allocation, role);

        Resource otherResource = new Resource(resourceID, email, projectCode,
                dates, costPerHour, allocation, role);

        //Act
        boolean actual = newResource.equals(otherResource);

        //Assert
        assertTrue(actual);
    }

    @Test
    @DisplayName("Two compared instances are the same.")
    void comparedResourcesAreTheSame() throws ParseException {
        //Arrange
        ResourceID resourceID = ResourceID.createResourceID();
        Email email = Email.createEmail("valid@gmail.com");
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(5);
        ResourcePercentageOfAllocation allocation =
                ResourcePercentageOfAllocation.create(50);
        Role role = Role.TeamMember;

        Resource newResource = new Resource(resourceID, email, projectCode,
                dates, costPerHour, allocation, role);

        Resource otherResource = newResource;

        //Act
        boolean actual = newResource.equals(otherResource);

        //Assert
        assertTrue(actual);
    }

    @Test
    @DisplayName("Two compared instances are not equals.")
    void comparedResourcesAreNotEquals() throws ParseException {
        //Arrange
        ResourceID resourceID = ResourceID.createResourceID();
        ResourceID resourceIdOther = ResourceID.createResourceID();
        Email email = Email.createEmail("valid@gmail.com");
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(5);
        ResourcePercentageOfAllocation allocation =
                ResourcePercentageOfAllocation.create(50);
        Role role = Role.TeamMember;

        Resource newResource = new Resource(resourceID, email, projectCode,
                dates, costPerHour, allocation, role);

        Resource otherResource =
                new Resource(resourceIdOther, email, projectCode,
                        dates, costPerHour, allocation, role);

        //Act
        boolean actual = newResource.equals(otherResource);

        //Assert
        assertFalse(actual);
    }

    @Test
    @DisplayName("Two compared objects are not instances of same class.")
    void comparedObjectsAreNotInstancesOfSameClass() throws ParseException {
        //Arrange
        ResourceID resourceID = ResourceID.createResourceID();
        Email email = Email.createEmail("valid@gmail.com");
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(5);
        ResourcePercentageOfAllocation allocation =
                ResourcePercentageOfAllocation.create(50);
        Role role = Role.TeamMember;

        Resource newResource = new Resource(resourceID, email, projectCode,
                dates, costPerHour, allocation, role);

        Object otherResource = new Object();

        //Act
        boolean actual = newResource.equals(otherResource);

        //Assert
        assertFalse(actual);
    }

    @Test
    @DisplayName("Other object is null. Expected false.")
    void otherObjectIsNull() throws ParseException {
        //Arrange
        ResourceID resourceID = ResourceID.createResourceID();
        Email email = Email.createEmail("valid@gmail.com");
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(5);
        ResourcePercentageOfAllocation allocation =
                ResourcePercentageOfAllocation.create(50);
        Role role = Role.TeamMember;

        Resource newResource = new Resource(resourceID, email, projectCode,
                dates, costPerHour, allocation, role);

        //Act
        boolean actual = newResource.equals(null);

        //Assert
        assertFalse(actual);
    }

    @Test
    @DisplayName("Objects are equals. Expected same hashcode.")
    void sameHashcode() throws ParseException {
        //Arrange
        ResourceID resourceID = ResourceID.createResourceID();
        Email email = Email.createEmail("valid@gmail.com");
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(5);
        ResourcePercentageOfAllocation allocation =
                ResourcePercentageOfAllocation.create(50);
        Role role = Role.TeamMember;

        Resource newResource = new Resource(resourceID, email, projectCode,
                dates, costPerHour, allocation, role);

        Resource otherResource = new Resource(resourceID, email, projectCode,
                dates, costPerHour, allocation, role);

        //Act
        boolean actual = newResource.hashCode() == otherResource.hashCode();

        //Assert
        assertTrue(actual);
    }

    @Test
    @DisplayName("Objects are not equals. Expected different hashcode.")
    void differentHashcode() throws ParseException {
        //Arrange
        ResourceID resourceID = ResourceID.createResourceID();
        ResourceID resourceIdOther = ResourceID.createResourceID();
        Email email = Email.createEmail("valid@gmail.com");
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(5);
        ResourcePercentageOfAllocation allocation =
                ResourcePercentageOfAllocation.create(50);
        Role role = Role.TeamMember;

        Resource newResource = new Resource(resourceID, email, projectCode,
                dates, costPerHour, allocation, role);

        Resource otherResource = new Resource(resourceIdOther, email, projectCode,
                dates, costPerHour, allocation, role);

        //Act
        boolean actual = newResource.hashCode() == otherResource.hashCode();

        //Assert
        assertFalse(actual);
    }

    @Test
    void compareUserAndSM() {

        // Act
        String userIDToCompare = "valid@gmail.com";
        boolean result = newResource.compareUserAndSM(userIDToCompare);

        // Assert
        assertFalse(result);
    }

    @Test
    void compareUserAndSMFailed() {

        // Act
        String userIDToCompare = "benfica@campeao.pt";
        boolean result = newResource.compareUserAndSM(userIDToCompare);

        // Assert
        assertFalse(result);
    }

    @Test
    void compareUserAndPO() {

        // Act
        String userIDToCompare = "valid@gmail.com";
        boolean result = newResource.compareUserAndPO(userIDToCompare);

        // Assert
        assertFalse(result);
    }

    @Test
    void compareUserAndPOFailed() {

        // Act
        String userIDToCompare = "benfica@campeao.pt";
        boolean result = newResource.compareUserAndPO(userIDToCompare);

        // Assert
        assertFalse(result);
    }

    @Test
    void compareUserAndTeamMember() {

        // Act
        String userIDToCompare = "valid@gmail.com";
        boolean result = newResource.compareUserAndTeamMember(userIDToCompare);
        // Assert
        assertFalse(result);

    }

    @Test
    void compareUserAndTeamMemberFailed() {

        // Act
        String userIDToCompare = "benfica@campeao.pt";
        boolean result = newResource.compareUserAndPO(userIDToCompare);

        // Assert
        assertFalse(result);
    }

    @Test
    void isProjectManagerTrue() {

        // Act
        boolean result = newResource.isProjectManager();

        // Assert
        assertTrue(result);
    }

    @Test
    void isProjectManagerFalse() throws ParseException {

        //Arrange
        ResourceID resourceID = ResourceID.createResourceID();
        Email email = Email.createEmail("valid@gmail.com");
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(5);
        ResourcePercentageOfAllocation allocation =
                ResourcePercentageOfAllocation.create(50);
        Role role = Role.TeamMember;

        Resource newResource1 = new Resource(resourceID, email, projectCode,
                dates, costPerHour, allocation, role);
        // Act
        boolean result = newResource1.isProjectManager();

        // Assert
        assertFalse(result);
    }

    @Test
    void getResourceId() {
        //Arrange
        ResourceID resourceID1 = ResourceID.createResourceID("123e4567-e89b-12d3-a456-426614174000");

        //Act
        boolean actual = Objects.equals(newResource.getResourceID(), resourceID1.toString());

        //Assert
        Assertions.assertTrue(actual);
    }
    
}
