package switchfive.project.ddomain.domainServices.implDomainServices;

import org.junit.jupiter.api.Test;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.sprint.Sprint;
import switchfive.project.ddomain.aggregates.userStory.UserStory;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserStoryDomainServiceTest {

    private ImplUserStoryDomainService userStoryDomainService = new ImplUserStoryDomainService();

    @Test
    void shouldReturnFalseWhenTheProjectStatusIsClosed() {
        Project projectMock = mock(Project.class);
        Sprint sprintMock = mock(Sprint.class);
        UserStory userStoryMock = mock(UserStory.class);

        when(projectMock.isProjectClosed()).thenReturn(true);
        when(sprintMock.isSprintStatusFinished()).thenReturn(false);
        when(userStoryMock.isUserStoryStatusFinished()).thenReturn(false);

        assertFalse(userStoryDomainService.
                canUSBeMovedFromProductBacklogToSprintBacklog(projectMock,
                sprintMock,
                userStoryMock));
    }

    @Test
    void shouldReturnFalseWhenTheSprintStatusIsFinished() {
        Project projectMock = mock(Project.class);
        Sprint sprintMock = mock(Sprint.class);
        UserStory userStoryMock = mock(UserStory.class);

        when(projectMock.isProjectClosed()).thenReturn(false);
        when(sprintMock.isSprintStatusFinished()).thenReturn(true);
        when(userStoryMock.isUserStoryStatusFinished()).thenReturn(false);

        assertFalse(userStoryDomainService.
                canUSBeMovedFromProductBacklogToSprintBacklog(projectMock,
                        sprintMock,
                        userStoryMock));
    }

    @Test
    void shouldReturnFalseWhenUserStoryStatusIsFinished() {
        Project projectMock = mock(Project.class);
        Sprint sprintMock = mock(Sprint.class);
        UserStory userStoryMock = mock(UserStory.class);

        when(projectMock.isProjectClosed()).thenReturn(false);
        when(sprintMock.isSprintStatusFinished()).thenReturn(false);
        when(userStoryMock.isUserStoryStatusFinished()).thenReturn(true);

        assertFalse(userStoryDomainService.
                canUSBeMovedFromProductBacklogToSprintBacklog(projectMock,
                        sprintMock,
                        userStoryMock));
    }

    @Test
    void userStoryMoveSuccessfully() {
        Project projectMock = mock(Project.class);
        Sprint sprintMock = mock(Sprint.class);
        UserStory userStoryMock = mock(UserStory.class);

        when(projectMock.isProjectClosed()).thenReturn(false);
        when(sprintMock.isSprintStatusFinished()).thenReturn(false);
        when(userStoryMock.isUserStoryStatusFinished()).thenReturn(false);

        assertTrue(userStoryDomainService.
                canUSBeMovedFromProductBacklogToSprintBacklog(projectMock,
                        sprintMock,
                        userStoryMock));
    }
}