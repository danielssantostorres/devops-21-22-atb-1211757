package switchfive.project.capplicationServices.assemblers.implAssemblers;

import org.junit.jupiter.api.Test;
import switchfive.project.binterfaceAdapters.dataTransferObjects.RequestDTO;
import switchfive.project.ddomain.aggregates.request.Request;
import switchfive.project.ddomain.shared.valueObjects.Email;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;
import switchfive.project.ddomain.shared.valueObjects.RequestID;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class RequestAssemblerTest {

    @Test
    void toDto() {
        // Arrange
        RequestID requestID = RequestID.createRequestID();
        Email userID = Email.createEmail("abilio@isep.ipp.pt");
        ProfileDescription profileID = ProfileDescription
                .createProfileDescription("Visitor");
        LocalDate creationDate = java.time.LocalDate.parse("2010-09-29");
        Request request = new Request(requestID, userID, profileID, creationDate,
                false);

        // Act
        RequestDTO result = RequestAssembler.toDto(request);

        // Assert
        assertEquals(request.getIdentity(), result.requestID);
        assertEquals("Visitor", result.profileDescription);
        assertEquals("2010-09-29", result.creationDate);
        assertEquals("abilio@isep.ipp.pt", result.userID);
        assertFalse(result.isApproved);
    }
}
