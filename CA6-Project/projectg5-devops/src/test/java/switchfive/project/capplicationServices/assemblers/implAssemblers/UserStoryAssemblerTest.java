package switchfive.project.capplicationServices.assemblers.implAssemblers;

import org.junit.jupiter.api.Test;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserStoryDTO;
import switchfive.project.ddomain.aggregates.userStory.UserStory;
import switchfive.project.ddomain.shared.valueObjects.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserStoryAssemblerTest {

    @Test
    void toUserStoryDTO() {
        //Arrange
        UserStory userStory = new UserStory(UserStoryID.createUserStoryID("P0101", "US1"),
                ProjectCode.createProjectCode("P0101"), UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1), UserStoryDescription.createUserStoryDescription("ABC"),
                SprintID.createSprintID("P0101", 1), EffortEstimate.createEffortEstimate(null),
                UserStoryStatus.PLANNED, null);

        UserStoryDTO expected = new UserStoryDTO();
        expected.setDescription("ABC");
        expected.setCode("US1");
        expected.setParentUserStoryCode(null);
        expected.setEffort(null);
        expected.setPriority(1);
        expected.setStatus("PLANNED");

        //Act
        UserStoryDTO userStoryDTO = UserStoryAssembler.toUserStoryDTO(userStory);

        //Assert
        assertEquals(expected, userStoryDTO);
    }

    @Test
    void toUserStoryDTOEmpty() {
        //Arrange
        UserStoryAssembler assembler = new UserStoryAssembler();

        UserStory userStory = new UserStory(UserStoryID.createUserStoryID("P0101", "US1"),
                ProjectCode.createProjectCode("P0101"), UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1), UserStoryDescription.createUserStoryDescription("ABC"));

        UserStoryDTO expected = new UserStoryDTO();
        expected.setDescription("ABC");
        expected.setCode("US1");
        expected.setEffort(null);
        expected.setPriority(1);

        //Act
        UserStoryDTO userStoryDTO = assembler.toUserStoryDTO(userStory);

        //Assert
        assertEquals(expected, userStoryDTO);
    }
}