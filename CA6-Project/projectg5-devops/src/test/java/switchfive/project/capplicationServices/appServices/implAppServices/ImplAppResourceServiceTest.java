package switchfive.project.capplicationServices.appServices.implAppServices;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceCreationDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceDTO;
import switchfive.project.capplicationServices.assemblers.implAssemblers.ResourceAssembler;
import switchfive.project.capplicationServices.iRepositories.IProjectRepository;
import switchfive.project.capplicationServices.iRepositories.IResourceRepository;
import switchfive.project.capplicationServices.iRepositories.IUserRepository;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.aggregates.user.User;
import switchfive.project.ddomain.domainServices.implDomainServices.ImplResourceDomainService;
import switchfive.project.ddomain.factories.iFactories.IResourceFactory;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ImplAppResourceServiceTest {

    @Mock
    IResourceFactory iResourceFactory;
    @Mock
    IUserRepository iUserRepository;
    @Mock
    IResourceRepository iResourceRepository;
    @Mock
    IProjectRepository iProjectRepository;
    @Mock
    ImplResourceDomainService implResourceDomainService;
    @InjectMocks
    ImplAppResourceService implAppResourceService;

    @Test
    void definedTeamMemberOfAProjectTrue() throws ParseException, NoSuchAlgorithmException {
        //Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("isep@ipp.pt", "TEAM2", startDate, endDate, 10, 100);
        Project project = mock(Project.class);
        ArrayList<Resource> resources = new ArrayList<>();
        Resource resource = mock(Resource.class);
        ResourceDTO resourceDTO = mock(ResourceDTO.class);
        User user = mock(User.class);

        when(iUserRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user));
        when(iResourceRepository.getResourcesByProjectCode(dto.projectCodeDto)).thenReturn(
                resources);
        when(iProjectRepository.findByCode(any(ProjectCode.class))).thenReturn(
                Optional.of(project));
        when(implResourceDomainService.validateNewTeamMemberResource(resources,
                project, dto)).thenReturn(true);
        when(iResourceFactory.createResource(any(), any(), any(), any(), any(),
                any(), any())).thenReturn(resource);
        doNothing().when(iResourceRepository).saveResource(resource);
        MockedStatic<ResourceAssembler> resourceAssembler = mockStatic(ResourceAssembler.class);
        when(ResourceAssembler.toDto(any())).thenReturn(resourceDTO);

        Optional<ResourceDTO> expected = Optional.of(resourceDTO);

        //Act

        Optional<ResourceDTO> result =
                implAppResourceService.definedTeamMemberOfAProject(dto);

        //Assert
        assertEquals(expected, result);
        resourceAssembler.close();
    }

    @Test
    void definedTeamMemberOfAProjectFailedBecauseNoProject()
            throws ParseException, NoSuchAlgorithmException {
        //Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("isep@ipp.pt", "TEAM2", startDate, endDate, 10, 100);
        Project project = mock(Project.class);

        when(iProjectRepository.findByCode(any(ProjectCode.class)))
                .thenReturn(Optional.of(project));

        Optional<ResourceDTO> expected = Optional.empty();

        //Act

        Optional<ResourceDTO> result =
                implAppResourceService.definedTeamMemberOfAProject(dto);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void definedTeamMemberOfAProjectFailedBecauseNoUser()
            throws ParseException, NoSuchAlgorithmException {


        //Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("isep@ipp.pt", "TEAM2", startDate, endDate, 10, 100);
        Project project = mock(Project.class);
        User user = mock(User.class);

        when(iProjectRepository.findByCode(any(ProjectCode.class)))
                .thenReturn(Optional.of(project));

        when(iUserRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user));
        Optional<ResourceDTO> expected = Optional.empty();

        //Act

        Optional<ResourceDTO> result =
                implAppResourceService.definedTeamMemberOfAProject(dto);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void definedProductOwnerOfAProjectTrue() throws ParseException, NoSuchAlgorithmException {
        //Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("isep@ipp.pt", "TEAM2", startDate, endDate, 10, 100);
        Project project = mock(Project.class);
        ArrayList<Resource> resources = new ArrayList<>();
        Resource resource = mock(Resource.class);
        ResourceDTO resourceDTO = mock(ResourceDTO.class);
        User user = mock(User.class);

        when(iUserRepository.findUserByEmail(anyString())).thenReturn(Optional.of(user));
        when(iResourceRepository.getResourcesByProjectCode(dto.projectCodeDto)).thenReturn(resources);
        when(iProjectRepository.findByCode(ProjectCode.createProjectCode(dto.projectCodeDto))).thenReturn(Optional.of(project));
        when(implResourceDomainService.validateNewProductOwnerResource(resources, project, dto)).thenReturn(true);
        when(iResourceFactory.createResource(any(), any(), any(), any(), any(), any(), any())).thenReturn(resource);
        doNothing().when(iResourceRepository).saveResource(resource);
        MockedStatic<ResourceAssembler> resourceAssembler = mockStatic(ResourceAssembler.class);
        when(ResourceAssembler.toDto(any())).thenReturn(resourceDTO);

        Optional<ResourceDTO> expected = Optional.of(resourceDTO);

        //Act

        Optional<ResourceDTO> result = implAppResourceService.definedProductOwnerOfAProject(dto);

        //Assert
        resourceAssembler.close();
        assertEquals(expected, result);
    }

    @Test
    void definedProductOwnerOfAProjectFailedBecauseNoProject() throws ParseException, NoSuchAlgorithmException {
        //Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("isep@ipp.pt", "TEAM2", startDate, endDate, 10, 100);
        Project project = mock(Project.class);

        when(iProjectRepository.findByCode(ProjectCode.createProjectCode(dto.projectCodeDto)))
                .thenReturn(Optional.of(project));

        Optional<ResourceDTO> expected = Optional.empty();

        //Act

        Optional<ResourceDTO> result = implAppResourceService.definedProductOwnerOfAProject(dto);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void definedProductOwnerOfAProjectFailedBecauseNoUser() throws ParseException, NoSuchAlgorithmException {


        //Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("isep@ipp.pt", "TEAM2", startDate, endDate, 10, 100);
        Project project = mock(Project.class);

        when(iProjectRepository.findByCode(ProjectCode.createProjectCode(dto.projectCodeDto)))
                .thenReturn(Optional.of(project));

        when(iUserRepository.findUserByEmail(anyString()))
                .thenReturn(Optional.empty());

        Optional<ResourceDTO> expected = Optional.empty();

        //Act

        Optional<ResourceDTO> result = implAppResourceService.definedProductOwnerOfAProject(dto);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getResourceDTO() throws ParseException {
        Resource resourceMock = mock(Resource.class);
        when(iResourceRepository.getResourceByID(any()))
                .thenReturn(Optional.of(resourceMock));

        ResourceDTO resourceDTO = mock(ResourceDTO.class);
        MockedStatic<ResourceAssembler> resourceAssembler = mockStatic(ResourceAssembler.class);
        when(ResourceAssembler.toDto(any())).thenReturn(resourceDTO);


        Optional<ResourceDTO> expected = Optional.of(resourceDTO);

        String resourceUUID = "123e4567-e89b-12d3-a456-426614174000";
        Optional<ResourceDTO> result =
                implAppResourceService.getResourceDTO(resourceUUID);

        assertEquals(expected, result);
        resourceAssembler.close();

    }

    @Test
    void getResourceDTOFails() throws ParseException {
        when(iResourceRepository.getResourceByID(any()))
                .thenReturn(Optional.empty());

        Optional<ResourceDTO> expected = Optional.empty();

        String resourceUUID = "123e4567-e89b-12d3-a456-426614174000";
        Optional<ResourceDTO> result =
                implAppResourceService.getResourceDTO(resourceUUID);

        assertEquals(expected, result);

    }

    @Test
    void getProjectManager() throws ParseException {
        Resource resourceMock = mock(Resource.class);
        when(iResourceRepository.getProjectManager(
                any(ProjectCode.class))).thenReturn(Optional.of(resourceMock));

        Optional<Resource> expected = Optional.of(resourceMock);

        Optional<Resource> result =
                implAppResourceService.getProjectManager(
                        ProjectCode.createProjectCode("ABCDE"));

        assertEquals(expected, result);

    }

    @Test
    void getProjectManagerFailed() throws ParseException {
        when(iResourceRepository.getProjectManager(
                any(ProjectCode.class))).thenReturn(Optional.empty());

        Optional<Resource> expected = Optional.empty();

        Optional<Resource> result =
                implAppResourceService.getProjectManager(
                        ProjectCode.createProjectCode("ABCDE"));

        assertEquals(expected, result);

    }

    @Test
    void findUser() throws NoSuchAlgorithmException {
        User userMock = mock(User.class);
        when(iUserRepository.findUserByEmail("isep@ipp.pt")).thenReturn(Optional.of(userMock));

        Optional<User> expected = Optional.of(userMock);

        Optional<User> result = implAppResourceService.findUser("isep@ipp.pt");

        assertEquals(expected, result);

    }

    @Test
    void findUserFailed() throws NoSuchAlgorithmException {
        when(iUserRepository.findUserByEmail("isep@ipp.pt")).thenReturn(Optional.empty());

        Optional<User> expected = Optional.empty();

        Optional<User> result = implAppResourceService.findUser("isep@ipp.pt");

        assertEquals(expected, result);

    }

    @Test
    void definedScrumMasterOfAProjectTrue() throws ParseException, NoSuchAlgorithmException {
        //Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("isep@ipp.pt", "TEAM2", startDate, endDate, 10, 100);
        Project project = mock(Project.class);
        ArrayList<Resource> resources = new ArrayList<>();
        Resource resource = mock(Resource.class);
        ResourceDTO resourceDTO = mock(ResourceDTO.class);
        User user = mock(User.class);

        when(iUserRepository.findUserByEmail(dto.userIdDto)).thenReturn(Optional.of(user));
        when(iResourceRepository.getResourcesByProjectCode(dto.projectCodeDto)).thenReturn(
                resources);
        when(iProjectRepository.findByCode(any(ProjectCode.class))).thenReturn(
                Optional.of(project));
        when(implResourceDomainService.validateNewScrumMasterResource(resources,
                project, dto)).thenReturn(true);
        when(iResourceFactory.createResource(any(), any(), any(), any(), any(),
                any(), any())).thenReturn(resource);
        doNothing().when(iResourceRepository).saveResource(resource);
        MockedStatic<ResourceAssembler> resourceAssembler = mockStatic(ResourceAssembler.class);
        when(ResourceAssembler.toDto(any())).thenReturn(resourceDTO);

        Optional<ResourceDTO> expected = Optional.of(resourceDTO);

        //Act

        Optional<ResourceDTO> result =
                implAppResourceService.definedScrumMasterOfAProject(dto);

        //Assert
        assertEquals(expected, result);
        resourceAssembler.close();
    }

    @Test
    void definedScrumMasterOfAProjectFailedBecauseNoProject()
            throws ParseException, NoSuchAlgorithmException {
        //Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("isep@ipp.pt", "TEAM2", startDate, endDate, 10, 100);
        Project project = mock(Project.class);

        when(iProjectRepository.findByCode(any(ProjectCode.class)))
                .thenReturn(Optional.of(project));

        Optional<ResourceDTO> expected = Optional.empty();

        //Act

        Optional<ResourceDTO> result =
                implAppResourceService.definedScrumMasterOfAProject(dto);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void definedScrumMasterOfAProjectFailedBecauseNoUser()
            throws ParseException, NoSuchAlgorithmException {


        //Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("isep@ipp.pt", "TEAM2", startDate, endDate, 10, 100);
        Project project = mock(Project.class);
        User user = mock(User.class);

        when(iProjectRepository.findByCode(any(ProjectCode.class)))
                .thenReturn(Optional.of(project));

        when(iUserRepository.findUserByEmail(dto.userIdDto)).thenReturn(Optional.of(user));
        Optional<ResourceDTO> expected = Optional.empty();

        //Act

        Optional<ResourceDTO> result =
                implAppResourceService.definedScrumMasterOfAProject(dto);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void createProjectManager() throws NoSuchAlgorithmException, ParseException {

        User userMock = mock(User.class);
        Email emailMock = mock(Email.class);
        ProjectCode codeMock = mock(ProjectCode.class);
        Time timeMock = mock(Time.class);
        ResourceCostPerHour costPerHourMock = mock(ResourceCostPerHour.class);
        ResourcePercentageOfAllocation allocation = mock(ResourcePercentageOfAllocation.class);
        Resource resourceMock = mock(Resource.class);


        when(iUserRepository.findUserByEmail(emailMock.getUserEmail())).thenReturn(Optional.of(userMock));
        when(iResourceFactory.createResource(any(), any(), any(), any(), any(),
                any(), any())).thenReturn(resourceMock);

        Optional<Resource> expected = Optional.of(resourceMock);

        Optional<Resource> result =
                implAppResourceService.createProjectManager(emailMock, codeMock, timeMock, costPerHourMock, allocation);

        assertEquals(expected, result);

    }

    @Test
    void createProjectManagerFailedNoUser() throws NoSuchAlgorithmException, ParseException {

        User userMock = mock(User.class);
        Email emailMock = mock(Email.class);
        ProjectCode codeMock = mock(ProjectCode.class);
        Time timeMock = mock(Time.class);
        ResourceCostPerHour costPerHourMock = mock(ResourceCostPerHour.class);
        ResourcePercentageOfAllocation allocation = mock(ResourcePercentageOfAllocation.class);

        when(iUserRepository.findUserByEmail(emailMock.getUserEmail())).thenReturn(Optional.of(userMock));

        Optional<Resource> expected = Optional.empty();

        Optional<Resource> result =
                implAppResourceService.createProjectManager(emailMock, codeMock, timeMock, costPerHourMock, allocation);

        assertEquals(expected, result);

    }

}


