package switchfive.project.capplicationServices.assemblers.implAssemblers;

import org.junit.jupiter.api.Test;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SearchUserDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserDTO;
import switchfive.project.ddomain.aggregates.user.User;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserAssemblerTest {

    @Test
    void userToDTO() throws NoSuchAlgorithmException {
        UserName userName = UserName.createUsername("Antonio");
        Email email = Email.createEmail("isep@ipp.pt");
        Password password = Password.createPassword("ant@ni0oO");
        Function function = Function.createFunction("function");
        Activation activation = Activation.createActivation();

        User user = new User(email,null,activation,password,userName,function);
        UserDTO expected = new UserDTO();
        expected.setUserName(userName.toString());
        expected.setUserFunction(function.toString());
        expected.setEmail(email.toString());
        expected.setActivation(activation.isActivated());

        //Act
        UserDTO actual = UserAssembler.toDTO(user);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void userToDTOWithPassword() throws NoSuchAlgorithmException {
        UserName userName = UserName.createUsername("Antonio");
        Email email = Email.createEmail("isep@ipp.pt");
        Password password = Password.createPassword("ant@ni0oO");
        Function function = Function.createFunction("function");
        Activation activation = Activation.createActivation();

        User user = new User(email,null,activation,password,userName,function);
        UserDTO expected = new UserDTO();
        expected.setUserName(userName.toString());
        expected.setUserFunction(function.toString());
        expected.setEmail(email.toString());
        expected.setPassword(password.toString());
        expected.setActivation(activation.isActivated());

        //Act
        UserDTO actual = UserAssembler.toDTOWithPassword(user);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void userToSearchUserDTO() throws NoSuchAlgorithmException {
        UserName userName = UserName.createUsername("Antonio");
        Email email = Email.createEmail("isep@ipp.pt");
        ProfileDescription profileDescription = ProfileDescription.createProfileDescription("Visitor");
        Password password = Password.createPassword("ant@ni0oO");
        Function function = Function.createFunction("function");
        Activation activation = Activation.createActivation();
        List<ProfileDescription> profileList = new ArrayList<>();
        profileList.add(profileDescription);
        List<String> profileStringList = new ArrayList<>();
        profileStringList.add("Visitor");

        User user = new User(email,profileList,activation,password,userName,function);

        SearchUserDTO expected  = SearchUserDTO.createSearchUserDTO(user.getEmail(),user.getProfileListAsDescription(),
                        user.getAccountStatus(),user.getUserName().getUserName(),user.getFunctionDescription());



        //Act
        SearchUserDTO actual = UserAssembler.toSearchUserDTO(user);

        //Assert
        assertEquals(expected, actual);
    }


    @Test
    void userToSearchUserDTOList() throws NoSuchAlgorithmException {
        List<SearchUserDTO> expected = new ArrayList<>();

        UserName userName = UserName.createUsername("Antonio");
        Email email = Email.createEmail("isep@ipp.pt");
        ProfileDescription profileDescription = ProfileDescription.createProfileDescription("Visitor");
        Password password = Password.createPassword("ant@ni0oO");
        Function function = Function.createFunction("function");
        Activation activation = Activation.createActivation();
        List<ProfileDescription> profileList = new ArrayList<>();
        profileList.add(profileDescription);
        List<String> profileStringList = new ArrayList<>();
        profileStringList.add("Visitor");

        User user = new User(email,profileList,activation,password,userName,function);

        SearchUserDTO searchUserDTO  = SearchUserDTO.createSearchUserDTO(user.getEmail(),user.getProfileListAsDescription(),
                user.getAccountStatus(),user.getUserName().getUserName(),user.getFunctionDescription());

        List<User> userList = new ArrayList<>();
        userList.add(user);

        expected.add(searchUserDTO);


        //Act
        List<SearchUserDTO> actual = UserAssembler.toSearchDTOList(userList);

        //Assert
        assertEquals(expected, actual);
    }

}
