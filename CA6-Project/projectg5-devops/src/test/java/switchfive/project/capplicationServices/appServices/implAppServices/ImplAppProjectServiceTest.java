
package switchfive.project.capplicationServices.appServices.implAppServices;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProjectDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.StatusOfActivitiesDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UpdateProjectDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppResourceService;
import switchfive.project.capplicationServices.assemblers.iAssemblers.IProjectAssembler;
import switchfive.project.capplicationServices.iRepositories.*;
import switchfive.project.ddomain.aggregates.customer.Customer;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.aggregates.typology.Typology;
import switchfive.project.ddomain.aggregates.user.User;
import switchfive.project.ddomain.factories.iFactories.IProjectBuilder;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ImplAppProjectServiceTest {
    ProjectDTO dto;
    UpdateProjectDTO updateProjectDto;

    @Mock
    ProjectDTO dtoMock;
    @Mock
    Customer customerMock;
    @Mock
    Typology typologyMock;
    @Mock
    Project projectMock;
    @Mock
    Resource projectManagerMock;

    @Mock
    IProjectRepository iProjectRepository;
    @Mock
    IResourceRepository iResourceRepository;
    @Mock
    IUserRepository iUserRepository;
    @Mock
    ITypologyRepository iTypologyRepository;
    @Mock
    ICustomerRepository iCustomerRepository;
    @Mock
    IProjectBuilder iProjectBuilder;
    @Mock
    IAppResourceService iResourceService;
    @Mock
    IProjectAssembler projectAssembler;

    @InjectMocks
    ImplAppProjectService implAppProjectService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        dto = new ProjectDTO();
        dto.setProjectCode("OTHER");
        dto.setProjectName("firstProject");
        dto.setProjectDescription("some description");
        dto.setProjectBusinessSector("educational");
        dto.setProjectNumberOfPlannedSprints(10);
        dto.setProjectSprintDuration(2);
        dto.setProjectBudget(1000);
        dto.setStartDate("25/04/2023");
        dto.setEndDate("25/04/2025");
        dto.setTypologyDescription("123e4567-e89b-12d3-a456-556642440000");
        dto.setCustomerName("torres");
        dto.setUserEmail("daniel@isep.ipp.pt");
        dto.setCostPerHour(10);
        dto.setPercentageOfAllocation(100);

        updateProjectDto = new UpdateProjectDTO();
        updateProjectDto.setProjectName("otherProject");
        updateProjectDto.setProjectDescription("other description");
        updateProjectDto.setProjectBusinessSector("otherEducational");
        updateProjectDto.setProjectNumberOfPlannedSprints(1);
        updateProjectDto.setProjectSprintDuration(5);
        updateProjectDto.setProjectBudget(0);
        updateProjectDto.setStartDate("26/04/2023");
        updateProjectDto.setEndDate("26/04/2025");
        updateProjectDto.setTypologyDescription(
                "123e4567-e89b-12d3-a456-556642440000");
        updateProjectDto.setCustomerName("torres");
        updateProjectDto.setStatus("Planned");
    }

    @Test
    void getEmptyProjectBecauseProjectDoesNotExist() throws ParseException {
        //Arrange
        when(iProjectRepository.findByCode(any(ProjectCode.class))).thenReturn(
                Optional.empty());

        //Act
        Optional<ProjectDTO> actual =
                implAppProjectService.getProjectDTO("test1");

        // Assert
        assertEquals(Optional.empty(), actual);
    }

    @Test
    void projectIsSuccessfullyCreated()
            throws ParseException, NoSuchAlgorithmException {
       // Arrange
        when(iProjectRepository.findByCode(any(ProjectCode.class))).thenReturn(
                Optional.empty());

        User userMock = mock(User.class);
        when(iUserRepository.findUserByEmail(anyString()))
                .thenReturn(Optional.of(userMock));

        when(iCustomerRepository.existsName(
                any(CustomerName.class))).thenReturn(true);

        when(iCustomerRepository.findCustomerByName(
                any(CustomerName.class))).thenReturn(Optional.of(customerMock));

        when(iTypologyRepository.existsDescription(
                any(TypologyDescription.class))).thenReturn(true);

        when(iTypologyRepository.findTypology(
                any(TypologyDescription.class))).thenReturn(
                Optional.of(typologyMock));

        doNothing().when(iProjectBuilder)
                .addCodeNameDescriptionCustomerStatus(any(ProjectCode.class),
                        any(ProjectName.class), any(ProjectDescription.class),
                        any(CustomerName.class),any(ProjectStatus.class));

        doNothing().when(iProjectBuilder).addBudget(any(ProjectBudget.class));

        doNothing().when(iProjectBuilder)
                .addBusinessSector(any(ProjectBusinessSector.class));

        doNothing().when(iProjectBuilder).addNumberOfPlannedSprints(
                any(ProjectNumberOfPlannedSprints.class));

        doNothing().when(iProjectBuilder)
                .addSprintDuration(any(ProjectSprintDuration.class));

        doNothing().when(iProjectBuilder).addTime(any(Time.class));

        doNothing().when(iProjectBuilder)
                .addTypologyDescription(any(TypologyDescription.class));

        when(iProjectBuilder.getProject()).thenReturn(projectMock);

        doNothing().when(iProjectRepository).saveProject(any(Project.class));

        when(iResourceService.createProjectManager(any(Email.class),
                any(ProjectCode.class), any(Time.class),
                any(ResourceCostPerHour.class),
                any(ResourcePercentageOfAllocation.class))).thenReturn(
                Optional.of(projectManagerMock));

        doNothing().when(iResourceRepository).saveResource(any(Resource.class));

        when(projectAssembler.toDTOWithPM(any(Project.class), anyString(),
                anyDouble(),
                anyDouble())).thenReturn(dtoMock);

       // Act
        ProjectDTO actual =
                implAppProjectService.createAndSaveProject(dto);

        // Assert
        assertEquals(dtoMock, actual);
        verify(iProjectRepository, times(1)).saveProject(any(Project.class));
        verify(iResourceRepository, times(1)).saveResource(any(Resource.class));
    }

    @Test
    void createdProjectIsEqualToExpected()
            throws ParseException, NoSuchAlgorithmException {
        // Arrange
        when(iProjectRepository.findByCode(any(ProjectCode.class))).thenReturn(
                Optional.empty());

        User userMock = mock(User.class);
        when(iUserRepository.findUserByEmail(anyString()))
                .thenReturn(Optional.of(userMock));

        when(iCustomerRepository.existsName(
                any(CustomerName.class))).thenReturn(true);

        when(iCustomerRepository.findCustomerByName(
                any(CustomerName.class))).thenReturn(Optional.of(customerMock));

        when(iTypologyRepository.existsDescription(
                any(TypologyDescription.class))).thenReturn(true);

        when(iTypologyRepository.findTypology(
                any(TypologyDescription.class))).thenReturn(
                Optional.of(typologyMock));

        doNothing().when(iProjectBuilder)
                .addCodeNameDescriptionCustomerStatus(any(ProjectCode.class),
                        any(ProjectName.class), any(ProjectDescription.class),
                        any(CustomerName.class),any(ProjectStatus.class));

        doNothing().when(iProjectBuilder).addBudget(any(ProjectBudget.class));

        doNothing().when(iProjectBuilder)
                .addBusinessSector(any(ProjectBusinessSector.class));

        doNothing().when(iProjectBuilder)
                .addNumberOfPlannedSprints(
                        any(ProjectNumberOfPlannedSprints.class));

        doNothing().when(iProjectBuilder)
                .addSprintDuration(any(ProjectSprintDuration.class));

        doNothing().when(iProjectBuilder)
                .addTime(any(Time.class));

        doNothing().when(iProjectBuilder)
                .addTypologyDescription(any(TypologyDescription.class));

        doNothing().when(iProjectBuilder)
                .addSprintDuration(any(ProjectSprintDuration.class));

        when(iProjectBuilder.getProject()).thenReturn(projectMock);

        when(iResourceService.createProjectManager(any(Email.class),
                any(ProjectCode.class), any(Time.class),
                any(ResourceCostPerHour.class),
                any(ResourcePercentageOfAllocation.class))).thenReturn(
                Optional.of(projectManagerMock));

        when(projectAssembler.toDTOWithPM(any(Project.class), anyString(),
                anyDouble(),
                anyDouble())).thenReturn(dtoMock);

       //  Act
        implAppProjectService.createAndSaveProject(dto);

        // Assert
        verify(iProjectBuilder, times(1)).addCodeNameDescriptionCustomerStatus(
                any(ProjectCode.class),
                any(ProjectName.class), any(ProjectDescription.class),
                any(CustomerName.class),any(ProjectStatus.class));
        verify(iProjectBuilder, times(1)).addBudget(any(ProjectBudget.class));
        verify(iProjectBuilder, times(1)).addBusinessSector(
                any(ProjectBusinessSector.class));
        verify(iProjectBuilder, times(1)).addNumberOfPlannedSprints(
                any(ProjectNumberOfPlannedSprints.class));
        verify(iProjectBuilder, times(1)).addSprintDuration(
                any(ProjectSprintDuration.class));
        verify(iProjectBuilder, times(1)).addTime(
                any(Time.class));
        verify(iProjectBuilder, times(1)).addTypologyDescription(
                any(TypologyDescription.class));
        verify(iProjectBuilder, times(1)).getProject();
        verify(iProjectRepository, times(1)).saveProject(any());
        verify(iResourceService, times((1))).createProjectManager(
                any(Email.class)
                , any(ProjectCode.class), any(Time.class),
                any(ResourceCostPerHour.class),
                any(ResourcePercentageOfAllocation.class));
    }


    @Test
    void projectCreationFailsBecauseProjectAlreadyExists()
            throws ParseException {
       // Arrange
        when(iProjectRepository.existsCode(any(ProjectCode.class))).thenReturn(
                true);

       // Act

       // Assert
        assertThrows(IllegalArgumentException.class,
                () -> {
                    implAppProjectService.createAndSaveProject(dto);
                });
    }

    @Test
    void projectCreationFailsBecauseCustomerDoesNotExists()
            throws ParseException {
       // Arrange
        when(iProjectRepository.existsCode(any(ProjectCode.class))).thenReturn(
                false);

        when(iCustomerRepository.existsName(
                any(CustomerName.class))).thenReturn(
                false);

        // Act

       // Assert
        assertThrows(IllegalArgumentException.class,
                () -> {
                    implAppProjectService.createAndSaveProject(dto);
                });
    }

    @Test
    void projectCreationFailsBecauseTypologyDoesNotExists()
            throws ParseException {
       // Arrange
        when(iProjectRepository.existsCode(any(ProjectCode.class))).thenReturn(
                false);

        when(iCustomerRepository.existsName(
                any(CustomerName.class))).thenReturn(
                true);

        when(iTypologyRepository.existsDescription(
                any(TypologyDescription.class))).thenReturn(false);

       // Act

       // Assert
        assertThrows(IllegalArgumentException.class,
                () -> {
                    implAppProjectService.createAndSaveProject(dto);
                });
    }

    @Test
    void projectCreationFailsBecauseUserDoesNotExists()
            throws ParseException, NoSuchAlgorithmException {
      // Arrange
        when(iProjectRepository.existsCode(any(ProjectCode.class))).thenReturn(
                false);

        when(iCustomerRepository.existsName(
                any(CustomerName.class))).thenReturn(
                true);

        when(iTypologyRepository.existsDescription(
                any(TypologyDescription.class))).thenReturn(true);

        when(iUserRepository.findUserByEmail(
                anyString())).thenReturn(Optional.empty());

       // Act

       // Assert
        assertThrows(IllegalArgumentException.class,
                () -> {
                    implAppProjectService.createAndSaveProject(dto);
                });
    }

    @Test
    void resourceCreationFails()
            throws ParseException, NoSuchAlgorithmException {
       // Arrange
        when(iProjectRepository.existsCode(any(ProjectCode.class))).thenReturn(
                false);

        when(iCustomerRepository.existsName(
                any(CustomerName.class))).thenReturn(
                true);

        when(iTypologyRepository.existsDescription(
                any(TypologyDescription.class))).thenReturn(true);

        when(iUserRepository.findUserByEmail(
                anyString())).thenReturn(Optional.of(mock(User.class)));

        when(iResourceService.createProjectManager(any(Email.class),
                any(ProjectCode.class), any(Time.class),
                any(ResourceCostPerHour.class),
                any(ResourcePercentageOfAllocation.class))).thenReturn(
                Optional.empty());

        // Act

       // Assert
        assertThrows(IllegalArgumentException.class,
                () -> {
                    implAppProjectService.createAndSaveProject(dto);
                });
    }

    @Test
    void updateProjectNotSuccessfulBecauseProjectDoesntExist()
            throws ParseException {
       // Arrange
        when(iProjectRepository.findByCode(any(ProjectCode.class))).thenReturn(
                Optional.empty());

       // Act
        Optional<ProjectDTO> actual =
                implAppProjectService.updateProject(
                        dto.getProjectCode(),
                        updateProjectDto);

       // Assert
        assertEquals(Optional.empty(), actual);

    }

    @Test
    void updateProjectNotSuccessfulBecauseTypologyDoesntExist()
            throws ParseException {
       // Arrange
        when(iProjectRepository.findByCode(any(ProjectCode.class))).thenReturn(
                Optional.of(mock(Project.class)));

        when(iCustomerRepository.existsName(
                any(CustomerName.class))).thenReturn(true);

        when(iTypologyRepository.existsDescription(
                any(TypologyDescription.class))).thenReturn(false);


       // Act

        // Assert
        assertThrows(IllegalArgumentException.class, () -> {
            implAppProjectService.updateProject(
                    dto.getProjectCode(),
                    updateProjectDto);
        });

    }

    @Test
    void updateProjectSuccess() throws ParseException {
       // Arrange
        Project project =
                new Project(ProjectCode.createProjectCode(dto.getProjectCode()),
                        ProjectName.create(dto.getProjectName()),
                        ProjectDescription.create(dto.getProjectDescription()),
                        CustomerName.create(dto.getCustomerName()));
        project.addBusinessSector(
                ProjectBusinessSector.create(dto.getProjectBusinessSector()));
        project.addDates(Time.create(dto.getStartDate(), dto.getEndDate()));
        project.addTypologyDescription(
                TypologyDescription.create(dto.getTypologyDescription()));
        project.addNumberOfPlannedSprints(ProjectNumberOfPlannedSprints.create(
                dto.getProjectNumberOfPlannedSprints()));
        project.addSprintDuration(
                ProjectSprintDuration.create(dto.getProjectSprintDuration()));
        project.addBudget(ProjectBudget.create(dto.getProjectBudget()));

        Project projectSpy = spy(project);

        when(iProjectRepository.findByCode(any(ProjectCode.class))).thenReturn(
                Optional.of(projectSpy));

        when(iCustomerRepository.existsName(
                any(CustomerName.class))).thenReturn(true);

        when(iCustomerRepository.findCustomerByName(
                any(CustomerName.class))).thenReturn(Optional.of(customerMock));

        when(iTypologyRepository.existsDescription(
                any(TypologyDescription.class))).thenReturn(true);

        when(iTypologyRepository.findTypology(
                any(TypologyDescription.class))).thenReturn(
                Optional.of(typologyMock));

        when(projectAssembler.toDTO(any(Project.class))).thenReturn(dto);

       // Act
        Optional<ProjectDTO> actual =
                implAppProjectService.updateProject(dto.getProjectCode(),
                        updateProjectDto);

       // Assert
        assertEquals(Optional.of(dto), actual);

        verify(iProjectRepository, times(1)).saveProject(any(Project.class));

        verify(projectSpy, times(1)).addName(any(ProjectName.class));
        verify(projectSpy, times(1)).addDescription(
                any(ProjectDescription.class));
        verify(projectSpy, times(1)).addBusinessSector(
                any(ProjectBusinessSector.class));
        verify(projectSpy, times(1)).addDates(any(Time.class));
        verify(projectSpy, times(1)).addCustomer(any(CustomerName.class));
        verify(projectSpy, times(1)).addTypologyDescription(
                any(TypologyDescription.class));
        verify(projectSpy, times(1)).addNumberOfPlannedSprints(
                any(ProjectNumberOfPlannedSprints.class));
        verify(projectSpy, times(1)).addSprintDuration(
                any(ProjectSprintDuration.class));
        verify(projectSpy, times(1)).addBudget(any(ProjectBudget.class));


    }

    @Test
    void getProjectDTO() throws ParseException {

        //Arrange
        Resource resourceMock = mock(Resource.class);

        Email emailMock = mock(Email.class);

        when(resourceMock.getUserID()).thenReturn(emailMock);
        when(emailMock.getUserEmail()).thenReturn("daniel@isep.ipp.pt");
        when(resourceMock.getCostPerHour()).thenReturn(0.0);
        when(resourceMock.getAllocation()).thenReturn(0.0);

        when(iProjectRepository.findByCode(any(ProjectCode.class))).thenReturn(
                Optional.of(projectMock));

        when(iResourceRepository.getProjectManager(
                any(ProjectCode.class))).thenReturn(
                Optional.of(resourceMock));

        when(projectAssembler.toDTOWithPM(any(Project.class),
                anyString(), anyDouble(), anyDouble())).thenReturn(dto);

        // Act
        Optional<ProjectDTO> actual =
                implAppProjectService.getProjectDTO("test1");

        // Assert
        assertEquals(dto.getCustomerName(), actual.get().getCustomerName());
    }

    @Test
    void getAllProjects() throws ParseException {

        //Arrange
        List<Project> projects = new ArrayList<>();
        when(iProjectRepository.findAll()).thenReturn(projects);

        List<ProjectDTO> projectsDTO = new ArrayList<>();

        assertEquals(projectsDTO,implAppProjectService.getAllProjects());

    }

    @Test
    void getStatusOfActivities_Succesfully() throws ParseException {

        //Arrange
        List<Project> projects = new ArrayList<>();
        when(iProjectRepository.findAll()).thenReturn(projects);

        List<ProjectDTO> projectsDTO = new ArrayList<>();

        assertEquals(projectsDTO,implAppProjectService.getAllProjects());

    }

    @Test
    void updateProjectNotSuccessfulBecauseCustomerDoesntExist()
            throws ParseException {
        // Arrange
        updateProjectDto.setCustomerName("ABCDE");

        when(iProjectRepository.findByCode(any(ProjectCode.class))).thenReturn(
                Optional.of(mock(Project.class)));

        when(iCustomerRepository.existsName(
                any(CustomerName.class))).thenReturn(true);

        when(iTypologyRepository.existsDescription(
                any(TypologyDescription.class))).thenReturn(true);

        CustomerName customer = CustomerName.create("ABCDE");
        when(iCustomerRepository.existsName(customer)).thenReturn(false);


        // Act

        // Assert
        assertThrows(IllegalArgumentException.class, () -> {
            implAppProjectService.updateProject(
                    dto.getProjectCode(),
                    updateProjectDto);
        });

    }

}

