package switchfive.project.capplicationServices.assemblers.implAssemblers;

import org.junit.jupiter.api.Test;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProfileDTO;
import switchfive.project.ddomain.aggregates.profile.Profile;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProfileAssemblerTest {

    @Test
    void toDTO() {
        // Arrange
        ProfileDescription profileDescription = ProfileDescription
                .createProfileDescription("Admin");
        Profile profile = new Profile(profileDescription);

        // Act
        ProfileDTO result = ProfileAssembler.toDTO(profile);

        // Assert
        assertEquals("Admin", result.designation);
    }
}
