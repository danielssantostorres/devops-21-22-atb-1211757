package switchfive.project.capplicationServices.appServices.implAppServices;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import switchfive.project.binterfaceAdapters.dataTransferObjects.TypologyDTO;
import switchfive.project.capplicationServices.assemblers.implAssemblers.TypologyAssembler;
import switchfive.project.capplicationServices.iRepositories.ITypologyRepository;
import switchfive.project.ddomain.aggregates.typology.Typology;
import switchfive.project.ddomain.factories.iFactories.ITypologyFactory;
import switchfive.project.ddomain.shared.valueObjects.TypologyDescription;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ImplAppTypologyServiceTest {


    @Mock
     ITypologyRepository iTypologyRepository;

    @Mock
    ITypologyFactory iTypologyFactory;

    @InjectMocks
    ImplAppTypologyService implAppTypologyService;


/*    @Test
    void newTypologyIsAdded() {
        //Arrange
        Typology typologyMock = mock(Typology.class);
        CreateTypologyDTO typologyDTOMock = mock(CreateTypologyDTO.class);

        when(iTypologyRepository.findTypology(any()).isEmpty();
        when(iTypologyFactory.createTypology(any())).thenReturn(typologyMock);

        MockedStatic typologyAssembler = mockStatic(TypologyAssembler.class);
        when(TypologyAssembler.toDTO(any())).thenReturn(typologyDTOMock);

        Optional<CreateTypologyDTO> expected = Optional.of(typologyDTOMock);

        // Act

        Optional<CreateTypologyDTO> result = Optional.ofNullable(implAppTypologyService.addNewTypology("FIXED COST"));

        // Assert

        assertEquals(expected,result);
        typologyAssembler.close();


    }*/

/*    @Test
    void newTypologyAlreadyExists() {
        //arrange
        ITypologyRepository typologyRepository = mock(ITypologyRepository.class);
        ImplTypologyFactory typologyFactory = mock(ImplTypologyFactory.class);
        TypologyDescription typologyDescription = TypologyDescription.create("Fixed Cost");
        Typology typology = mock(Typology.class);
        when(typologyRepository.findTypology(typologyDescription)).thenReturn(Optional.of(typology));

        ImplAppTypologyService typologyService = new ImplAppTypologyService(typologyRepository, typologyFactory);

        // Act
        assertFalse(typologyService.addNewTypology("Fixed Cost"));
    }*/

    @Test
    void getAllTypologies() {
        // Arrange
        List<Typology> typologyList = new ArrayList<>();
        TypologyDescription description = TypologyDescription.create("Valor teste");
        Typology typology = new Typology(description);
        typologyList.add(typology);
        when(iTypologyRepository.findAll()).thenReturn(typologyList);

        TypologyDTO dto = new TypologyDTO();
        List<TypologyDTO> expectedList = new ArrayList<>();
        expectedList.add(dto);


        // Assert
        Assertions.assertEquals(expectedList,implAppTypologyService.getAll());
     }
}
