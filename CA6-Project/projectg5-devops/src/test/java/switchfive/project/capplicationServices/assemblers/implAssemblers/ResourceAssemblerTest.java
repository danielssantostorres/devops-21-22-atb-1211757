package switchfive.project.capplicationServices.assemblers.implAssemblers;

import org.junit.jupiter.api.Test;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.TimeDTO;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ResourceAssemblerTest {

    @Test
    void toDto() throws ParseException {
        ResourceID resourceID = ResourceID.createResourceID();
        Email userID = Email.createEmail("benfica@campeao.pt");
        ProjectCode code = ProjectCode.createProjectCode("ISEP1");
        Time time = Time.create("27/05/2025", "27/04/2026");
        TimeDTO timeDTO = new TimeDTO("27/05/2025", "27/04/2026");
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(50);
        ResourcePercentageOfAllocation allocation = ResourcePercentageOfAllocation.create(50);
        Role role = Role.TeamMember;

        Resource resource = new Resource(resourceID, userID, code, time,costPerHour,allocation,role);

        ResourceDTO expected = new ResourceDTO(resourceID.toString(), userID.toString(), code.toString(),
                timeDTO, costPerHour.getCostPerHour(), allocation.getAllocation(), role.toString());

        ResourceDTO actual = ResourceAssembler.toDto(resource);

        assertEquals(expected, actual);


    }
}