package switchfive.project.capplicationServices.appServices.implAppServices;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import switchfive.project.binterfaceAdapters.dataTransferObjects.RefineUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserStoryDTO;
import switchfive.project.capplicationServices.assemblers.implAssemblers.UserStoryAssembler;
import switchfive.project.capplicationServices.iRepositories.IProjectRepository;
import switchfive.project.capplicationServices.iRepositories.IUserStoryRepository;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.userStory.UserStory;
import switchfive.project.ddomain.factories.iFactories.UserStoryFactory;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.UserStoryCode;
import switchfive.project.ddomain.shared.valueObjects.UserStoryDescription;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ImplAppUserStoryServiceTest {

    @Mock
    UserStoryFactory userStoryFactory;
    @Mock
    IUserStoryRepository iUserStoryStore;
    @Mock
    IProjectRepository iProjectStore;

    @InjectMocks
    @Spy
    ImplAppUserStoryService implAppCreateUserStoryService;


    @Test
    void createAndAddUserStoryTrue() throws ParseException {
        //Arrange
        UserStoryDTO userStoryDTOmock = mock(UserStoryDTO.class);
        Project projectMock = mock(Project.class);
        UserStory userStoryMock = mock(UserStory.class);

        Optional<Project> optionalProject = Optional.of(projectMock);

        when(iProjectStore.findByCode(any(ProjectCode.class))).thenReturn(optionalProject);
        when(iUserStoryStore.generatorCode(any(ProjectCode.class))).thenReturn("US1");
        when(iUserStoryStore.nextUserStoryNumber(any(ProjectCode.class))).thenReturn(1);
        when(userStoryFactory.createUserStory(any(), any(), any(), any(), any())).thenReturn(userStoryMock);

        MockedStatic userStoryAssemblerMockedStatic = mockStatic(UserStoryAssembler.class);
        when(UserStoryAssembler.toUserStoryDTO(userStoryMock)).thenReturn(userStoryDTOmock);

        //Act
        implAppCreateUserStoryService.
                createAndAddUserStory("P0101", "ABC");

        //Assert
        verify(iUserStoryStore, times(1)).save(userStoryMock);
        userStoryAssemblerMockedStatic.close();
    }

    @Test
    void createAndAddUserStoryFalse() throws ParseException {

        //Assert
        assertThrows(IllegalArgumentException.class,
                () -> {
                    implAppCreateUserStoryService.createAndAddUserStory("P0101", "ABC");
                });
    }

    @Test
    void getUserStoryListByPriority() throws ParseException {
        //Arrange
        UserStoryDTO userStoryDTOmock = mock(UserStoryDTO.class);
        Project projectMock = mock(Project.class);
        UserStory userStoryMock = mock(UserStory.class);
        List<UserStory> userStoryList = new ArrayList<>();
        userStoryList.add(userStoryMock);

        Optional<Project> optionalProject = Optional.of(projectMock);

        when(iProjectStore.findByCode(any(ProjectCode.class))).thenReturn(optionalProject);
        when(iUserStoryStore.getUserStoryListProductBacklog(any())).thenReturn(userStoryList);

        MockedStatic userStoryAssemblerMockedStatic = mockStatic(UserStoryAssembler.class);
        when(UserStoryAssembler.toUserStoryDTO(userStoryMock)).thenReturn(userStoryDTOmock);

        //Act

        List<UserStoryDTO> productBacklog = implAppCreateUserStoryService.
                getUserStoryListByPriority("P0101");

        //Assert
        assertEquals(1, productBacklog.size());
        userStoryAssemblerMockedStatic.close();
    }

    @Test
    void getUserStoryListByPriorityWithoutProject() throws ParseException {

        //Assert
        assertThrows(IllegalArgumentException.class,
                () -> {
                    implAppCreateUserStoryService.getUserStoryListByPriority("P0101");
                });
    }

    @Test
    void getUserStoryDTOtrue() throws ParseException {
        //Arrange
        Project projectMock = mock(Project.class);
        Optional<Project> optionalProject = Optional.of(projectMock);
        UserStory userStoryMock = mock(UserStory.class);
        Optional<UserStory> optionalUserStory = Optional.of(userStoryMock);
        UserStoryDTO userStoryDTOmock = mock(UserStoryDTO.class);
        MockedStatic userStoryAssemblerMockedStatic = mockStatic(UserStoryAssembler.class);

        when(iProjectStore.findByCode(any())).thenReturn(optionalProject);
        when(iUserStoryStore.getUserStory(any(), any())).thenReturn(optionalUserStory);
        when(UserStoryAssembler.toUserStoryDTO(userStoryMock)).thenReturn(userStoryDTOmock);

        //Act
        Optional<UserStoryDTO> userStoryDTO = implAppCreateUserStoryService.getUserStoryDTO("P0101", "US1");

        //Assert
        assertTrue(userStoryDTO.isPresent());
        userStoryAssemblerMockedStatic.close();
    }

    @Test
    void getUserStoryDTOWithoutProject() throws ParseException {

        //Assert
        assertThrows(IllegalArgumentException.class,
                () -> {
                    implAppCreateUserStoryService.getUserStoryDTO("P0101", "US1");
                });

    }

    @Test
    void refineUserStory() throws ParseException {
        // Arrange
        ArrayList<String> newDescriptions = new ArrayList<>();
        newDescriptions.add("First Description");
        newDescriptions.add("Second Description");

        RefineUserStoryDTO refineUserStoryDTO = new RefineUserStoryDTO();
        refineUserStoryDTO.setUserStoryCode("US1");
        refineUserStoryDTO.setNewUserStoryDescription(newDescriptions);
        refineUserStoryDTO.setProjectCode("PROJ1");

        Project projectMock = mock(Project.class);
        when(iProjectStore.findByCode(any()))
                .thenReturn(Optional.of(projectMock));
        when(projectMock.isProjectClosed()).thenReturn(false);

        UserStory parentUSmock = mock(UserStory.class);
        when(iUserStoryStore.getUserStory(any(), any()))
                .thenReturn(Optional.of(parentUSmock));
        when(parentUSmock.isUserStoryAvailableForRefinement()).thenReturn(true);


        UserStoryDTO userStoryDTO1 = new UserStoryDTO();
        UserStoryDTO userStoryDTO2 = new UserStoryDTO();

        ProjectCode projectCode = ProjectCode.createProjectCode("PROJ1");
        UserStoryCode usCode = UserStoryCode.createUserStoryCode("US1");
        UserStoryDescription firstUSDescription = UserStoryDescription
                .createUserStoryDescription("First Description");
        UserStoryDescription secondUSDescription = UserStoryDescription
                .createUserStoryDescription("Second Description");

        doReturn(userStoryDTO1).when(implAppCreateUserStoryService).createNewUSWithParentCode(projectCode, usCode
                , firstUSDescription);
        doReturn(userStoryDTO2).when(implAppCreateUserStoryService).createNewUSWithParentCode(projectCode, usCode
                , secondUSDescription);

        MockedStatic userStoryAssemblerMockStatic = mockStatic(UserStoryAssembler.class);
        UserStoryDTO parentUSDTO = new UserStoryDTO();
        when(UserStoryAssembler.toUserStoryDTO(parentUSmock)).thenReturn(parentUSDTO);


        ArrayList<UserStoryDTO> expected = new ArrayList<>();
        expected.add(userStoryDTO1);
        expected.add(userStoryDTO2);
        expected.add(parentUSDTO);

        // Act

        ArrayList<UserStoryDTO> result =
                implAppCreateUserStoryService.refineUserStory(refineUserStoryDTO);


        // Assert
        userStoryAssemblerMockStatic.close();
        assertEquals(expected, result);
        verify(parentUSmock).setStatusToRefined();
        verify(iUserStoryStore).save(parentUSmock);
    }

    @Test
    void refineUserStoryEmptyInputDescriptions() throws ParseException {
        // Arrange
        ArrayList<String> newDescriptions = new ArrayList<>();

        RefineUserStoryDTO refineUserStoryDTO = new RefineUserStoryDTO();
        refineUserStoryDTO.setUserStoryCode("US1");
        refineUserStoryDTO.setNewUserStoryDescription(newDescriptions);
        refineUserStoryDTO.setProjectCode("PROJ1");

        Project projectMock = mock(Project.class);
        when(iProjectStore.findByCode(any()))
                .thenReturn(Optional.of(projectMock));
        when(projectMock.isProjectClosed()).thenReturn(false);

        UserStory parentUSmock = mock(UserStory.class);
        when(iUserStoryStore.getUserStory(any(), any()))
                .thenReturn(Optional.of(parentUSmock));
        when(parentUSmock.isUserStoryAvailableForRefinement()).thenReturn(true);

        // Act

        assertThrows(IllegalArgumentException.class,
                () -> implAppCreateUserStoryService.refineUserStory(refineUserStoryDTO));

    }

    @Test
    void refineUserStoryUSNotAvailableForRefinement() throws ParseException {
        // Arrange
        ArrayList<String> newDescriptions = new ArrayList<>();

        RefineUserStoryDTO refineUserStoryDTO = new RefineUserStoryDTO();
        refineUserStoryDTO.setUserStoryCode("US1");
        refineUserStoryDTO.setNewUserStoryDescription(newDescriptions);
        refineUserStoryDTO.setProjectCode("PROJ1");

        Project projectMock = mock(Project.class);
        when(iProjectStore.findByCode(any()))
                .thenReturn(Optional.of(projectMock));
        when(projectMock.isProjectClosed()).thenReturn(false);

        UserStory parentUSmock = mock(UserStory.class);
        when(iUserStoryStore.getUserStory(any(), any()))
                .thenReturn(Optional.of(parentUSmock));
        when(parentUSmock.isUserStoryAvailableForRefinement()).thenReturn(false);

        // Act

        assertThrows(UnsupportedOperationException.class,
                () -> implAppCreateUserStoryService.refineUserStory(refineUserStoryDTO));

    }

    @Test
    void refineUserStoryParentUSNotFound() throws ParseException {
        // Arrange
        ArrayList<String> newDescriptions = new ArrayList<>();

        RefineUserStoryDTO refineUserStoryDTO = new RefineUserStoryDTO();
        refineUserStoryDTO.setUserStoryCode("US1");
        refineUserStoryDTO.setNewUserStoryDescription(newDescriptions);
        refineUserStoryDTO.setProjectCode("PROJ1");

        Project projectMock = mock(Project.class);
        when(iProjectStore.findByCode(any()))
                .thenReturn(Optional.of(projectMock));
        when(projectMock.isProjectClosed()).thenReturn(false);

        UserStory parentUSmock = mock(UserStory.class);
        when(iUserStoryStore.getUserStory(any(), any()))
                .thenReturn(Optional.empty());

        // Act

        assertThrows(IllegalArgumentException.class,
                () -> implAppCreateUserStoryService.refineUserStory(refineUserStoryDTO));

    }

    @Test
    void refineUserStoryProjectFinished() throws ParseException {
        // Arrange
        ArrayList<String> newDescriptions = new ArrayList<>();

        RefineUserStoryDTO refineUserStoryDTO = new RefineUserStoryDTO();
        refineUserStoryDTO.setUserStoryCode("US1");
        refineUserStoryDTO.setNewUserStoryDescription(newDescriptions);
        refineUserStoryDTO.setProjectCode("PROJ1");

        Project projectMock = mock(Project.class);
        when(iProjectStore.findByCode(any()))
                .thenReturn(Optional.of(projectMock));
        when(projectMock.isProjectClosed()).thenReturn(true);

        // Act

        assertThrows(UnsupportedOperationException.class,
                () -> implAppCreateUserStoryService.refineUserStory(refineUserStoryDTO));

    }

    @Test
    void refineUserStoryProjectNotFound() throws ParseException {
        // Arrange
        ArrayList<String> newDescriptions = new ArrayList<>();

        RefineUserStoryDTO refineUserStoryDTO = new RefineUserStoryDTO();
        refineUserStoryDTO.setUserStoryCode("US1");
        refineUserStoryDTO.setNewUserStoryDescription(newDescriptions);
        refineUserStoryDTO.setProjectCode("PROJ1");

        Project projectMock = mock(Project.class);
        when(iProjectStore.findByCode(any()))
                .thenReturn(Optional.empty());

        // Act

        assertThrows(IllegalArgumentException.class,
                () -> implAppCreateUserStoryService.refineUserStory(refineUserStoryDTO));

    }

    @Test
    void createNewUSWithParentCode() throws ParseException {
        // Arrange
        ProjectCode projectCode = ProjectCode.createProjectCode("PROJ1");
        UserStoryCode parentUserStoryCode = UserStoryCode.createUserStoryCode("US1");
        UserStoryDescription userStoryDescription = UserStoryDescription
                .createUserStoryDescription("Valid description");

        Project projectMock = mock(Project.class);
        when(iProjectStore.findByCode(any()))
                .thenReturn(Optional.of(projectMock));

        when(iUserStoryStore.generatorCode(projectCode)).thenReturn("US2");
        when(iUserStoryStore.nextUserStoryNumber(projectCode)).thenReturn(1);

        UserStoryDTO userStoryDTOexpected = new UserStoryDTO();
        MockedStatic userStoryAssemblerMockStatic = mockStatic(UserStoryAssembler.class);
        when(UserStoryAssembler.toUserStoryDTO(any()))
                .thenReturn(userStoryDTOexpected);


        // Act
        UserStoryDTO result =
                implAppCreateUserStoryService.createNewUSWithParentCode(
                        projectCode,parentUserStoryCode,userStoryDescription);

        // Assert
        userStoryAssemblerMockStatic.close();
        assertEquals(userStoryDTOexpected, result);
        verify(iUserStoryStore).save(any());
    }

    @Test
    void createNewUSWithParentCodeProjectNotFound() throws ParseException {
        // Arrange
        ProjectCode projectCode = ProjectCode.createProjectCode("PROJ1");
        UserStoryCode parentUserStoryCode = UserStoryCode.createUserStoryCode("US1");
        UserStoryDescription userStoryDescription = UserStoryDescription
                .createUserStoryDescription("Valid description");

        Project projectMock = mock(Project.class);
        when(iProjectStore.findByCode(any()))
                .thenReturn(Optional.empty());

        // Act
        assertThrows(IllegalArgumentException.class,
                () -> implAppCreateUserStoryService.createNewUSWithParentCode(projectCode,
                        parentUserStoryCode,userStoryDescription));
    }


}


