package switchfive.project.capplicationServices.appServices.implAppServices;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProfileDTO;
import switchfive.project.capplicationServices.assemblers.implAssemblers.ProfileAssembler;
import switchfive.project.capplicationServices.iRepositories.IProfileRepository;
import switchfive.project.ddomain.aggregates.profile.Profile;
import switchfive.project.ddomain.factories.iFactories.ProfileFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ImplAppProfileServiceTest {

    @Mock
    IProfileRepository iProfileRepository;

    @Mock
    ProfileFactory profileFactory;

    @InjectMocks
    ImplAppProfileService implAppProfileService;

    @Test
    void addNewProfileTrue() {
        // Arrange
        Profile profileMock = mock(Profile.class);
        ProfileDTO profileDTOMock = mock(ProfileDTO.class);

        when(iProfileRepository.getProfileByDescription(any()))
                .thenReturn(Optional.empty());

        when(profileFactory.createProfile(any())).thenReturn(profileMock);

        MockedStatic profileAssembler = mockStatic(ProfileAssembler.class);
        when(ProfileAssembler.toDTO(any())).thenReturn(profileDTOMock);

        Optional<ProfileDTO> expected = Optional.of(profileDTOMock);

        // Act
        Optional<ProfileDTO> result =
                implAppProfileService.addNewProfile("New Profile");

        // Assert
        assertEquals(expected, result);
        profileAssembler.close();

    }

    @Test
    void addNewProfileProfileAlreadyInStore() {
        // Arrange
        Profile profileMock = mock(Profile.class);

        when(iProfileRepository.getProfileByDescription(any()))
                .thenReturn(Optional.of(profileMock));

        Optional<ProfileDTO> expected = Optional.empty();

        // Act
        Optional<ProfileDTO> result =
                implAppProfileService.addNewProfile("New Profile");

        // Assert
        assertEquals(expected, result);

    }


    @Test
    void getProfileTrue() {
        // Arrange
        Profile profileToFind = mock(Profile.class);

        when(iProfileRepository.getProfileByDescription(any()))
                .thenReturn(Optional.of(profileToFind));

        ProfileDTO profileDTOexpected = mock(ProfileDTO.class);
        MockedStatic profileAssemblerMockStatic = mockStatic(ProfileAssembler.class);
        when(ProfileAssembler.toDTO(any())).thenReturn(profileDTOexpected);

        Optional<ProfileDTO> expected = Optional.of(profileDTOexpected);

        // Act
        String profileID = "Visitor";
        Optional<ProfileDTO> result = implAppProfileService
                .getProfile(profileID);

        // Assert
        assertEquals(expected, result);
        profileAssemblerMockStatic.close();
    }

    @Test
    void getProfileProfileNotInRepo() {
        // Arrange
        when(iProfileRepository.getProfileByDescription(any()))
                .thenReturn(Optional.empty());

        Optional<ProfileDTO> expected = Optional.empty();

        // Act
        String profileID = "Visitor";
        Optional<ProfileDTO> result = implAppProfileService
                .getProfile(profileID);

        // Assert
        assertEquals(expected, result);
    }


    @Test
    void getProfiles() {
        // Arrange
        List<Profile> profileList = new ArrayList<>();
        Profile profileToFind = mock(Profile.class);
        profileList.add(profileToFind);

        when(iProfileRepository.getProfiles())
                .thenReturn(profileList);

        ProfileDTO profileDTOexpected = mock(ProfileDTO.class);
        MockedStatic profifileAssemblerMockStatic = mockStatic(ProfileAssembler.class);
        when(ProfileAssembler.toDTO(any())).thenReturn(profileDTOexpected);

        List<ProfileDTO> expected = new ArrayList<>();
        expected.add(profileDTOexpected);

        // Act
        List<ProfileDTO> result = implAppProfileService
                .getProfiles();

        // Assert
        assertEquals(expected, result);
        profifileAssemblerMockStatic.close();
    }

    @Test
    void getProfilesNotInRepo() {
        // Arrange
        List<Profile> profileList = new ArrayList<>();

        when(iProfileRepository.getProfiles())
                .thenReturn(profileList);
        List<ProfileDTO> expected = new ArrayList<>();

        // Act
        List<ProfileDTO> result = implAppProfileService
                .getProfiles();

        // Assert
        assertEquals(expected, result);}
}
