package switchfive.project.binterfaceAdapters.implRepositories;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IProfileAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.ProfileJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.IProfileRepositoryJPA;
import switchfive.project.ddomain.aggregates.profile.Profile;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ImplProfileRepositoryTest {

    @Mock
    IProfileRepositoryJPA iProfileRepositoryJPA;

    @Mock
    IProfileAssemblerJPA iProfileAssemblerJPA;

    @InjectMocks
    ImplProfileRepository implProfileRepository;


    @Test
    void save() {
        // Arrange
        Profile profileMock = mock(Profile.class);
        ProfileJPA profileJPAMock = mock(ProfileJPA.class);

        // Act
        when(iProfileAssemblerJPA.toData(any())).thenReturn(profileJPAMock);
        when(iProfileRepositoryJPA.save(profileJPAMock)).thenReturn(profileJPAMock);
        when(iProfileAssemblerJPA.toDomain(profileJPAMock)).thenReturn(profileMock);

        Profile result = implProfileRepository.save(profileMock);

        // Assert
        assertEquals(profileMock, result);
    }

    @Test
    void getProfileByDescriptionProfileFound() {
        // Arrange
        Profile profileMock = mock(Profile.class);
        ProfileJPA profileJPAMock = mock(ProfileJPA.class);

        when(iProfileAssemblerJPA.toDomain(any())).thenReturn(profileMock);
        when(iProfileRepositoryJPA.findProfileJPAByProfileDescription(anyString()))
                .thenReturn(Optional.of(profileJPAMock));

        // Act
        ProfileDescription profileDescription = ProfileDescription
                .createProfileDescription("Valid description");
        Optional<Profile> actual =
                implProfileRepository.getProfileByDescription(profileDescription);


        // Assert
        assertEquals(Optional.of(profileMock), actual);

    }

    @Test
    void getProfileByDescriptionProfileNotFound() {
        // Arrange
        when(iProfileRepositoryJPA.findProfileJPAByProfileDescription(anyString()))
                .thenReturn(Optional.empty());

        // Act
        ProfileDescription profileDescription = ProfileDescription
                .createProfileDescription("Valid description");
        Optional<Profile> actual =
                implProfileRepository.getProfileByDescription(profileDescription);

        // Assert
        assertEquals(Optional.empty(), actual);

    }

    @Test
    void getProfiles() {
        // Arrange
        Profile profileMock = mock(Profile.class);
        ProfileJPA profileJPAMock = mock(ProfileJPA.class);
        List<ProfileJPA> profileJPAList = new ArrayList<>();
        profileJPAList.add(profileJPAMock);

        when(iProfileAssemblerJPA.toDomain(profileJPAMock)).thenReturn(profileMock);
        when(iProfileRepositoryJPA.findAll())
                .thenReturn(profileJPAList);


        List<Profile> expected = new ArrayList<>();
        expected.add(profileMock);

        // Act
        List<Profile> actual =
                implProfileRepository.getProfiles();


        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void getProfilesNotInJPARepo() {
        // Arrange
        List<ProfileJPA> profileJPAList = new ArrayList<>();
        when(iProfileRepositoryJPA.findAll())
                .thenReturn(profileJPAList);

        List<Profile> expected = new ArrayList<>();

        // Act
        List<Profile> actual =
                implProfileRepository.getProfiles();


        // Assert
        assertEquals(expected, actual);
    }
}
