package switchfive.project.binterfaceAdapters.dataTransferObjects;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProfileCreationDTOTest {



    @Test
    void sameProfile() {

        ProfileCreationDTO dtoOne = new ProfileCreationDTO("Profile");
        ProfileCreationDTO dtoTwo = new ProfileCreationDTO("Profile");

        assertEquals(dtoOne,dtoTwo);
        assertEquals(dtoOne.hashCode(),dtoTwo.hashCode());
    }

}