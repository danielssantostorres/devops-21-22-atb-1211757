package switchfive.project.binterfaceAdapters.implRepositories;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IResourceAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.ResourceJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.IResourceRepositoryJPA;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.ResourceID;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class ImplResourceRepositoryTest {

    @Mock
    IResourceRepositoryJPA iResourceRepositoryJPA;

    @Mock
    IResourceAssemblerJPA iResourceAssemblerJPA;

    @InjectMocks
    ImplResourceRepository implResourceRepository;


    @Test
    void saveResource() {
        //Arrange
        Resource resourceMock = mock(Resource.class);
        ResourceJPA resourceJPAMock = mock(ResourceJPA.class);

        IResourceRepositoryJPA resourceRepositoryJpaMock =
                mock(IResourceRepositoryJPA.class);
        IResourceAssemblerJPA resourceAssemblerJPAMock =
                mock(IResourceAssemblerJPA.class);

        ImplResourceRepository resourceRepository =
                new ImplResourceRepository(resourceRepositoryJpaMock,
                        resourceAssemblerJPAMock);

        when(resourceAssemblerJPAMock.toData(
                any(Resource.class))).thenReturn(resourceJPAMock);

        when(resourceRepositoryJpaMock.save(any(ResourceJPA.class))).thenReturn(
                resourceJPAMock);

        //Act
        resourceRepository.saveResource(resourceMock);

        //Assert
        verify(resourceAssemblerJPAMock, times(1)).toData(
                any(Resource.class));
        verify(resourceRepositoryJpaMock, times(1)).save(
                any(ResourceJPA.class));
    }

    @Test
    void getProjectManager() throws ParseException {
        // Arrange
        Resource resourceMock = mock(Resource.class);
        ResourceJPA resourceJPAMock = mock(ResourceJPA.class);

        when(iResourceAssemblerJPA.toDomainWithEmail(any())).thenReturn(resourceMock);
        when(iResourceRepositoryJPA.getResourceJPAByProjectCodeAndRole(anyString(), anyString()))
                .thenReturn(Optional.of(resourceJPAMock));

        // Act
        ProjectCode code = ProjectCode
                .createProjectCode("ISEP1");
        Optional<Resource> actual =
                implResourceRepository.getProjectManager(code);


        // Assert
        assertEquals(Optional.of(resourceMock), actual);

    }

    @Test
    void getProjectManagerPMNotFound() throws ParseException {
        // Arrange


        when(iResourceRepositoryJPA.getResourceJPAByProjectCodeAndRole(anyString(), anyString()))
                .thenReturn(Optional.empty());

        // Act
        ProjectCode code = ProjectCode
                .createProjectCode("ISEP1");
        Optional<Resource> actual =
                implResourceRepository.getProjectManager(code);


        // Assert
        assertEquals(Optional.empty(), actual);

    }

    @Test
    void getResourceByID() throws ParseException {
        // Arrange
        Resource resourceMock = mock(Resource.class);
        ResourceJPA resourceJPAMock = mock(ResourceJPA.class);

        when(iResourceAssemblerJPA.toDomain(any())).thenReturn(resourceMock);
        when(iResourceRepositoryJPA.findById((anyString()))).thenReturn(Optional.of(resourceJPAMock));

        // Act
        ResourceID resourceID = ResourceID.createResourceID("123e4567-e89b-12d3-a456-426614174000");
        Optional<Resource> actual =
                implResourceRepository.getResourceByID(resourceID);


        // Assert
        assertEquals(Optional.of(resourceMock), actual);

    }

    @Test
    void getResourcesByEmail() throws ParseException {
        //Arrange
        ResourceJPA resourceJPAmock = mock(ResourceJPA.class);
        Resource resourceMock = mock(Resource.class);
        List<ResourceJPA> resourceJPAList = new ArrayList<>();
        ArrayList<Resource> resourcesListMock = mock(ArrayList.class);
        resourcesListMock.add(resourceMock);
        resourceJPAList.add(resourceJPAmock);

        when(iResourceRepositoryJPA.getResourcesJPAByEmail(any())).thenReturn(resourceJPAList);
        when(iResourceAssemblerJPA.toDomain(resourceJPAmock)).thenReturn(resourceMock);

        //Act
        List<Resource> expected = implResourceRepository.getResourcesByEmail(any());

        //Assert

        assertEquals(expected.size(), 1);
    }
}
