/*
package switchfive.project.b_interfaceAdapters.implRepositories;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import switchfive.project.a_infrastructure.persistence.assemblers.iAssemblers.IRequestAssemblerJPA;
import switchfive.project.a_infrastructure.persistence.data.RequestJPA;
import switchfive.project.a_infrastructure.persistence.iRepositories.IRequestRepositoryJPA;
import switchfive.project.d_domain.aggregates.request.Request;
import switchfive.project.d_domain.shared.valueObjects.ProfileDescription;
import switchfive.project.d_domain.shared.valueObjects.RequestID;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ImplRequestRepositoryTest {

    @Mock
    IRequestRepositoryJPA iRequestRepositoryJPA;

    @Mock
    IRequestAssemblerJPA iRequestAssemblerJPA;

    @InjectMocks
    private ImplRequestRepository implRequestRepository;

    @Test
    void save() {
        // Arrange
        Request requestMock = mock(Request.class);
        RequestJPA requestJPAMock = mock(RequestJPA.class);

        when(iRequestAssemblerJPA.toData(requestMock)).thenReturn(requestJPAMock);
        when(iRequestRepositoryJPA.save(requestJPAMock)).thenReturn(requestJPAMock);
        when(iRequestAssemblerJPA.toDomain(requestJPAMock)).thenReturn(requestMock);

        // Act
        Request actual = implRequestRepository.save(requestMock);

        // Assert
        assertEquals(requestMock, actual);
    }

    @Test
    void getRequestByUserIDAndProfileID() {
        // Arrange
        Request requestMock = mock(Request.class);
        RequestJPA requestJPAMock = mock(RequestJPA.class);
        UserID userID = UserID.createUserID(1);
        ProfileDescription profileID = ProfileDescription
                .createProfileDescription("Visitor");

        when(iRequestRepositoryJPA
                .findByUserIDAndProfileDescription(any(), any()))
                .thenReturn(Optional.of(requestJPAMock));
        when(iRequestAssemblerJPA.toDomain(requestJPAMock))
                .thenReturn(requestMock);

        // Act
        Optional<Request> actual = implRequestRepository
                .getRequestByUserIDAndProfileID(userID, profileID);

        // Assert
        assertEquals(Optional.of(requestMock), actual);
    }

    @Test
    void getRequestByUserIDAndProfileIDRequestNotInRepo() {
        // Arrange
        UserID userID = UserID.createUserID(1);
        ProfileDescription profileID = ProfileDescription
                .createProfileDescription("Visitor");

        when(iRequestRepositoryJPA
                .findByUserIDAndProfileDescription(any(), any()))
                .thenReturn(Optional.empty());

        // Act
        Optional<Request> actual = implRequestRepository
                .getRequestByUserIDAndProfileID(userID, profileID);

        // Assert
        assertEquals(Optional.empty(), actual);
    }

    @Test
    void getRequestByID() {
        // Arrange
        Request requestMock = mock(Request.class);
        RequestJPA requestJPAMock = mock(RequestJPA.class);
        RequestID requestID = RequestID.createRequestID();

        when(iRequestRepositoryJPA
                .findById(any()))
                .thenReturn(Optional.of(requestJPAMock));
        when(iRequestAssemblerJPA.toDomain(requestJPAMock))
                .thenReturn(requestMock);

        // Act
        Optional<Request> actual = implRequestRepository
                .getRequestByID(requestID);

        // Assert
        assertEquals(Optional.of(requestMock), actual);
    }

    @Test
    void getRequestByIDNotInRepo() {
        // Arrange
        RequestID requestID = RequestID.createRequestID();

        when(iRequestRepositoryJPA
                .findById(any()))
                .thenReturn(Optional.empty());
        // Act
        Optional<Request> actual = implRequestRepository
                .getRequestByID(requestID);

        // Assert
        assertEquals(Optional.empty(), actual);
    }
}*/
