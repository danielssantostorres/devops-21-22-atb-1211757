package switchfive.project.binterfaceAdapters.controllers.implControllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceCreationDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppResourceService;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@ExtendWith(MockitoExtension.class)
class ImplResourceControllerTest {
    @Mock
    IAppResourceService iAppResourceService;

    @InjectMocks
    ImplResourceController implResourceController;

    @Test
    void definedTeamMemberOfAProjectSucessfully() throws ParseException, NoSuchAlgorithmException {
        // Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("email@gmail.com", "TEAM2", startDate, endDate, 10, 100);
        ResourceDTO resourceDTO = mock(ResourceDTO.class);
        resourceDTO.resourceID = "resourceID";
        when(iAppResourceService.definedTeamMemberOfAProject(dto)).thenReturn(Optional.of(resourceDTO));

        Link expectedLink = linkTo(methodOn(ImplResourceController.class).getResource("resourceID"))
                .withSelfRel();

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedLink, HttpStatus.CREATED);

        // Act
        ResponseEntity<Object> result = implResourceController.definedTeamMemberOfAProject(dto);

        // Assert
        assertEquals(expected, result);
    }

    @Test
    void definedTeamMemberOfAProjectFail() throws ParseException, NoSuchAlgorithmException {
        // Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("email@gmail.com", "TEAM2", startDate, endDate, 10, 100);

        when(iAppResourceService.definedTeamMemberOfAProject(dto)).thenReturn(Optional.empty());

        ResponseEntity<Object> expected = new ResponseEntity<>("Resource creation failed.", HttpStatus.NOT_FOUND);

        // Act
        ResponseEntity<Object> result = implResourceController.definedTeamMemberOfAProject(dto);

        // Assert
        assertEquals(expected, result);
    }

    @Test
    void getResource() throws ParseException {
        //Arrange
        ResourceDTO resourceDTO = mock(ResourceDTO.class);
        resourceDTO.resourceID = "resourceID";

        when(iAppResourceService.getResourceDTO(resourceDTO.resourceID)).thenReturn(Optional.of(resourceDTO));

        ResponseEntity<Object> expected = new ResponseEntity<>(resourceDTO, HttpStatus.OK);

        //Act
        ResponseEntity<Object> actual = implResourceController.getResource(resourceDTO.resourceID);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getResourceFailed() throws ParseException {
        //Arrange

        ResourceDTO resourceDTO = mock(ResourceDTO.class);
        resourceDTO.resourceID = "resourceID";

        when(iAppResourceService.getResourceDTO(resourceDTO.resourceID)).thenReturn(Optional.empty());

        ResponseEntity<Object> expected = new ResponseEntity<>("Resource not found in Repository", HttpStatus.NOT_FOUND);

        //Act
        ResponseEntity<Object> actual = implResourceController.getResource(resourceDTO.resourceID);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void definedProductOwnerOfAProjectSucessfully() throws ParseException, NoSuchAlgorithmException {
        // Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("email@gmail.com", "TEAM2", startDate, endDate, 10, 100);
        ResourceDTO resourceDTO = mock(ResourceDTO.class);
        resourceDTO.resourceID = "resourceID";
        when(iAppResourceService.definedProductOwnerOfAProject(dto)).thenReturn(Optional.of(resourceDTO));

        Link expectedLink = linkTo(methodOn(ImplResourceController.class).getResource("resourceID"))
                .withSelfRel();

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedLink, HttpStatus.CREATED);

        // Act
        ResponseEntity<Object> result = implResourceController.definedProductOwnerOfAProject(dto);

        // Assert
        assertEquals(expected, result);
    }

    @Test
    void definedProductOwnerOfAProjectFail() throws ParseException, NoSuchAlgorithmException {
        // Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("email@gmail.com", "TEAM2", startDate, endDate, 10, 100);

        when(iAppResourceService.definedProductOwnerOfAProject(dto)).thenReturn(Optional.empty());

        ResponseEntity<Object> expected = new ResponseEntity<>("Resource creation failed.", HttpStatus.NOT_FOUND);

        // Act
        ResponseEntity<Object> result = implResourceController.definedProductOwnerOfAProject(dto);

        // Assert
        assertEquals(expected, result);
    }

    @Test
    void definedScrumMasterOfAProjectSucessfully() throws ParseException, NoSuchAlgorithmException {
        // Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("email@gmail.com", "TEAM2", startDate, endDate, 10, 100);
        ResourceDTO resourceDTO = mock(ResourceDTO.class);
        resourceDTO.resourceID = "resourceID";
        when(iAppResourceService.definedScrumMasterOfAProject(dto)).thenReturn(Optional.of(resourceDTO));

        Link expectedLink = linkTo(methodOn(ImplResourceController.class).getResource("resourceID"))
                .withSelfRel();

        ResponseEntity<Object> expected = new ResponseEntity<>(expectedLink, HttpStatus.CREATED);

        // Act
        ResponseEntity<Object> result = implResourceController.definedScrumMasterOfAProject(dto);

        // Assert
        assertEquals(expected, result);
    }

    @Test
    void definedScrumMasterOfAProjectFail() throws ParseException, NoSuchAlgorithmException {
        // Arrange

        String startDate = "27/05/2025";
        String endDate = "27/05/2030";
        ResourceCreationDTO dto = new ResourceCreationDTO("email@gmail.com", "TEAM2", startDate, endDate, 10, 100);

        when(iAppResourceService.definedScrumMasterOfAProject(dto)).thenReturn(Optional.empty());

        ResponseEntity<Object> expected = new ResponseEntity<>("Resource creation failed.", HttpStatus.NOT_FOUND);

        // Act
        ResponseEntity<Object> result = implResourceController.definedScrumMasterOfAProject(dto);

        // Assert
        assertEquals(expected, result);
    }
}
