package switchfive.project.binterfaceAdapters.dataTransferObjects;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AllocatedProjectsDTOTest {

    AllocatedProjectsDTO dtoOne = new AllocatedProjectsDTO();
    AllocatedProjectsDTO dtoTwo = new AllocatedProjectsDTO();

    @Test
    void allGettersAndSetters() {

        dtoOne.setProjectName("Teste");
        dtoTwo.setProjectName("Teste");


        dtoOne.setProjectCode("A1234");
        dtoTwo.setProjectCode("A1234");

        dtoOne.setRole("PM");
        dtoTwo.setRole("PM");

        assertEquals(dtoOne,dtoTwo);
        assertEquals(dtoOne.getProjectName(),dtoTwo.getProjectName());
        assertEquals(dtoOne.getRole(),dtoTwo.getRole());
        assertEquals(dtoOne.getProjectCode(),dtoTwo.getProjectCode());
    }
}