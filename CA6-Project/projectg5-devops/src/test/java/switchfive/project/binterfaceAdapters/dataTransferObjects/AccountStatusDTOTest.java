package switchfive.project.binterfaceAdapters.dataTransferObjects;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountStatusDTOTest {

    AccountStatusDTO dtoOne = new AccountStatusDTO("123@gmail.com",true);
    AccountStatusDTO dtoTwo = new AccountStatusDTO("123@gmail.com",true);

    @Test
    void testEquals() {

        // Arrange
        assertEquals(dtoOne,dtoTwo);
    }

    @Test
    void testHashCode() {

        // Arrange
        assertEquals(dtoOne.hashCode(),dtoTwo.hashCode());

    }
}