package switchfive.project.binterfaceAdapters.implRepositories;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import switchfive.project.ddomain.aggregates.userStory.UserStory;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

//Integration tests

@SpringBootTest
class UserStoryRepositoryTest {

    @Autowired
    UserStoryRepository userStoryRepository;

    /*@Test
    void findDefaultPriorityWithPreviousSavedUS() {
        //Arrange
        UserStory userStoryA = new UserStory(
                UserStoryID.createUserStoryID("P0101", "US1"),
                ProjectCode.createProjectCode("P0101"),
                UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.saveNewUserStory(userStoryA);

        UserStory userStoryB = new UserStory(
                UserStoryID.createUserStoryID("P0101", "US2"),
                ProjectCode.createProjectCode("P0101"),
                UserStoryCode.createUserStoryCode("US2"),
                Priority.createPriority(2),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.saveNewUserStory(userStoryB);

        UserStory userStoryC = new UserStory(
                UserStoryID.createUserStoryID("P0202", "US1"),
                ProjectCode.createProjectCode("P0202"),
                UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.saveNewUserStory(userStoryC);

        //Act
        int nextPriority= userStoryRepository.nextUserStoryNumber(ProjectCode.createProjectCode("P0101"));
        //Assert
        assertEquals(3, nextPriority);
    }*/

    @Test
    void findDefaultPriorityWithoutPreviousSavedUS() {

        //Act
        int nextPriority = userStoryRepository.nextUserStoryNumber(ProjectCode.createProjectCode("P0709"));
        //Assert
        assertEquals(1, nextPriority);
    }

    @Test
    void generatorCodeWithPreviousSavedUS() {
        //Arrange
        UserStory userStoryA = new UserStory(
                UserStoryID.createUserStoryID("P0506", "US1"),
                ProjectCode.createProjectCode("P0506"),
                UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryA);

        UserStory userStoryB = new UserStory(
                UserStoryID.createUserStoryID("P0506", "US2"),
                ProjectCode.createProjectCode("P0506"),
                UserStoryCode.createUserStoryCode("US2"),
                Priority.createPriority(2),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryB);

        UserStory userStoryC = new UserStory(
                UserStoryID.createUserStoryID("P0202", "US1"),
                ProjectCode.createProjectCode("P0202"),
                UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryC);

        //Act
        String nextUserStoryCode = userStoryRepository.generatorCode(
                ProjectCode.createProjectCode("P0506"));
        String expected = "US3";

        //Assert
        assertEquals(expected, nextUserStoryCode);
    }

    @Test
    void generatorCodeWithoutPreviousSavedUS() {
        //Arrange
        UserStory userStoryC = new UserStory(
                UserStoryID.createUserStoryID("P0202", "US1"),
                ProjectCode.createProjectCode("P0202"),
                UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryC);

        //Act
        String nextUserStoryCode = userStoryRepository.generatorCode(
                ProjectCode.createProjectCode("P0303"));
        String expected = "US1";

        //Assert
        assertEquals(expected, nextUserStoryCode);
    }

    @Test
    void getUserStoryListProductBacklogSuccessfully() {
        //Arrange
        UserStory userStoryA = new UserStory(
                UserStoryID.createUserStoryID("P0101", "US1"),
                ProjectCode.createProjectCode("P0101"),
                UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryA);

        UserStory userStoryB = new UserStory(
                UserStoryID.createUserStoryID("P0101", "US2"),
                ProjectCode.createProjectCode("P0101"),
                UserStoryCode.createUserStoryCode("US2"),
                Priority.createPriority(2),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryB);

        UserStory userStoryC = new UserStory(
                UserStoryID.createUserStoryID("P0101", "US3"),
                ProjectCode.createProjectCode("P0101"),
                UserStoryCode.createUserStoryCode("US3"),
                Priority.createPriority(3),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryC);

        UserStory userStoryD = new UserStory(
                UserStoryID.createUserStoryID("P0202", "US1"),
                ProjectCode.createProjectCode("P0202"),
                UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryD);

        //Act
        List<UserStory> userStoriesList = userStoryRepository.
                getUserStoryListProductBacklog(ProjectCode.createProjectCode("P0101"));

        List<UserStory> expectedList = new ArrayList<>();
        expectedList.add(userStoryA);
        expectedList.add(userStoryB);
        expectedList.add(userStoryC);

        //Assert
        assertEquals(expectedList, userStoriesList);
        assertEquals(expectedList.size(), userStoriesList.size());
    }

    @Test
    void getUserStorySuccessfully() {
        //Arrange
        UserStory userStoryA = new UserStory(
                UserStoryID.createUserStoryID("P0838", "US1"),
                ProjectCode.createProjectCode("P0838"),
                UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryA);

        UserStory userStoryB = new UserStory(
                UserStoryID.createUserStoryID("P0838", "US2"),
                ProjectCode.createProjectCode("P0838"),
                UserStoryCode.createUserStoryCode("US2"),
                Priority.createPriority(2),
                UserStoryDescription.createUserStoryDescription("HIJ"));
        userStoryRepository.save(userStoryB);

        //Act
        Optional<UserStory> selectedUserStory = userStoryRepository.
                getUserStory(ProjectCode.createProjectCode("P0838"),
                        UserStoryCode.createUserStoryCode("US2"));

        //Assert
        assertEquals(userStoryB, selectedUserStory.get());
        assertTrue(selectedUserStory.isPresent());
    }

    @Test
    void getUserStoryEmpty() {
        //Arrange
        UserStory userStoryA = new UserStory(
                UserStoryID.createUserStoryID("P0838", "US1"),
                ProjectCode.createProjectCode("P0838"),
                UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryA);

        UserStory userStoryB = new UserStory(
                UserStoryID.createUserStoryID("P0838", "US2"),
                ProjectCode.createProjectCode("P0838"),
                UserStoryCode.createUserStoryCode("US2"),
                Priority.createPriority(2),
                UserStoryDescription.createUserStoryDescription("HIJ"));
        userStoryRepository.save(userStoryB);

        //Act
        Optional<UserStory> selectedUserStory = userStoryRepository.
                getUserStory(ProjectCode.createProjectCode("P0838"),
                        UserStoryCode.createUserStoryCode("US3"));

        //Assert
        assertFalse(selectedUserStory.isPresent());
    }

    @Test
    void existsUserStory() {
        //Arrange
        UserStory userStoryA = new UserStory(
                UserStoryID.createUserStoryID("P0222", "US1"),
                ProjectCode.createProjectCode("P0222"),
                UserStoryCode.createUserStoryCode("US1"),
                Priority.createPriority(1),
                UserStoryDescription.createUserStoryDescription("ABC"));
        userStoryRepository.save(userStoryA);

        UserStory userStoryB = new UserStory(
                UserStoryID.createUserStoryID("P0222", "US2"),
                ProjectCode.createProjectCode("P0222"),
                UserStoryCode.createUserStoryCode("US2"),
                Priority.createPriority(2),
                UserStoryDescription.createUserStoryDescription("HIJ"));
        userStoryRepository.save(userStoryB);

        //Act
        boolean selectedUserStoryTrue = userStoryRepository.
                existsUserStory(ProjectCode.createProjectCode("P0222"),
                        UserStoryCode.createUserStoryCode("US2"));

        boolean selectedUserStoryFalse = userStoryRepository.
                existsUserStory(ProjectCode.createProjectCode("P0222"),
                        UserStoryCode.createUserStoryCode("US3"));

        //Assert
        assertTrue(selectedUserStoryTrue);
        assertFalse(selectedUserStoryFalse);
    }


}
