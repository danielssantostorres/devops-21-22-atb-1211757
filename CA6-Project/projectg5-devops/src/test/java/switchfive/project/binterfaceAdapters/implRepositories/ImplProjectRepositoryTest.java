package switchfive.project.binterfaceAdapters.implRepositories;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;
import java.util.Optional;

//Integration tests

@SpringBootTest
class ImplProjectRepositoryTest {

    @Autowired
    ImplProjectRepository projectRepository;

    @Test
    void getProjectWithOneResult() throws ParseException {
        //Arrange
        ProjectCode projectCode = ProjectCode.createProjectCode("DANI1");

        Project test = new Project(projectCode,
                ProjectName.create("MyFirstProject"),
                ProjectDescription.create("ToTestProjectCreation"),
                CustomerName.create("TestCustomer"));
        test.addBudget(ProjectBudget.create(1000000));
        test.addNumberOfPlannedSprints(
                ProjectNumberOfPlannedSprints.create(5));
        test.addTypologyDescription(TypologyDescription.create("myTypology"));
        test.addDates(Time.create("09/05/2023", "09/05/2024"));
        test.addBusinessSector(ProjectBusinessSector.create(
                "EducationalPurposes"));
        test.addSprintDuration(ProjectSprintDuration.create(5));

        //Act
        projectRepository.saveProject(test);
        Project actualProject = projectRepository.findByCode(projectCode).get();

        //Assert
        Assertions.assertEquals(test, actualProject);
    }

    @Test
    void getEmptyOptionalBecauseProjectDoesNotExist() throws ParseException {
        //Arrange
        ProjectCode projectCode = ProjectCode.createProjectCode("DANI1");
        ProjectCode codeThatNotExists = ProjectCode.createProjectCode(
                "00000");

        Project test = new Project(projectCode,
                ProjectName.create("MyFirstProject"),
                ProjectDescription.create("ToTestProjectCreation"),
                CustomerName.create("TestCustomer"));
        test.addBudget(ProjectBudget.create(1000000));
        test.addNumberOfPlannedSprints(
                ProjectNumberOfPlannedSprints.create(5));
        test.addTypologyDescription(TypologyDescription.create("myTypology"));
        test.addDates(Time.create("09/05/2023", "09/05/2024"));
        test.addBusinessSector(ProjectBusinessSector.create(
                "EducationalPurposes"));
        test.addSprintDuration(ProjectSprintDuration.create(5));

        //Act
        projectRepository.saveProject(test);
        Optional<Project> actualProject =
                projectRepository.findByCode(codeThatNotExists);

        //Assert
        Assertions.assertEquals(Optional.empty(), actualProject);
    }

    @Test
    void projectExists() throws ParseException {
        //Arrange
        ProjectCode projectCode = ProjectCode.createProjectCode("DANI1");

        Project test = new Project(projectCode,
                ProjectName.create("MyFirstProject"),
                ProjectDescription.create("ToTestProjectCreation"),
                CustomerName.create("TestCustomer"));
        test.addBudget(ProjectBudget.create(1000000));
        test.addNumberOfPlannedSprints(
                ProjectNumberOfPlannedSprints.create(5));
        test.addTypologyDescription(TypologyDescription.create("myTypology"));
        test.addDates(Time.create("09/05/2023", "09/05/2024"));
        test.addBusinessSector(ProjectBusinessSector.create(
                "EducationalPurposes"));
        test.addSprintDuration(ProjectSprintDuration.create(5));

        //Act
        projectRepository.saveProject(test);
        boolean existsCode = projectRepository.existsCode(projectCode);

        //Assert
        Assertions.assertTrue(existsCode);
    }

    @Test
    void projectDoesNotExists() throws ParseException {
        //Arrange
        ProjectCode projectCode = ProjectCode.createProjectCode("DANI1");
        ProjectCode codeThatNotExists = ProjectCode.createProjectCode(
                "00000");

        Project test = new Project(projectCode,
                ProjectName.create("MyFirstProject"),
                ProjectDescription.create("ToTestProjectCreation"),
                CustomerName.create("TestCustomer"));
        test.addBudget(ProjectBudget.create(1000000));
        test.addNumberOfPlannedSprints(
                ProjectNumberOfPlannedSprints.create(5));
        test.addTypologyDescription(TypologyDescription.create("myTypology"));
        test.addDates(Time.create("09/05/2023", "09/05/2024"));
        test.addBusinessSector(ProjectBusinessSector.create(
                "EducationalPurposes"));
        test.addSprintDuration(ProjectSprintDuration.create(5));

        //Act
        projectRepository.saveProject(test);

        boolean existsCode =
                projectRepository.existsCode(codeThatNotExists);

        //Assert
        Assertions.assertFalse(existsCode);
    }
}
