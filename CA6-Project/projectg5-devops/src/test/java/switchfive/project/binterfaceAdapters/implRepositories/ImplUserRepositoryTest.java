package switchfive.project.binterfaceAdapters.implRepositories;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IUserAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.UserJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.IUserRepositoryJPA;
import switchfive.project.ddomain.aggregates.user.User;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ImplUserRepositoryTest {

    @Spy
    IUserRepositoryJPA userStoreJPA;

    @Spy
    IUserAssemblerJPA userAssemblerJPA;

    @InjectMocks
    ImplUserRepository userRepository;

    @Test
    void updateShouldTryToAssembleTheUserIntoUserJPA() throws NoSuchAlgorithmException {
        User userMock = mock(User.class);
        UserJPA userJPAMock = mock(UserJPA.class);
        doReturn(userMock).when(userAssemblerJPA).toUser(any());
        when(userStoreJPA.save(any())).thenReturn(userJPAMock);

        userRepository.update(userMock);

        verify(userAssemblerJPA, times(1)).toJPA(userMock);
    }

    @Test
    void updateShouldTryToSaveTheUser() throws NoSuchAlgorithmException {
        User userMock = mock(User.class);
        UserJPA userJPAMock = mock(UserJPA.class);

        doReturn(userJPAMock).when(userAssemblerJPA).toJPA(any());
        doReturn(userMock).when(userAssemblerJPA).toUser(any());

        userRepository.update(userMock);

        verify(userStoreJPA, times(1)).save(any(UserJPA.class));
    }

    @Test
    void updateShouldTryToAssembleTheUserJPAIntoUser() throws NoSuchAlgorithmException {
        User userMock = mock(User.class);
        UserJPA userJPAMock = mock(UserJPA.class);
        doReturn(userJPAMock).when(userAssemblerJPA).toJPA(any());
        when(userStoreJPA.save(any())).thenReturn(userJPAMock);

        userRepository.update(userMock);

        verify(userAssemblerJPA, times(1)).toUser(userJPAMock);
    }

    @Test
    void shouldReturnAnOptionalOfTheUserUpdated() throws NoSuchAlgorithmException {
        User userMock = mock(User.class);
        UserJPA userJPAMock = mock(UserJPA.class);
        doReturn(userJPAMock).when(userAssemblerJPA).toJPA(any());
        when(userStoreJPA.save(any())).thenReturn(userJPAMock);
        doReturn(userMock).when(userAssemblerJPA).toUser(any());
        Optional<User> expected = Optional.of(userMock);

        Optional<User> actual = userRepository.update(userMock);

        assertEquals(expected, actual);
    }
}
