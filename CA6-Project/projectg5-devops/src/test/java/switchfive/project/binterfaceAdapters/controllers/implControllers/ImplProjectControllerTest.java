package switchfive.project.binterfaceAdapters.controllers.implControllers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProjectDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UpdateProjectDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppProjectService;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

class ImplProjectControllerTest {

    ProjectDTO dto;
    UpdateProjectDTO updateDto;
    ProjectDTO dtoMock;
    IAppProjectService projectServiceMock;
    ImplProjectController projectControllerMock;

    final String WRONG_INFORMATION = "Wrong information";
    final String PROJECT_DO_NOT_EXIST = "Project do not exist";

    @BeforeEach
    void setUp() {
        dtoMock = mock(ProjectDTO.class);
        projectServiceMock = mock(IAppProjectService.class);
        projectControllerMock = new ImplProjectController(projectServiceMock);

        MockitoAnnotations.openMocks(this);

        dto = new ProjectDTO();
        dto.setProjectCode("ISEP1");
        dto.setUserEmail("daniel@isep.ipp.pt");
        dto.setCostPerHour(10);
        dto.setPercentageOfAllocation(100);

        updateDto = new UpdateProjectDTO();
        dto.setProjectName("otherProject");
        dto.setProjectDescription("other description");
        dto.setProjectBusinessSector("otherEducational");
        dto.setProjectNumberOfPlannedSprints(1);
        dto.setProjectSprintDuration(20);
        dto.setProjectBudget(0);
        dto.setStartDate("26/04/2023");
        dto.setEndDate("26/04/2025");
        dto.setTypologyDescription("123e4567-e89b-12d3-a456-556642440000");
        dto.setCustomerName("torres");
    }

    @Test
    void projectIsSuccessfullyCreated()
            throws ParseException, NoSuchAlgorithmException {
        //Arrange
        when(projectServiceMock.createAndSaveProject(
                any(ProjectDTO.class))).thenReturn(dtoMock);

        when(dtoMock.add(any(Link.class))).thenReturn(dtoMock);

        ResponseEntity<Object> expected =
                new ResponseEntity<>(dtoMock, HttpStatus.CREATED);

        //Act
        ResponseEntity<Object> actual = projectControllerMock.createProject(dto);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void invalidProjectIsNotCreated()
            throws ParseException, NoSuchAlgorithmException {
        //Arrange
        final String WRONG_INFORMATION = "Wrong information";

        when(projectServiceMock.createAndSaveProject(
                any(ProjectDTO.class))).thenThrow(
                IllegalArgumentException.class);

        ResponseEntity<Object> expected =
                new ResponseEntity<>(WRONG_INFORMATION, HttpStatus.BAD_REQUEST);

        //Act
        ResponseEntity<Object> actual = projectControllerMock.createProject(dto);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getValidProject() throws ParseException, NoSuchAlgorithmException {
        //Arrange
        when(projectServiceMock.getProjectDTO(anyString())).thenReturn(
                Optional.of(dto));

        ResponseEntity<Object> expected =
                new ResponseEntity<>(dto, HttpStatus.OK);


        //Act
        ResponseEntity<Object> actual =
                projectControllerMock.getProject(dto.getProjectCode());

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getProjectThatDoNotExist() throws ParseException, NoSuchAlgorithmException {
        //Arrange
        when(projectServiceMock.getProjectDTO(anyString())).thenReturn(
                Optional.empty());

        ResponseEntity<Object> expected =
                new ResponseEntity<>(PROJECT_DO_NOT_EXIST, HttpStatus.OK);

        //Act
        ResponseEntity<Object> actual =
                projectControllerMock.getProject(dto.getProjectCode());

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void tryToGetInvalidProject() throws ParseException, NoSuchAlgorithmException {
        //Arrange
        when(projectServiceMock.getProjectDTO(anyString())).thenThrow(
                IllegalArgumentException.class);

        ResponseEntity<Object> expected =
                new ResponseEntity<>(WRONG_INFORMATION, HttpStatus.BAD_REQUEST);

        //Act
        ResponseEntity<Object> actual =
                projectControllerMock.getProject(dto.getProjectCode());

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void updateProjectFailsBecauseProjectDoesNotExist()
            throws ParseException, NoSuchAlgorithmException {
        //Arrange
        when(projectServiceMock.updateProject(anyString(),
                any(UpdateProjectDTO.class))).thenReturn(Optional.empty());

        ResponseEntity<Object> expected =
                new ResponseEntity<>(PROJECT_DO_NOT_EXIST,
                        HttpStatus.NOT_FOUND);

        //Act
        ResponseEntity<Object> actual =
                projectControllerMock.updateProject(dto.getProjectCode(),
                        updateDto);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void updateProjectSuccess() throws ParseException, NoSuchAlgorithmException {
        //Arrange
        when(projectServiceMock.updateProject(anyString(),
                any(UpdateProjectDTO.class))).thenReturn(Optional.of(dtoMock));

        when(dtoMock.add(any(Link.class))).thenReturn(dtoMock);

        ResponseEntity<Object> expected =
                new ResponseEntity<>(dtoMock, HttpStatus.OK);

        //Act
        ResponseEntity<Object> actual =
                projectControllerMock.updateProject(dto.getProjectCode(),
                        updateDto);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void updateProjectGenerateIllegalArgumentException()
            throws ParseException, NoSuchAlgorithmException {
        //Arrange
        when(projectServiceMock.updateProject(anyString(),
                any(UpdateProjectDTO.class))).thenThrow(
                new IllegalArgumentException());

        ResponseEntity<Object> expected =
                new ResponseEntity<>(WRONG_INFORMATION, HttpStatus.BAD_REQUEST);

        //Act
        ResponseEntity<Object> actual =
                projectControllerMock.updateProject(dto.getProjectCode(),
                        updateDto);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
}
