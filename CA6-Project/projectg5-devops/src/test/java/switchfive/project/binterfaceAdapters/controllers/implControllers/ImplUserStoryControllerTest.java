package switchfive.project.binterfaceAdapters.controllers.implControllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import switchfive.project.binterfaceAdapters.dataTransferObjects.CreateUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.CustomerDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProjectDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserDTO;
import switchfive.project.binterfaceAdapters.implRepositories.ImplProjectRepository;
import switchfive.project.capplicationServices.appServices.implAppServices.ImplAppCustomerService;
import switchfive.project.capplicationServices.appServices.implAppServices.ImplAppProjectService;
import switchfive.project.capplicationServices.appServices.implAppServices.ImplAppTypologyService;
import switchfive.project.capplicationServices.appServices.implAppServices.ImplAppUserService;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.shared.valueObjects.CustomerName;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.ProjectDescription;
import switchfive.project.ddomain.shared.valueObjects.ProjectName;

import javax.transaction.Transactional;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class ImplUserStoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @SpyBean
    ImplAppProjectService implAppProjectService;

    @SpyBean
    ImplAppCustomerService implAppCustomerService;

    @SpyBean
    ImplAppTypologyService implAppTypologyService;

    @SpyBean
    ImplAppUserService implAppUserService;

    @Test
    void createUserStory_Success() throws Exception {
        //Arrange
        UserDTO newUserDTO = new UserDTO();
        newUserDTO.userName = "oldName";
        newUserDTO.email = "random@email.com";
        newUserDTO.userFunction = "oldFunction";
        newUserDTO.password = "123456Aa@";
        implAppUserService.createAndSaveUser(newUserDTO);

        implAppCustomerService.createAndSaveCustomer("internal");
        implAppTypologyService.addNewTypology("Fixed");

        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setProjectName("ProjectX");
        projectDTO.setProjectDescription("Test purposes");
        projectDTO.setProjectBusinessSector("Engineering");
        projectDTO.setProjectNumberOfPlannedSprints(10);
        projectDTO.setProjectSprintDuration(2);
        projectDTO.setProjectBudget(1000);
        projectDTO.setStartDate("25/04/2023");
        projectDTO.setEndDate("25/04/2025");
        projectDTO.setUserEmail("random@email.com");
        projectDTO.setCostPerHour(10);
        projectDTO.setPercentageOfAllocation(100);
        projectDTO.setProjectCode("P0101");
        projectDTO.setTypologyDescription("Fixed");
        projectDTO.setCustomerName("internal");
        implAppProjectService.createAndSaveProject(projectDTO);

        CreateUserStoryDTO description = new CreateUserStoryDTO();
        description.setDescription("ABC");
        //Act
        mockMvc.perform(MockMvcRequestBuilders.post("/userStories/projectCode/{projectCode}",
                                "P0101")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(description))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void createUserStory_faillingNonExistingProject() throws Exception {
        UserDTO newUserDTO = new UserDTO();
        newUserDTO.userName = "oldName";
        newUserDTO.email = "random@email.com";
        newUserDTO.userFunction = "oldFunction";
        newUserDTO.password = "123456Aa@";
        implAppUserService.createAndSaveUser(newUserDTO);

        implAppCustomerService.createAndSaveCustomer("internal");
        implAppTypologyService.addNewTypology("Fixed");

        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setProjectName("ProjectX");
        projectDTO.setProjectDescription("Test purposes");
        projectDTO.setProjectBusinessSector("Engineering");
        projectDTO.setProjectNumberOfPlannedSprints(10);
        projectDTO.setProjectSprintDuration(2);
        projectDTO.setProjectBudget(1000);
        projectDTO.setStartDate("25/04/2023");
        projectDTO.setEndDate("25/04/2025");
        projectDTO.setUserEmail("random@email.com");
        projectDTO.setCostPerHour(10);
        projectDTO.setPercentageOfAllocation(100);
        projectDTO.setProjectCode("P0202");
        projectDTO.setTypologyDescription("Fixed");
        projectDTO.setCustomerName("internal");
        implAppProjectService.createAndSaveProject(projectDTO);

        CreateUserStoryDTO description = new CreateUserStoryDTO();
        description.setDescription(RandomString.make(300));

        mockMvc.perform(MockMvcRequestBuilders.post("/userStories/projectCode/{projectCode}",
                                "P0202")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(description))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void createUserStory_faillingInvalidDescription() throws Exception {
        //Arrange
        UserDTO newUserDTO = new UserDTO();
        newUserDTO.userName = "oldName";
        newUserDTO.email = "random@email.com";
        newUserDTO.userFunction = "oldFunction";
        newUserDTO.password = "123456Aa@";
        implAppUserService.createAndSaveUser(newUserDTO);

        implAppCustomerService.createAndSaveCustomer("internal");
        implAppTypologyService.addNewTypology("Fixed");

        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setProjectName("ProjectX");
        projectDTO.setProjectDescription("Test purposes");
        projectDTO.setProjectBusinessSector("Engineering");
        projectDTO.setProjectNumberOfPlannedSprints(10);
        projectDTO.setProjectSprintDuration(2);
        projectDTO.setProjectBudget(1000);
        projectDTO.setStartDate("25/04/2023");
        projectDTO.setEndDate("25/04/2025");
        projectDTO.setUserEmail("random@email.com");
        projectDTO.setCostPerHour(10);
        projectDTO.setPercentageOfAllocation(100);
        projectDTO.setProjectCode("P0303");
        projectDTO.setTypologyDescription("Fixed");
        projectDTO.setCustomerName("internal");
        implAppProjectService.createAndSaveProject(projectDTO);

        CreateUserStoryDTO description = new CreateUserStoryDTO();
        description.setDescription("ABC");

        mockMvc.perform(MockMvcRequestBuilders.post("/userStories/projectCode/{projectCode}",
                                "P0303")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(description))
                        .accept(MediaType.APPLICATION_JSON));

        //Act
        mockMvc.perform(MockMvcRequestBuilders.
                        get("/userStories/{userStoryCode}/projectCode/{projectCode}",
                                "US1", "P0303")
                        .contentType("application/json")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

    }

    @Test
    void getUserStoryNonExists() throws Exception {
        //Arrange
        UserDTO newUserDTO = new UserDTO();
        newUserDTO.userName = "oldName";
        newUserDTO.email = "random@email.com";
        newUserDTO.userFunction = "oldFunction";
        newUserDTO.password = "123456Aa@";
        implAppUserService.createAndSaveUser(newUserDTO);

        implAppCustomerService.createAndSaveCustomer("internal");
        implAppTypologyService.addNewTypology("Fixed");

        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setProjectName("ProjectX");
        projectDTO.setProjectDescription("Test purposes");
        projectDTO.setProjectBusinessSector("Engineering");
        projectDTO.setProjectNumberOfPlannedSprints(10);
        projectDTO.setProjectSprintDuration(2);
        projectDTO.setProjectBudget(1000);
        projectDTO.setStartDate("25/04/2023");
        projectDTO.setEndDate("25/04/2025");
        projectDTO.setUserEmail("random@email.com");
        projectDTO.setCostPerHour(10);
        projectDTO.setPercentageOfAllocation(100);
        projectDTO.setProjectCode("P0404");
        projectDTO.setTypologyDescription("Fixed");
        projectDTO.setCustomerName("internal");
        implAppProjectService.createAndSaveProject(projectDTO);

        CreateUserStoryDTO description = new CreateUserStoryDTO();
        description.setDescription("ABC");

        mockMvc.perform(MockMvcRequestBuilders.post("/userStories/projectCode/{projectCode}",
                        "P0404")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(description))
                .accept(MediaType.APPLICATION_JSON));

        //Act
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.
                        get("/userStories/{userStoryCode}/projectCode/{projectCode}",
                                "US2", "P0404")
                        .contentType("application/json")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        String resultContent = result.getResponse().getContentAsString();

        assertEquals("User Story doesn't exists!", resultContent);
    }

    @Test
    void getUserStoryWithoutProject() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/userStories/{userStoryCode}/projectCode/{projectCode}",
                                "US2", "P0909")
                        .contentType("application/json")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getAllocatedProjectsTest() throws Exception {

        //Act
        mockMvc.perform(MockMvcRequestBuilders.get("/users/projects/{email}",
                                "tc@mymail.com")
                        .contentType("application/json")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
