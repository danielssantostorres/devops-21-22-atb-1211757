package switchfive.project.binterfaceAdapters.implRepositories;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.ICustomerAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.CustomerJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.ICustomerRepositoryJPA;
import switchfive.project.ddomain.aggregates.customer.Customer;
import switchfive.project.ddomain.shared.valueObjects.CustomerName;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ImplCustomerRepositoryTest {

    @Test
    void saveCustomer() {
        //Arrange
        Customer customerMock = mock(Customer.class);
        CustomerJPA customerJPAMock = mock(CustomerJPA.class);

        ICustomerRepositoryJPA customerRepositoryJpaMock =
                mock(ICustomerRepositoryJPA.class);
        ICustomerAssemblerJPA customerAssemblerJPAMock =
                mock(ICustomerAssemblerJPA.class);

        ImplCustomerRepository customerRepository =
                new ImplCustomerRepository(customerRepositoryJpaMock,
                        customerAssemblerJPAMock);

        when(customerAssemblerJPAMock.customerToCustomerJPA(
                any(Customer.class))).thenReturn(customerJPAMock);

        when(customerRepositoryJpaMock.save(any(CustomerJPA.class))).thenReturn(
                customerJPAMock);

        //Act
        customerRepository.saveCustomer(customerMock);

        //Assert
        verify(customerAssemblerJPAMock, times(1)).customerToCustomerJPA(
                any(Customer.class));
        verify(customerRepositoryJpaMock, times(1)).save(
                any(CustomerJPA.class));
    }

    @Test
    void findCustomerByName_NameDoesNotExists() {
        // Arrange
        ICustomerRepositoryJPA customerRepositoryJpaMock =
                mock(ICustomerRepositoryJPA.class);
        ICustomerAssemblerJPA customerAssemblerJPAMock =
                mock(ICustomerAssemblerJPA.class);

        ImplCustomerRepository customerRepository =
                new ImplCustomerRepository(customerRepositoryJpaMock,
                        customerAssemblerJPAMock);

        when(customerRepositoryJpaMock.existsByName(
                anyString())).thenReturn(false);

        // Act
        Optional<Customer> actual =
                customerRepository.findCustomerByName(CustomerName.create(
                        "customer"));

        // Assert
        Assertions.assertEquals(Optional.empty(), actual);
    }

    @Test
    void findCustomerByName_NameExists() {
        // Arrange
        Customer customerMock = mock(Customer.class);
        Optional<CustomerJPA> customerJPAMock =
                Optional.of(mock(CustomerJPA.class));

        ICustomerRepositoryJPA customerRepositoryJpaMock =
                mock(ICustomerRepositoryJPA.class);
        ICustomerAssemblerJPA customerAssemblerJPAMock =
                mock(ICustomerAssemblerJPA.class);

        ImplCustomerRepository customerRepository =
                new ImplCustomerRepository(customerRepositoryJpaMock,
                        customerAssemblerJPAMock);

        when(customerRepositoryJpaMock.existsByName(
                anyString())).thenReturn(true);

        when(customerRepositoryJpaMock.findByName(anyString())).thenReturn(
                customerJPAMock);

        when(customerAssemblerJPAMock.customerJpaToCustomer(
                any(CustomerJPA.class))).thenReturn(customerMock);

        // Act
        Optional<Customer> actual =
                customerRepository.findCustomerByName(CustomerName.create(
                        "customer"));

        // Assert
        Assertions.assertEquals(Optional.ofNullable(customerMock), actual);
    }

    @Test
    void existsName() {
        //Arrange
        ICustomerRepositoryJPA customerRepositoryJpaMock =
                mock(ICustomerRepositoryJPA.class);
        ICustomerAssemblerJPA customerAssemblerJPAMock =
                mock(ICustomerAssemblerJPA.class);

        ImplCustomerRepository customerRepository =
                new ImplCustomerRepository(customerRepositoryJpaMock,
                        customerAssemblerJPAMock);

        when(customerRepositoryJpaMock.existsByName(anyString())).thenReturn(
                true);

        //Act
        boolean actual = customerRepository.existsName(CustomerName.create(
                "customer"));

        //Assert
        Assertions.assertEquals(true, actual);
    }
}
