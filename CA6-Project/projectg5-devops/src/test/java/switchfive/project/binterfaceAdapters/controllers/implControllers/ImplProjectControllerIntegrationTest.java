package switchfive.project.binterfaceAdapters.controllers.implControllers;

/*@SpringBootTest
public class ImplProjectControllerIntegrationTest {
    @Autowired
    ImplProjectController projectController;
    @Autowired
    ImplUserController userController;
    @Autowired
    ImplCustomerController customerController;
    @Autowired
    ImplTypologyController typologyController;
    @Autowired
    ImplAppTypologyService typologyService;
    @Autowired
    ImplAppCustomerService customerService;

    ProjectDTO newPrjDto;
    ProjectDTO newPrjResultDto;
    UpdateProjectDTO updatePrjDto;
    ProjectDTO updatePrjResultDto;
    UserDTO newUserDto;

    final String WRONG_INFORMATION = "Wrong information";
    final String PROJECT_DO_NOT_EXIST = "Project do not exist";

    @BeforeEach
    public void setUp() {
        newUserDto = new UserDTO();
        newPrjDto = new ProjectDTO();
        newPrjResultDto = new ProjectDTO();
        updatePrjDto = new UpdateProjectDTO();
        updatePrjResultDto = new ProjectDTO();

        String projectNameIn = "ProjectX";
        String descriptionIn = "Test purposes";
        String businessIn = "Engineering";
        String startDateIn = "25/04/2023";
        String endDateIn = "25/04/2025";
        int numberOfSprintsIn = 10;
        int durationIn = 2;
        int userIdIn = 1000;
        double budgetIn = 1000;
        double costPerHourIn = 10;
        double percentageOdAllocationIn = 100;

        String projectNameUp = "ProjectY";
        String descriptionUp = "Other tests";
        String businessUp = "Information";
        String startDateUp = "25/04/2023";
        String endDateUp = "25/04/2025";
        int numberOfSprintsUp = 1;
        int durationUp = 1;
        int userIdUp = 1000;
        double budgetUp = 1000;
        double costPerHourUp = 10;
        double percentageOdAllocationUp = 100;

        newUserDto.email = "abc@email.com";
        newUserDto.password = "123546Aa@";
        newUserDto.userName = "validName";
        newUserDto.function = "Tester";

        newPrjDto.setProjectName(projectNameIn);
        newPrjDto.setProjectDescription(descriptionIn);
        newPrjDto.setProjectBusinessSector(businessIn);
        newPrjDto.setProjectNumberOfPlannedSprints(numberOfSprintsIn);
        newPrjDto.setProjectSprintDuration(durationIn);
        newPrjDto.setProjectBudget(budgetIn);
        newPrjDto.setStartDate(startDateIn);
        newPrjDto.setEndDate(endDateIn);
        newPrjDto.setUserID(userIdIn);
        newPrjDto.setCostPerHour(costPerHourIn);
        newPrjDto.setPercentageOfAllocation(percentageOdAllocationIn);

        newPrjResultDto.setProjectName(projectNameIn);
        newPrjResultDto.setProjectDescription(descriptionIn);
        newPrjResultDto.setProjectBusinessSector(businessIn);
        newPrjResultDto.setProjectNumberOfPlannedSprints(numberOfSprintsIn);
        newPrjResultDto.setProjectSprintDuration(durationIn);
        newPrjResultDto.setProjectBudget(budgetIn);
        newPrjResultDto.setStartDate(startDateIn);
        newPrjResultDto.setEndDate(endDateIn);
        newPrjResultDto.setUserID(userIdIn);
        newPrjResultDto.setCostPerHour(costPerHourIn);
        newPrjResultDto.setPercentageOfAllocation(percentageOdAllocationIn);

        updatePrjDto.setProjectName(projectNameUp);
        updatePrjDto.setProjectDescription(descriptionUp);
        updatePrjDto.setProjectBusinessSector(businessUp);
        updatePrjDto.setProjectNumberOfPlannedSprints(numberOfSprintsUp);
        updatePrjDto.setProjectSprintDuration(durationUp);
        updatePrjDto.setProjectBudget(budgetUp);
        updatePrjDto.setStartDate(startDateUp);
        updatePrjDto.setEndDate(endDateUp);

        updatePrjResultDto.setProjectName(projectNameUp);
        updatePrjResultDto.setProjectDescription(descriptionUp);
        updatePrjResultDto.setProjectBusinessSector(businessUp);
        updatePrjResultDto.setProjectNumberOfPlannedSprints(numberOfSprintsUp);
        updatePrjResultDto.setProjectSprintDuration(durationUp);
        updatePrjResultDto.setProjectBudget(budgetUp);
        updatePrjResultDto.setStartDate(endDateUp);
        updatePrjResultDto.setEndDate(endDateUp);
        updatePrjResultDto.setUserID(userIdUp);
        updatePrjResultDto.setCostPerHour(costPerHourUp);
        updatePrjResultDto.setPercentageOfAllocation(percentageOdAllocationUp);

    }

    @Test
    void projectIsSuccessfullyCreated() throws NoSuchAlgorithmException {
        //Arrange
        //Add new typology
        String typologyDescription = "description one";
        typologyController.createTypology(typologyDescription);
        //Add new user
        newUserDto.email = "test1@sapo.pt";
        userController.createUser(newUserDto);
        //Add new customer
        String customerID = UUID.randomUUID().toString();
        String customerName = "customer one";
        CustomerDTO customerDTO = new CustomerDTO(customerID, customerName);
        customerController.createAndSaveCustomer(customerDTO);
        //Add new project
        newPrjDto.setProjectCode("new11");
        newPrjResultDto.setProjectCode("new11");
        newPrjDto.setTypologyID(typologyService.findTypologyByDescription(
                typologyDescription).get().getIdentity());
        newPrjDto.setCustomerID(customerService.findCustomerByDescription(
                customerName).get().getCustomerID());
        //Add link to output
        Link link = linkTo(methodOn(ImplProjectController.class).
                getProject(newPrjResultDto.getProjectCode())).withSelfRel();
        newPrjResultDto.add(link);

        ResponseEntity<Object> expected = new ResponseEntity<>(
                newPrjResultDto,
                HttpStatus.CREATED);

        //Act
        ResponseEntity<Object> actual = projectController.createProject(
                newPrjDto);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void projectCreationFailsBecauseAlreadyExistsInProjectStore()
            throws NoSuchAlgorithmException {
        //Arrange
        //Add new typology
        String typologyDescription = "description two";
        typologyController.createTypology(typologyDescription);
        //Add new user
        newUserDto.email = "test2@sapo.pt";
        userController.createUser(newUserDto);
        //Add new customer
        String customerID = UUID.randomUUID().toString();
        String customerName = "customer two";
        CustomerDTO customerDTO = new CustomerDTO(customerID, customerName);
        customerController.createAndSaveCustomer(customerDTO);
        //Add new project
        newPrjDto.setProjectCode("new12");
        newPrjResultDto.setProjectCode("new12");
        newPrjDto.setTypologyID(typologyService.findTypologyByDescription(
                typologyDescription).get().getIdentity());
        newPrjDto.setCustomerID(customerService.findCustomerByDescription(
                customerName).get().getCustomerID());

        ResponseEntity<Object> expected =
                new ResponseEntity<>(WRONG_INFORMATION,
                        HttpStatus.BAD_REQUEST);

        //Act
        projectController.createProject(newPrjDto);
        ResponseEntity<Object> actual = projectController.createProject(
                newPrjDto);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getProjectIsSuccessful() throws NoSuchAlgorithmException {
        //Arrange
        //Add new typology
        String typologyDescription = "description three";
        typologyController.createTypology(typologyDescription);
        //Add new user
        newUserDto.email = "test3@sapo.pt";
        userController.createUser(newUserDto);
        //Add new customer
        String customerID = UUID.randomUUID().toString();
        String customerName = "customer three";
        CustomerDTO customerDTO = new CustomerDTO(customerID, customerName);
        customerController.createAndSaveCustomer(customerDTO);
        //ProjectDto
        newPrjDto.setProjectCode("new13");
        newPrjResultDto.setProjectCode("new13");
        newPrjDto.setTypologyID(typologyService.findTypologyByDescription(
                typologyDescription).get().getIdentity());
        newPrjDto.setCustomerID(customerService.findCustomerByDescription(
                customerName).get().getCustomerID());

        ResponseEntity<Object> expected = new ResponseEntity<>(
                newPrjDto,
                HttpStatus.OK);

        //Create project
        projectController.createProject(newPrjDto);

        //Act
        ResponseEntity<Object> actual =
                projectController.getProject(newPrjDto.getProjectCode());

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getProjectFailsBecauseProjectDoesntExist() {
        //Arrange
        //ProjectDto
        final String thisCodeDoesntExist = "12345";

        ResponseEntity<Object> expected =
                new ResponseEntity<>(PROJECT_DO_NOT_EXIST,
                        HttpStatus.OK);

        //Act
        ResponseEntity<Object> actual =
                projectController.getProject(thisCodeDoesntExist);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void getProjectFailsBecauseProjectIsInvalid() {
        //Arrange
        //ProjectDto
        final String invalidCode = "12345xxx";

        ResponseEntity<Object> expected =
                new ResponseEntity<>(WRONG_INFORMATION,
                        HttpStatus.BAD_REQUEST);

        //Act
        ResponseEntity<Object> actual =
                projectController.getProject(invalidCode);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void updateProjectFailsBecauseProjectCodeIsInvalid() {
        //Arrange
        //ProjectDto
        final String invalidCode = "12345xxx";

        ResponseEntity<Object> expected =
                new ResponseEntity<>(WRONG_INFORMATION,
                        HttpStatus.BAD_REQUEST);

        //Act
        ResponseEntity<Object> actual =
                projectController.updateProject(invalidCode, updatePrjDto);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void updateProjectFailsBecauseProjectDoesntExist()
            throws NoSuchAlgorithmException {
        //Arrange
        //Add new typology
        String typologyDescription = "description four";
        typologyController.createTypology(typologyDescription);
        //Add new user
        newUserDto.email = "test4@sapo.pt";
        userController.createUser(newUserDto);
        //Add new customer
        String customerID = UUID.randomUUID().toString();
        String customerName = "customer four";
        CustomerDTO customerDTO = new CustomerDTO(customerID, customerName);
        customerController.createAndSaveCustomer(customerDTO);
        //ProjectDto
        newPrjDto.setProjectCode("new14");
        newPrjResultDto.setProjectCode("new14");
        newPrjDto.setTypologyID(typologyService.findTypologyByDescription(
                typologyDescription).get().getIdentity());
        newPrjDto.setCustomerID(customerService.findCustomerByDescription(
                customerName).get().getCustomerID());
        //UpdateProjectDto
        updatePrjDto.setCustomerID(newPrjDto.getCustomerID());
        updatePrjDto.setTypologyID(newPrjDto.getTypologyID());

        ResponseEntity<Object> expected =
                new ResponseEntity<>(PROJECT_DO_NOT_EXIST,
                        HttpStatus.NOT_FOUND);

        //Act
        ResponseEntity<Object> actual =
                projectController.updateProject("inval",
                        updatePrjDto);

        //Assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void updateProjectSuccessful() throws NoSuchAlgorithmException {
        //Arrange
        //Add new typology
        String typologyDescription = "description four";
        typologyController.createTypology(typologyDescription);
        //Add new user
        newUserDto.email = "test4@sapo.pt";
        userController.createUser(newUserDto);
        //Add new customer
        String customerID = UUID.randomUUID().toString();
        String customerName = "customer five";
        CustomerDTO customerDTO = new CustomerDTO(customerID, customerName);
        customerController.createAndSaveCustomer(customerDTO);
        //ProjectDto
        newPrjDto.setProjectCode("new14");
        newPrjResultDto.setProjectCode("new14");
        newPrjDto.setTypologyID(typologyService.findTypologyByDescription(
                typologyDescription).get().getIdentity());
        newPrjDto.setCustomerID(customerService.findCustomerByDescription(
                customerName).get().getCustomerID());
        //UpdateProjectDto
        updatePrjDto.setCustomerID(newPrjDto.getCustomerID());
        updatePrjDto.setTypologyID(newPrjDto.getTypologyID());
        //Add link to output
        Link link = linkTo(methodOn(ImplProjectController.class).
                getProject(newPrjResultDto.getProjectCode())).withSelfRel();
        newPrjResultDto.add(link);

        ResponseEntity<Object> expected =
                new ResponseEntity<>(newPrjResultDto, HttpStatus.OK);

        projectController.createProject(newPrjDto);

        //Act
        ResponseEntity<Object> actual =
                projectController.updateProject(newPrjDto.getProjectCode(),
                        updatePrjDto);

        //Assert
        Assertions.assertEquals(expected, actual);
    }
}*/
