//package switchfive.project.model;
//
//import org.junit.jupiter.api.Test;
//import switchfive.project.d_domain.shared.valueObjects.ProfileID;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class ID_ProfileTest {
//
//    @Test
//    void compareIfObjectIsTheSame(){
//
//        //Arrange
//        ProfileID id_profile1 = ProfileID.createProfileID(1);
//
//        //Act
//        boolean result = id_profile1.equals(id_profile1);
//
//        //Assert
//        assertTrue(result);
//
//
//    }
//
//    @Test
//    void compareIfObjectIsNull(){
//
//        //Arrange
//        ProfileID id_profile1 = ProfileID.createProfileID(1);
//
//        //Act
//        boolean result = id_profile1.equals(null);
//
//        //Assert
//        assertFalse(result);
//
//    }
//
//    @Test
//    void compareIfObjectsAreDifferentl(){
//
//        //Arrange
//        ProfileID id_profile1 = ProfileID.createProfileID(1);
//        String profile2 = "123";
//
//        //Act
//        boolean result = id_profile1.equals(profile2);
//
//        //Assert
//        assertFalse(result);
//    }
//
//    @Test
//    void compareIfObjectAreEqualsCaseTrue(){
//
//        //Arrange
//        ProfileID id_profile1 = ProfileID.createProfileID(1);
//        ProfileID id_profile2 = ProfileID.createProfileID(1);
//
//        //Act
//        boolean result = id_profile1.equals(id_profile2);
//
//        //Assert
//        assertTrue(result);
//    }
//
//    @Test
//    void compareIfObjectAreEqualsCaseFalse(){
//
//        //Arrange
//        ProfileID id_profile1 = ProfileID.createProfileID(1);
//        ProfileID id_profile2 = ProfileID.createProfileID(2);
//
//        //Act
//        boolean result = id_profile1.equals(id_profile2);
//
//        //Assert
//        assertFalse(result);
//    }
//}
