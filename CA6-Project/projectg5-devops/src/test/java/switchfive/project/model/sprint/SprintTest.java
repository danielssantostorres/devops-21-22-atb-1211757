package switchfive.project.model.sprint;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switchfive.project.ddomain.aggregates.sprint.Sprint;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.SprintDescription;
import switchfive.project.ddomain.shared.valueObjects.SprintID;
import switchfive.project.ddomain.shared.valueObjects.Time;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SprintTest {

    /*@Test
    @DisplayName("Two compared instances are equals.")
    void comparedSprintsAreEquals() throws ParseException {
        //Arrange
        SprintID sprintID = SprintID.createSprintID("XPTO1", 1);
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        SprintDescription sprintDescription = SprintDescription.create("cost " +
                "per hour");

        Sprint newSprint =
                new Sprint(sprintID, projectCode, dates, sprintDescription);

        Sprint otherSprint =
                new Sprint(sprintID, projectCode, dates, sprintDescription);

        //Act
        boolean actual = newSprint.equals(otherSprint);

        //Assert
        assertTrue(actual);
    }

    @Test
    @DisplayName("Two compared instances are the same.")
    void comparedSprintsAreTheSame() throws ParseException {
        //Arrange
        SprintID sprintID = SprintID.createSprintID("XPTO1", 1);
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        SprintDescription sprintDescription = SprintDescription.create("cost " +
                "per hour");

        Sprint newSprint =
                new Sprint(sprintID, projectCode, dates, sprintDescription);

        Sprint otherSprint = newSprint;

        //Act
        boolean actual = newSprint.equals(otherSprint);

        //Assert
        assertTrue(actual);

    }

    @Test
    @DisplayName("Two compared instances are not equals.")
    void comparedSprintsAreNotEquals() throws ParseException {
        //Arrange
        SprintID sprintID = SprintID.createSprintID("XPTO1", 1);
        SprintID otherSprintID = SprintID.createSprintID("XPTO1", 2);
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        SprintDescription sprintDescription = SprintDescription.create("cost " +
                "per hour");

        Sprint newSprint =
                new Sprint(sprintID, projectCode, dates, sprintDescription);

        Sprint otherSprint =
                new Sprint(otherSprintID, projectCode, dates, sprintDescription);

        //Act
        boolean actual = newSprint.equals(otherSprint);

        //Assert
        assertFalse(actual);
    }

    @Test
    @DisplayName("Two compared objects are not instances of same class.")
    void comparedObjectsAreNotInstancesOfSameClass() throws ParseException {
        //Arrange
        SprintID sprintID = SprintID.createSprintID("XPTO1", 1);
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        SprintDescription sprintDescription = SprintDescription.create("cost " +
                "per hour");

        Sprint newSprint =
                new Sprint(sprintID, projectCode, dates, sprintDescription);

        Object otherSprint = new Object();

        //Act
        boolean actual = newSprint.equals(otherSprint);

        //Assert
        assertFalse(actual);
    }

    @Test
    @DisplayName("Other object is null. Expected false.")
    void otherObjectIsNull() throws ParseException {
        //Arrange
        SprintID sprintID = SprintID.createSprintID("XPTO1", 1);
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        SprintDescription sprintDescription = SprintDescription.create("cost " +
                "per hour");

        Sprint newSprint =
                new Sprint(sprintID, projectCode, dates, sprintDescription);

        //Act
        boolean actual = newSprint.equals(null);

        //Assert
        assertFalse(actual);
    }

    @Test
    @DisplayName("Objects are equals. Expected same hashcode.")
    void sameHashcode() throws ParseException {
        //Arrange
        SprintID sprintID = SprintID.createSprintID("XPTO1", 1);
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        SprintDescription sprintDescription = SprintDescription.create("cost " +
                "per hour");

        Sprint newSprint =
                new Sprint(sprintID, projectCode, dates, sprintDescription);

        Sprint otherSprint =
                new Sprint(sprintID, projectCode, dates, sprintDescription);

        //Act
        boolean actual = newSprint.hashCode() == otherSprint.hashCode();

        //Assert
        assertTrue(actual);
    }

    @Test
    @DisplayName("Objects are not equals. Expected different hashcode.")
    void differentHashcode() throws ParseException {
        //Arrange
        SprintID sprintID = SprintID.createSprintID("XPTO1", 1);
        SprintID otherSprintID = SprintID.createSprintID("XPTO1", 2);
        ProjectCode projectCode = ProjectCode.createProjectCode("XPTO1");
        Time dates = Time.create("06/04/2023", "06/04/2024");
        SprintDescription sprintDescription = SprintDescription.create("cost " +
                "per hour");

        Sprint newSprint =
                new Sprint(sprintID, projectCode, dates, sprintDescription);

        Sprint otherSprint =
                new Sprint(otherSprintID, projectCode, dates, sprintDescription);

        //Act
        boolean actual = newSprint.hashCode() == otherSprint.hashCode();

        //Assert
        assertFalse(actual);
    }*/
}
