package switchfive.project.zbootstrapping.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.capplicationServices.appServices.iappServices.IAppTypologyService;

@Component
public class TypologyBootstrap {

    private static final Logger LOG = LoggerFactory.getLogger(TypologyBootstrap.class);

    private IAppTypologyService typologyService;


    @Autowired
    public TypologyBootstrap(IAppTypologyService typologyService) {
        this.typologyService = typologyService;
    }


    public void execute() {
        LOG.info("Loading typologies ...");
        loadProfiles();
        LOG.info("Typologies loaded");
    }

    public void loadProfiles() {
        typologyService.addNewTypology("Fixed cost");
        typologyService.addNewTypology("Time and materials");
    }
}
