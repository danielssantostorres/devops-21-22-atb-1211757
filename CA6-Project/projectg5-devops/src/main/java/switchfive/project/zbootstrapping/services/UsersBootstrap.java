package switchfive.project.zbootstrapping.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppUserService;

import java.security.NoSuchAlgorithmException;

@Component
public class UsersBootstrap {

    private static final Logger LOG = LoggerFactory.getLogger(UsersBootstrap.class);

    private IAppUserService userService;


    @Autowired
    public UsersBootstrap(IAppUserService userService) {
        this.userService = userService;
    }


    public void execute() throws NoSuchAlgorithmException {
        LOG.info("Loading users ...");
        loadProfiles();
        LOG.info("Users loaded");
    }

    public void loadProfiles() throws NoSuchAlgorithmException {
        userService.createAndSaveUser(addUser("js@mymail.com", "P@ssw0rd!", "JoaoSilva", "Anything"));
        userService.createAndSaveUser(addUser("ms@mymail.com", "P@ssw0rd!", "ManelCosta", "Anything"));
        userService.createAndSaveUser(addUser("xf@mymail.com", "P@ssw0rd!", "XicoFerreira", "Anything"));
        userService.createAndSaveUser(addUser("tc@mymail.com", "P@ssw0rd!", "TiagoCancado", "Anything"));
        userService.createAndSaveUser(addUser("udu@mymail.com", "P@ssw0rd!", "UrbinoUrzes", "Anything"));
        userService.createAndSaveUser(addUser("ze@mymail.com", "P@ssw0rd!", "ZeEsquina", "Anything"));
        userService.createAndSaveUser(addUser("nel.m@mymail.com", "P@ssw0rd!", "NelMoleiro", "Flexible"));
        userService.createAndSaveUser(addUser("zb@mymail.com", "P@ssw0rd!", "ZeBento", "Flexible"));
        userService.createAndSaveUser(addUser("to.f@mymail.com", "P@ssw0rd!", "ToFarrulo", "Flexible"));
        userService.createAndSaveUser(addUser("tdc@mymail.com", "P@ssw0rd!", "TinoCruzes", "Not so flexible"));
        userService.createAndSaveUser(addUser("qb@mymail.com", "P@ssw0rd!", "QuimBarreiros", "Anything"));
        userService.createAndSaveUser(addUser("tg@mymail.com", "P@ssw0rd!", "TiagoGeringonca", "Anything"));
        userService.createAndSaveUser(addUser("zm@mymail.com", "P@ssw0rd!", "ZeManel", "Anything"));
        userService.createAndSaveUser(addUser("as@mymail.com", "P@ssw0rd!", "AntonioSilva", "Anything"));

        this.userService.activateAll();

        this.userService.validateAndAddProfile("udu@mymail.com","Administrator");
        this.userService.validateAndAddProfile("tc@mymail.com","Director");
        this.userService.validateAndAddProfile("js@mymail.com","User");
        this.userService.validateAndAddProfile("ms@mymail.com","User");
        this.userService.validateAndAddProfile("xf@mymail.com","User");
        this.userService.validateAndAddProfile("nel.m@mymail.com","User");
        this.userService.validateAndAddProfile("zb@mymail.com","User");
        this.userService.validateAndAddProfile("to.f@mymail.com","User");
        this.userService.validateAndAddProfile("tdc@mymail.com","User");
        this.userService.validateAndAddProfile("qb@mymail.com","User");
        this.userService.validateAndAddProfile("tg@mymail.com","User");
        this.userService.validateAndAddProfile("zm@mymail.com","User");
        this.userService.validateAndAddProfile("as@mymail.com","User");

        this.userService.validateAndRemoveProfile("udu@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("tc@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("js@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("ms@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("xf@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("nel.m@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("zb@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("to.f@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("tdc@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("qb@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("tg@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("zm@mymail.com","Visitor");
        this.userService.validateAndRemoveProfile("as@mymail.com","Visitor");
    }

    private UserDTO addUser(final String email, final String password, final String name, final String function) {
        UserDTO userDTO = new UserDTO();

        userDTO.email = email;
        userDTO.password = password;
        userDTO.userName = name;
        userDTO.userFunction = function;

        return userDTO;
    }
}
