package switchfive.project.zbootstrapping.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SprintCreationDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppSprintService;

import java.text.ParseException;

@Component
public class SprintsBootstrap {

    private static final Logger LOG = LoggerFactory.getLogger(SprintsBootstrap.class);

    private IAppSprintService sprintService;


    @Autowired
    public SprintsBootstrap(IAppSprintService sprintService) {
        this.sprintService = sprintService;
    }


    public void execute() throws ParseException {
        LOG.info("Loading sprints ...");
        loadProfiles();
        LOG.info("Sprints loaded");
    }

    public void loadProfiles() throws ParseException {
        sprintService.createAndSaveSprint(addSprint("22/03/2023","04/04/2023","dummy sprint 1"),"A0001");
        sprintService.createAndSaveSprint(addSprint("05/04/2023","25/04/2023","dummy sprint 2"), "A0001");
        sprintService.createAndSaveSprint(addSprint("26/04/2023","09/05/2023","dummy sprint 3"), "A0001");
        sprintService.createAndSaveSprint(addSprint("10/05/2023","23/05/2023","dummy sprint 4"), "A0001");
        sprintService.createAndSaveSprint(addSprint("24/05/2023","06/06/2023","dummy sprint 5"), "A0001");
        sprintService.createAndSaveSprint(addSprint("07/06/2023","20/06/2023","dummy sprint 6"), "A0001");
        sprintService.createAndSaveSprint(addSprint("21/06/2023","18/07/2023","dummy sprint 7"), "A0001");
        sprintService.createAndSaveSprint(addSprint("19/07/2023","02/08/2023","dummy sprint 8"), "A0001");
        sprintService.createAndSaveSprint(addSprint("07/06/2023","20/06/2023","dummy sprint 1"), "A0002");
        sprintService.createAndSaveSprint(addSprint("05/07/2023","01/08/2023","dummy sprint 2"), "A0002");
        sprintService.createAndSaveSprint(addSprint("02/08/2023","29/08/2023","dummy sprint 3"), "A0002");
        sprintService.createAndSaveSprint(addSprint("30/08/2023","26/09/2023","dummy sprint 4"), "A0002");
        sprintService.createAndSaveSprint(addSprint("27/09/2023","17/10/2023","dummy sprint 5"), "A0002");
        sprintService.createAndSaveSprint(addSprint("18/10/2023","14/11/2023","dummy sprint 6"), "A0002");
        sprintService.createAndSaveSprint(addSprint("15/11/2023","05/12/2023","dummy sprint 7"), "A0002");
        sprintService.createAndSaveSprint(addSprint("06/12/2023","09/01/2024","dummy sprint 8"), "A0002");
        sprintService.createAndSaveSprint(addSprint("10/01/2024","06/02/2024","dummy sprint 9"), "A0002");
        sprintService.createAndSaveSprint(addSprint("07/02/2024","06/03/2024","dummy sprint 10"), "A0002");
        sprintService.createAndSaveSprint(addSprint("07/03/2024","03/04/2024","dummy sprint 11"), "A0002");
        sprintService.createAndSaveSprint(addSprint("04/04/2024","18/04/2024","dummy sprint 12"), "A0002");
    }

    private SprintCreationDTO addSprint(final String startDate, final String endDate, final String description) {
        SprintCreationDTO sprintDTO = new SprintCreationDTO();

        sprintDTO.setDescription(description);
        sprintDTO.setStartDate(startDate);
        sprintDTO.setEndDate(endDate);

        return sprintDTO;
    }
}
