package switchfive.project.zbootstrapping.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.capplicationServices.appServices.iappServices.IAppUserStoryService;

import java.text.ParseException;

@Component
public class UserStoriesBootstrap {

    private static final Logger LOG = LoggerFactory.getLogger(UserStoriesBootstrap.class);

    private IAppUserStoryService userStoryService;


    @Autowired
    public UserStoriesBootstrap(IAppUserStoryService userStoryService) {
        this.userStoryService = userStoryService;
    }


    public void execute() throws ParseException {
        LOG.info("Loading user stories ...");
        loadProfiles();
        LOG.info("User stories loaded");
    }

    public void loadProfiles() throws ParseException {
        userStoryService.createAndAddUserStory("A0001","Dummy 01");
        userStoryService.createAndAddUserStory("A0001","Dummy 02");
        userStoryService.createAndAddUserStory("A0001","Dummy 03");
        userStoryService.createAndAddUserStory("A0001","Dummy 04");
        userStoryService.createAndAddUserStory("A0001","Dummy 05");
        userStoryService.createAndAddUserStory("A0001","Dummy 06");
        userStoryService.createAndAddUserStory("A0001","Dummy 07");
        userStoryService.createAndAddUserStory("A0001","Dummy 08");
        userStoryService.createAndAddUserStory("A0001","Dummy 09");
        userStoryService.createAndAddUserStory("A0001","Dummy 10");
        userStoryService.createAndAddUserStory("A0001","Dummy 11");
        userStoryService.createAndAddUserStory("A0001","Dummy 12");
        userStoryService.createAndAddUserStory("A0001","Dummy 13");
        userStoryService.createAndAddUserStory("A0001","Dummy 14");
        userStoryService.createAndAddUserStory("A0001","Dummy 15");
        userStoryService.createAndAddUserStory("A0001","Dummy 16");
        userStoryService.createAndAddUserStory("A0001","Dummy 17");
        userStoryService.createAndAddUserStory("A0001","Dummy 18");
        userStoryService.createAndAddUserStory("A0001","Dummy 19");
        userStoryService.createAndAddUserStory("A0001","Dummy 20");
        userStoryService.createAndAddUserStory("A0001","Dummy 21");
        userStoryService.createAndAddUserStory("A0001","Dummy 22");
        userStoryService.createAndAddUserStory("A0001","Dummy 23");
        userStoryService.createAndAddUserStory("A0001","Dummy 24");
        userStoryService.createAndAddUserStory("A0001","Dummy 25");
        userStoryService.createAndAddUserStory("A0001","Dummy 26");
        userStoryService.createAndAddUserStory("A0001","Dummy 27");
        userStoryService.createAndAddUserStory("A0001","Dummy 28");
        userStoryService.createAndAddUserStory("A0001","Dummy 29");
        userStoryService.createAndAddUserStory("A0001","Dummy 30");

        userStoryService.createAndAddUserStory("A0002","A2 dummy 001");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 002");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 003");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 004");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 005");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 006");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 007");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 008");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 009");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 010");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 011");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 012");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 013");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 014");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 015");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 016");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 017");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 018");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 019");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 020");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 021");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 022");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 023");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 024");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 025");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 026");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 027");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 028");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 029");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 030");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 031");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 032");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 033");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 034");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 035");
        userStoryService.createAndAddUserStory("A0002","A2 dummy 036");
    }
}
