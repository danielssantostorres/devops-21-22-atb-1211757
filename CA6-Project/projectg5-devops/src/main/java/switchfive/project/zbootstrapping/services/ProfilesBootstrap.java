package switchfive.project.zbootstrapping.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.capplicationServices.appServices.iappServices.IAppProfileService;

@Component
public class ProfilesBootstrap {

    private static final Logger LOG = LoggerFactory.getLogger(ProfilesBootstrap.class);

    private IAppProfileService createProfileService;


    @Autowired
    public ProfilesBootstrap(IAppProfileService createProfileService) {
        this.createProfileService = createProfileService;
    }


    public void execute() {
        LOG.info("Loading Profiles ...");
        loadProfiles();
        LOG.info("Profiles loaded");
    }


    public void loadProfiles() {
        createProfileService.addNewProfile("Administrator");
        createProfileService.addNewProfile("Visitor");
        createProfileService.addNewProfile("Project Manager");
        createProfileService.addNewProfile("Product Owner");
        createProfileService.addNewProfile("Scrum Master");
        createProfileService.addNewProfile("User");
        createProfileService.addNewProfile("Director");
    }
}
