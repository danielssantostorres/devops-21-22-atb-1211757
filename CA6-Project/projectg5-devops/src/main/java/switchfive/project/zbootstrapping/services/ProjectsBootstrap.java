package switchfive.project.zbootstrapping.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProjectDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UpdateProjectDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppProjectService;
import switchfive.project.ddomain.shared.valueObjects.ProjectStatus;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@Component
public class ProjectsBootstrap {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectsBootstrap.class);

    private IAppProjectService projectService;


    @Autowired
    public ProjectsBootstrap(IAppProjectService projectService) {
        this.projectService = projectService;
    }


    public void execute() throws NoSuchAlgorithmException, ParseException {
        LOG.info("Loading projects ...");
        loadProfiles();
        LOG.info("Projects loaded");
    }


    public void loadProfiles() throws NoSuchAlgorithmException, ParseException {
        this.projectService.createAndSaveProject(addProject("A0001", "Dummy 01", "Just a dummy project", "01/03/2023",
                2, 6, "31/07/2024", "XPTO, SA", "It doesn't matter", "Fixed cost",
                150000, "tc@mymail.com", 35, 20));
        this.projectService.createAndSaveProject(addProject("A0002", "Dummy 02", "Just another dummy project", "01/06/2023",
                4, 12, "29/04/2024", "XYZ, Lda", "It doesn't matter", "Fixed cost",
                350000, "js@mymail.com", 50, 100));
        this.projectService.createAndSaveProject(addProject("A0666", "Inevitable nightmare", "Doomed from the start", "10/03/2023",
                3, 15, "29/04/2024", "Hell, LLC", "Hospitality industry", "Time and materials",
                500000, "js@mymail.com", 50, 100));
    }


    private ProjectDTO addProject(final String code, final String name, final String description, final String startDate,
                                  final int sprintDuration, final int numberOfPlannedSprints, final String endDate,
                                  final String customer, final String businessSector, final String typology, final double budget,
                                  final String pmEmail, final double pmCostPerHour, final double pmAllocation) {
        ProjectDTO projectDTO = new ProjectDTO();

        projectDTO.setProjectCode(code);
        projectDTO.setProjectName(name);
        projectDTO.setProjectDescription(description);
        projectDTO.setStartDate(startDate);
        projectDTO.setProjectSprintDuration(sprintDuration);
        projectDTO.setProjectNumberOfPlannedSprints(numberOfPlannedSprints);
        projectDTO.setEndDate(endDate);
        projectDTO.setCustomerName(customer);
        projectDTO.setProjectBusinessSector(businessSector);
        projectDTO.setTypologyDescription(typology);
        projectDTO.setProjectBudget(budget);
        projectDTO.setUserEmail(pmEmail);
        projectDTO.setCostPerHour(pmCostPerHour);
        projectDTO.setPercentageOfAllocation(pmAllocation);

        return projectDTO;
    }
}
