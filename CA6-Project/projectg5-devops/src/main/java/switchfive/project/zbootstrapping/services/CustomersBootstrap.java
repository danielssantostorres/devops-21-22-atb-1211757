package switchfive.project.zbootstrapping.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.capplicationServices.appServices.iappServices.IAppCustomerService;

@Component
public class CustomersBootstrap {

    private static final Logger LOG = LoggerFactory.getLogger(CustomersBootstrap.class);

    private IAppCustomerService customerService;


    @Autowired
    public CustomersBootstrap(IAppCustomerService customerService) {
        this.customerService = customerService;
    }


    public void execute() {
        LOG.info("Loading customers ...");
        loadProfiles();
        LOG.info("Customers loaded");
    }

    public void loadProfiles() {
        customerService.createAndSaveCustomer("XPTO, SA");
        customerService.createAndSaveCustomer("XYZ, Lda");
        customerService.createAndSaveCustomer("Hell, LLC");
    }
}
