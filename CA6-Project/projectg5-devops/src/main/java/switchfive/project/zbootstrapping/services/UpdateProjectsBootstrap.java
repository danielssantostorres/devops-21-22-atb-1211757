package switchfive.project.zbootstrapping.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UpdateProjectDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppProjectService;
import switchfive.project.ddomain.shared.valueObjects.ProjectStatus;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@Component
public class UpdateProjectsBootstrap {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateProjectsBootstrap.class);

    private IAppProjectService projectService;


    @Autowired
    public UpdateProjectsBootstrap(IAppProjectService projectService) {
        this.projectService = projectService;
    }


    public void execute() throws NoSuchAlgorithmException, ParseException {
        LOG.info("Updating projects ...");
        loadProfiles();
        LOG.info("Projects updated (closed)");
    }


    public void loadProfiles() throws ParseException {
        this.projectService.updateProject("A0001", updateProject("Dummy 01", "Just a dummy project", "01/03/2023",
                2, 6, "31/07/2024", "XPTO, SA", "It doesn't matter", "Fixed cost", 150000));
        this.projectService.updateProject("A0002", updateProject("Dummy 02", "Just another dummy project", "01/06/2023",
                4, 12, "29/04/2024", "XYZ, Lda", "It doesn't matter", "Fixed cost", 350000));
    }

    private UpdateProjectDTO updateProject(final String name, final String description, final String startDate,
                                           final int sprintDuration, final int numberOfPlannedSprints, final String endDate,
                                           final String customer, final String businessSector, final String typology, final double budget) {
        UpdateProjectDTO updateProjectDTO = new UpdateProjectDTO();

        updateProjectDTO.setProjectName(name);
        updateProjectDTO.setProjectDescription(description);
        updateProjectDTO.setStartDate(startDate);
        updateProjectDTO.setProjectSprintDuration(sprintDuration);
        updateProjectDTO.setProjectNumberOfPlannedSprints(numberOfPlannedSprints);
        updateProjectDTO.setEndDate(endDate);
        updateProjectDTO.setCustomerName(customer);
        updateProjectDTO.setProjectBusinessSector(businessSector);
        updateProjectDTO.setTypologyDescription(typology);
        updateProjectDTO.setProjectBudget(budget);
        updateProjectDTO.setStatus(ProjectStatus.Closed.toString());

        return updateProjectDTO;
    }
}
