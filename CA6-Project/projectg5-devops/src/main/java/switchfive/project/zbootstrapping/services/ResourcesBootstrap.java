package switchfive.project.zbootstrapping.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceCreationDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppResourceService;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@Component
public class ResourcesBootstrap {

    private static final Logger LOG = LoggerFactory.getLogger(ResourcesBootstrap.class);

    private IAppResourceService resourceService;


    @Autowired
    public ResourcesBootstrap(IAppResourceService resourceService) {
        this.resourceService = resourceService;
    }


    public void execute() throws NoSuchAlgorithmException, ParseException {
        LOG.info("Loading resources ...");
        loadProfiles();
        LOG.info("Resources loaded");
    }


    public void loadProfiles() throws NoSuchAlgorithmException, ParseException {
        resourceService.definedProductOwnerOfAProject(addProject("A0001", "js@mymail.com", "03/03/2023",
                "31/07/2024", 25, 20));
        resourceService.definedScrumMasterOfAProject(addProject("A0001", "ms@mymail.com", "04/03/2023",
                "31/07/2024", 25, 30));
        resourceService.definedTeamMemberOfAProject(addProject("A0002", "xf@mymail.com", "05/06/2023",
                "29/04/2024", 20, 100));
        resourceService.definedTeamMemberOfAProject(addProject("A0002", "nel.m@mymail.com", "06/06/2023",
                "29/04/2024", 20, 100));
        resourceService.definedTeamMemberOfAProject(addProject("A0002", "zb@mymail.com", "07/06/2023",
                "29/04/2024", 20, 100));
        resourceService.definedTeamMemberOfAProject(addProject("A0666", "to.f@mymail.com", "10/03/2023",
                "29/04/2024", 20, 100));
        resourceService.definedTeamMemberOfAProject(addProject("A0666", "tdc@mymail.com", "11/03/2023",
                "29/04/2024", 20, 100));
    }


    private ResourceCreationDTO addProject(final String code, final String email, final String startDate,
                                           final String endDate, final double costPerHour, final double percentageOfAllocation) {

        return new ResourceCreationDTO(email, code, startDate, endDate, costPerHour, percentageOfAllocation);
    }
}
