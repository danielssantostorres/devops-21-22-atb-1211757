package switchfive.project.capplicationServices.iRepositories;

import switchfive.project.ddomain.aggregates.request.Request;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;
import switchfive.project.ddomain.shared.valueObjects.Email;
import switchfive.project.ddomain.shared.valueObjects.RequestID;

import java.util.Optional;

public interface IRequestRepository {
    // boolean approveRequestIfRequestInStore(UserID selectedUser, ProfileID requestedProfile);

    // boolean deleteRequestOfOldProfileIfInStore(UserID selectedUser, ProfileID actualProfile);

    Request save(Request newRequest);

    Optional<Request> getRequestByUserIDAndProfileID(Email userID, ProfileDescription profileID);

    Optional<Request> getRequestByID(RequestID requestIDToFind);
}
