package switchfive.project.capplicationServices.assemblers.implAssemblers;

import org.springframework.stereotype.Component;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProjectDTO;
import switchfive.project.capplicationServices.assemblers.iAssemblers.IProjectAssembler;
import switchfive.project.ddomain.aggregates.project.Project;

@Component
public final class ImplProjectAssembler implements IProjectAssembler {

    protected ImplProjectAssembler() {
    }

    public ProjectDTO toDTO(final Project project) {
        ProjectDTO dto = new ProjectDTO();

        dto.setProjectCode(project.getCode());
        dto.setProjectName(project.getName());
        dto.setProjectDescription(project.getDescription());
        dto.setProjectBusinessSector(project.getBusinessSector());
        dto.setProjectNumberOfPlannedSprints(
                project.getNumberOfPlannedSprints());
        dto.setProjectSprintDuration(project.getSprintDuration());
        dto.setProjectBudget(project.getBudget());
        dto.setStartDate(project.getDates().startDate);
        dto.setEndDate(project.getDates().endDate);
        dto.setTypologyDescription(project.getTypologyDescription());
        dto.setCustomerName(project.getCustomerName());
        dto.setStatus(project.getStatus());

        return dto;
    }

    public ProjectDTO toDTOWithPM(final Project newProject,
                                  final String projectManagerEmail,
                                  final double projectManagerCostPerHour,
                                  final double projectManagerAllocation) {

        ProjectDTO dto = new ProjectDTO();

        dto.setProjectCode(newProject.getCode());
        dto.setProjectName(newProject.getName());
        dto.setProjectDescription(newProject.getDescription());
        dto.setProjectBusinessSector(newProject.getBusinessSector());
        dto.setProjectNumberOfPlannedSprints(
                newProject.getNumberOfPlannedSprints());
        dto.setProjectSprintDuration(newProject.getSprintDuration());
        dto.setProjectBudget(newProject.getBudget());
        dto.setStartDate(newProject.getDates().startDate);
        dto.setEndDate(newProject.getDates().endDate);
        dto.setUserEmail(projectManagerEmail);
        dto.setCostPerHour(projectManagerCostPerHour);
        dto.setPercentageOfAllocation(projectManagerAllocation);
        dto.setCustomerName(newProject.getCustomerName());
        dto.setTypologyDescription(newProject.getTypologyDescription());
        dto.setStatus(newProject.getStatus());

        return dto;
    }
}
