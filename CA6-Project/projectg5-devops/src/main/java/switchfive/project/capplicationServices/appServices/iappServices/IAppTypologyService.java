package switchfive.project.capplicationServices.appServices.iappServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.TypologyDTO;
import switchfive.project.ddomain.aggregates.typology.Typology;

import java.util.List;
import java.util.Optional;

public interface IAppTypologyService {

    Typology addNewTypology(String description);

    Optional<Typology> findTypologyByDescription(String description);

    List<TypologyDTO> getAll();
}
