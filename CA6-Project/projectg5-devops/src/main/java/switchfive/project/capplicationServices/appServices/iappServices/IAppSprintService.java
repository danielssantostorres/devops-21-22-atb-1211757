package switchfive.project.capplicationServices.appServices.iappServices;

import org.springframework.beans.PropertyValues;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SprintCreationDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SprintDTO;
import switchfive.project.ddomain.shared.valueObjects.SprintID;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface IAppSprintService {

    SprintDTO createAndSaveSprint(SprintCreationDTO sprintCreationDTO,
                                  String projectCode)
            throws ParseException;

    Optional<SprintDTO> getSprintBySprintId(SprintID sprintID)
            throws ParseException;

    Optional<List<SprintDTO>> getSprintsByProjectCode(String projectCode) throws ParseException;
}
