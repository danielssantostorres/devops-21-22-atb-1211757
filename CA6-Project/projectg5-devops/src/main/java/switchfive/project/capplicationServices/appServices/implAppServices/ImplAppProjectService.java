package switchfive.project.capplicationServices.appServices.implAppServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;
import switchfive.project.binterfaceAdapters.controllers.implControllers.ImplTaskController;
import switchfive.project.binterfaceAdapters.controllers.implControllers.ImplUserStoryController;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProjectDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.StatusOfActivitiesDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UpdateProjectDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppProjectService;
import switchfive.project.capplicationServices.appServices.iappServices.IAppResourceService;
import switchfive.project.capplicationServices.assemblers.iAssemblers.IProjectAssembler;
import switchfive.project.capplicationServices.iRepositories.*;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.aggregates.task.Task;
import switchfive.project.ddomain.aggregates.userStory.UserStory;
import switchfive.project.ddomain.factories.iFactories.IProjectBuilder;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Implementation of project application-service interface.
 */
@Service
public final class ImplAppProjectService implements
        IAppProjectService {
    private final IProjectRepository projectRepository;
    private final IUserRepository userRepository;
    private final ITypologyRepository typologyRepository;
    private final ICustomerRepository customerRepository;
    private final IResourceRepository resourceRepository;
    private final IProjectBuilder projectBuilder;
    private final IAppResourceService resourceService;
    private final IProjectAssembler projectAssembler;
    private final IUserStoryRepository userStoryRepository;
    private final ITaskRepository taskRepository;


    @Autowired
    public ImplAppProjectService(
            final IProjectRepository projectRepositoryInput,
            final IUserRepository userRepositoryInput,
            final ITypologyRepository typologyRepositoryInput,
            final ICustomerRepository customerRepositoryInput,
            final IResourceRepository resourceRepositoryInput,
            final IProjectBuilder iProjectBuilderInput,
            final IAppResourceService iResourceServiceInput,
            final IProjectAssembler projectAssembler,
            IUserStoryRepository userStoryRepository,
            ITaskRepository taskRepository) {

        this.projectRepository = projectRepositoryInput;
        this.userRepository = userRepositoryInput;
        this.typologyRepository = typologyRepositoryInput;
        this.customerRepository = customerRepositoryInput;
        this.resourceRepository = resourceRepositoryInput;
        this.projectBuilder = iProjectBuilderInput;
        this.resourceService = iResourceServiceInput;
        this.projectAssembler = projectAssembler;
        this.userStoryRepository = userStoryRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Method to get a project dto.
     */
    public Optional<ProjectDTO> getProjectDTO(final String code)
            throws ParseException {
        ProjectDTO dto = null;
        Project project;
        Resource pm;

        ProjectCode projectCode = ProjectCode.createProjectCode(code);

        if (this.projectRepository.findByCode(projectCode).isPresent()) {

            project = this.projectRepository.findByCode(projectCode).get();

            pm = this.resourceRepository.getProjectManager(projectCode).get();

            dto = this.projectAssembler.toDTOWithPM(project,
                    pm.getUserID().getUserEmail(), pm.getCostPerHour(),
                    pm.getAllocation());
        }

        return Optional.ofNullable(dto);
    }


    public List<ProjectDTO> getAllProjects() throws ParseException {
        List<ProjectDTO> projectDTOList = new ArrayList<>();
        List<Project> projectList = this.projectRepository.findAll();

        for (Project each : projectList) {
            ProjectDTO dto = this.projectAssembler.toDTO(each);
            projectDTOList.add(dto);
        }

        return projectDTOList;
    }


    /**
     * Method to create and save new project.
     *
     * @return ProjectDTO.
     */
    public ProjectDTO createAndSaveProject(final ProjectDTO dtoInput)
            throws ParseException, NoSuchAlgorithmException {

        Email projectManagerEmail = Email.createEmail(dtoInput.getUserEmail());
        ProjectCode code =
                ProjectCode.createProjectCode(dtoInput.getProjectCode());
        TypologyDescription typologyDescription =
                TypologyDescription.create(dtoInput.getTypologyDescription());
        CustomerName customerName =
                CustomerName.create(dtoInput.getCustomerName());
        ProjectName name = ProjectName.create(dtoInput.getProjectName());
        ProjectDescription description =
                ProjectDescription.create(dtoInput.getProjectDescription());
        ProjectBudget budget =
                ProjectBudget.create(dtoInput.getProjectBudget());
        ProjectBusinessSector businessSector = ProjectBusinessSector.create(
                dtoInput.getProjectBusinessSector());
        ProjectNumberOfPlannedSprints numberOfPlannedSprints =
                ProjectNumberOfPlannedSprints.create(
                        dtoInput.getProjectNumberOfPlannedSprints());
        ProjectSprintDuration sprintDuration = ProjectSprintDuration.create(
                dtoInput.getProjectSprintDuration());
        Time dates =
                Time.create(dtoInput.getStartDate(), dtoInput.getEndDate());
        ResourceCostPerHour projectManagerCostPerHour =
                ResourceCostPerHour.create(dtoInput.getCostPerHour());
        ResourcePercentageOfAllocation projectManagerAllocation =
                ResourcePercentageOfAllocation.create(
                        dtoInput.getPercentageOfAllocation());
        ProjectStatus projectStatus = ProjectStatus.Planned;


        if (this.projectRepository.existsCode(code)) {
            throw new IllegalArgumentException("Project already exists.");
        }

        if (!this.customerRepository.existsName(customerName)) {
            throw new IllegalArgumentException("Customer doesn't exist.");
        }

        if (!this.typologyRepository.existsDescription(typologyDescription)) {
            throw new IllegalArgumentException("Typology doesn't exist.");
        }

        if (this.userRepository.findUserByEmail(dtoInput.getUserEmail())
                .isEmpty()) {
            throw new IllegalArgumentException("Email doesn't exist.");
        }


        this.projectBuilder.addCodeNameDescriptionCustomerStatus(code, name,
                description, customerName, projectStatus);
        this.projectBuilder.addBudget(budget);
        this.projectBuilder.addBusinessSector(businessSector);
        this.projectBuilder.addNumberOfPlannedSprints(numberOfPlannedSprints);
        this.projectBuilder.addSprintDuration(sprintDuration);
        this.projectBuilder.addTime(dates);
        this.projectBuilder.addTypologyDescription(typologyDescription);
        this.projectBuilder.addStatus(projectStatus);
        Project newProject = this.projectBuilder.getProject();


        Optional<Resource> resource =
                this.resourceService.createProjectManager(projectManagerEmail,
                        code, dates, projectManagerCostPerHour,
                        projectManagerAllocation);

        if (resource.isEmpty()) {
            throw new IllegalArgumentException("Resource creation fails");
        }

        Resource projectManager = resource.get();


        this.projectRepository.saveProject(newProject);
        this.resourceRepository.saveResource(projectManager);


        return this.projectAssembler.toDTOWithPM(newProject,
                dtoInput.getUserEmail(), dtoInput.getCostPerHour(),
                dtoInput.getPercentageOfAllocation());

        //return  this.projectAssembler.toDTO(newProject);
    }


    /**
     * @param projectCode projectCode as string
     * @param dtoInput    object with update data.
     * @return dto with updated project data.<p></p>
     * <p>When we use findById() to retrieve an entity within a transactional
     * method, the returned entity is managed by the persistence provider.</p>
     * So, any change to that entity will be automatically persisted in the
     * database, regardless of whether we are invoking the save() method.
     */


    public Optional<ProjectDTO> updateProject(final String projectCode,
                                              final UpdateProjectDTO dtoInput)
            throws ParseException {
        Project thisProject;

        ProjectCode code = ProjectCode.createProjectCode(projectCode);
        ProjectName name = ProjectName.create(dtoInput.getProjectName());
        ProjectDescription description =
                ProjectDescription.create(dtoInput.getProjectDescription());
        ProjectBusinessSector business = ProjectBusinessSector.create(
                dtoInput.getProjectBusinessSector());
        Time dates =
                Time.create(dtoInput.getStartDate(), dtoInput.getEndDate());
        ProjectNumberOfPlannedSprints sprintNumber =
                ProjectNumberOfPlannedSprints.create(
                        dtoInput.getProjectNumberOfPlannedSprints());
        ProjectSprintDuration sprintDuration = ProjectSprintDuration.create(
                dtoInput.getProjectSprintDuration());
        ProjectBudget budget =
                ProjectBudget.create(dtoInput.getProjectBudget());
        CustomerName customerName =
                CustomerName.create(dtoInput.getCustomerName());
        TypologyDescription typologyDescription =
                TypologyDescription.create(dtoInput.getTypologyDescription());
        ProjectStatus projectStatus = ProjectStatus.valueOf(dtoInput.getStatus());

        if (this.projectRepository.findByCode(code).isEmpty()) {
            return Optional.empty();
        } else {
            thisProject = this.projectRepository.findByCode(code).get();
        }


        if (this.customerRepository.existsName(customerName)) {

            if (this.typologyRepository.existsDescription(
                    typologyDescription)) {

                thisProject.addName(name);
                thisProject.addDescription(description);
                thisProject.addBusinessSector(business);
                thisProject.addDates(dates);
                thisProject.addNumberOfPlannedSprints(sprintNumber);
                thisProject.addSprintDuration(sprintDuration);
                thisProject.addBudget(budget);
                thisProject.addCustomer(customerName);
                thisProject.addTypologyDescription(typologyDescription);
                thisProject.addStatus(projectStatus);

                this.projectRepository.saveProject(thisProject);

            } else {
                throw new IllegalArgumentException("Typology does not exist!");
            }

        } else {
            throw new IllegalArgumentException("Customer does not exist!");
        }

        return Optional.of(this.projectAssembler.toDTO(thisProject));
    }

    @Override
    public List<StatusOfActivitiesDTO> getStatusOfActivities(String projectCode) throws ParseException {
        List<StatusOfActivitiesDTO> statusOfActivitiesDTOList =
                new ArrayList<>();

        statusOfActivitiesDTOList = getUserStoriesInProjectStatus(
                projectCode, statusOfActivitiesDTOList);

        statusOfActivitiesDTOList = getTasksInProjectStatus(
                projectCode, statusOfActivitiesDTOList);

        return statusOfActivitiesDTOList;
    }

    public List<StatusOfActivitiesDTO> getUserStoriesInProjectStatus(
            String projectCode,
            List<StatusOfActivitiesDTO> statusOfActivitiesDTOList) {

        ProjectCode projectCodeObj = ProjectCode.createProjectCode(projectCode);
        List<UserStory> userStoryList = userStoryRepository.
                getUserStoryListInProjectOrderedByStatus(projectCodeObj);

        for (UserStory userStory : userStoryList) {
            String typeOfActivity = "User Story";
            String activityCode = userStory.getUserStoryCode();
            String activityStatus = userStory.getStatus();

            StatusOfActivitiesDTO statusOfActivitiesDTO = new StatusOfActivitiesDTO(
                    typeOfActivity, activityCode, activityStatus);

            Link link = linkTo(methodOn(ImplUserStoryController.class)
                    .getUserStory(projectCode,
                            activityCode)).withSelfRel();


            statusOfActivitiesDTO.add(link);

            statusOfActivitiesDTOList.add(statusOfActivitiesDTO);
        }

        return statusOfActivitiesDTOList;
    }

    public List<StatusOfActivitiesDTO> getTasksInProjectStatus(
            String projectCode,
            List<StatusOfActivitiesDTO> statusOfActivitiesDTOList) throws ParseException {

        ProjectCode projectCodeObj = ProjectCode.createProjectCode(projectCode);
        List<Task> taskList = taskRepository.
                findTaskInProjectOrderedByStatus(projectCodeObj);

        for (Task task : taskList) {
            String typeOfActivity = "Task";
            String activityCode = task.getIdTask().getTaskCode();
            String activityStatus = task.getTaskStatus();

            StatusOfActivitiesDTO statusOfActivitiesDTO = new StatusOfActivitiesDTO(
                    typeOfActivity, activityCode, activityStatus);

            Link link = linkTo(methodOn(ImplTaskController.class).getTask(activityCode,
                    projectCode)).withSelfRel();
            statusOfActivitiesDTO.add(link);

            statusOfActivitiesDTOList.add(statusOfActivitiesDTO);
        }

        return statusOfActivitiesDTOList;
    }


}
