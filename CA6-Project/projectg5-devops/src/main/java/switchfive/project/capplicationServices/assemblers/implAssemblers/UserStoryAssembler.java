package switchfive.project.capplicationServices.assemblers.implAssemblers;

import switchfive.project.binterfaceAdapters.dataTransferObjects.MoveUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserStoryDTO;
import switchfive.project.ddomain.aggregates.userStory.UserStory;

/**
 * UserStory Assembler : UserStory -> UserStoryDTO
 */
public class UserStoryAssembler {

    public UserStoryAssembler() {
    }

    /**
     * Map method that transforms a UserStory object into a UserStoryDTO object.
     *
     * @param userStoryInput UserStory object
     * @return UserStoryDTO object
     */
    public static UserStoryDTO toUserStoryDTO(UserStory userStoryInput) {

        String code = userStoryInput.getUserStoryCode();
        String description = userStoryInput.getUserStoryDescription();
        Integer effort;
        effort = userStoryInput.getEffort();
        /*try {
            effort = userStoryInput.getEffort();
        } catch (Exception e) {
            effort = null;
        }*/
        Integer priority = userStoryInput.getPriority();
        String status = userStoryInput.getStatus();

        UserStoryDTO userStoryDTO = new UserStoryDTO();
        userStoryDTO.setCode(code);
        userStoryDTO.setEffort(effort);
        userStoryDTO.setPriority(priority);
        userStoryDTO.setDescription(description);
        userStoryDTO.setStatus(status);

        String parentUSCode = userStoryInput.getParentUserStory();
        userStoryDTO.setParentUserStoryCode(parentUSCode);

        return userStoryDTO;
    }

    /**
     * Map method that transforms a UserStory object into a UserStoryDTO object.
     *
     * @param userStoryInput UserStory object
     * @return UserStoryDTO object
     */
    public static MoveUserStoryDTO toUserStoryDTOComplete(UserStory userStoryInput) {

        MoveUserStoryDTO userStoryDTO = new MoveUserStoryDTO();

        userStoryDTO.setProjectCode(userStoryInput.getProjectCodeOfUserStory());
        userStoryDTO.setCode(userStoryInput.getUserStoryCode());
        userStoryDTO.setEffort(userStoryInput.getEffort());
        userStoryDTO.setPriority(userStoryInput.getPriority());
        userStoryDTO.setDescription(userStoryInput.getUserStoryDescription());
        userStoryDTO.setSprintID(userStoryInput.getSprintID().getSprintNumber());
        userStoryDTO.setStatus(userStoryInput.getStatus());

        return userStoryDTO;
    }
}
