package switchfive.project.capplicationServices.iRepositories;

import switchfive.project.ddomain.aggregates.customer.Customer;
import switchfive.project.ddomain.shared.valueObjects.CustomerName;

import java.util.List;
import java.util.Optional;

public interface ICustomerRepository {

    Optional<Customer> findCustomerByName(CustomerName customerName);

    void saveCustomer(Customer customer);

    boolean existsName(CustomerName customerName);

    List<Customer> findAll();
}
