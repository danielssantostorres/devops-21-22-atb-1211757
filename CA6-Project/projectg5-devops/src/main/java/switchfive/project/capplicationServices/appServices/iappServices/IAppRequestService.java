package switchfive.project.capplicationServices.appServices.iappServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.RequestDTO;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

public interface IAppRequestService {
    Optional<RequestDTO> createNewProfileRequest(String userID, String profileDescription) throws NoSuchAlgorithmException;

    Optional<RequestDTO> getRequestDTO(String requestID);
}
