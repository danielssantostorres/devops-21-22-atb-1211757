package switchfive.project.capplicationServices.appServices.implAppServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceCreationDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppResourceService;
import switchfive.project.capplicationServices.assemblers.implAssemblers.ResourceAssembler;
import switchfive.project.capplicationServices.iRepositories.IProjectRepository;
import switchfive.project.capplicationServices.iRepositories.IResourceRepository;
import switchfive.project.capplicationServices.iRepositories.IUserRepository;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.aggregates.user.User;
import switchfive.project.ddomain.domainServices.implDomainServices.ImplResourceDomainService;
import switchfive.project.ddomain.factories.iFactories.IResourceFactory;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Optional;

@Service
public final class ImplAppResourceService implements IAppResourceService {
    /**
     * ProjectStore interface. Constructor injected.
     */
    private final IProjectRepository iProjectStore;
    /**
     * UserRepository interface. Constructor injected.
     */
    private final IUserRepository iUserRepository;
    /**
     * ResourceRepository interface. Constructor injected.
     */
    private final IResourceRepository iResourceRepository;
    /**
     * Resource domain-service interface. Constructor injected.
     */
    private final ImplResourceDomainService implResourceDomainService;
    /**
     * ResourceFactory interface. Constructor injected.
     */
    private final IResourceFactory resourceFactory;

    @Autowired
    public ImplAppResourceService(
            final IProjectRepository iProjectStoreInput,
            final IUserRepository iUserRepositoryInput,
            final IResourceRepository iResourceRepositoryInput,
            final ImplResourceDomainService implResourceDomainServiceInput,
            final IResourceFactory resourceFactoryInput) {
        this.iProjectStore = iProjectStoreInput;
        this.iUserRepository = iUserRepositoryInput;
        this.iResourceRepository = iResourceRepositoryInput;
        this.implResourceDomainService = implResourceDomainServiceInput;
        this.resourceFactory = resourceFactoryInput;
    }

    /**
     * Method to find user by userID.
     */
    public Optional<User> findUser(final String userID) throws NoSuchAlgorithmException {
        return this.iUserRepository.findUserByEmail(userID);
    }

    /**
     * Method to find ProjectManager by role.
     */
    public Optional<Resource> getProjectManager(final ProjectCode code)
            throws ParseException {
        return this.iResourceRepository.getProjectManager(code);
    }

    @Override
    public Optional<ResourceDTO> getResourceDTO(String resourceID)
            throws ParseException {
        ResourceID resourceIDToFind = ResourceID.createResourceID(resourceID);

        Optional<Resource> resourceInRepo =
                iResourceRepository.getResourceByID(resourceIDToFind);

        ResourceDTO resourceDTO = null;
        if (resourceInRepo.isPresent()) {
            resourceDTO = ResourceAssembler.toDto(resourceInRepo.get());
        }
        return Optional.ofNullable(resourceDTO);
    }

    /**
     * Method to create and save projectManager of a project.
     */
    @Override
    public Optional<Resource> createProjectManager(final Email email,
                                                   final ProjectCode projectCode,
                                                   final Time projectTime,
                                                   final ResourceCostPerHour costPerHour,
                                                   final ResourcePercentageOfAllocation allocation)
            throws ParseException, NoSuchAlgorithmException {

        Resource newResource = null;

        if (this.iUserRepository.findUserByEmail(email.getUserEmail()).isPresent()) {

            ResourceID resourceID = ResourceID.createResourceID();
            Role role = Role.ProjectManager;

            newResource =
                    this.resourceFactory.createResource(resourceID, email,
                            projectCode, projectTime, costPerHour, allocation,
                            role);
        }

        return Optional.ofNullable(newResource);
    }

    /**
     * Method to create and save team member of a project.
     */
    public Optional<ResourceDTO> definedTeamMemberOfAProject(
            ResourceCreationDTO dto)
            throws ParseException, NoSuchAlgorithmException {
        ResourceDTO resourceDTO = null;
        ProjectCode projectCodeVO = ProjectCode.createProjectCode(dto.projectCodeDto);

        Optional<Project> optproject = this.iProjectStore.findByCode(projectCodeVO);
        Optional<User> userExists = this.iUserRepository.findUserByEmail(dto.userIdDto);

        if (optproject.isPresent() && userExists.isPresent()) {

            ArrayList<Resource> resources =
                    this.iResourceRepository.getResourcesByProjectCode(dto.projectCodeDto);

            Project project = optproject.get();

            if (this.implResourceDomainService.validateNewTeamMemberResource(
                    resources, project, dto)) {
                ProjectCode projectCode =
                        ProjectCode.createProjectCode(dto.projectCodeDto);
                ResourceID resourceID = ResourceID.createResourceID();
                Email userID = Email.createEmail(dto.userIdDto);
                Time dates = Time.create(dto.startDateDto,
                        dto.endDateDto);
                ResourceCostPerHour costPerHour =
                        ResourceCostPerHour.create(dto.costPerHourDto);
                ResourcePercentageOfAllocation allocation =
                        ResourcePercentageOfAllocation.create(
                                dto.percentageOfAllocationDto);
                Role role = Role.TeamMember;

                Resource newResource =
                        this.resourceFactory.createResource(resourceID, userID,
                                projectCode, dates, costPerHour, allocation,
                                role);

                this.iResourceRepository.saveResource(newResource);
                resourceDTO = ResourceAssembler.toDto(newResource);
            }

        }

        return Optional.ofNullable(resourceDTO);
    }

    /**
     * Method to create and save product owner of a project.
     */
    @Override
    public Optional<ResourceDTO> definedProductOwnerOfAProject(final ResourceCreationDTO dto) throws ParseException, NoSuchAlgorithmException {

        ResourceDTO resourceDTO = null;
        ProjectCode projectCodeVO = ProjectCode.createProjectCode(dto.projectCodeDto);

        if (this.iProjectStore.findByCode(projectCodeVO).isPresent()
                && this.iUserRepository.findUserByEmail(dto.userIdDto).isPresent()) {

            ArrayList<Resource> resources =
                    this.iResourceRepository.getResourcesByProjectCode(dto.projectCodeDto);

            Project project =
                    this.iProjectStore.findByCode(projectCodeVO)
                            .get();

            if (this.implResourceDomainService.validateNewProductOwnerResource(
                    resources, project, dto)) {
                ProjectCode projectCode =
                        ProjectCode.createProjectCode(dto.projectCodeDto);
                ResourceID resourceID = ResourceID.createResourceID();
                Email userID = Email.createEmail(dto.userIdDto);
                Time dates = Time.create(dto.startDateDto,
                        dto.endDateDto);
                ResourceCostPerHour costPerHour =
                        ResourceCostPerHour.create(dto.costPerHourDto);
                ResourcePercentageOfAllocation allocation =
                        ResourcePercentageOfAllocation.create(
                                dto.percentageOfAllocationDto);
                Role role = Role.ProductOwner;

                Resource newResource =
                        this.resourceFactory.createResource(resourceID, userID,
                                projectCode, dates, costPerHour, allocation,
                                role);

                this.iResourceRepository.saveResource(newResource);
                resourceDTO = ResourceAssembler.toDto(newResource);
            }

        }

        return Optional.ofNullable(resourceDTO);
    }

    /**
     * Method to create and save scrum master of a project.
     */
    @Override
    public Optional<ResourceDTO> definedScrumMasterOfAProject(final ResourceCreationDTO dto) throws ParseException, NoSuchAlgorithmException {

        ResourceDTO resourceDTO = null;
        ProjectCode projectCodeVO = ProjectCode.createProjectCode(dto.projectCodeDto);

        if (this.iProjectStore.findByCode(projectCodeVO).isPresent()
                && this.iUserRepository.findUserByEmail(dto.userIdDto).isPresent()) {

            ArrayList<Resource> resources =
                    this.iResourceRepository.getResourcesByProjectCode(dto.projectCodeDto);

            Project project =
                    this.iProjectStore.findByCode(projectCodeVO)
                            .get();

            if (this.implResourceDomainService.validateNewScrumMasterResource(
                    resources, project, dto)) {
                ProjectCode projectCode =
                        ProjectCode.createProjectCode(dto.projectCodeDto);
                ResourceID resourceID = ResourceID.createResourceID();
                Email userID = Email.createEmail(dto.userIdDto);
                Time dates = Time.create(dto.startDateDto,
                        dto.endDateDto);
                ResourceCostPerHour costPerHour =
                        ResourceCostPerHour.create(dto.costPerHourDto);
                ResourcePercentageOfAllocation allocation =
                        ResourcePercentageOfAllocation.create(
                                dto.percentageOfAllocationDto);
                Role role = Role.ScrumMaster;

                Resource newResource =
                        this.resourceFactory.createResource(resourceID, userID,
                                projectCode, dates, costPerHour, allocation,
                                role);

                this.iResourceRepository.saveResource(newResource);
                resourceDTO = ResourceAssembler.toDto(newResource);
            }

        }

        return Optional.ofNullable(resourceDTO);
    }

}

