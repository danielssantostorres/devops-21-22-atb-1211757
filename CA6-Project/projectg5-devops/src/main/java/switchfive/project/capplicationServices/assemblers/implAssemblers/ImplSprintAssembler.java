package switchfive.project.capplicationServices.assemblers.implAssemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SprintDTO;
import switchfive.project.capplicationServices.assemblers.iAssemblers.ISprintAssembler;
import switchfive.project.ddomain.aggregates.sprint.Sprint;

@Component
public class ImplSprintAssembler implements ISprintAssembler {

    @Autowired
    protected ImplSprintAssembler() {
    }

    public SprintDTO toDTO(Sprint sprint) {
        SprintDTO output = new SprintDTO();

        output.setProjectCode(sprint.getProjectCode());
        output.setSprintNumber(sprint.getSprintNumber());
        output.setStartDate(sprint.getDates().startDate);
        output.setEndDate(sprint.getDates().endDate);
        output.setDescription(sprint.getDescription());
        output.setStatus(sprint.getStatus());

        return output;
    }
}
