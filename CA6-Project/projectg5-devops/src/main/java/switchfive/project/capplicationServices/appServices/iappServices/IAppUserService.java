package switchfive.project.capplicationServices.appServices.iappServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.AllocatedProjectsDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ChangePasswordDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SearchUserDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserDTO;
import switchfive.project.capplicationServices.iRepositories.IUserRepository;
import switchfive.project.ddomain.aggregates.user.User;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface IAppUserService {
    UserDTO createAndSaveUser(UserDTO userDTO) throws NoSuchAlgorithmException;

    Optional<SearchUserDTO> validateAndAddProfile(String email, String profileID) throws NoSuchAlgorithmException;

    public Optional<SearchUserDTO> validateAndRemoveProfile(String email, String profileID) throws NoSuchAlgorithmException;

    List<SearchUserDTO> getUsersByEmail(String email) throws NoSuchAlgorithmException;

    List<SearchUserDTO> getUsersByProfile(String profileId) throws NoSuchAlgorithmException;

    IUserRepository getUserStore();

    //new
    Optional<UserDTO> changePassword (ChangePasswordDTO passwordDTO) throws NoSuchAlgorithmException;

    Optional<SearchUserDTO> setUserToInactive(String userID) throws NoSuchAlgorithmException;

    Optional<SearchUserDTO> setUserToActive(String userID) throws NoSuchAlgorithmException;

    Optional<UserDTO> updateUser(UserDTO userDTO) throws NoSuchAlgorithmException;

    Optional<UserDTO> activateAccount(UserDTO userDTO) throws NoSuchAlgorithmException;

    Optional<User> findById(String id) throws NoSuchAlgorithmException;

    Optional<SearchUserDTO> getUserByEmail(String email) throws NoSuchAlgorithmException;

    Optional<List<AllocatedProjectsDTO>> findAllocatedProjects(String email) throws ParseException;

    Optional<List<UserDTO>> activateAll() throws NoSuchAlgorithmException;
}
