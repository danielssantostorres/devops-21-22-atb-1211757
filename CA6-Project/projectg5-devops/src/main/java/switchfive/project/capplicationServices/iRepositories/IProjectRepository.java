package switchfive.project.capplicationServices.iRepositories;

import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface IProjectRepository {
    void saveProject(Project entity);

    Optional<Project> findByCode(ProjectCode code) throws ParseException;

    boolean existsCode(ProjectCode code);

    List<Project> findAll() throws ParseException;
}
