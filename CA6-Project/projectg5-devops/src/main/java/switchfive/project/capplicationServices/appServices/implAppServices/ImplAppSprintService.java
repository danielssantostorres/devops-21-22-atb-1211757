package switchfive.project.capplicationServices.appServices.implAppServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SprintCreationDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SprintDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppSprintService;
import switchfive.project.capplicationServices.assemblers.iAssemblers.ISprintAssembler;
import switchfive.project.capplicationServices.iRepositories.IProjectRepository;
import switchfive.project.capplicationServices.iRepositories.ISprintRepository;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.sprint.Sprint;
import switchfive.project.ddomain.domainServices.iDomainServices.ISprintDomainService;
import switchfive.project.ddomain.factories.iFactories.ISprintFactory;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ImplAppSprintService implements IAppSprintService {
    private ISprintRepository sprintRepository;
    private IProjectRepository projectRepository;
    private ISprintFactory sprintFactory;
    private ISprintDomainService sprintDomainService;
    private ISprintAssembler sprintAssembler;

    @Autowired
    public ImplAppSprintService(ISprintRepository sprintRepository,
                                IProjectRepository projectRepository,
                                ISprintFactory sprintFactory,
                                ISprintDomainService sprintDomainService,
                                ISprintAssembler sprintAssembler) {
        this.sprintRepository = sprintRepository;
        this.projectRepository = projectRepository;
        this.sprintFactory = sprintFactory;
        this.sprintDomainService = sprintDomainService;
        this.sprintAssembler = sprintAssembler;
    }

    public Optional<SprintDTO> getSprintBySprintId(final SprintID id)
            throws ParseException {

        ProjectCode code = ProjectCode.createProjectCode(id.getProjectCode());

        if (!this.projectRepository.existsCode(code)) {
            throw new IllegalArgumentException("Selected project does not exist!");
        }

        if (this.sprintRepository.findBySprintID(id).isEmpty()) {
            return Optional.empty();
        }

        Sprint selectedSprint = this.sprintRepository.findBySprintID(id).get();

        return Optional.of(this.sprintAssembler.toDTO(selectedSprint));
    }

    public Optional<List<SprintDTO>> getSprintsByProjectCode(String code) throws ParseException {
        SprintDTO eachSprintDTO;
        List<SprintDTO> sprintsDTO = new ArrayList<>();

        ProjectCode projectCode = ProjectCode.createProjectCode(code);

        if (!this.projectRepository.existsCode(projectCode)) {
            throw new IllegalArgumentException("Selected project does not exist!");
        }

        if (sprintRepository.findAllSprintsByProjectCode(projectCode).isEmpty()) {
            return Optional.empty();
        }

        List<Sprint> sprints = sprintRepository.findAllSprintsByProjectCode(projectCode).get();

        for (Sprint each : sprints) {
            eachSprintDTO = sprintAssembler.toDTO(each);
            sprintsDTO.add(eachSprintDTO);
        }

        return Optional.of(sprintsDTO);
    }

    public SprintDTO createAndSaveSprint(final SprintCreationDTO dto, final String codeByPath)
            throws ParseException {
        int index = 0;
        Time previousSprintTime = null;

        SprintStatus status = SprintStatus.PLANNED;
        ProjectCode code = ProjectCode.createProjectCode(codeByPath);
        Time newSprintTime = Time.create(dto.getStartDate(), dto.getEndDate());
        SprintDescription description = SprintDescription.create(dto.getDescription());

        if (!projectRepository.existsCode(code)) {
            throw new IllegalArgumentException("Selected project does not exist!");
        }

        Project project = this.projectRepository.findByCode(code).get();

        if (sprintRepository.findAllSprintsByProjectCode(code).isPresent()) {
            index = (int) this.sprintRepository.countSprintsByProjectCode(code);

            SprintID previousSprintID = SprintID.createSprintID(codeByPath, index);

            Sprint previousSprint = sprintRepository.findBySprintID(previousSprintID).get();

            String previousSprintStartDate = previousSprint.getDates().startDate;
            String previousSprintEndDate = previousSprint.getDates().endDate;
            previousSprintTime = Time.create(previousSprintStartDate, previousSprintEndDate);
        }

        if (!sprintDomainService.sprintIsValid(previousSprintTime, newSprintTime, project)) {
            throw new IllegalArgumentException("Invalid dates!");
        }

        SprintNumber number = SprintNumber.create(index + 1);
        SprintID id = SprintID.createSprintID(codeByPath, index + 1);

        Sprint newSprint = this.sprintFactory.create(id, code, number, newSprintTime, description, status);

        Sprint saved = this.sprintRepository.saveSprint(newSprint);

        return this.sprintAssembler.toDTO(saved);
    }
}
