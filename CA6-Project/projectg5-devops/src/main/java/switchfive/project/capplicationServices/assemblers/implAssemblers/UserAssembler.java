package switchfive.project.capplicationServices.assemblers.implAssemblers;

import switchfive.project.binterfaceAdapters.dataTransferObjects.SearchUserDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserDTO;
import switchfive.project.ddomain.aggregates.user.User;
import switchfive.project.ddomain.factories.iFactories.IUserFactory;

import java.util.ArrayList;
import java.util.List;

public class UserAssembler {

    public static List<SearchUserDTO> toSearchDTOList(List<User> userList) {
        List<SearchUserDTO> resultList = new ArrayList<>();
        for (User selectedUser : userList) {
            SearchUserDTO userDTO = UserAssembler.toSearchUserDTO(selectedUser);
            resultList.add(userDTO);
        }
        return resultList;
    }


    public static SearchUserDTO toSearchUserDTO(User user) {
        return SearchUserDTO.createSearchUserDTO(user.getEmail(),
                user.getProfileListAsDescription(),
                user.getAccountStatus(),
                user.getUserName().getUserName(),
                user.getFunctionDescription());
    }

    public static UserDTO toDTO(User user) {
        UserDTO dto = new UserDTO();
        dto.userName = user.getUserName().getUserName();
        dto.email = user.getEmail();
        dto.userFunction = user.getFunctionDescription();
        dto.activation = user.getActivation().isActivated();
        return dto;
    }

    public static UserDTO toDTOWithPassword(User user) {
        UserDTO dto = new UserDTO();
        dto.userName = user.getUserName().getUserName();
        dto.email = user.getEmail();
        dto.password = user.getPassword().getUserPassword();
        dto.activation = user.isUserActive();
        return dto;
    }

}
