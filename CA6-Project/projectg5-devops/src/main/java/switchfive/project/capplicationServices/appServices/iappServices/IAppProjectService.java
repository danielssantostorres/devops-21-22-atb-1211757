package switchfive.project.capplicationServices.appServices.iappServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.ProjectDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.StatusOfActivitiesDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UpdateProjectDTO;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

/**
 * Project application service.
 */
public interface IAppProjectService {

    /**
     * @return Optional Project data transfer object (dto)
     */
    Optional<ProjectDTO> getProjectDTO(String code) throws ParseException;

    /**
     * @param dto that represents project.
     * @return dto if project is created and saved.
     */
    ProjectDTO createAndSaveProject(ProjectDTO dto)
            throws ParseException, NoSuchAlgorithmException;

    /**
     * @param projectCode projectCode as string
     * @param dto         object with update data.
     * @return dto if project is successfully updated.
     */
    Optional<ProjectDTO> updateProject(String projectCode, UpdateProjectDTO dto)
            throws ParseException;

    List<ProjectDTO> getAllProjects() throws ParseException;

    List<StatusOfActivitiesDTO> getStatusOfActivities(String projectCode) throws ParseException;
}
