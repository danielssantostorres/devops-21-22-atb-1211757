package switchfive.project.capplicationServices.iRepositories;

import switchfive.project.ddomain.aggregates.typology.Typology;
import switchfive.project.ddomain.shared.valueObjects.TypologyDescription;


import java.util.List;
import java.util.Optional;

public interface ITypologyRepository {

    Optional<Typology> findTypology(TypologyDescription typologyDescription);

    void saveNewTypology(Typology newTypology);

    boolean existsDescription(TypologyDescription typologyDescription);

    List<Typology> findAll();
}
