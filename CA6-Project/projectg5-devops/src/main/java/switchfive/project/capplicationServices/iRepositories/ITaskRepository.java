package switchfive.project.capplicationServices.iRepositories;

import switchfive.project.ainfrastructure.persistence.data.TaskIDJPA;
import switchfive.project.ddomain.aggregates.task.Task;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.TaskID;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface ITaskRepository {

    String nextTaskCode (ProjectCode projectCode);

    Optional<Task> findTaskById (TaskIDJPA taskId) throws ParseException;

    Task save(Task task) throws ParseException;

    List<Task> findAllTasks() throws ParseException;

    List<Task> findTaskInProjectOrderedByStatus(ProjectCode projectCode) throws ParseException;
}
