package switchfive.project.capplicationServices.assemblers.iAssemblers;

import switchfive.project.binterfaceAdapters.dataTransferObjects.CustomerDTO;
import switchfive.project.ddomain.aggregates.customer.Customer;

public interface ICustomerAssembler {
    CustomerDTO toDTO(Customer customer);
}
