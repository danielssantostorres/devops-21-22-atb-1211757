package switchfive.project.capplicationServices.iRepositories;


import switchfive.project.ainfrastructure.persistence.data.UserJPA;
import switchfive.project.ddomain.aggregates.user.User;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

public interface IUserRepository {

    UserJPA save(User user);

    List<User> findUsersByProfile(String profileId) throws NoSuchAlgorithmException;

    List<User> findUsersByEmail(final String email) throws NoSuchAlgorithmException;

    Optional<User> addProfile (String email, String profileID) throws NoSuchAlgorithmException;

    Optional<User> removeProfile (String email, String profileID) throws NoSuchAlgorithmException;

    Optional<User> findUserByEmail(final String email) throws NoSuchAlgorithmException;

    Optional<User> update(User user) throws NoSuchAlgorithmException;

    Optional<List<User>> findAllUsers() throws NoSuchAlgorithmException;

}
