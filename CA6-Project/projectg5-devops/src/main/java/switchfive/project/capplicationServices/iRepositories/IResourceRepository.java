package switchfive.project.capplicationServices.iRepositories;

import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.ResourceID;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface IResourceRepository {
    ArrayList<Resource> getResourcesByProjectCode(String projectCode) throws ParseException;

    /**
     * Method to save a new resource of a project.
     */
    void saveResource(Resource newResource);

    /**
     * Method to get project manager of a project.
     */
    Optional<Resource> getProjectManager(ProjectCode code) throws ParseException;

    /**
     * Method to get resource by id.
     */
    Optional<Resource> getResourceByID(ResourceID resourceIDToFind) throws ParseException;

    List<Resource> getResourcesByEmail(String email) throws ParseException;
}

