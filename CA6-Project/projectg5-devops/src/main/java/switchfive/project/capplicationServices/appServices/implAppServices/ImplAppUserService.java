package switchfive.project.capplicationServices.appServices.implAppServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchfive.project.binterfaceAdapters.dataTransferObjects.AllocatedProjectsDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ChangePasswordDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SearchUserDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserDTO;
import switchfive.project.binterfaceAdapters.implRepositories.ImplProjectRepository;
import switchfive.project.capplicationServices.appServices.iappServices.IAppUserService;
import switchfive.project.capplicationServices.assemblers.implAssemblers.UserAssembler;
import switchfive.project.capplicationServices.iRepositories.IProfileRepository;
import switchfive.project.capplicationServices.iRepositories.IResourceRepository;
import switchfive.project.capplicationServices.iRepositories.IUserRepository;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.aggregates.user.User;
import switchfive.project.ddomain.factories.iFactories.IUserFactory;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ImplAppUserService implements IAppUserService {

    private final IUserRepository userStore;

    private final IResourceRepository resourceStore;

    private final IProfileRepository profileStore;

    private final IUserFactory userFactory;

    private final ImplProjectRepository projectStore;


    @Autowired
    public ImplAppUserService(IUserRepository userStore, IProfileRepository profileStore,
                              IUserFactory userFactory, IResourceRepository resourceStore,
                              ImplProjectRepository projectStore) {
        this.userStore = userStore;
        this.profileStore = profileStore;
        this.userFactory = userFactory;
        this.resourceStore = resourceStore;
        this.projectStore = projectStore;
    }

    public UserDTO createAndSaveUser(UserDTO userDTO) throws NoSuchAlgorithmException {

        if (userStore.findUserByEmail(userDTO.email).isPresent()) {
            throw new IllegalArgumentException("Email Taken!");
        }

        Email email = Email.createEmail(userDTO.email);
        Password password = Password.createPassword(userDTO.password);
        UserName userName = UserName.createUsername(userDTO.userName);
        Function function = Function.createFunction(userDTO.userFunction);

        User newUser = userFactory.createUser(email, password, userName, function);
        userStore.save(newUser);

        return UserAssembler.toDTO(newUser);
    }

    public Optional<UserDTO> activateAccount(UserDTO userDTO) throws NoSuchAlgorithmException {

        Optional<User> userInStore = userStore.findUserByEmail(userDTO.email);

        if (userInStore.isPresent()) {
            User selectedUser = userInStore.get();
            userDTO.activation = selectedUser.activateAccount(userDTO.code);
            Optional<User> updatedUser = userStore.update(selectedUser);
            if (updatedUser.isPresent()) {
                userDTO = UserAssembler.toDTO(updatedUser.get());
            }
        }

        return Optional.of(userDTO);
    }

    public Optional<List<UserDTO>> activateAll() throws NoSuchAlgorithmException {
        UserDTO userDTO;
        List<UserDTO> usersDTO = new ArrayList<>();

        Optional<List<User>> usersInStore = userStore.findAllUsers();

        if (usersInStore.isEmpty()) {
            return Optional.empty();
        }

        for (User each : usersInStore.get()) {
            each.setToActive();
            userStore.save(each);
            userDTO = UserAssembler.toDTO(each);
            usersDTO.add(userDTO);
        }

        return Optional.of(usersDTO);
    }

    /**
     * if the store has a user, updates him with a new password.
     *
     * @param passwordDTO containing the user email, the old and the new password
     * @return true if the user is found and updated, otherwise false.
     */
    public Optional<UserDTO> changePassword(ChangePasswordDTO passwordDTO) throws NoSuchAlgorithmException {

        Optional<User> optUserInStore = userStore.findUserByEmail(passwordDTO.email);
        UserDTO userDTO = null;

        if (optUserInStore.isPresent()) {
            User userInStore = optUserInStore.get();

            boolean wasUpdate = userInStore.updatePassword(passwordDTO.oldPassword, passwordDTO.newPassword);

            if (!wasUpdate) {
                throw new UnsupportedOperationException("Update was not successful!" + "Old password is not the same.");
            }

            userStore.update(userInStore);
            userDTO = UserAssembler.toDTOWithPassword(userInStore);
        }

        return Optional.ofNullable(userDTO);
    }


    public Optional<User> findById(String id) throws NoSuchAlgorithmException {
        return userStore.findUserByEmail(id);
    }


    /**
     * If the store has a user with that id updates him with a new name and anew function.
     * If the name and/or function are not validated throws an Exception.
     *
     * @param userDTO the DTO containing the user email, the new name and the new function.
     * @return true if the user is found and updated, otherwise false.
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) throws NoSuchAlgorithmException {
        Optional<User> theUser = userStore.findUserByEmail(userDTO.email);
        UserDTO updatedUserDTO = null;
        if (theUser.isPresent()) {
            UserName newName = UserName.createUsername(userDTO.userName);
            Function newFunction = Function.createFunction(userDTO.userFunction);
            theUser.get().updateUserInformation(newName, newFunction);
            theUser = userStore.update(theUser.get());
            if (theUser.isPresent()) {
                updatedUserDTO = UserAssembler.toDTO(theUser.get());
            }
        }

        return Optional.ofNullable(updatedUserDTO);
    }

    public List<SearchUserDTO> getUsersByEmail(String email) throws NoSuchAlgorithmException {
        return UserAssembler.toSearchDTOList(userStore.findUsersByEmail(email));
    }

    public List<SearchUserDTO> getUsersByProfile(String profileId) throws NoSuchAlgorithmException {
        return UserAssembler.toSearchDTOList(userStore.findUsersByProfile(profileId));
    }

    public Optional<SearchUserDTO> getUserByEmail(String email) throws NoSuchAlgorithmException {
        return Optional.of(UserAssembler.toSearchUserDTO(userStore.findUserByEmail(email).get()));
    }


    public IUserRepository getUserStore() {
        return userStore;
    }

    public Optional<SearchUserDTO> setUserToActive(String email) throws NoSuchAlgorithmException {
        Optional<User> userOpt = userStore.findUserByEmail(email);
        if (userOpt.isPresent() && !userOpt.get().isUserActive()) {
            userOpt.get().setToActive();
        }
        return Optional.of(UserAssembler.toSearchUserDTO(userOpt.get()));
    }


    public Optional<SearchUserDTO> setUserToInactive(String email) throws NoSuchAlgorithmException {
        Optional<User> userOpt = userStore.findUserByEmail(email);
        if (userOpt.isPresent() && userOpt.get().isUserActive()) {
            userOpt.get().setToInactive();
        }
        return Optional.of(UserAssembler.toSearchUserDTO(userOpt.get()));
    }

    public Optional<SearchUserDTO> validateAndAddProfile(String email, String profileDescription) throws NoSuchAlgorithmException {

        Optional<User> user = this.userStore.findUserByEmail(email);

        ProfileDescription profile = ProfileDescription.createProfileDescription(profileDescription);

        if (user.isEmpty()) {
            throw new IllegalArgumentException("User does not exist.");
        } else if (!this.profileStore.profileExists(profileDescription)) {
            throw new IllegalArgumentException("Profile does not exist.");
        } else if (user.get().hasProfile(profile)) {
            throw new IllegalArgumentException("User already has selected profile.");
        } else {
            user.get().addProfile(profile);
            this.userStore.addProfile(email, profileDescription);
            return Optional.of(UserAssembler.toSearchUserDTO(user.get()));
        }
    }

    public Optional<SearchUserDTO> validateAndRemoveProfile(String email, String profileDescription) throws NoSuchAlgorithmException {

        Optional<User> user = this.userStore.findUserByEmail(email);

        ProfileDescription profile = ProfileDescription.createProfileDescription(profileDescription);

        if (user.isEmpty()) {
            throw new IllegalArgumentException("User does not exist.");
        } else if (!user.get().hasProfile(profile)) {
            throw new IllegalArgumentException("User does not have selected profile.");
        } else {
            user.get().removeProfile(profile);
            this.userStore.removeProfile(email, profileDescription);
            return Optional.of(UserAssembler.toSearchUserDTO(user.get()));
        }
    }

    public Optional<List<AllocatedProjectsDTO>> findAllocatedProjects(String email) throws ParseException {

        List<Resource> resourcesList = resourceStore.getResourcesByEmail(email);
        List<AllocatedProjectsDTO> allocatedProjectsDTOList = new ArrayList<>();

        for (Resource resource : resourcesList) {
            String role = resource.getRole();
            String projectCode = resource.projectCode();

            ProjectCode projCode = ProjectCode.createProjectCode(projectCode);
            Optional<Project> project = projectStore.findByCode(projCode);
            String projectName = project.get().getName();

            AllocatedProjectsDTO allocatedProjectsDTO =
                    new AllocatedProjectsDTO();
            allocatedProjectsDTO.setProjectName(projectName);
            allocatedProjectsDTO.setRole(role);
            allocatedProjectsDTO.setProjectCode(projectCode);

            allocatedProjectsDTOList.add(allocatedProjectsDTO);
        }
        return Optional.of(allocatedProjectsDTOList);
    }

}
