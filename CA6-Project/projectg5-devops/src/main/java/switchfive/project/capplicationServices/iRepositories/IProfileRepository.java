package switchfive.project.capplicationServices.iRepositories;

import switchfive.project.ddomain.aggregates.profile.Profile;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;

import java.util.List;
import java.util.Optional;

public interface IProfileRepository {
    Optional<Profile> getProfileByDescription(ProfileDescription profileDescription);

    Profile save(final Profile newProfile);

    boolean profileExists(String profileDescription);

    List<Profile> getProfiles();
}
