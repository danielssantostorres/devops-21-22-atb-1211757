package switchfive.project.capplicationServices.appServices.iappServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.MoveUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.RefineUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserStoryDTO;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.UserStoryCode;
import switchfive.project.ddomain.shared.valueObjects.UserStoryDescription;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface IAppUserStoryService {
    UserStoryDTO createAndAddUserStory(String projectCode, String description)
            throws ParseException;

    List<UserStoryDTO> getUserStoryListByPriority(String projectCode)
            throws ParseException;

    Optional<UserStoryDTO> getUserStoryDTO(String projectCode, String userStoryCode) throws ParseException;

    List<UserStoryDTO> refineUserStory(RefineUserStoryDTO refineUserStoryDTO) throws ParseException;

    UserStoryDTO createNewUSWithParentCode(ProjectCode projectCode,
                                           UserStoryCode parentUserStoryCode,
                                           UserStoryDescription userStoryDescription) throws ParseException;

   Optional<MoveUserStoryDTO> moveUSFromProductBacklogToSprintBacklog(String projecCode, String userStoryCode, MoveUserStoryDTO theUserStory) throws ParseException;

    Optional<UserStoryDTO> userStoryChangeStatus (String projectCode, String userStoryCode, String status) throws ParseException;

    Optional<UserStoryDTO> userStoryChangePriority(String projectCode, String userStoryCode, int priority);
}

