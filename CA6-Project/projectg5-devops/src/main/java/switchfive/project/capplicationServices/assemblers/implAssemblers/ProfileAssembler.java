package switchfive.project.capplicationServices.assemblers.implAssemblers;

import switchfive.project.binterfaceAdapters.dataTransferObjects.ProfileDTO;
import switchfive.project.ddomain.aggregates.profile.Profile;

public class ProfileAssembler {

    public static ProfileDTO toDTO(Profile profile) {

        String description = profile.getDesignation();

        return new ProfileDTO(description);
    }


}
