package switchfive.project.capplicationServices.appServices.iappServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.ProfileDTO;

import java.util.List;
import java.util.Optional;

public interface IAppProfileService {

    Optional<ProfileDTO> addNewProfile(String description);

    Optional<ProfileDTO> getProfile(String profileDescription);

    List<ProfileDTO> getProfiles();

}
