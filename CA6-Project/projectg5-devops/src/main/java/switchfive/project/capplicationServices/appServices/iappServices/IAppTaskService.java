package switchfive.project.capplicationServices.appServices.iappServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.TaskCreationDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.TaskDTO;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface IAppTaskService {
    Optional<TaskDTO> createAndSaveTask (TaskCreationDTO taskCreationDTO) throws ParseException;

    Optional<TaskDTO> getTask(String projectCode, String taskCode) throws ParseException;

    List<TaskDTO> getTasks() throws ParseException;
}
