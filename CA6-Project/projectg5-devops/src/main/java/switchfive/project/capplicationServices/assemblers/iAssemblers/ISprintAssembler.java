package switchfive.project.capplicationServices.assemblers.iAssemblers;

import switchfive.project.binterfaceAdapters.dataTransferObjects.SprintDTO;
import switchfive.project.ddomain.aggregates.sprint.Sprint;

public interface ISprintAssembler {
    SprintDTO toDTO(Sprint sprint);
}
