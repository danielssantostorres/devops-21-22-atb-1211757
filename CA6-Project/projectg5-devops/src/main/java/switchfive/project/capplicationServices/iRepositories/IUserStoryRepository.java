package switchfive.project.capplicationServices.iRepositories;

import switchfive.project.ddomain.aggregates.userStory.UserStory;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.UserStoryCode;
import switchfive.project.ddomain.shared.valueObjects.UserStoryStatus;

import java.util.List;
import java.util.Optional;

public interface IUserStoryRepository {
    UserStory save(UserStory newUserStory);

    int nextUserStoryNumber(ProjectCode projectCode);

    String generatorCode(ProjectCode projectCode);

    List<UserStory> getUserStoryListProductBacklog(ProjectCode projectCode);

    Optional<UserStory> getUserStory(ProjectCode projectCode, UserStoryCode userStoryCode);

    boolean existsUserStory(ProjectCode projectCode, UserStoryCode userStoryCode);

    Optional<UserStory> updateUserStory(UserStory userStory);

    List<UserStory> getUserStoryListInProjectOrderedByStatus(ProjectCode projectCode);

    Optional<UserStory> userStoryUpdateStatus(ProjectCode projCode,
                                              UserStoryCode userStoryCode1,
                                              UserStoryStatus userStoryStatus);

}
