package switchfive.project.capplicationServices.appServices.iappServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceCreationDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceDTO;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.aggregates.user.User;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Optional;

/**
 * Resource application service.
 */
public interface IAppResourceService {

    /**
     * Method to define team member of a project.
     */
    Optional<ResourceDTO> definedTeamMemberOfAProject(ResourceCreationDTO dto)
            throws ParseException, NoSuchAlgorithmException;

    /**
     * Method to define product owner of a project.
     */
    Optional<ResourceDTO> definedProductOwnerOfAProject(ResourceCreationDTO dto)
            throws ParseException, NoSuchAlgorithmException;

    /**
     * Method to define scrum master of a project.
     */
    Optional<ResourceDTO> definedScrumMasterOfAProject(ResourceCreationDTO dto)
            throws ParseException, NoSuchAlgorithmException;

    /**
     * Method to create and save Project Manager.
     */
    Optional<Resource> createProjectManager(Email userID,
                                            ProjectCode projectCode,
                                            Time projectTime,
                                            ResourceCostPerHour costPerHour,
                                            ResourcePercentageOfAllocation allocation)
            throws ParseException, NoSuchAlgorithmException;

    /**
     * Method to find user by user id.
     */
    Optional<User> findUser(final String userID) throws NoSuchAlgorithmException;

    /**
     * @return project manager as an instance of Resource.
     */
    Optional<Resource> getProjectManager(ProjectCode code) throws ParseException;


    Optional<ResourceDTO> getResourceDTO(String resourceID) throws ParseException;

}
