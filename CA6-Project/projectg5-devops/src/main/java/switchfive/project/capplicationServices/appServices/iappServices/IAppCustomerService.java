package switchfive.project.capplicationServices.appServices.iappServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.CustomerDTO;
import switchfive.project.ddomain.aggregates.customer.Customer;

import java.util.List;
import java.util.Optional;

public interface IAppCustomerService {

    Optional<Customer> findCustomerByDescription(String description);

    Customer createAndSaveCustomer(String name);

    List<CustomerDTO> getAllCustomers();

}
