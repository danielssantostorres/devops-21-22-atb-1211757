package switchfive.project.capplicationServices.assemblers.implAssemblers;

import org.springframework.stereotype.Component;
import switchfive.project.binterfaceAdapters.dataTransferObjects.TypologyDTO;
import switchfive.project.capplicationServices.assemblers.iAssemblers.ITypologyAssembler;
import switchfive.project.ddomain.aggregates.typology.Typology;

@Component
public class TypologyAssembler implements ITypologyAssembler {

    public TypologyDTO toDTO(Typology typology) {
        TypologyDTO typologyDTO = new TypologyDTO();
        typologyDTO.setDescription(typology.getDescription());

        return typologyDTO;
    }
}
