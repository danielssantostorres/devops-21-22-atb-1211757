package switchfive.project.capplicationServices.assemblers.iAssemblers;

import switchfive.project.binterfaceAdapters.dataTransferObjects.ProjectDTO;
import switchfive.project.ddomain.aggregates.project.Project;

public interface IProjectAssembler {
    ProjectDTO toDTO(Project project);

    ProjectDTO toDTOWithPM(Project newProject,
                           String projectManagerEmail,
                           double projectManagerCostPerHour,
                           double projectManagerAllocation);
}
