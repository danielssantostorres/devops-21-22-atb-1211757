package switchfive.project.capplicationServices.appServices.iappServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.SearchUserDTO;
import switchfive.project.capplicationServices.iRepositories.IUserRepository;

import java.util.List;

public interface iSearchService {

    List<SearchUserDTO> getUserByEmail(String email);

    IUserRepository getUserStore();

    List<SearchUserDTO> getUserByProfile(int profileId);


}
