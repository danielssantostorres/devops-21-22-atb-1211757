package switchfive.project.capplicationServices.assemblers.iAssemblers;

import switchfive.project.binterfaceAdapters.dataTransferObjects.TypologyDTO;
import switchfive.project.ddomain.aggregates.typology.Typology;

public interface ITypologyAssembler {
    TypologyDTO toDTO(Typology typology);
}
