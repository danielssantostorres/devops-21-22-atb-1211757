package switchfive.project.capplicationServices.assemblers.implAssemblers;

import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.TimeDTO;
import switchfive.project.ddomain.aggregates.resource.Resource;

public class ResourceAssembler {

    public static ResourceDTO toDto(final Resource resource) {

        String resourceID = resource.getResourceID();
        String userID = resource.getUserID().getUserEmail();
        String projectCode = resource.projectCode();
        String startDate = resource.getStartDate();
        String endDate = resource.getEndDate();
        TimeDTO dates = new TimeDTO(startDate, endDate);
        double costPerHour = resource.getCostPerHour();
        double allocation = resource.getAllocation();
        String role = resource.getRole();

        return new ResourceDTO(resourceID, userID, projectCode, dates
                , costPerHour, allocation, role);

    }
}
