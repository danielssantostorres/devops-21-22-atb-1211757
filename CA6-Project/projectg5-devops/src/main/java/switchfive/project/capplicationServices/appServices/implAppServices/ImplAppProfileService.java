package switchfive.project.capplicationServices.appServices.implAppServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProfileDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppProfileService;
import switchfive.project.capplicationServices.assemblers.implAssemblers.ProfileAssembler;
import switchfive.project.capplicationServices.iRepositories.IProfileRepository;
import switchfive.project.ddomain.aggregates.profile.Profile;
import switchfive.project.ddomain.factories.iFactories.ProfileFactory;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ImplAppProfileService implements IAppProfileService {

    private final IProfileRepository iProfileRepository;

    private final ProfileFactory profileFactory;

    @Autowired
    public ImplAppProfileService(IProfileRepository iProfileRepository,
                                 ProfileFactory profileFactory) {
        this.iProfileRepository = iProfileRepository;
        this.profileFactory = profileFactory;
    }

    @Override
    public Optional<ProfileDTO> addNewProfile(String description) {
        ProfileDTO profileDTO = null;

        ProfileDescription profileDescription = ProfileDescription
                .createProfileDescription(description);
        Optional<Profile> profileInRepo = iProfileRepository
                .getProfileByDescription(profileDescription);

        if (profileInRepo.isEmpty()) {

            Profile newProfile = profileFactory.createProfile(profileDescription);

            Profile profileInDB = iProfileRepository.save(newProfile);

            profileDTO = ProfileAssembler.toDTO(profileInDB);
        }

        return Optional.ofNullable(profileDTO);
    }

    @Override
    public Optional<ProfileDTO> getProfile(String profileDescription) {

        ProfileDescription profileDescriptionToFind = ProfileDescription
                .createProfileDescription(profileDescription);

        Optional<Profile> profileInRepo = iProfileRepository
                .getProfileByDescription(profileDescriptionToFind);

        ProfileDTO profileDTO = null;

        if (profileInRepo.isPresent()) {
            profileDTO = ProfileAssembler.toDTO(profileInRepo.get());
        }

        return Optional.ofNullable(profileDTO);
    }

    @Override
    public List<ProfileDTO> getProfiles() {

        List<Profile> profilesInRepo = iProfileRepository.getProfiles();
        List<ProfileDTO> profilesDTO = new ArrayList<>();

        for (Profile profile : profilesInRepo) {

            ProfileDTO profileDTO = ProfileAssembler.toDTO(profile);

            profilesDTO.add(profileDTO);
        }

        return profilesDTO;
    }
}
