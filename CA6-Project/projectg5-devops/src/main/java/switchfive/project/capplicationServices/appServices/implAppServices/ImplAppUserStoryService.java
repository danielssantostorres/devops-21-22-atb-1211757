package switchfive.project.capplicationServices.appServices.implAppServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchfive.project.binterfaceAdapters.dataTransferObjects.MoveUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.RefineUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserStoryDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppUserStoryService;
import switchfive.project.capplicationServices.assemblers.implAssemblers.UserStoryAssembler;
import switchfive.project.capplicationServices.iRepositories.IProjectRepository;
import switchfive.project.capplicationServices.iRepositories.ISprintRepository;
import switchfive.project.capplicationServices.iRepositories.IUserStoryRepository;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.sprint.Sprint;
import switchfive.project.ddomain.aggregates.userStory.UserStory;
import switchfive.project.ddomain.domainServices.iDomainServices.IUserStoryDomainService;
import switchfive.project.ddomain.factories.iFactories.UserStoryFactory;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ImplAppUserStoryService implements IAppUserStoryService {

    private final IUserStoryRepository iUserStoryStore;
    private final UserStoryFactory userStoryFactory;
    private final IProjectRepository iProjectRepository;
    private final ISprintRepository iSprintRepository;
    private final IUserStoryDomainService iUserStoryDomainService;

    @Autowired
    public ImplAppUserStoryService(IUserStoryRepository iUserStoryStore,
                                   UserStoryFactory userStoryFactory,
                                   IProjectRepository iProjectRepository, ISprintRepository iSprintRepository, IUserStoryDomainService iUserStoryDomainService) {
        this.iUserStoryStore = iUserStoryStore;
        this.userStoryFactory = userStoryFactory;
        this.iProjectRepository = iProjectRepository;
        this.iSprintRepository = iSprintRepository;
        this.iUserStoryDomainService = iUserStoryDomainService;
    }

    /**
     * Public method create and add user story to userStoryList.
     *
     * @param description String description
     * @return true if user story was successfully created and added to userStoryList
     */
    @Override
    public UserStoryDTO createAndAddUserStory(String projectCode, String description)
            throws ParseException {

        ProjectCode projCode = ProjectCode.createProjectCode(projectCode);
        Optional<Project> opProject = iProjectRepository.findByCode(projCode);
        if (opProject.isEmpty()) {
            throw new IllegalArgumentException("Project doesn't exist!");
        }

        String defaultCode = iUserStoryStore.generatorCode(projCode);
        UserStoryCode code = UserStoryCode.createUserStoryCode(defaultCode);

        int defaultPriority = iUserStoryStore.nextUserStoryNumber(projCode);
        Priority priority = Priority.createPriority(defaultPriority);

        UserStoryID userStoryID = UserStoryID.
                createUserStoryID(projectCode, code.getIdentity());

        UserStoryDescription userStoryDescription = UserStoryDescription.
                createUserStoryDescription(description);

        UserStory newUserStory = this.userStoryFactory.createUserStory(userStoryID, projCode,
                code, priority, userStoryDescription);
        this.iUserStoryStore.save(newUserStory);

        return UserStoryAssembler.toUserStoryDTO(newUserStory);
    }

    @Override
    public List<UserStoryDTO> getUserStoryListByPriority(String projectCode)
            throws ParseException {

        ProjectCode projCode = ProjectCode.createProjectCode(projectCode);
        Optional<Project> opProject = iProjectRepository.findByCode(projCode);
        if (opProject.isEmpty()) {
            throw new IllegalArgumentException("Project doesn't exist!");
        }

        List<UserStoryDTO> listByPriority = new ArrayList<>();
        List<UserStory> userStoryList = iUserStoryStore.getUserStoryListProductBacklog(projCode);
        for (UserStory userStory : userStoryList) {
            UserStoryDTO userStoryDTO = UserStoryAssembler.toUserStoryDTO(userStory);
            listByPriority.add(userStoryDTO);
        }
        return listByPriority;
    }

    @Override
    public Optional<UserStoryDTO> getUserStoryDTO(String projectCode, String userStoryCode) throws ParseException {

        UserStoryDTO userStoryDTO = null;

        ProjectCode projCode = ProjectCode.createProjectCode(projectCode);
        Optional<Project> opProject = iProjectRepository.findByCode(projCode);
        if (opProject.isEmpty()) {
            throw new IllegalArgumentException("Project doesn't exist!");
        }

        UserStoryCode uSCode = UserStoryCode.createUserStoryCode(userStoryCode);

        Optional<UserStory> selectedUserStory = iUserStoryStore.getUserStory(projCode, uSCode);
        if (selectedUserStory.isPresent()) {
            userStoryDTO = UserStoryAssembler.toUserStoryDTO(selectedUserStory.get());
        }

        return Optional.ofNullable(userStoryDTO);
    }

    @Override
    public ArrayList<UserStoryDTO> refineUserStory(RefineUserStoryDTO refineUserStoryDTO)
            throws ParseException {

        String projectCode = refineUserStoryDTO.getProjectCode();
        String userStoryCode = refineUserStoryDTO.getUserStoryCode();
        List<String> newUserStoryDescription = refineUserStoryDTO
                .getNewUserStoryDescription();

        ProjectCode projCode = ProjectCode.createProjectCode(projectCode);
        Optional<Project> opProject = iProjectRepository.findByCode(projCode);
        if (opProject.isEmpty()) {
            throw new IllegalArgumentException("Project doesn't exist!");
        }

        Project project = opProject.get();
        if (project.isProjectClosed()) {
            throw new UnsupportedOperationException("Method not allowed. Project is finished");
        }

        // Find and get Parent US
        UserStoryCode parentUsCode = UserStoryCode.createUserStoryCode(userStoryCode);
        Optional<UserStory> optUserStoryToRefine = iUserStoryStore.getUserStory(projCode,
                parentUsCode);
        if (optUserStoryToRefine.isEmpty()) {
            throw new IllegalArgumentException("Parent User Story doesn't exist!");
        }

        UserStory userStoryToRefine = optUserStoryToRefine.get();

        // Check if US is available for refinement
        if (!userStoryToRefine.isUserStoryAvailableForRefinement()) {
            throw new UnsupportedOperationException("Method not allowed. User Story " +
                    "in not in a planned status.");
        }

        // Create New User Stories
        ArrayList<UserStoryDescription> newUSdescriptionVOList =
                userStoryDescriptionStringtoUserStoryDescriptionVO(newUserStoryDescription);

        ArrayList<UserStoryDTO> newUSDTOList = new ArrayList<>();

        if (!newUSdescriptionVOList.isEmpty()) {
            for (UserStoryDescription userStoryDescription : newUSdescriptionVOList) {
                UserStoryDTO newUS = this.createNewUSWithParentCode(projCode,
                        parentUsCode,
                        userStoryDescription);
                newUSDTOList.add(newUS);
            }
        } else {
            throw new IllegalArgumentException("New User Story description can't be empty");
        }

        // Set Parent US to Refined and Save
        userStoryToRefine.setStatusToRefined();

        // Save changes to original US
        iUserStoryStore.save(userStoryToRefine);

        UserStoryDTO userStoryDTOParent = UserStoryAssembler.toUserStoryDTO(userStoryToRefine);
        newUSDTOList.add(userStoryDTOParent);

        return newUSDTOList;

    }

    private ArrayList<UserStoryDescription>
    userStoryDescriptionStringtoUserStoryDescriptionVO
            (List<String> newUserStoryDescription) {

        ArrayList<UserStoryDescription> newUSdescriptionVOList =
                new ArrayList<>();

        for (String description : newUserStoryDescription) {
            UserStoryDescription usDescriptionVO = UserStoryDescription
                    .createUserStoryDescription(description);

            newUSdescriptionVOList.add(usDescriptionVO);
        }

        return newUSdescriptionVOList;
    }

    @Override
    public UserStoryDTO createNewUSWithParentCode(ProjectCode projectCode,
                                                  UserStoryCode parentUserStoryCode,
                                                  UserStoryDescription userStoryDescription)
            throws ParseException {

        Optional<Project> opProject = iProjectRepository.findByCode(projectCode);
        if (opProject.isEmpty()) {
            throw new IllegalArgumentException("Project doesn't exist!");
        }

        String defaultCode = iUserStoryStore.generatorCode(projectCode);
        UserStoryCode code = UserStoryCode.createUserStoryCode(defaultCode);

        int defaultPriority = iUserStoryStore.nextUserStoryNumber(projectCode);
        Priority priority = Priority.createPriority(defaultPriority);
        UserStoryID userStoryID = UserStoryID.createUserStoryID(projectCode.getCode(),
                code.getIdentity());

        UserStory newUserStory = this.userStoryFactory.createUserStory(userStoryID, projectCode,
                code, priority, userStoryDescription, parentUserStoryCode);
        this.iUserStoryStore.save(newUserStory);

        return UserStoryAssembler.toUserStoryDTO(newUserStory);
    }


    public Optional<MoveUserStoryDTO> moveUSFromProductBacklogToSprintBacklog(String projectCode,
                                                                              String userStoryCode,
                                                                              MoveUserStoryDTO moveUserStoryDTO) throws ParseException {
        ProjectCode newProjectCode = ProjectCode.createProjectCode(projectCode);
        Optional<Project> projectOptional = iProjectRepository.findByCode(newProjectCode);

        Optional<MoveUserStoryDTO> updatedDTOOptional = Optional.empty();
        Optional<UserStory> updatedUserStoryOptional = Optional.empty();

        if (projectOptional.isPresent()) {
            SprintID sprintID = SprintID.createSprintID(projectCode,
                    moveUserStoryDTO.getSprintID());
            Optional<Sprint> sprintOptional = iSprintRepository.findBySprintID(sprintID);

            if (sprintOptional.isPresent()) {
                UserStoryCode newUserStoryCode = UserStoryCode.createUserStoryCode(userStoryCode);
                Optional<UserStory> userStoryOptional = iUserStoryStore.getUserStory(newProjectCode, newUserStoryCode);

                if (userStoryOptional.isPresent()) {
                    boolean isUserStoryMovable = iUserStoryDomainService.canUSBeMovedFromProductBacklogToSprintBacklog(projectOptional.get(),
                            sprintOptional.get(),
                            userStoryOptional.get()) && isUSInProductBacklog(userStoryOptional);

                    if (isUserStoryMovable) {
                        userStoryOptional.get().setSprintID(projectCode, moveUserStoryDTO.getSprintID());
                        updatedUserStoryOptional = iUserStoryStore.updateUserStory(userStoryOptional.get());
                    }
                }
            }
        }

        if (updatedUserStoryOptional.isPresent()) {
            MoveUserStoryDTO updatedDTO = UserStoryAssembler.toUserStoryDTOComplete(updatedUserStoryOptional.get());
            updatedDTOOptional = Optional.of(updatedDTO);
        }

        return updatedDTOOptional;
    }

    public Optional<UserStoryDTO> userStoryChangeStatus(String projectCode, String userStoryCode, String status) throws ParseException {

        UserStoryDTO userStoryDTO = null;

        ProjectCode projCode = ProjectCode.createProjectCode(projectCode);
        Optional<Project> opProject = iProjectRepository.findByCode(projCode);
        if (opProject.isEmpty()) {
            throw new IllegalArgumentException("Project doesn't exist!");
        }

        UserStoryCode userStoryCode1 = UserStoryCode.createUserStoryCode(userStoryCode);
        UserStoryStatus userStoryStatus = UserStoryStatus.valueOf(status.toUpperCase());

        if (iUserStoryStore.existsUserStory(projCode, userStoryCode1)) {
            Optional<UserStory> userStory = iUserStoryStore.userStoryUpdateStatus(projCode,
                    userStoryCode1, userStoryStatus);
            userStoryDTO = UserStoryAssembler.toUserStoryDTO(userStory.get());
        }

        return Optional.ofNullable(userStoryDTO);
    }

    private boolean isUSInProductBacklog(Optional<UserStory> userStoryOptional) {
        return userStoryOptional.get().getSprintID().getSprintNumber() == 0;
    }

    public Optional<UserStoryDTO> userStoryChangePriority(String projectCode, String userStoryCode, int priority) {

        UserStoryDTO userStoryDTO = null;

        ProjectCode projectCodeVO = ProjectCode.createProjectCode(projectCode);
        UserStoryCode userStoryCodeVO = UserStoryCode.createUserStoryCode(userStoryCode);

        Optional<UserStory> userStoryOpt = iUserStoryStore
                .getUserStory(projectCodeVO, userStoryCodeVO);

        if (userStoryOpt.isPresent()) {

            List<UserStory> userStoriesInProject =
                    iUserStoryStore.getUserStoryListProductBacklog(projectCodeVO);

            UserStory userStoryToChangePriority = userStoryOpt.get();
            int oldPriority = userStoryToChangePriority.getPriority();
            Priority newPriority = Priority.createPriority(priority);

            if (priority <= userStoriesInProject.size()) {
                // If new US priority is under old priority
                if (oldPriority > priority) {
                    for (int i = priority - 1; i < oldPriority; i++) {

                        UserStory userStoryPriorityOrder = userStoriesInProject.get(i);

                        Priority newPriorityInList = null;
                        if (i == oldPriority - 1) {
                            newPriorityInList = newPriority;
                        } else {
                            newPriorityInList = Priority.createPriority(i + 2);
                        }
                        userStoryPriorityOrder.setPriority(newPriorityInList);
                        UserStory userStoryInDB = iUserStoryStore.save(userStoryPriorityOrder);

                        if (i == oldPriority - 1) {
                            userStoryDTO = UserStoryAssembler.toUserStoryDTO(userStoryInDB);
                        }

                    }
                    // If new US priority is above old priority
                } else {
                    for (int i = oldPriority - 1; i < priority; i++) {

                        UserStory userStoryPriorityOrder = userStoriesInProject.get(i);

                        Priority newPriorityInList = null;
                        if (i == oldPriority - 1) {
                            newPriorityInList = newPriority;
                        } else {
                            newPriorityInList = Priority.createPriority(userStoryPriorityOrder.getPriority() - 1);
                        }
                        userStoryPriorityOrder.setPriority(newPriorityInList);
                        UserStory userStoryInDB = iUserStoryStore.save(userStoryPriorityOrder);

                        if (i == oldPriority - 1) {
                            userStoryDTO = UserStoryAssembler.toUserStoryDTO(userStoryInDB);
                        }
                    }
                }
            }
        }

        return Optional.ofNullable(userStoryDTO);
    }
}




