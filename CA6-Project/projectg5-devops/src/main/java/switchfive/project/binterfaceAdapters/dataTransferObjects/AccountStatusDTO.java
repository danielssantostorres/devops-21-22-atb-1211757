package switchfive.project.binterfaceAdapters.dataTransferObjects;

import java.util.Objects;

public class AccountStatusDTO {

    public final String email;
    public final boolean isActivated;

    public AccountStatusDTO(String email, boolean isActivated) {
        this.email = email;
        this.isActivated = isActivated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountStatusDTO that = (AccountStatusDTO) o;
        return isActivated == that.isActivated && Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, isActivated);
    }
}
