package switchfive.project.binterfaceAdapters.controllers.iControllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import switchfive.project.binterfaceAdapters.dataTransferObjects.RequestCreationDTO;

import java.security.NoSuchAlgorithmException;

public interface IRequestController {
    ResponseEntity<Object> createNewProfileRequest(RequestCreationDTO requestCreationDTO) throws NoSuchAlgorithmException;

    ResponseEntity<Object> getRequest(@PathVariable String requestID);
}
