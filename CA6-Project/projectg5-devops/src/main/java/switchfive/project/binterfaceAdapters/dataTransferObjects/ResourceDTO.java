package switchfive.project.binterfaceAdapters.dataTransferObjects;

import org.springframework.hateoas.RepresentationModel;

public class ResourceDTO extends RepresentationModel<ResourceDTO> {

    /**
     * Unique email associated with the Resource.
     */
    public String resourceID;
    /**
     * The name of the resource.
     */
    public final String userID;

    public final String projectCode;

    public final TimeDTO dates;

    public final double costPerHour;

    public final double percentageOfAllocation;

    public final String role;


    public ResourceDTO(String resourceID, String userID, String projectCode, TimeDTO dates,
                       double costPerHour, double percentageOfAllocation, String role) {
        this.resourceID = resourceID;
        this.userID = userID;
        this.projectCode = projectCode;
        this.dates = dates;
        this.costPerHour = costPerHour;
        this.percentageOfAllocation = percentageOfAllocation;
        this.role = role;
    }
}

