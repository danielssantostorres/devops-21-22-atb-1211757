package switchfive.project.binterfaceAdapters.controllers.implControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import switchfive.project.binterfaceAdapters.controllers.iControllers.IUserStoryController;
import switchfive.project.binterfaceAdapters.dataTransferObjects.CreateUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.MoveUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.RefineUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserStoryDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppUserStoryService;
import switchfive.project.ddomain.shared.valueObjects.UserStoryStatus;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin
@Controller
@RestController
public class ImplUserStoryController implements IUserStoryController {

    public IAppUserStoryService iAppCreateUserStoryService;

    @Autowired
    public ImplUserStoryController(IAppUserStoryService iAppCreateUserStoryService) {
        this.iAppCreateUserStoryService = iAppCreateUserStoryService;
    }

    @PostMapping("/userStories/projectCode/{projectCode}")
    @ResponseBody
    public ResponseEntity<Object> createUserStory(@PathVariable(value = "projectCode") String projectCode,
                                                  @RequestBody CreateUserStoryDTO description) {

        try {
            UserStoryDTO userStoryOutput = this.iAppCreateUserStoryService.createAndAddUserStory(projectCode, description.getDescription());

            Link link = linkTo(methodOn(ImplUserStoryController.class).getUserStory(projectCode, userStoryOutput.getCode())).withSelfRel();

            userStoryOutput.add(link);

            return new ResponseEntity<>(userStoryOutput, HttpStatus.CREATED);
        } catch (IllegalArgumentException | ParseException operationFailed) {
            return new ResponseEntity<>(operationFailed.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/userStories/{userStoryCode}/projectCode/{projectCode}")
    public ResponseEntity<Object> getUserStory(@PathVariable(value = "projectCode") String projectCode,
                                               @PathVariable(value = "userStoryCode") String userStoryCode) {
        try {
            Optional<UserStoryDTO> userStoryDTO = this.iAppCreateUserStoryService
                    .getUserStoryDTO(projectCode, userStoryCode);

            if (userStoryDTO.isEmpty()) {
                return new ResponseEntity<>("User Story doesn't exists!", HttpStatus.OK);
            }

            Link linkMoveToSprint = linkTo(methodOn(ImplUserStoryController.class)
                    .moveUSFromProductBacklogToSprintBacklog(projectCode, userStoryCode, null))
                    .withRel("moveToSprint").withType("PATCH");

            Link linkStatus = linkTo(methodOn(ImplUserStoryController.class).userStoryUpdateStatus(projectCode,
                userStoryCode, null)).withRel("updateStatus").withType("PATCH");

            Link linkPriority = linkTo(methodOn(ImplUserStoryController.class).
                    userStoryUpdatePriority(projectCode, userStoryCode, 0)).withRel("updatePriority")
                    .withType("PATCH");

            Link linkRefine = linkTo(methodOn(ImplUserStoryController.class).refineUserStory(null))
                    .withRel("refineUserStory").withType("POST");


            userStoryDTO.get().add(linkStatus, linkMoveToSprint, linkPriority, linkRefine);

            return new ResponseEntity<>(userStoryDTO.get(), HttpStatus.OK);
        } catch (IllegalArgumentException | ParseException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/userStories/projectCode/{projectCode}")
    public ResponseEntity<Object> getUserStoryByPriority(@PathVariable(value = "projectCode") String projectCode) {

        try {
            List<UserStoryDTO> userStoriesOfProject = iAppCreateUserStoryService.
                    getUserStoryListByPriority(projectCode);

            for (UserStoryDTO userStoryDTO : userStoriesOfProject) {

                Link link = linkTo(methodOn(ImplUserStoryController.class).getUserStory(projectCode,
                        userStoryDTO.getCode())).withSelfRel();

                userStoryDTO.add(link);
            }
            return new ResponseEntity<>(userStoriesOfProject, HttpStatus.OK);
        } catch (IllegalArgumentException | ParseException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.OK);
        }

    }

    @PostMapping("/userStories/refineUS")
    public ResponseEntity<Object> refineUserStory(@RequestBody RefineUserStoryDTO refineUserStoryDTO) {

        try {
            List<UserStoryDTO> newUserStoryList =
                    iAppCreateUserStoryService.refineUserStory(refineUserStoryDTO);

            for (UserStoryDTO userStoryDTO : newUserStoryList) {

                Link link = linkTo(methodOn(ImplUserStoryController.class)
                        .getUserStory(refineUserStoryDTO.getProjectCode(),
                                userStoryDTO.getCode())).withSelfRel();

                userStoryDTO.add(link);
            }

            return new ResponseEntity<>(newUserStoryList, HttpStatus.OK);

        } catch (IllegalArgumentException |
                UnsupportedOperationException |
                ParseException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.OK);
        }

    }

    @PatchMapping("/projects/{projectCode}/productBacklog/{userStoryCode}")
    public ResponseEntity<Object> moveUSFromProductBacklogToSprintBacklog(@PathVariable(value = "projectCode") String projectCode,
                                                                          @PathVariable(value = "userStoryCode") String userStoryCode,
                                                                          @RequestBody MoveUserStoryDTO moveUserStoryDTO) throws ParseException {
        Optional<MoveUserStoryDTO> moveUserStoryDTOOptional = iAppCreateUserStoryService.moveUSFromProductBacklogToSprintBacklog(projectCode, userStoryCode, moveUserStoryDTO);

        if (moveUserStoryDTOOptional.isPresent()) {
            Link link = linkTo(methodOn(ImplUserStoryController.class).getUserStory(projectCode, userStoryCode))
                    .withSelfRel()
                    .withType("GET");

            Link link2 = linkTo(methodOn(ImplSprintController.class).getSprintById(moveUserStoryDTO.getSprintID(), moveUserStoryDTO.getProjectCode()))
                    .withRel("sprint")
                    .withType("GET");

            moveUserStoryDTOOptional.get().add(link, link2);

            return new ResponseEntity<>(moveUserStoryDTOOptional.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping("/userStories/{userStoryCode}/projectCode/{projectCode}/status")
    public ResponseEntity<Object> userStoryUpdateStatus(@PathVariable(value = "projectCode") String projectCode,
                                                        @PathVariable(value = "userStoryCode") String userStoryCode,
                                                        @RequestBody String status) throws ParseException {
        try {
            Optional<UserStoryDTO> userStoryOutput = this.iAppCreateUserStoryService.userStoryChangeStatus(projectCode,
                    userStoryCode, status);

            Link link = linkTo(methodOn(ImplUserStoryController.class).getUserStory(projectCode,
                    userStoryOutput.get().getCode())).withSelfRel();

            userStoryOutput.get().add(link);

            return new ResponseEntity<>(userStoryOutput, HttpStatus.OK);
        } catch (IllegalArgumentException operationFailed) {
            return new ResponseEntity<>(operationFailed.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping("userStories/{userStoryCode}/projectCode/{projectCode}/priority")
    public ResponseEntity<Object> userStoryUpdatePriority(@PathVariable(value = "projectCode") String projectCode,
                                                          @PathVariable(value = "userStoryCode") String userStoryCode,
                                                          @RequestBody int priority) {

        try {
            Optional<UserStoryDTO> userStoryDto = this.iAppCreateUserStoryService.userStoryChangePriority
                    (projectCode, userStoryCode, priority);

            Link link = linkTo(methodOn(ImplUserStoryController.class).getUserStory
                    (projectCode, userStoryDto.get().getCode())).withSelfRel();

            userStoryDto.get().add(link);

            return new ResponseEntity<>(userStoryDto, HttpStatus.OK);
        } catch (IllegalArgumentException operationFailed) {

            return new ResponseEntity<>(operationFailed.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }
}
