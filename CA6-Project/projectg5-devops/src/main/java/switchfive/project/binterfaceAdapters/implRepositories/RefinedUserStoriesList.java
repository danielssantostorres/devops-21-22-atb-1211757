package switchfive.project.binterfaceAdapters.implRepositories;

import switchfive.project.ddomain.aggregates.userStory.UserStory;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Class that stores the User Stories that where refined into more focused User Stories
 */
public class RefinedUserStoriesList {

    /**
     * List of Refined User Stories.
     */
    private final ArrayList<UserStory> userStoryList;

    /**
     * Empty class constructor
     */
    public RefinedUserStoriesList() {
        this.userStoryList = new ArrayList<>();
    }

    /**
     * Adds a User Story that was simplified into more focused UserStories
     * to the RefinedUserStoryList
     *
     * @param refinedUserStory UserStory object
     * @return boolean
     */
    public boolean addUserStoryToRefinedUserStoryList(UserStory refinedUserStory) {
        boolean addUserStoryToRefinedUserStoryList = false;

        if (refinedUserStory != null) {
            userStoryList.add(refinedUserStory);
            addUserStoryToRefinedUserStoryList = true;
        }

        return addUserStoryToRefinedUserStoryList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RefinedUserStoriesList that = (RefinedUserStoriesList) o;
        return Objects.equals(userStoryList, that.userStoryList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userStoryList);
    }
}
