package switchfive.project.binterfaceAdapters.dataTransferObjects;

public class UserStoryStatusDTO {
    /**
     * Email that identifies the user who takes the action.
     */
    public String userEmail;
    /**
     * Project code in which the user story will be modified.
     */
    public String projectCode;
    /**
     * User story code that will be modified.
     */
    public String userStoryCode;
    /**
     * New status that will be given to the user story.
     */
    public String newStatus;

    /**
     * @param userEmail     final String userEmail
     * @param projectCode   final String projectCode
     * @param userStoryCode final String userStoryCode
     * @param newStatus     final String newStatus
     */
    public UserStoryStatusDTO(final String userEmail, final String projectCode,
                              final String userStoryCode, final String newStatus) {
        this.userEmail = userEmail;
        this.projectCode = projectCode;
        this.userStoryCode = userStoryCode;
        this.newStatus = newStatus;
    }
}
