package switchfive.project.binterfaceAdapters.controllers.implControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchfive.project.binterfaceAdapters.controllers.iControllers.IProjectController;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProjectDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.StatusOfActivitiesDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UpdateProjectDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppProjectService;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin
@RestController
@RequestMapping(value = "/api")
public class ImplProjectController implements IProjectController {
    private final String WRONG_INFORMATION = "Wrong information";
    private final String PROJECT_DOES_NOT_EXIST = "Project do not exist";
    private final IAppProjectService projectService;


    @Autowired
    public ImplProjectController(final IAppProjectService iAppProjectService) {
        this.projectService = iAppProjectService;
    }


    @PostMapping(value = "/projects")
    public ResponseEntity<Object> createProject(
            @RequestBody final ProjectDTO dtoInput) {
        try {

            ProjectDTO dtoOutput =
                    this.projectService.createAndSaveProject(dtoInput);

            Link link = linkTo(methodOn(ImplProjectController.class).
                    getProject(dtoInput.getProjectCode())).withSelfRel();

            dtoOutput.add(link);

            return new ResponseEntity<>(dtoOutput, HttpStatus.CREATED);

        } catch (IllegalArgumentException | ParseException |
                 NoSuchAlgorithmException exception) {

            return new ResponseEntity<>(WRONG_INFORMATION,
                    HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping(value = "/projects")
    public ResponseEntity<Object> getAllProjects() throws ParseException, NoSuchAlgorithmException {

        List<ProjectDTO> dto = this.projectService.getAllProjects();
        RepresentationModel<ProjectDTO> linkList = new RepresentationModel<>();

        for (ProjectDTO each : dto) {
            Link link = linkTo(methodOn(ImplProjectController.class).
                    getProject(each.getProjectCode())).withRel(each.getProjectCode())
                    .withType("GET");
            linkList.add(link);
        }

        return new ResponseEntity<>(linkList, HttpStatus.OK);
    }


    @GetMapping(value = "/projects/{projectCode}")
    public ResponseEntity<Object> getProject(@PathVariable final String projectCode)
            throws ParseException, NoSuchAlgorithmException{
        try {

            if (this.projectService.getProjectDTO(projectCode).isEmpty()) {

                return new ResponseEntity<>(PROJECT_DOES_NOT_EXIST,
                        HttpStatus.OK);
            }

            ProjectDTO dto = this.projectService.getProjectDTO(projectCode).get();

            addLinksToDto(dto);

            return new ResponseEntity<>(dto, HttpStatus.OK);

        } catch (IllegalArgumentException | ParseException exception) {

            return new ResponseEntity<>(WRONG_INFORMATION,
                    HttpStatus.BAD_REQUEST);
        }
    }


    private void addLinksToDto(ProjectDTO dto) throws ParseException, NoSuchAlgorithmException {
        Link link = linkTo(methodOn(ImplProjectController.class)
                .getProject(dto.getProjectCode())).withSelfRel()
                .withType("PUT");

        Link linkToProductBacklog = linkTo(methodOn(ImplUserStoryController.class)
                .getUserStoryByPriority(dto.getProjectCode())).withRel("productBacklog")
                .withType("GET, PUT");

        Link linkToSprints = linkTo(methodOn(ImplSprintController.class)
                .getAllSprintsByProjectCode(dto.getProjectCode())).withRel("sprints")
                .withType("GET, PUT");

        Link linkToActivities = linkTo(methodOn(ImplProjectController.class)
                .getStatusOfActivities(dto.getProjectCode())).withRel("activities")
                .withType("GET");

        Link linkToResourceAddTeamMember = linkTo(methodOn(ImplResourceController.class)
                .definedTeamMemberOfAProject(null))
                .withRel("addTeamMember")
                .withType("POST");

        Link linkToResourceAddScrumMaster = linkTo(methodOn(ImplResourceController.class)
                .definedScrumMasterOfAProject(null))
                .withRel("addScrumMaster")
                .withType("POST");

        Link linkToResourceAddProductOwner = linkTo(methodOn(ImplResourceController.class)
                .definedProductOwnerOfAProject(null))
                .withRel("addProductOwner")
                .withType("POST");

        dto.add(link, linkToProductBacklog, linkToSprints, linkToActivities,
                linkToResourceAddTeamMember, linkToResourceAddScrumMaster,
                linkToResourceAddProductOwner);
    }


    /**
     * @param projectCode as path variable
     * @param dto         object with update data, from json.
     * @return HTTP response using Spring ResponseEntity Object.
     */
    @PutMapping(value = "/projects/{projectCode}")
    public ResponseEntity<Object> updateProject (
            @PathVariable String projectCode,
            @RequestBody UpdateProjectDTO dto)
            throws ParseException, NoSuchAlgorithmException{
        try {

            if (this.projectService.updateProject(projectCode, dto)
                    .isEmpty()) {
                return new ResponseEntity<>(PROJECT_DOES_NOT_EXIST,
                        HttpStatus.NOT_FOUND);
            }

            ProjectDTO dtoOutput =
                    this.projectService.updateProject(projectCode, dto).get();

            Link link = linkTo(methodOn(ImplProjectController.class).
                    getProject(projectCode)).withSelfRel();

            dtoOutput.add(link);

            return new ResponseEntity<>(dtoOutput, HttpStatus.OK);

        } catch (IllegalArgumentException | ParseException exception) {

            return new ResponseEntity<>(WRONG_INFORMATION,
                    HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/projects/{projectCode}/activities")
    public ResponseEntity<Object> getStatusOfActivities(
            @PathVariable(value = "projectCode") String projectCode) throws ParseException {

        List<StatusOfActivitiesDTO> statusOfActivitiesDTO =
                this.projectService.getStatusOfActivities(projectCode);


        return new ResponseEntity<>(statusOfActivitiesDTO, HttpStatus.OK);

    }


}
