package switchfive.project.binterfaceAdapters.controllers.iControllers;

import org.springframework.http.ResponseEntity;
import switchfive.project.binterfaceAdapters.dataTransferObjects.CustomerDTO;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public interface ICustomerController {

    ResponseEntity<Object> getCustomer(String customerCode);

    ResponseEntity<Object> createAndSaveCustomer(CustomerDTO dto);

    ResponseEntity<Object> getAllCustomers() throws ParseException, NoSuchAlgorithmException;

}
