package switchfive.project.binterfaceAdapters.implRepositories;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IResourceAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.ResourceJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.IResourceRepositoryJPA;
import switchfive.project.capplicationServices.iRepositories.IResourceRepository;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.ResourceID;
import switchfive.project.ddomain.shared.valueObjects.Role;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ImplResourceRepository implements IResourceRepository {

    /**
     * Repository of resources.
     */
    private final IResourceRepositoryJPA iResourceRepositoryJPA;
    private final IResourceAssemblerJPA iResourceAssemblerJPA;

    @Autowired
    public ImplResourceRepository(IResourceRepositoryJPA iResourceRepositoryJPA,
                                  IResourceAssemblerJPA iResourceAssemblerJPA) {
        this.iResourceRepositoryJPA = iResourceRepositoryJPA;
        this.iResourceAssemblerJPA = iResourceAssemblerJPA;
    }

    public ArrayList<Resource> getResourcesByProjectCode(final String projectCode) throws ParseException {
        ArrayList<ResourceJPA> resourceListJPA = iResourceRepositoryJPA.
                getResourceJPAByProjectCode(projectCode);

        ArrayList<Resource> resourceList = new ArrayList<>();

        for (ResourceJPA resourceJPA : resourceListJPA) {
            Resource resource = iResourceAssemblerJPA.toDomain(resourceJPA);
            resourceList.add(resource);
        }

        return resourceList;
    }



    /**
     * Private method that adds the new Resource to the system
     *
     * @param newResource New Resource object to be validated and added to DataManagement System
     */
    public void saveResource(final Resource newResource) {
        ResourceJPA resourceJPA = this.iResourceAssemblerJPA.toData(newResource);

        this.iResourceRepositoryJPA.save(resourceJPA);
    }


    /**
     * Looks for a specific resource and returns an object
     *
     * @param code that will be searched
     * @return an Optional project manager (model domain object)
     */
    public Optional<Resource> getProjectManager(final ProjectCode code) throws ParseException {

        Resource resourceToFind = null;
        String projectCode = code.getCode();
        String pmRole = Role.ProjectManager.toString();

        Optional<ResourceJPA> optResourceJPA = this.iResourceRepositoryJPA
                .getResourceJPAByProjectCodeAndRole(projectCode, pmRole);

        if (optResourceJPA.isPresent()) {
            ResourceJPA resourceJPA = optResourceJPA.get();

            resourceToFind = iResourceAssemblerJPA.toDomainWithEmail(resourceJPA);
        }

        return Optional.ofNullable(resourceToFind);
    }


    /**
     * Looks for a specific profile description and returns an object
     *
     * @param resourceID that will be searched
     * @return an resource (model domain object)
     */
    public Optional<Resource> getResourceByID(
            final ResourceID resourceID) throws ParseException {

        Resource resourceToFind = null;

        Optional<ResourceJPA> optionalResourceJPA = iResourceRepositoryJPA.findById(resourceID.toString());
        if (optionalResourceJPA.isPresent()) {
            ResourceJPA resourceJPA = optionalResourceJPA.get();
            resourceToFind = iResourceAssemblerJPA.toDomain((resourceJPA));

        }

        return Optional.ofNullable(resourceToFind);

    }

    public List<Resource> getResourcesByEmail (String email) throws ParseException {
        List<Resource> resourcesList = new ArrayList<>();
        List<ResourceJPA> resourcesJPAList = iResourceRepositoryJPA.getResourcesJPAByEmail(email);
        for (ResourceJPA selectedResource : resourcesJPAList) {
            Resource resource = iResourceAssemblerJPA.toDomain(selectedResource);
            resourcesList.add(resource);
        }
        return resourcesList;
    }
}
