package switchfive.project.binterfaceAdapters.controllers.iControllers;

import org.springframework.http.ResponseEntity;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SprintCreationDTO;

import java.text.ParseException;

public interface ISprintController {

    ResponseEntity<Object> createAndAddNewSprint(SprintCreationDTO sprintCreationDTO, String projectCode)
            throws ParseException;

}
