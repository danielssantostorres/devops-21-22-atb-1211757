package switchfive.project.binterfaceAdapters.controllers.iControllers;

import org.springframework.http.ResponseEntity;
import switchfive.project.binterfaceAdapters.dataTransferObjects.TypologyDTO;

public interface ITypologyController {

    ResponseEntity<Object> createTypology(TypologyDTO dto);

    ResponseEntity<Object> getTypology(String typology);

    ResponseEntity<Object> getTypologies();


}
