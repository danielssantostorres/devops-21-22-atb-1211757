package switchfive.project.binterfaceAdapters.controllers.implControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchfive.project.binterfaceAdapters.controllers.iControllers.IUserController;
import switchfive.project.binterfaceAdapters.dataTransferObjects.AllocatedProjectsDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ChangePasswordDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.SearchUserDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UserDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppUserService;
import switchfive.project.ddomain.aggregates.user.User;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin
@RestController
@RequestMapping
public class ImplUserController implements IUserController {

    private final static String ERROR_MESSAGE = "Operation failed.";

    private final static String FAILED = "the password has not been updated";

    /**
     * Inject User application service.
     */
    private final IAppUserService userService;

    @Autowired
    /**
     * Constructor with injected service.
     *
     * @param userService User application service.
     */
    public ImplUserController(final IAppUserService userService) {
        this.userService = userService;
    }


    /**
     * Creates User if all fields are valid.
     *
     * @param userDTO - fields to be inserted by user.
     * @return HTTPStatus.OK with message if user is created successfully,
     * otherwise returns HTTPStatus Conflict with error message.
     */
    @PostMapping("/users/")
    @ResponseBody
    public ResponseEntity<Object> createUser(@RequestBody UserDTO userDTO) throws NoSuchAlgorithmException {
        try {
            UserDTO DTOOutput = userService.createAndSaveUser(userDTO);

            Link link = linkTo(methodOn(ImplUserController.class).getUsersByEmail(userDTO.email)).withSelfRel();

            DTOOutput.add(link);

            return new ResponseEntity<>(DTOOutput, HttpStatus.CREATED);
        } catch (IllegalArgumentException operationFailed) {
            return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.CONFLICT);
        }
    }


    @PostMapping("/profiles/{profileID}/updateuser/{email}")
    public ResponseEntity<Object> addProfile(@PathVariable(value = "email") String email, @PathVariable(value = "profileID") String profileID) {
        try {
            Optional<SearchUserDTO> userDTO = userService.validateAndAddProfile(email, profileID);

            Link link = linkTo(methodOn(ImplUserController.class).getUsersByEmail(email)).withSelfRel();

            userDTO.get().add(link);

            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (IllegalArgumentException | NoSuchAlgorithmException | NoSuchElementException e) {
            return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/profiles/{profileID}/updateuser/{email}")
    public ResponseEntity<Object> removeProfile(@PathVariable(value = "email") String email, @PathVariable(value = "profileID") String profileID) {
        try {
            Optional<SearchUserDTO> userDTO = userService.validateAndRemoveProfile(email, profileID);

            Link link = linkTo(methodOn(ImplUserController.class).getUsersByEmail(email)).withSelfRel();

            userDTO.get().add(link);

            return new ResponseEntity<>(userDTO, HttpStatus.OK);

        } catch (IllegalArgumentException | NoSuchAlgorithmException | NoSuchElementException e) {

            return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.BAD_REQUEST);

        }
    }

    @GetMapping(value = "/users")
    @ResponseBody
    public ResponseEntity<Object> getUsersByEmail(@RequestParam(value = "email") String email) throws NoSuchAlgorithmException {
        List<SearchUserDTO> userList = userService.getUsersByEmail(email);
        addLinksToDTOList(userList);
        return new ResponseEntity<>(userList, HttpStatus.OK);
    }


    @GetMapping(value = "/users/email/{email}")
    @ResponseBody
    public ResponseEntity<Object> getUserByEmail(@PathVariable(value = "email") String email) throws NoSuchAlgorithmException {
        try {
            Optional<SearchUserDTO> user = userService.getUserByEmail(email);
            Link link = linkTo(methodOn(ImplUserController.class).getUserByEmail(email)).withSelfRel().withType("GET");

            user.get().add(link);

            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch ( NoSuchElementException e) {
            return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * The purpose of this method is to receive list of users (domain) and convert it to a SearchUserDTO with selfLink.
     *
     * @param users
     * @return list of searchUserDTO with selflink.
     */

    private List<SearchUserDTO> addLinksToDTOList(List<SearchUserDTO> users) throws NoSuchAlgorithmException {
        List<SearchUserDTO> resultList = new ArrayList<>();
        for (SearchUserDTO selectedUser : users) {
            Link link = linkTo(methodOn(ImplUserController.class).getUsersByEmail(selectedUser.getEmail())).withSelfRel();
            selectedUser.add(link);
            resultList.add(selectedUser);
        }
        return resultList;
    }


    @GetMapping(value = "/users/profiles/{id}")
    @ResponseBody
    public ResponseEntity<Object> getUsersByProfile(@PathVariable(value = "id") String profileDescription) throws NoSuchAlgorithmException {
        List<SearchUserDTO> userList = userService.getUsersByProfile(profileDescription);
        List<SearchUserDTO> resultList = addLinksToDTOList(userList);
        return new ResponseEntity<>(resultList, HttpStatus.OK);
    }

    /**
     * If code is the same as the one generated by the system user account is activated.
     *
     * @param userDTO - code to activate account.
     * @return HttpStatus.OK with message if User account is successfully activated,
     * otherwise returns HttpStatus.NOT_ACCEPTABLE with error message.
     */
    @PatchMapping("/users/")
    public ResponseEntity<Object> activateAccount(@RequestBody UserDTO userDTO) throws NoSuchAlgorithmException {

        Optional<UserDTO> userDTOOpt = userService.activateAccount(userDTO);

        if (userDTOOpt.isPresent() && userDTOOpt.get().activation) {

            UserDTO userDTOOutput = userDTOOpt.get();

            Link link = linkTo(methodOn(ImplUserController.class).getUserByEmail(userDTO.email)).withSelfRel();

            userDTOOutput.add(link);

            return new ResponseEntity<>(userDTOOutput, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.UNAUTHORIZED);
        }
    }


    /**
     * Update user if fields to be changed are successfully inserted.
     *
     * @param userDTO - field to update user.
     * @return HttpStatus.OK with message if User is successfully updated,
     * otherwise returns HttpStatus Conflict with error message.
     */
    @PutMapping("/users")
    @ResponseBody
    public ResponseEntity<Object> updateUser(@RequestBody UserDTO userDTO) throws NoSuchAlgorithmException {

        Optional<UserDTO> updatedUserDTO = userService.updateUser(userDTO);

        if (updatedUserDTO.isPresent()) {
            Link link = linkTo(methodOn(ImplUserController.class).getUserByEmail(userDTO.email)).withSelfRel();
            updatedUserDTO.get().add(link);

            return new ResponseEntity<>(updatedUserDTO.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Change the password of the user
     *
     * @param passwordDto field to change password
     * @return HttpStatus.OK with message if password of the User is successfully changed,
     * otherwise returns HttpStatus Conflict with error message.
     */
    @PatchMapping("/users/password")
    public ResponseEntity<Object> changePassword(@RequestBody ChangePasswordDTO passwordDto)
            throws NoSuchAlgorithmException {

        try {
            Optional<UserDTO> userDTOOpt = userService.changePassword(passwordDto);

            if (userDTOOpt.isPresent()) {
                UserDTO userDTOoutp = userDTOOpt.get();

                Link link = linkTo(methodOn(ImplUserController.class)
                        .getUsersByEmail(userDTOoutp.email)).withSelfRel();

                userDTOoutp.add(link);

                return new ResponseEntity<>(userDTOoutp, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping(value = "/users/projects/{email}")
    public ResponseEntity<Object> getAllocatedProjects(@PathVariable final String email) throws NoSuchAlgorithmException, ParseException {

        Optional<User> searchUser = userService.findById(email);
        if (searchUser.isPresent()) {
            Optional<List<AllocatedProjectsDTO>> allocatedProjectsDTO = userService.findAllocatedProjects(email);

            for (AllocatedProjectsDTO selectedDTO : allocatedProjectsDTO.get()) {

                Link link = linkTo(methodOn(ImplProjectController.class)
                        .getProject(selectedDTO.getProjectCode())).withSelfRel();

                selectedDTO.add(link);
            }

            return new ResponseEntity<>(allocatedProjectsDTO.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.NOT_FOUND);
        }
    }


    @PatchMapping("users/email/{email}/activate-user")
    public ResponseEntity<Object> setUserToActive(@PathVariable(value = "email") String email) throws NoSuchAlgorithmException {

        Optional<SearchUserDTO> user = userService.setUserToActive(email);

        if(user.isPresent()) {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping("users/email/{email}/inactivate-user")
    public ResponseEntity<Object> setUserToInactive(@PathVariable(value = "email") String email) throws NoSuchAlgorithmException {

        Optional<SearchUserDTO> user = userService.setUserToInactive(email);

        if(user.isPresent()) {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
        }

    }
}
