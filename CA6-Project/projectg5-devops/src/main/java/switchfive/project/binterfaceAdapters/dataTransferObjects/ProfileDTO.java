package switchfive.project.binterfaceAdapters.dataTransferObjects;

import org.springframework.hateoas.RepresentationModel;

public class ProfileDTO extends
        RepresentationModel<ProfileDTO> {

    public String designation;

    public ProfileDTO(String designation) {
        this.designation = designation;
    }

}
