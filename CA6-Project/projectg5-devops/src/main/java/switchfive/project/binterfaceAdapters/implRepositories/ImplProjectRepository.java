package switchfive.project.binterfaceAdapters.implRepositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IProjectAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.ProjectJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.IProjectRepositoryJPA;
import switchfive.project.capplicationServices.iRepositories.IProjectRepository;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ImplProjectRepository implements IProjectRepository {
    private final IProjectRepositoryJPA projectRepositoryJPA;
    private final IProjectAssemblerJPA projectAssembler;

    @Autowired
    public ImplProjectRepository(IProjectRepositoryJPA projectRepositoryJPA,
                                 IProjectAssemblerJPA projectAssembler) {
        this.projectRepositoryJPA = projectRepositoryJPA;
        this.projectAssembler = projectAssembler;
    }


    public void saveProject(Project newProject) {
        ProjectJPA projectJPA = new ProjectJPA(newProject);

        this.projectRepositoryJPA.save(projectJPA);
    }


    public Optional<Project> findByCode(ProjectCode code)
            throws ParseException {
        ProjectJPA projectJPA;

        if (this.projectRepositoryJPA.existsByCode(code.getCode())) {
            projectJPA =
                    this.projectRepositoryJPA.findByCode(code.getCode()).get();

            Project project = this.projectAssembler.toDomain(projectJPA);

            return Optional.of(project);
        }

        return Optional.empty();
    }

    public boolean existsCode(ProjectCode projectCode) {
        return this.projectRepositoryJPA.existsByCode(projectCode.getCode());
    }

    public List<Project> findAll() throws ParseException {
        List<Project> projectList = new ArrayList<>();
        List<ProjectJPA> projectJPAList = this.projectRepositoryJPA.findAll();

        for (ProjectJPA each : projectJPAList) {
            projectList.add(this.projectAssembler.toDomain(each));
        }

        return projectList;
    }
}
