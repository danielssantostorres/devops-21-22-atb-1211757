package switchfive.project.binterfaceAdapters.dataTransferObjects;

import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.Objects;

public class SearchUserDTO extends RepresentationModel<SearchUserDTO> {

    public String email;

    public List<String> userProfileList;

    public boolean activation;

    public String userName;

    public String function;


    private SearchUserDTO(String email, List<String> userProfileList, boolean activation, String userName, String function) {
        this.email = email;
        this.userProfileList = userProfileList;
        this.activation = activation;
        this.userName = userName;
        this.function = function;
    }

    public static SearchUserDTO createSearchUserDTO(String email, List<String> userProfileList, boolean activation, String name, String function) {
        return new SearchUserDTO(email, userProfileList, activation, name, function);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchUserDTO that = (SearchUserDTO) o;
        return activation == that.activation && Objects.equals(email, that.email) && Objects.equals(userProfileList, that.userProfileList) && Objects.equals(userName, that.userName) && Objects.equals(function, that.function);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, userProfileList, activation, userName, function);
    }

    public String getEmail() {
        return email;
    }
}
