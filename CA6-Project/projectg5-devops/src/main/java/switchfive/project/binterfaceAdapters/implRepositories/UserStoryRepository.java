package switchfive.project.binterfaceAdapters.implRepositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IUserStoryAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.UserStoryJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.IUserStoryRepositoryJPA;
import switchfive.project.capplicationServices.iRepositories.IUserStoryRepository;
import switchfive.project.ddomain.aggregates.userStory.UserStory;
import switchfive.project.ddomain.shared.valueObjects.Priority;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.UserStoryCode;
import switchfive.project.ddomain.shared.valueObjects.UserStoryStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * ProductBacklog class describes the data and the methods of its objects.
 *
 * @author Maurício Pinto Barros
 * @version 0
 * @since Meeting 28-12-2021
 */

@Repository
public class UserStoryRepository implements IUserStoryRepository {

    private final IUserStoryRepositoryJPA userStoryRepositoryJPA;

    private final IUserStoryAssemblerJPA userStoryAssemblerJPA;

    @Autowired
    public UserStoryRepository(IUserStoryRepositoryJPA userStoryRepositoryJPA,
                               IUserStoryAssemblerJPA userStoryAssemblerJPA) {
        this.userStoryRepositoryJPA = userStoryRepositoryJPA;
        this.userStoryAssemblerJPA = userStoryAssemblerJPA;
    }

    @Override
    public UserStory save(UserStory newUserStory) {

        UserStoryJPA userStoryJPA = userStoryAssemblerJPA.userStoryToUserStoryJPA(newUserStory);
        UserStoryJPA userStoryJPAInDb = userStoryRepositoryJPA.save(userStoryJPA);
        return userStoryAssemblerJPA.userStoryJPAtoUserStory(userStoryJPAInDb);
    }

    /**
     * Method to calculate the default priority of an UserStory when created.
     * When created, the userStory will go to the bottom of the list of priorities.
     *
     * @return an incremental priority
     */
    @Override
    public int nextUserStoryNumber(ProjectCode projectCode) {

        List<UserStoryJPA> userStoryList = this.userStoryRepositoryJPA.findByUserStoryID_ProjectCode_OrderByPriority(projectCode.getCode());
        int nextUserStoryNumber = userStoryList.size();
        return nextUserStoryNumber + 1;
    }

    /**
     * Private method «Generate Code».
     *
     * @return an incremetal ID_UserStory that match with the created UserStory, e.g. "US1", "US2", "US3"
     */
    @Override
    public String generatorCode(ProjectCode projectCode) {
        int nextNumber = nextUserStoryNumber(projectCode);
        String code = "US" + nextNumber;
        return code;
    }

    /**
     * Get method for User Story List.
     *
     * @return a list of user stories.
     */
    @Override
    public List<UserStory> getUserStoryListProductBacklog(ProjectCode projectCode) {

        List<UserStory> userStoryList = new ArrayList<>();
        Integer sprintNumber = 0;
        List<UserStoryJPA> userStoryJPAList = this.userStoryRepositoryJPA
                .findByUserStoryID_ProjectCodeAndSprintNumberEqualsAndStatusEqualsOrderByPriority(projectCode.getCode(),
                        sprintNumber,
                        "PLANNED");

        for (UserStoryJPA userStoryJPA : userStoryJPAList) {

            UserStory userStory = this.userStoryAssemblerJPA.userStoryJPAtoUserStory(userStoryJPA);
            userStoryList.add(userStory);

        }
        return userStoryList;
    }


    /**
     * Returns the User Story in the Product Backlog given an identifying code.
     *
     * @param userStoryCode UserStoryCode obj.
     * @return UserStory object. Null if User Story is not found.
     */
    public Optional<UserStory> getUserStory(ProjectCode projectCode, UserStoryCode userStoryCode) {

        if (this.userStoryRepositoryJPA.existsByUserStoryID_UserStoryCodeAndUserStoryID_ProjectCode(userStoryCode.getIdentity(),
                projectCode.getCode())) {
            UserStoryJPA userStoryJPA = this.userStoryRepositoryJPA
                    .findByUserStoryID_UserStoryCodeAndUserStoryID_ProjectCode(userStoryCode.getIdentity(),
                            projectCode.getCode());

            UserStory userStory = this.userStoryAssemblerJPA.userStoryJPAtoUserStory(userStoryJPA);

            return Optional.of(userStory);
        }
        return Optional.empty();
    }


    /**
     * Method to set a new priority to a certain UserStory.
     *
     * @param code        String code
     * @param newPriority int priority
     * @return
     */
    /*public boolean setPriority(String code, int newPriority) {
        boolean priorityChanged = false;

        UserStory userStoryToSetPriority = getUserStory(code);

        if (userStoryToSetPriority != null && validPriority(newPriority)) {
            userStoryList.remove(userStoryToSetPriority);
            userStoryList.add(newPriority - 1, userStoryToSetPriority);
            updatesAllUserStoriesPriorities();
            priorityChanged = true;
        }


        return priorityChanged;
    }*/

    /**
     * Updates the priority of all the user stories in the userStoryList
     * if a new priority was established in the list.
     */
    /*private void updatesAllUserStoriesPriorities() {
        for (int i = 0; i < userStoryList.size(); i++) {
            UserStory userStory = userStoryList.get(i);
            // TODO Priority method with reference to Project
            Priority wrongPriority = Priority.createPriority(1);
            userStory.setPriority(wrongPriority);
        }
    }*/
    @Override
    public boolean existsUserStory(ProjectCode projectCode, UserStoryCode userStoryCode) {
        String projectCodeStr = projectCode.getCode();
        String userStoryCodeStr = userStoryCode.getIdentity();

        return userStoryRepositoryJPA
                .existsByUserStoryID_UserStoryCodeAndUserStoryID_ProjectCode(userStoryCodeStr,
                        projectCodeStr);

    }

    @Override
    public Optional<UserStory> updateUserStory(UserStory userStory) {
        UserStoryJPA userStoryJPA = userStoryAssemblerJPA.userStoryToUserStoryJPA(userStory);

        UserStoryJPA updatedUserStoryJPA = userStoryRepositoryJPA.save(userStoryJPA);

        UserStory updatedUserStory = userStoryAssemblerJPA.userStoryJPAtoUserStory(updatedUserStoryJPA);

        return Optional.of(updatedUserStory);
    }

    @Override
    public List<UserStory> getUserStoryListInProjectOrderedByStatus(ProjectCode projectCode) {

        List<UserStory> userStoryList = new ArrayList<>();
        List<UserStoryJPA> userStoryJPAList = this.userStoryRepositoryJPA
                .findByUserStoryID_ProjectCodeOrderByStatus(projectCode.getCode());

        for (UserStoryJPA userStoryJPA : userStoryJPAList) {
            UserStory userStory = this.userStoryAssemblerJPA.userStoryJPAtoUserStory(userStoryJPA);
            userStoryList.add(userStory);
        }
        return userStoryList;
    }

    public Optional<UserStory> userStoryUpdateStatus(ProjectCode projectCode,
                                                     UserStoryCode userStoryCode,
                                                     UserStoryStatus status) {

        UserStoryJPA userStoryJPA = userStoryRepositoryJPA.findByUserStoryID_UserStoryCodeAndUserStoryID_ProjectCode(
                userStoryCode.getIdentity(), projectCode.getCode());
        UserStory userStory = userStoryAssemblerJPA.userStoryJPAtoUserStory(userStoryJPA);
        if (userStory.getStatus() != status.toString()) {
            userStory.updateStatus(status.toString());
            UserStoryJPA userStoryJPAupdated = userStoryAssemblerJPA.userStoryToUserStoryJPA(userStory);
            userStoryRepositoryJPA.save(userStoryJPAupdated);
        }
        return Optional.of(userStory);
    }
}
