package switchfive.project.binterfaceAdapters.controllers.iControllers;

import org.springframework.http.ResponseEntity;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public interface IApplicationController {

    ResponseEntity<Object> applicationOptionsMapping() throws ParseException, NoSuchAlgorithmException;
}
