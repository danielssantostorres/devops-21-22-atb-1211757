package switchfive.project.binterfaceAdapters.controllers.iControllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import switchfive.project.binterfaceAdapters.dataTransferObjects.CreateUserStoryDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.RefineUserStoryDTO;

public interface IUserStoryController {

    ResponseEntity<Object> createUserStory(@PathVariable(value = "projectCode") String projectCode,
                                           @RequestBody CreateUserStoryDTO description);

    ResponseEntity<Object> refineUserStory(@RequestBody RefineUserStoryDTO refineUserStoryDTO);
}
