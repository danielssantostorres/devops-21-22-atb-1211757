package switchfive.project.binterfaceAdapters.implRepositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.ITaskAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.TaskIDJPA;
import switchfive.project.ainfrastructure.persistence.data.TaskJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.ITaskRepositoryJPA;
import switchfive.project.capplicationServices.iRepositories.ITaskRepository;
import switchfive.project.ddomain.aggregates.task.Task;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.TaskID;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ImplTaskRepository implements ITaskRepository {

    private ITaskRepositoryJPA iTaskRepositoryJPA;

    private ITaskAssemblerJPA iTaskAssemblerJPA;

    @Autowired
    public ImplTaskRepository(ITaskRepositoryJPA iTaskRepositoryJPA, ITaskAssemblerJPA iTaskAssemblerJPA) {
        this.iTaskRepositoryJPA = iTaskRepositoryJPA;
        this.iTaskAssemblerJPA = iTaskAssemblerJPA;
    }


    public String nextTaskCode(ProjectCode projectCode) {
        List<TaskJPA> taskJPAList = iTaskRepositoryJPA.
                findTaskJPAByProjectCode(projectCode.getCode());
        int nextTaskNumber = taskJPAList.size() + 1;
        return "T" + nextTaskNumber;
    }

    public Optional<Task> findTaskById (TaskIDJPA taskId) throws ParseException {
        Optional<TaskJPA> taskJPAOpt = iTaskRepositoryJPA.findById(taskId);

        Task task = null;
        if (taskJPAOpt.isPresent()) {
            TaskJPA taskJPA = taskJPAOpt.get();
            task = iTaskAssemblerJPA.toDomain(taskJPA);
        }

        return  Optional.ofNullable(task);
    }

    @Override
    public Task save(Task task) throws ParseException {
        TaskJPA taskJPA = iTaskAssemblerJPA.toData(task);
        TaskJPA taskJPAInRepo = iTaskRepositoryJPA.save(taskJPA);
        return iTaskAssemblerJPA.toDomain(taskJPAInRepo);
    }

    public List<Task> findAllTasks() throws ParseException {
        List<Task> taskList = new ArrayList<>();
        Iterable<TaskJPA> tasksJPA = iTaskRepositoryJPA.findAll();

        for (TaskJPA taskJPA : tasksJPA){
            Task task = iTaskAssemblerJPA.toDomain(taskJPA);
            taskList.add(task);
        }

        return taskList;
    }

    public List<Task> findTaskInProjectOrderedByStatus(ProjectCode projectCode
    ) throws ParseException {

        List<Task> taskList = new ArrayList<>();
        Iterable<TaskJPA> tasksJPA = iTaskRepositoryJPA
                .findTaskJPAByProjectCodeOrderByTaskStatus(projectCode.getCode());

        for (TaskJPA taskJPA : tasksJPA){
            Task task = iTaskAssemblerJPA.toDomain(taskJPA);
            taskList.add(task);
        }

        return taskList;
    }
}
