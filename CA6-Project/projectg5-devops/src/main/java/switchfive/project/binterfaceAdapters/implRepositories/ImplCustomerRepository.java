package switchfive.project.binterfaceAdapters.implRepositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.ICustomerAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.CustomerJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.ICustomerRepositoryJPA;
import switchfive.project.capplicationServices.iRepositories.ICustomerRepository;
import switchfive.project.ddomain.aggregates.customer.Customer;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.shared.valueObjects.CustomerName;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ImplCustomerRepository implements ICustomerRepository {
    private final ICustomerRepositoryJPA customerRepositoryJPA;
    private final ICustomerAssemblerJPA customerAssemblerJPA;

    @Autowired
    public ImplCustomerRepository(ICustomerRepositoryJPA customerRepositoryJPA,
                                  ICustomerAssemblerJPA customerAssemblerJPA) {
        this.customerRepositoryJPA = customerRepositoryJPA;
        this.customerAssemblerJPA = customerAssemblerJPA;
    }

    public void saveCustomer(Customer customer) {
        CustomerJPA entityJPA =
                this.customerAssemblerJPA.customerToCustomerJPA(customer);

        this.customerRepositoryJPA.save(entityJPA);
    }

    public Optional<Customer> findCustomerByName(CustomerName customerName) {
        CustomerJPA customerJPA;

        if (this.customerRepositoryJPA.findByName(customerName.getName())
                .isPresent()) {
            customerJPA =
                    this.customerRepositoryJPA.findByName(
                            customerName.getName()).get();

            Customer customer =
                    this.customerAssemblerJPA.customerJpaToCustomer(
                            customerJPA);

            return Optional.ofNullable(customer);
        }

        return Optional.empty();
    }

    public boolean existsName(CustomerName customerName) {
        return this.customerRepositoryJPA.existsByName(customerName.getName());
    }

    public List<Customer> findAll() {
        List<Customer> customerList = new ArrayList<>();
        List<CustomerJPA> customerJPAList =
                this.customerRepositoryJPA.findAll();

        for (CustomerJPA each : customerJPAList) {
            customerList.add(this.customerAssemblerJPA.customerJpaToCustomer(each));
        }

        return customerList;
    }
}
