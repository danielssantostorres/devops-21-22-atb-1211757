package switchfive.project.binterfaceAdapters.dataTransferObjects;

import org.springframework.hateoas.RepresentationModel;

public class AllocatedProjectsDTO extends RepresentationModel<AllocatedProjectsDTO> {

    private String projectName;

    private String projectCode;

    private String role;

    public AllocatedProjectsDTO() {
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }
}
