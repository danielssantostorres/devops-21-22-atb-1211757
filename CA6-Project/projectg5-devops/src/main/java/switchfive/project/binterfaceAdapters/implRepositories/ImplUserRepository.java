package switchfive.project.binterfaceAdapters.implRepositories;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IUserAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.UserJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.IUserRepositoryJPA;
import switchfive.project.capplicationServices.iRepositories.IUserRepository;
import switchfive.project.ddomain.aggregates.user.User;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ImplUserRepository implements IUserRepository {

    /**
     * Attribute UserList.
     */
    private final IUserRepositoryJPA userStore;
    private final IUserAssemblerJPA userAssembler;

    @Autowired
    public ImplUserRepository(IUserRepositoryJPA userStore,
                              IUserAssemblerJPA userAssembler) {
        this.userStore = userStore;
        this.userAssembler = userAssembler;
    }

    /**
     * A method that loops through a users list and finds a user with a given
     * profile description adding it to a result list.
     *
     * @param profileId that will be searched.
     * @return a list with all the users with a given profileName.
     */
    public List<User> findUsersByProfile(final String profileId)
            throws NoSuchAlgorithmException {
        ArrayList<User> resultList = new ArrayList<>();
        List<UserJPA> usersJPA =
                this.userStore.findUsersJPAByProfile(profileId);

        for (UserJPA userjpa : usersJPA) {
            User user = this.userAssembler.toUser(userjpa);
            resultList.add(user);
        }

        return resultList;
    }

    public UserJPA save(User user) {
        UserJPA userJPA = this.userAssembler.toJPA(user);

        return userStore.save(userJPA);
    }

    public Optional<User> update(User user) throws NoSuchAlgorithmException {
        UserJPA userJPA = userAssembler.toJPA(user);

        userJPA = userStore.save(userJPA);

        return Optional.ofNullable(userAssembler.toUser(userJPA));
    }

    public Optional<List<User>> findAllUsers() throws NoSuchAlgorithmException {
        User user;
        List<User> users = new ArrayList<>();

        List<UserJPA> usersJPA = this.userStore.findAll();

        if (usersJPA.isEmpty()) {
            return Optional.empty();
        }

        for (UserJPA each : usersJPA) {
            user = userAssembler.toUser(each);
            users.add(user);
        }

        return Optional.of(users);
    }


    /**
     * /**
     * A method that loops through a users list until it finds a given email.
     *
     * @param email that will be searched
     * @return a list with the user with a given email
     */

    public List<User> findUsersByEmail(final String email) throws NoSuchAlgorithmException {

        List<UserJPA> usersJPA = userStore.findUsersByEmail(email);

        List<User> users = new ArrayList<>();

        for (UserJPA userJPA : usersJPA) {
            User user = userAssembler.toUser(userJPA);
            users.add(user);
        }

        return users;
    }

    public Optional<User> findUserByEmail(final String email) throws NoSuchAlgorithmException {

        Optional<UserJPA> userJPA = userStore.findUserJPAByEmail(email);
        User selectedUser = null;

        if (userJPA.isPresent()) {
            selectedUser = userAssembler.toUser(userJPA.get());
        }

        return Optional.ofNullable(selectedUser);
    }


    public Optional<User> addProfile(String email, String profileDescription) throws NoSuchAlgorithmException {

        UserJPA userJPA;

        userJPA = this.userStore.findUserJPAByEmail(email).get();

        userJPA.addProfile(profileDescription);
        this.userStore.save(userJPA);

        User user = this.userAssembler.toUser(userJPA);

        return Optional.of(user);

    }

    public Optional<User> removeProfile(String email, String profileDescription) throws NoSuchAlgorithmException {

        UserJPA userJPA;

        userJPA = this.userStore.findUserJPAByEmail(email).get();

        userJPA.removeProfile(profileDescription);
        this.userStore.save(userJPA);

        User user = this.userAssembler.toUser(userJPA);

        return Optional.of(user);
    }


}
