package switchfive.project.binterfaceAdapters.implRepositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.ISprintAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.SprintJPA;
import switchfive.project.ainfrastructure.persistence.iRepositories.ISprintRepositoryJPA;
import switchfive.project.capplicationServices.iRepositories.ISprintRepository;
import switchfive.project.ddomain.aggregates.sprint.Sprint;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.SprintID;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ImplSprintRepository implements ISprintRepository {
    private ISprintRepositoryJPA sprintRepositoryJPA;
    private ISprintAssemblerJPA sprintAssemblerJPA;

    @Autowired
    public ImplSprintRepository(ISprintRepositoryJPA repositoryJPA,
                                ISprintAssemblerJPA assemblerJPA) {
        this.sprintRepositoryJPA = repositoryJPA;
        this.sprintAssemblerJPA = assemblerJPA;
    }

    public boolean existsBySprintID(final SprintID id) {
        return this.sprintRepositoryJPA.existsById(id);
    }

    public Optional<Sprint> findBySprintID(final SprintID id) throws ParseException {
        SprintJPA selected;

        if (this.sprintRepositoryJPA.findById(id).isPresent()) {
            selected = this.sprintRepositoryJPA.findById(id).get();

            return Optional.of(this.sprintAssemblerJPA.toDomain(selected));
        }

        return Optional.empty();
    }

    public Sprint saveSprint(final Sprint sprint) throws ParseException {
        SprintJPA sprintJPA = new SprintJPA(sprint);

        SprintJPA result = this.sprintRepositoryJPA.save(sprintJPA);

        return this.sprintAssemblerJPA.toDomain(result);
    }

    public Optional<List<Sprint>> findAllSprintsByProjectCode(final ProjectCode code)
            throws ParseException {
        List<Sprint> sprintList = new ArrayList<>();
        Sprint thisSprint;

        List<SprintJPA> sprintJpa = (List<SprintJPA>) sprintRepositoryJPA.findAllBySprintID_projectCode(code.getCode());

        if (sprintJpa.isEmpty()) {

            return Optional.empty();
        }

        for (SprintJPA each : sprintJpa) {
            thisSprint = this.sprintAssemblerJPA.toDomain(each);
            sprintList.add(thisSprint);
        }

        return Optional.of(sprintList);
    }

    public long countSprintsByProjectCode(final ProjectCode code) {
        return sprintRepositoryJPA.countSprintJPABySprintID_projectCode(code.getCode());
    }
}
