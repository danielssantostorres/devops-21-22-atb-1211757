package switchfive.project.binterfaceAdapters.controllers.iControllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProjectDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.UpdateProjectDTO;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public interface IProjectController {

    ResponseEntity<Object> getProject(String projectCode) throws ParseException, NoSuchAlgorithmException;

    ResponseEntity<Object> getAllProjects() throws ParseException, NoSuchAlgorithmException;

    ResponseEntity<Object> createProject(ProjectDTO dto) throws ParseException;

    ResponseEntity<Object> updateProject(String projectCode, UpdateProjectDTO dto)throws ParseException, NoSuchAlgorithmException;

    ResponseEntity<Object> getStatusOfActivities(@PathVariable String projectCode) throws ParseException;

}
