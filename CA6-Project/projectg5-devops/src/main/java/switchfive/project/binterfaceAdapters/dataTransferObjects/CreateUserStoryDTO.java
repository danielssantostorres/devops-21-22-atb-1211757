package switchfive.project.binterfaceAdapters.dataTransferObjects;

public class CreateUserStoryDTO {
    /**
     *
     */
    public String description;

    public CreateUserStoryDTO() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
