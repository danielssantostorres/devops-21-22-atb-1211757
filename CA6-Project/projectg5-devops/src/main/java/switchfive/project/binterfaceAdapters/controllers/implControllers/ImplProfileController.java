package switchfive.project.binterfaceAdapters.controllers.implControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchfive.project.binterfaceAdapters.controllers.iControllers.IProfileController;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProfileCreationDTO;
import switchfive.project.binterfaceAdapters.dataTransferObjects.ProfileDTO;
import switchfive.project.capplicationServices.appServices.iappServices.IAppProfileService;

import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping(value = "/api")
public class ImplProfileController implements IProfileController {

    @Autowired
    private IAppProfileService iAppProfileService;

    public ImplProfileController(IAppProfileService iAppProfileService) {
        this.iAppProfileService = iAppProfileService;
    }

    @GetMapping("/profiles/{profileDescription}")
    public ResponseEntity<Object> getProfile(@PathVariable String profileDescription) {
        try {
            Optional<ProfileDTO> optProfileDTO = iAppProfileService.getProfile(profileDescription);

            if (optProfileDTO.isPresent()) {
                ProfileDTO profileDTO = optProfileDTO.get();
                Link link = linkTo(methodOn(ImplProfileController.class)
                        .getProfile(profileDTO.designation)).withSelfRel();
                profileDTO.add(link);
                return new ResponseEntity<>(profileDTO, HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Profile not found in repository",
                        HttpStatus.NOT_FOUND);
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/profiles")
    public ResponseEntity<Object> getProfiles() {
        try {
            List<ProfileDTO> profilesDTO = iAppProfileService.getProfiles();
            RepresentationModel<ProfileDTO> profileLinks = new RepresentationModel<>();

            for (ProfileDTO profileDTO : profilesDTO) {
                Link link = linkTo(methodOn(ImplProfileController.class)
                        .getProfile(profileDTO.designation)).withRel(profileDTO.designation);
                profileLinks.add(link);
            }

            return new ResponseEntity<>(profileLinks, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping("/profiles")
    public ResponseEntity<Object> addNewProfile(@RequestBody ProfileCreationDTO
                                                        profileCreationDTO) {
        try {
            Optional<ProfileDTO> profileDTOOpt = iAppProfileService
                    .addNewProfile(profileCreationDTO.description);

            if (profileDTOOpt.isPresent()) {
                ProfileDTO profileDTO = profileDTOOpt.get();

                Link link = linkTo(methodOn(ImplProfileController.class)
                        .getProfile(profileDTO.designation)).withSelfRel();

                profileDTO.add(link);
                return new ResponseEntity<>(profileDTO, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>("Profile already in Store",
                        HttpStatus.BAD_REQUEST);
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }
    }


}
