package switchfive.project.ddomain.factories.implFactories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.ddomain.aggregates.sprint.Sprint;
import switchfive.project.ddomain.factories.iFactories.ISprintFactory;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.SprintDescription;
import switchfive.project.ddomain.shared.valueObjects.SprintID;
import switchfive.project.ddomain.shared.valueObjects.SprintNumber;
import switchfive.project.ddomain.shared.valueObjects.SprintStatus;
import switchfive.project.ddomain.shared.valueObjects.Time;

@Component
public class ImplSprintFactory implements ISprintFactory {

    @Autowired
    public ImplSprintFactory() {
    }

    public Sprint create(final SprintID sprintID, final ProjectCode projectCode,
                         final SprintNumber sprintNumber, final Time dates,
                         final SprintDescription description,
                         final SprintStatus status) {

        return new Sprint(sprintID, projectCode, sprintNumber, dates,
                description, status);
    }
}
