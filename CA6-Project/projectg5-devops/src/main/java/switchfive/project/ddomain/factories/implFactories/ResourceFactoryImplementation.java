package switchfive.project.ddomain.factories.implFactories;

import org.springframework.stereotype.Component;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.factories.iFactories.IResourceFactory;
import switchfive.project.ddomain.shared.valueObjects.*;

@Component
public final class ResourceFactoryImplementation implements IResourceFactory {

    /**
     * @param identityInput    ID_Resource identityInput
     * @param userIdInput      ID_User userIdInput
     * @param projectCodeInput ProjectCode projectCodeInput
     * @param datesInput       Time datesInput
     * @param costPerHourInput ResourceCostPerHour costPerHourInput
     * @param allocationInput  ResourcePercentageOfAllocation allocationInput
     * @param roleInput        Role roleInput
     * @return new instance of REsource class.
     */
    @Override
    public Resource createResource(final ResourceID identityInput,
                                   final Email userIdInput,
                                   final ProjectCode projectCodeInput,
                                   final Time datesInput,
                                   final ResourceCostPerHour costPerHourInput,
                                   final ResourcePercentageOfAllocation
                                           allocationInput,
                                   final Role roleInput) {

        return new Resource(identityInput, userIdInput, projectCodeInput,
                datesInput, costPerHourInput, allocationInput, roleInput);
    }

    public Resource createResourceWithEmail(ResourceID identityInput,
                                            Email email,
                                            ProjectCode projectCodeInput,
                                            Time datesInput,
                                            ResourceCostPerHour costPerHourInput,
                                            ResourcePercentageOfAllocation allocationInput,
                                            Role roleInput) {

        return new Resource(identityInput, email, projectCodeInput,
                datesInput, costPerHourInput, allocationInput, roleInput);
    }
}
