package switchfive.project.ddomain.shared.valueObjects;

public enum SprintStatus {
    PLANNED,
    RUNNING,
    FINISHED
}
