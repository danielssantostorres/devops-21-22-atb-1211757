package switchfive.project.ddomain.domainServices.iDomainServices;

import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.sprint.Sprint;
import switchfive.project.ddomain.shared.valueObjects.Time;


import java.text.ParseException;

public interface ISprintDomainService {

    boolean sprintIsValid(Time previousSprintTime, Time newSprintTime,
                          Project project)
            throws ParseException;

}
