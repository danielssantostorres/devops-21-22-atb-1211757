package switchfive.project.ddomain.factories.implFactories;

import org.springframework.stereotype.Component;
import switchfive.project.ddomain.aggregates.typology.Typology;
import switchfive.project.ddomain.factories.iFactories.ITypologyFactory;
import switchfive.project.ddomain.shared.valueObjects.TypologyDescription;

@Component
public class ImplTypologyFactory implements ITypologyFactory {

    public Typology createTypology(TypologyDescription description) {
        return new Typology(description);
    }
}
