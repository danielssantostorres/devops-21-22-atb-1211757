package switchfive.project.ddomain.aggregates.task;

public interface TaskContainer {

    String getProjectCode();

    Object getTaskContainerID();

}
