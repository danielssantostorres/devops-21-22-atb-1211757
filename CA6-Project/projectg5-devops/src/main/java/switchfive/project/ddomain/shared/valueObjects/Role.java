package switchfive.project.ddomain.shared.valueObjects;

public enum Role {
    /**
     * Each project have one scrum master.
     */
    ScrumMaster,
    /**
     * Each project have one product owner.
     */
    ProductOwner,
    /**
     * Each project have one project manager.
     */
    ProjectManager,
    /**
     * Each project have zero or many team members.
     */
    TeamMember
}


