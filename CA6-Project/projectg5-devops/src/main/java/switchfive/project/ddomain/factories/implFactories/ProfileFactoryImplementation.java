package switchfive.project.ddomain.factories.implFactories;

import org.springframework.stereotype.Component;
import switchfive.project.ddomain.aggregates.profile.Profile;
import switchfive.project.ddomain.factories.iFactories.ProfileFactory;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;

@Component
public class ProfileFactoryImplementation implements ProfileFactory {

    @Override
    public Profile createProfile(ProfileDescription profileDescription) {
        return new Profile(profileDescription);
    }
}
