package switchfive.project.ddomain.factories.iFactories;

import switchfive.project.ddomain.aggregates.typology.Typology;
import switchfive.project.ddomain.shared.valueObjects.TypologyDescription;


public interface ITypologyFactory {

    Typology createTypology(TypologyDescription description);
}
