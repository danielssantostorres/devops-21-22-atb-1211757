package switchfive.project.ddomain.domainServices.iDomainServices;

import switchfive.project.binterfaceAdapters.dataTransferObjects.ResourceCreationDTO;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.resource.Resource;

import java.text.ParseException;
import java.util.ArrayList;

public interface IResourceDomainService {

    boolean validateNewTeamMemberResource(ArrayList<Resource> resources,
                                          Project project,
                                          ResourceCreationDTO dto) throws ParseException;

    boolean validateNewProductOwnerResource(ArrayList<Resource> resources,
                                            Project project,
                                            ResourceCreationDTO dto)
            throws ParseException;

    boolean validateNewScrumMasterResource(ArrayList<Resource> resources,
                                           Project project,
                                           ResourceCreationDTO dto)
            throws ParseException;

}
