package switchfive.project.ddomain.shared.valueObjects;

import switchfive.project.ddomain.shared.dddTypes.ValueObject;

public enum TypeOfTask {

    MEETING,
    DOCUMENTATION,
    DESIGN,
    IMPLEMENTATION,
    TESTING,
    DEPLOYMENT
}
