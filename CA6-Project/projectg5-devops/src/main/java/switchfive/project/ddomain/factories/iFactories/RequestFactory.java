package switchfive.project.ddomain.factories.iFactories;

import switchfive.project.ddomain.aggregates.request.Request;
import switchfive.project.ddomain.shared.valueObjects.Email;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;
import switchfive.project.ddomain.shared.valueObjects.RequestID;

import java.time.LocalDate;

public interface RequestFactory {
    Request createNewRequest(final RequestID requestIdentity,
                             final Email requestedUserID,
                             final ProfileDescription selectedProfileID);

    Request createNewRequest(final RequestID requestIdentity,
                             final Email requestedUserID,
                             final ProfileDescription selectedProfileID,
                             final LocalDate creationDate,
                             final boolean isApproved);
}
