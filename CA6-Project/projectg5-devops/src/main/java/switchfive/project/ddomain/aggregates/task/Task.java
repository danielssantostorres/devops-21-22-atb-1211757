package switchfive.project.ddomain.aggregates.task;

import switchfive.project.ainfrastructure.persistence.data.TaskIDJPA;
import switchfive.project.ddomain.shared.dddTypes.Entity;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Task class describes the data and the methods of its objects.
 *
 * @author dst
 */
public class Task implements Entity<Task> {
    /**
     * Number of the task in Sprint. Starts with 1 and it's auto-incremented.
     */
    TaskID idTask;
    /**
     * Designation of the task.
     */
    TaskName name;
    /**
     * Description of the task.
     */
    TaskDescription description;
    /**
     * Start date, end date.
     */
    Time taskTime;
    /**
     * Hours spent (0 at the time it is created).
     */
    Hour hoursSpent;
    /**
     * Effort estimate (start by having the initial estimate, but can be
     * updated several times throughout the project; uses Fibonacci series
     * for duration in hours.).
     */
    EffortEstimate effortEstimate;
    /**
     * Percentage of execution (0% if it has not yet started, up to 100% when
     * it is completed; value calculated automatically based on the
     * relationship between the hours spent and the estimated effort).
     */
    double percentageOfExecution;
    /**
     * Precedence (optional, list of tasks that must be completed before the
     * start of the task).
     */
    List<TaskIDJPA> precedenceList;
    /**
     * Type of task (Meeting, Documentation, Design, Implementation, Testing,
     * Deployment, etc.).
     */
    TypeOfTask typeOfTask;
    /**
     * Responsible (human resource responsible for the execution of the task).
     */
    ResourceID responsible;

    /**
     * List of logs that store the work done on a task.
     */
    List<Log> taskLogs = new ArrayList<>();
    /**
     * List of possible status in a task.
     */
    TaskStatus taskStatus;

    public Task(TaskID idTask, TaskName name, TaskDescription description, Time taskTime,
                EffortEstimate effortEstimate, List<TaskIDJPA> precedenceList, TypeOfTask typeOfTask,
                ResourceID responsible) {
        this.idTask = idTask;
        this.name = name;
        this.description = description;
        this.taskTime = taskTime;
        this.hoursSpent = Hour.createHour(0);
        this.effortEstimate = effortEstimate;
        this.percentageOfExecution = 0;
        this.precedenceList = precedenceList;
        this.typeOfTask = typeOfTask;
        this.responsible = responsible;
        this.taskStatus = TaskStatus.PLANNED;
    }

    public Task(TaskID idTask, TaskName name,
                TaskDescription description,
                Time taskTime, Hour hoursSpent,
                EffortEstimate effortEstimate,
                double percentageOfExecution,
                List<TaskIDJPA> precedenceList, TypeOfTask typeOfTask,
                ResourceID responsible, List<Log> taskLogs,
                TaskStatus taskStatus) {
        this.idTask = idTask;
        this.name = name;
        this.description = description;
        this.taskTime = taskTime;
        this.hoursSpent = hoursSpent;
        this.effortEstimate = effortEstimate;
        this.percentageOfExecution = percentageOfExecution;
        this.precedenceList = precedenceList;
        this.typeOfTask = typeOfTask;
        this.responsible = responsible;
        this.taskLogs = taskLogs;
        this.taskStatus = taskStatus;
    }

    /**
     * Adds a new log entry to this taskLogs.
     *
     * @param timeSpent       the amount of time in hours spent in a given
     *                        work session.
     * @param workDescription a description of the work done in a given work
     *                        session.
     * @return true if it was added, false otherwise.
     */
    public boolean addTaskLog(final Hour timeSpent,
                              final TaskDescription workDescription) {
        final int logCode = generateLogCode();
        Log newLog = Log.createLog(logCode, timeSpent, workDescription);
        updateHoursSpent(timeSpent);
        return this.taskLogs.add(newLog);
    }

    private void updateHoursSpent(Hour timeSpent) {
        double currentHours = this.hoursSpent.getAmountOfHours();
        double newHours = timeSpent.getAmountOfHours();
        double newTotal = currentHours + newHours;
        this.hoursSpent = Hour.createHour(newTotal);
    }

    private int generateLogCode() {
        return this.taskLogs.size() + 1;
    }

    public boolean isTaskFinished() {
        return this.taskStatus.equals(TaskStatus.FINISHED);
    }


    @Override
    public boolean sameIdentityAs(final Task other) {
        return other != null && this.idTask.equals(other.idTask);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;
        Task task = (Task) o;
        return sameIdentityAs(task);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTask);
    }


    public TaskID getIdTask() {
        return idTask;
    }

    public String getName() {
        return name.getName();
    }

    public String getDescription() {
        return description.getDescription();
    }

    public String getStartDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date startDate = this.taskTime.getStartDate();

        return dateFormat.format(startDate);

    }

    public String getEndDate() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date endDate = this.taskTime.getEndDate();

        return dateFormat.format(endDate);
    }

    public double getHoursSpent() {
        return hoursSpent.getAmountOfHours();
    }

    public Integer getEffortEstimate() {
        return effortEstimate.getEffort();
    }

    public double getPercentageOfExecution() {
        return percentageOfExecution;
    }

    public List<TaskIDJPA> getPrecedenceList() {
        return precedenceList;
    }

    public String getTypeOfTask() {
        return typeOfTask.toString();
    }

    public String getResponsible() {
        return responsible.getResourceID().toString();
    }

    public List<Log> getTaskLogs() {
        return taskLogs;
    }

    public String getTaskStatus() {
        return taskStatus.toString();
    }
}
