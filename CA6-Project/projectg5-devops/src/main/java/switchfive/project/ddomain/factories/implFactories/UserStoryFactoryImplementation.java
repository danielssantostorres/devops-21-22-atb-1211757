package switchfive.project.ddomain.factories.implFactories;

import org.springframework.stereotype.Component;
import switchfive.project.ddomain.aggregates.userStory.UserStory;
import switchfive.project.ddomain.factories.iFactories.UserStoryFactory;
import switchfive.project.ddomain.shared.valueObjects.*;

@Component
public class UserStoryFactoryImplementation implements UserStoryFactory {

    @Override
    public UserStory createUserStory(UserStoryID userStoryID, ProjectCode projectCode, UserStoryCode userStoryCode,
                                     Priority priority, UserStoryDescription description) {
        return new UserStory(userStoryID, projectCode, userStoryCode, priority, description);
    }

    @Override
    public UserStory createUserStory(UserStoryID userStoryID, ProjectCode projectCode, UserStoryCode userStoryCode,
                                     Priority priority, UserStoryDescription description, UserStoryCode parentUserStoryCode) {
        return new UserStory(userStoryID, projectCode, userStoryCode, priority, description, parentUserStoryCode);
    }


    @Override
    public UserStory createUserStory(UserStoryID userStoryID, ProjectCode projectCode, UserStoryCode userStoryCode,
                                     Priority priority, UserStoryDescription description, SprintID sprintID,
                                     EffortEstimate effortEstimate, UserStoryStatus userStoryStatus,
                                     UserStoryCode parentUserStoryCode) {
        return new UserStory(userStoryID, projectCode, userStoryCode, priority, description, sprintID,
                effortEstimate, userStoryStatus, parentUserStoryCode);
    }
}
