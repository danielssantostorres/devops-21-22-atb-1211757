package switchfive.project.ddomain.shared.valueObjects;

public enum UserStoryStatus {
    PLANNED,
    RUNNING,
    BLOCKED,
    FINISHED,
    REFINED
}
