package switchfive.project.ddomain.factories.iFactories;

import switchfive.project.ddomain.aggregates.sprint.Sprint;
import switchfive.project.ddomain.shared.valueObjects.*;

public interface ISprintFactory {

    Sprint create(SprintID sprintID, ProjectCode projectCode,
                  SprintNumber sprintNumber, Time dates,
                  SprintDescription description, SprintStatus status);

}
