package switchfive.project.ddomain.domainServices.iDomainServices;

import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.aggregates.sprint.Sprint;
import switchfive.project.ddomain.aggregates.userStory.UserStory;

public interface IUserStoryDomainService {

    boolean canUSBeMovedFromProductBacklogToSprintBacklog(Project theProject,
                                                          Sprint theSprint,
                                                          UserStory theUserStory);
}
