package switchfive.project.ddomain.shared.valueObjects;

public enum TaskStatus {
    PLANNED,
    RUNNING,
    BLOCKED,
    FINISHED
}
