package switchfive.project.ddomain.factories.iFactories;

import switchfive.project.ddomain.aggregates.user.User;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.util.List;


public interface IUserFactory {

    User createUser(Email email,
                    Password password,
                    UserName userName,
                    Function function
    );

    User createUser(Email email,
                    List<ProfileDescription> userProfileList,
                    Activation activation,
                    Password password,
                    UserName userName,
                    Function function);
}
