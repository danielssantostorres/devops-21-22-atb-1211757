package switchfive.project.ddomain.shared.valueObjects;

import switchfive.project.ddomain.aggregates.task.TaskContainer;
import switchfive.project.ddomain.shared.dddTypes.ValueObject;

import java.io.Serializable;
import java.util.Objects;

public class TaskID implements ValueObject<TaskID>, Serializable {

    private TaskContainer taskContainer;
    private TaskCode taskCode;

    private TaskID(TaskCode taskCode, TaskContainer taskContainer) {
        this.taskCode = taskCode;
        this.taskContainer = taskContainer;
    }

    public static TaskID createIDTask(TaskCode taskCode, TaskContainer taskContainer) {
        return new TaskID(taskCode, taskContainer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskCode, taskContainer);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof TaskID)) {
            return false;
        }
        TaskID that = (TaskID) object;
        return sameValueAs(that);
    }

    @Override
    public boolean sameValueAs(TaskID other) {
        return other != null && taskCode == other.taskCode && taskContainer.equals(other.taskContainer);
    }

    public TaskContainer getTaskContainer() {
        return taskContainer;
    }

    public String getTaskCode() {
        return taskCode.getCode();
    }
}
