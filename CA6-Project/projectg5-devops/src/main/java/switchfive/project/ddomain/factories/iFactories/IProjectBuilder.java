package switchfive.project.ddomain.factories.iFactories;

import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;

/**
 * This is the «BUILDER» component of builder pattern.
 */
public interface IProjectBuilder {
    void addCodeNameDescriptionCustomerStatus(ProjectCode codeInput,
                                        ProjectName nameInput,
                                        ProjectDescription descriptionInput,
                                        CustomerName customerNameInput,
                                        ProjectStatus projectStatusInput);

    void addBusinessSector(ProjectBusinessSector businessSectorInput);

    void addBudget(ProjectBudget budgetInput);

    void addTime(Time datesInput) throws ParseException;

    void addSprintDuration(ProjectSprintDuration sprintDurationInput);

    void addNumberOfPlannedSprints(
            ProjectNumberOfPlannedSprints numberOfPlannedSprintsInput);

    void addTypologyDescription(TypologyDescription selectedTypology);

    void addStatus(ProjectStatus projectStatus);

    Project getProject();
}
