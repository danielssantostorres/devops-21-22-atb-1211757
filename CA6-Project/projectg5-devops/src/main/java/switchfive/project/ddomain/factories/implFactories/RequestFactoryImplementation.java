package switchfive.project.ddomain.factories.implFactories;

import org.springframework.stereotype.Component;
import switchfive.project.ddomain.aggregates.request.Request;
import switchfive.project.ddomain.factories.iFactories.RequestFactory;
import switchfive.project.ddomain.shared.valueObjects.Email;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;
import switchfive.project.ddomain.shared.valueObjects.RequestID;

import java.time.LocalDate;

@Component
public class RequestFactoryImplementation implements RequestFactory {

    @Override
    public Request createNewRequest(RequestID requestIdentity, Email requestedUserID,
                                    ProfileDescription selectedProfileID) {
        return new Request(requestIdentity, requestedUserID, selectedProfileID);
    }

    @Override
    public Request createNewRequest(RequestID requestIdentity,
                                    Email requestedUserID,
                                    ProfileDescription selectedProfileID,
                                    LocalDate creationDate,
                                    boolean isApproved) {
        return new Request(requestIdentity,
                requestedUserID,
                selectedProfileID,
                creationDate,
                isApproved);
    }
}
