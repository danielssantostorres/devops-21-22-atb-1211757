package switchfive.project.ddomain.factories.iFactories;

import switchfive.project.ddomain.aggregates.customer.Customer;
import switchfive.project.ddomain.shared.valueObjects.CustomerName;

public interface ICustomerFactory {

    Customer createCustomer(CustomerName name);

}
