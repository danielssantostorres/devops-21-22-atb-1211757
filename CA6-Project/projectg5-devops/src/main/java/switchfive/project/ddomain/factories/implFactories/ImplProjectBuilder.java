package switchfive.project.ddomain.factories.implFactories;

import org.springframework.stereotype.Component;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.factories.iFactories.IProjectBuilder;
import switchfive.project.ddomain.shared.valueObjects.*;

@Component
public class ImplProjectBuilder implements IProjectBuilder {
    private Project project;

    public ImplProjectBuilder() {
    }

    public void addCodeNameDescriptionCustomerStatus(final ProjectCode codeInput,
                                               final ProjectName nameInput,
                                               final ProjectDescription descriptionInput,
                                               final CustomerName customerNameInput,
                                               final ProjectStatus projectStatusInput) {
        project = new Project();
        this.project.addCode(codeInput);
        this.project.addName(nameInput);
        this.project.addDescription(descriptionInput);
        this.project.addCustomerName(customerNameInput);
        this.project.addStatus(projectStatusInput);
    }

    public void addBusinessSector(
            final ProjectBusinessSector businessSectorInput) {
        this.project.addBusinessSector(businessSectorInput);
    }

    public void addBudget(final ProjectBudget budgetInput) {
        this.project.addBudget(budgetInput);
    }

    public void addTime(final Time datesInput) {
        this.project.addDates(datesInput);
    }

    public void addSprintDuration(
            final ProjectSprintDuration sprintDurationInput) {
        this.project.addSprintDuration(sprintDurationInput);
    }

    public void addNumberOfPlannedSprints(
            final ProjectNumberOfPlannedSprints numberOfPlannedSprintsInput) {
        this.project.addNumberOfPlannedSprints(numberOfPlannedSprintsInput);
    }

    public void addTypologyDescription(
            final TypologyDescription selectedTypology) {
        this.project.addTypologyDescription(selectedTypology);
    }

    public void addStatus(final ProjectStatus projectStatus) {
        this.project.addStatus(projectStatus);
    }

    public Project getProject() {
        return this.project;
    }
}
