package switchfive.project.ddomain.factories.iFactories;

import switchfive.project.ainfrastructure.persistence.data.TaskIDJPA;
import switchfive.project.ainfrastructure.persistence.data.TaskJPA;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.aggregates.task.Task;
import switchfive.project.ddomain.aggregates.task.TaskContainer;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.util.List;
import java.util.Set;

public interface ITaskFactory {

    Task createTask(TaskID taskID, TaskName taskName,
                    TaskDescription taskDescription, Time taskTime, EffortEstimate effortEstimate,
                    List<TaskIDJPA> precedenceList, TypeOfTask typeOfTask, ResourceID responsible);

    Task createTaskFromDB(TaskID idTask, TaskName name,
                    TaskDescription description,
                    Time taskTime, Hour hoursSpent,
                    EffortEstimate effortEstimate,
                    double percentageOfExecution,
                    List<TaskIDJPA> precedenceList, TypeOfTask typeOfTask,
                    ResourceID responsible, List<Log> taskLogs,
                    TaskStatus taskStatus);

}
