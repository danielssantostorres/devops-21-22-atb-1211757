package switchfive.project.ddomain.factories.implFactories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.ddomain.aggregates.customer.Customer;
import switchfive.project.ddomain.factories.iFactories.ICustomerFactory;
import switchfive.project.ddomain.shared.valueObjects.CustomerName;

@Component
public class ImplCustomerFactory implements ICustomerFactory {

    @Autowired
    public ImplCustomerFactory() {
    }

    public Customer createCustomer(CustomerName customerName) {
        return new Customer(customerName);
    }
}
