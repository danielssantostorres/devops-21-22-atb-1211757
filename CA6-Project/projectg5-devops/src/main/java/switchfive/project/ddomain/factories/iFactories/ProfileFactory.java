package switchfive.project.ddomain.factories.iFactories;

import switchfive.project.ddomain.aggregates.profile.Profile;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;

public interface ProfileFactory {
    Profile createProfile(ProfileDescription profileDescription);
}
