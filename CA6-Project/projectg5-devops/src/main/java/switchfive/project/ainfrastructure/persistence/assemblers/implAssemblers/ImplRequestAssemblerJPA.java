package switchfive.project.ainfrastructure.persistence.assemblers.implAssemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IRequestAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.RequestJPA;
import switchfive.project.ddomain.aggregates.request.Request;
import switchfive.project.ddomain.factories.iFactories.RequestFactory;
import switchfive.project.ddomain.shared.valueObjects.Email;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;
import switchfive.project.ddomain.shared.valueObjects.RequestID;

import java.time.LocalDate;

@Service
public class ImplRequestAssemblerJPA implements IRequestAssemblerJPA {

    @Autowired
    private RequestFactory requestFactory;

    public Request toDomain(RequestJPA requestJPA) {
        String requestIDJPA = requestJPA.getIdentity();
        String userIDJPA = requestJPA.getUserID();
        String profileIDJPA = requestJPA.getProfileDescription();
        String creationDateJPA = requestJPA.getCreationDate();
        boolean isApprovedJPA = requestJPA.isApproved();

        RequestID requestID = RequestID.createRequestID(requestIDJPA);
        Email userID = Email.createEmail(userIDJPA);
        ProfileDescription profileID = ProfileDescription.createProfileDescription(profileIDJPA);
        LocalDate creationDate = java.time.LocalDate.parse(creationDateJPA);

        return requestFactory.createNewRequest(requestID,
                userID,
                profileID,
                creationDate,
                isApprovedJPA);
    }

    public RequestJPA toData(Request request) {

        String requestIDJPA = request.getIdentity();
        String userIDJPA = request.getUserID();
        String profileIDJPA = request.getProfileDescription();
        String creationDateJPA = request.getCreationDate();
        boolean isApprovedJPA = request.isRequestApproved();

        RequestJPA requestJPA = new RequestJPA();
        requestJPA.setIdentity(requestIDJPA);
        requestJPA.setUserID(userIDJPA);
        requestJPA.setProfileDescription(profileIDJPA);
        requestJPA.setCreationDate(creationDateJPA);
        requestJPA.setApproved(isApprovedJPA);

        return requestJPA;
    }

}
