package switchfive.project.ainfrastructure.persistence.iRepositories;

import org.springframework.data.repository.CrudRepository;
import switchfive.project.ainfrastructure.persistence.data.ProfileJPA;

import java.util.Optional;

public interface IProfileRepositoryJPA extends CrudRepository<ProfileJPA, Integer> {

    Optional<ProfileJPA> findProfileJPAByProfileDescription(String profileDescription);

    boolean existsProfileJPAByProfileDescription(String profileDescription);

}
