package switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers;

import switchfive.project.ainfrastructure.persistence.data.UserJPA;
import switchfive.project.ddomain.aggregates.user.User;

import java.security.NoSuchAlgorithmException;

public interface IUserAssemblerJPA {

    UserJPA toJPA(User user);

    User toUser(UserJPA userJPA) throws NoSuchAlgorithmException;

}
