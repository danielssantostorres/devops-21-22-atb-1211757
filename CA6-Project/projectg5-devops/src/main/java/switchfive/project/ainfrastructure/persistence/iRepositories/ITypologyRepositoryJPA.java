package switchfive.project.ainfrastructure.persistence.iRepositories;

import org.springframework.data.repository.CrudRepository;
import switchfive.project.ainfrastructure.persistence.data.TypologyJPA;

import java.util.Optional;

public interface ITypologyRepositoryJPA extends CrudRepository<TypologyJPA, Integer> {

    Optional<TypologyJPA> findByDescription(String description);

    boolean existsByDescription(String description);

}
