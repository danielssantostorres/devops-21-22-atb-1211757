package switchfive.project.ainfrastructure.persistence.assemblers.implAssemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IResourceAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.ResourceJPA;
import switchfive.project.ddomain.aggregates.resource.Resource;
import switchfive.project.ddomain.factories.iFactories.IResourceFactory;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;

@Service
public class ImplResourceAssemblerJPA implements IResourceAssemblerJPA {

    private final IResourceFactory resourceFactory;

    @Autowired
    public ImplResourceAssemblerJPA(IResourceFactory resourceFactory) {
        this.resourceFactory = resourceFactory;
    }

    public Resource toDomain(ResourceJPA resourceJPA) throws ParseException {
        String resourceIDJPA = resourceJPA.getResourceID();
        String userIDJPA = resourceJPA.getUserID();
        String projectCodeJPA = resourceJPA.getProjectCode();
        String startDateJPA = resourceJPA.getStartDate();
        String endDateJPA = resourceJPA.getEndDate();
        Double costPerHourJPA = resourceJPA.getCostPerHour();
        Double allocationJPA = resourceJPA.getPercentageOfAllocation();
        String roleJPA = resourceJPA.getRole();

        ResourceID resourceID = ResourceID.createResourceID(resourceIDJPA);
        Email userID = Email.createEmail(userIDJPA);
        ProjectCode projectCode = ProjectCode.createProjectCode(projectCodeJPA);
        Time dates = Time.create(startDateJPA, endDateJPA);
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(costPerHourJPA);
        ResourcePercentageOfAllocation allocation = ResourcePercentageOfAllocation.create(allocationJPA);
        Role role = Role.valueOf(roleJPA);

        return resourceFactory.createResource(resourceID, userID, projectCode, dates
                , costPerHour, allocation, role);

    }

    public Resource toDomainWithEmail(ResourceJPA resourceJPA)
            throws ParseException {
        String resourceIDJPA = resourceJPA.getResourceID();
        String emailJPA = resourceJPA.getEmail();
        String projectCodeJPA = resourceJPA.getProjectCode();
        String startDateJPA = resourceJPA.getStartDate();
        String endDateJPA = resourceJPA.getEndDate();
        Double costPerHourJPA = resourceJPA.getCostPerHour();
        Double allocationJPA = resourceJPA.getPercentageOfAllocation();
        String roleJPA = resourceJPA.getRole();

        ResourceID resourceID = ResourceID.createResourceID(resourceIDJPA);
        Email email = Email.createEmail(emailJPA);
        ProjectCode projectCode = ProjectCode.createProjectCode(projectCodeJPA);
        Time dates = Time.create(startDateJPA, endDateJPA);
        ResourceCostPerHour costPerHour = ResourceCostPerHour.create(costPerHourJPA);
        ResourcePercentageOfAllocation allocation = ResourcePercentageOfAllocation.create(allocationJPA);
        Role role = Role.valueOf(roleJPA);

        return resourceFactory.createResourceWithEmail(resourceID, email, projectCode, dates
                , costPerHour, allocation, role);

    }

    public ResourceJPA toData(Resource resource) {

        ResourceJPA resourceJPA = new ResourceJPA();

        resourceJPA.setResourceID(resource.getResourceID());
        resourceJPA.setUserID(resource.getUserID().getUserEmail());
        resourceJPA.setProjectCode(resource.projectCode());
        resourceJPA.setStartDate(resource.getStartDate());
        resourceJPA.setEndDate(resource.getEndDate());
        resourceJPA.setCostPerHour(resource.getCostPerHour());
        resourceJPA.setPercentageOfAllocation(resource.getAllocation());
        resourceJPA.setRole(resource.getRole());
        resourceJPA.setEmail(resource.getUserID().getUserEmail());

        return resourceJPA;
    }
}
