package switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers;

import switchfive.project.ainfrastructure.persistence.data.ProfileJPA;
import switchfive.project.ddomain.aggregates.profile.Profile;

public interface IProfileAssemblerJPA {

    Profile toDomain(ProfileJPA profileJPA);

    ProfileJPA toData(Profile profile);

}
