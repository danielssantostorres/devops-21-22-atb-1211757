package switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers;

import switchfive.project.ainfrastructure.persistence.data.SprintJPA;
import switchfive.project.ddomain.aggregates.sprint.Sprint;

import java.text.ParseException;

public interface ISprintAssemblerJPA {

    Sprint toDomain(SprintJPA sprintJPA) throws ParseException;

}
