package switchfive.project.ainfrastructure.persistence.iRepositories;

import org.springframework.data.repository.CrudRepository;
import switchfive.project.ainfrastructure.persistence.data.RequestJPA;

import java.util.Optional;

public interface IRequestRepositoryJPA extends CrudRepository<RequestJPA, String> {

    Optional<RequestJPA> findByUserIDAndProfileDescription (String userID,
                                                            String profileDescription);

}
