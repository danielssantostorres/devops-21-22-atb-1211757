package switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers;

import switchfive.project.ainfrastructure.persistence.data.ProjectJPA;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;

public interface IProjectAssemblerJPA {
    TypologyDescription getTypologyDescriptionFromProjectJPA(ProjectJPA projectJPA);

    CustomerName getCustomerNameFromProjectJPA(ProjectJPA projectJPA);

    ProjectCode getProjectCodeFromProjectJPA(ProjectJPA projectJPA);

    ProjectName getProjectNameFromProjectJPA(ProjectJPA projectJPA);

    ProjectDescription getDescriptionFromProjectJPA(ProjectJPA projectJPA);

    ProjectBusinessSector getBusinessSectorFromProjectJPA(ProjectJPA projectJPA);

    Time getDatesFromProjectJPA(ProjectJPA projectJPA) throws ParseException;

    ProjectNumberOfPlannedSprints getNumberOfSprintsFromProjectJPA(ProjectJPA projectJPA);

    ProjectBudget getBudgetFromProjectJPA(ProjectJPA projectJPA);

    ProjectSprintDuration getSprintDurationFromProjectJPA(ProjectJPA projectJPA);

    Project toDomain(ProjectJPA projectJPA) throws ParseException;
}
