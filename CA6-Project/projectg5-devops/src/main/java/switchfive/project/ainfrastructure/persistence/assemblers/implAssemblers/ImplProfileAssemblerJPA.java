package switchfive.project.ainfrastructure.persistence.assemblers.implAssemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IProfileAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.ProfileJPA;
import switchfive.project.ddomain.aggregates.profile.Profile;
import switchfive.project.ddomain.factories.iFactories.ProfileFactory;
import switchfive.project.ddomain.shared.valueObjects.ProfileDescription;

@Service
public class ImplProfileAssemblerJPA implements IProfileAssemblerJPA {

    @Autowired
    private ProfileFactory profileFactory;

    public Profile toDomain(ProfileJPA profileJPA) {
        String profileDescriptionJPA = profileJPA.getProfileDescription();

        ProfileDescription profileDescription = ProfileDescription
                .createProfileDescription(profileDescriptionJPA);

        return profileFactory.createProfile(profileDescription);

    }

    public ProfileJPA toData(Profile profile) {
        String profileDescriptionJPA = profile.getDesignation();

        ProfileJPA profileJPA = new ProfileJPA();
        profileJPA.setProfileDescription(profileDescriptionJPA);

        return profileJPA;

    }


}
