package switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers;

import switchfive.project.ainfrastructure.persistence.data.UserStoryJPA;
import switchfive.project.ddomain.aggregates.userStory.UserStory;
import switchfive.project.ddomain.shared.valueObjects.*;

public interface IUserStoryAssemblerJPA {

    UserStoryID getUserStoryIdFromUserStoryJPA(UserStoryJPA userStoryJPA);

    ProjectCode getProjectCodeFromUserStoryJPA(UserStoryJPA userStoryJPA);

    UserStoryCode getUserStoryCodeFromUserStoryJPA(UserStoryJPA userStoryJPA);

    UserStoryDescription getDescriptionFromUserStoryJPA(UserStoryJPA userStoryJPA);

    SprintID getSprintIdFromUserStoryJPA(UserStoryJPA userStoryJPA);

    EffortEstimate getEffortEstimateFromUserStoryJPA(UserStoryJPA userStoryJPA);

    Priority getPriorityFromUserStoryJPA(UserStoryJPA userStoryJPA);

    UserStoryStatus getStatusFromUserStoryJPA(UserStoryJPA userStoryJPA);

    UserStoryCode getParentUserStoryCodeFromUserStoryJPA(UserStoryJPA userStoryJPA);

    UserStoryJPA userStoryToUserStoryJPA(UserStory userStory);

    UserStory userStoryJPAtoUserStory(UserStoryJPA userStoryJPA);
}
