package switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers;

import switchfive.project.ainfrastructure.persistence.data.CustomerJPA;
import switchfive.project.ddomain.aggregates.customer.Customer;

public interface ICustomerAssemblerJPA {

    Customer customerJpaToCustomer (CustomerJPA customerJPA);

    CustomerJPA customerToCustomerJPA (Customer customer);

}
