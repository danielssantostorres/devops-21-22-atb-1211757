package switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers;

import switchfive.project.ainfrastructure.persistence.data.TaskJPA;
import switchfive.project.ddomain.aggregates.task.Task;

import java.text.ParseException;

public interface ITaskAssemblerJPA {

    Task toDomain(TaskJPA taskJPA) throws ParseException;

    TaskJPA toData(Task task);

}
