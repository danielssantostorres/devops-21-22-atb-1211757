package switchfive.project.ainfrastructure.persistence.iRepositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import switchfive.project.ainfrastructure.persistence.data.UserJPA;

import java.util.List;
import java.util.Optional;

public interface IUserRepositoryJPA extends CrudRepository<UserJPA, String> {

    @Query("SELECT e FROM UserJPA e WHERE e.email like %:email%")
    List<UserJPA> findUsersByEmail(@Param("email") String email);

    Optional<UserJPA> findUserJPAByEmail(@Param("email") String email);

    @Query("SELECT e FROM UserJPA e join e.profileList p WHERE p = :profile")
    List<UserJPA> findUsersJPAByProfile(String profile);

    List<UserJPA> findAll();
}
