package switchfive.project.ainfrastructure.persistence.iRepositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import switchfive.project.ainfrastructure.persistence.data.TaskIDJPA;
import switchfive.project.ainfrastructure.persistence.data.TaskJPA;
import switchfive.project.ddomain.shared.valueObjects.ProjectCode;
import switchfive.project.ddomain.shared.valueObjects.TaskID;

import java.util.List;

public interface ITaskRepositoryJPA extends CrudRepository<TaskJPA, TaskIDJPA> {

    List<TaskJPA> findTaskJPAByProjectCode(String taskCode);

    List<TaskJPA> findTaskJPAByProjectCodeOrderByTaskStatus(String projectCode);

}
