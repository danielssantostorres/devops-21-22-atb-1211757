package switchfive.project.ainfrastructure.persistence.assemblers.implAssemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.IProjectAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.ProjectJPA;
import switchfive.project.ddomain.aggregates.project.Project;
import switchfive.project.ddomain.factories.iFactories.IProjectBuilder;
import switchfive.project.ddomain.shared.valueObjects.*;

import java.text.ParseException;

@Component
public class ImplProjectAssemblerJPA implements IProjectAssemblerJPA {
    IProjectBuilder builder;

    @Autowired
    public ImplProjectAssemblerJPA(IProjectBuilder builder) {
        this.builder = builder;
    }

    public TypologyDescription getTypologyDescriptionFromProjectJPA(
            ProjectJPA projectJPA) {
        return TypologyDescription.create(projectJPA.getTypologyDescription());
    }

    public CustomerName getCustomerNameFromProjectJPA(ProjectJPA projectJPA) {
        return CustomerName.create(projectJPA.getCustomerName());
    }

    public ProjectCode getProjectCodeFromProjectJPA(ProjectJPA projectJPA) {
        return ProjectCode.createProjectCode(projectJPA.getCode());
    }

    public ProjectName getProjectNameFromProjectJPA(ProjectJPA projectJPA) {
        return ProjectName.create(projectJPA.getName());
    }

    public ProjectDescription getDescriptionFromProjectJPA(
            ProjectJPA projectJPA) {
        return ProjectDescription.create(projectJPA.getDescription());
    }

    public ProjectBusinessSector getBusinessSectorFromProjectJPA(
            ProjectJPA projectJPA) {
        return ProjectBusinessSector.create(projectJPA.getBusinessSector());
    }

    public Time getDatesFromProjectJPA(ProjectJPA projectJPA)
            throws ParseException {
        return Time.create(projectJPA.getStartDate(), projectJPA.getEndDate());
    }

    public ProjectNumberOfPlannedSprints getNumberOfSprintsFromProjectJPA(
            ProjectJPA projectJPA) {
        return ProjectNumberOfPlannedSprints.create(
                projectJPA.getNumberOfPlannedSprints());
    }

    public ProjectBudget getBudgetFromProjectJPA(ProjectJPA projectJPA) {
        return ProjectBudget.create(projectJPA.getBudget());
    }

    public ProjectSprintDuration getSprintDurationFromProjectJPA(
            ProjectJPA projectJPA) {
        return ProjectSprintDuration.create(projectJPA.getSprintDuration());
    }

    public ProjectStatus getProjectStatusFromProjectJPA(ProjectJPA projectJPA) {
        return ProjectStatus.valueOf(projectJPA.getStatus());

    }

    public Project toDomain(ProjectJPA projectJPA)
            throws ParseException {
        ProjectCode code =
                this.getProjectCodeFromProjectJPA(projectJPA);
        ProjectName name =
                this.getProjectNameFromProjectJPA(projectJPA);
        ProjectDescription description =
                this.getDescriptionFromProjectJPA(projectJPA);
        CustomerName customerID =
                this.getCustomerNameFromProjectJPA(projectJPA);
        TypologyDescription typologyDescription =
                this.getTypologyDescriptionFromProjectJPA(projectJPA);
        Time dates = this.getDatesFromProjectJPA(projectJPA);
        ProjectSprintDuration sprintDuration =
                this.getSprintDurationFromProjectJPA(
                        projectJPA);
        ProjectNumberOfPlannedSprints numberOfSprints =
                this.getNumberOfSprintsFromProjectJPA(
                        projectJPA);
        ProjectBudget budget =
                this.getBudgetFromProjectJPA(projectJPA);
        ProjectBusinessSector businessSector =
                this.getBusinessSectorFromProjectJPA(
                        projectJPA);
        ProjectStatus status = this.getProjectStatusFromProjectJPA(projectJPA);

        this.builder.addCodeNameDescriptionCustomerStatus(code, name, description, customerID, status);
        this.builder.addTypologyDescription(typologyDescription);
        this.builder.addTime(dates);
        this.builder.addSprintDuration(sprintDuration);
        this.builder.addNumberOfPlannedSprints(numberOfSprints);
        this.builder.addBudget(budget);
        this.builder.addBusinessSector(
                businessSector);

        return this.builder.getProject();

    }
}
