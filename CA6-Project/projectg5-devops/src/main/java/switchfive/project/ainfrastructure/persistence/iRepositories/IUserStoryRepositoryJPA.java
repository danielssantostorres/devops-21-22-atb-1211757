package switchfive.project.ainfrastructure.persistence.iRepositories;

import org.springframework.data.repository.CrudRepository;
import switchfive.project.ainfrastructure.persistence.data.UserStoryJPA;
import switchfive.project.ddomain.aggregates.userStory.UserStory;

import java.util.List;

public interface IUserStoryRepositoryJPA extends CrudRepository<UserStoryJPA,
        String> {

    boolean existsByUserStoryID_UserStoryCodeAndUserStoryID_ProjectCode(String userStoryCode, String projectCode);

    List<UserStoryJPA> findByUserStoryID_ProjectCode_OrderByPriority(String projectCode);

    List<UserStoryJPA> findByUserStoryID_ProjectCodeAndSprintNumberEqualsAndStatusEqualsOrderByPriority(String projectCode,
                                                                                                        int sprintNumber, String status);

    UserStoryJPA findByUserStoryID_UserStoryCodeAndUserStoryID_ProjectCode(String userStoryCode, String projectCode);

    List<UserStoryJPA> findByUserStoryID_ProjectCodeOrderByStatus(String projectCode);
}
