package switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers;

import switchfive.project.ainfrastructure.persistence.data.TypologyJPA;
import switchfive.project.ddomain.aggregates.typology.Typology;
import switchfive.project.ddomain.shared.valueObjects.TypologyDescription;


public interface ITypologyAssemblerJPA {

    TypologyDescription getTypologyDescription(TypologyJPA typologyJPA);

    Typology toDomain(TypologyJPA typologyJPA);

    TypologyJPA toData(Typology typology);

}
