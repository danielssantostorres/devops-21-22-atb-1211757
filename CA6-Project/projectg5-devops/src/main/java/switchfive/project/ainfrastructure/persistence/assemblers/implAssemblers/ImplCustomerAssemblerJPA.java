package switchfive.project.ainfrastructure.persistence.assemblers.implAssemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.ICustomerAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.CustomerJPA;
import switchfive.project.ddomain.aggregates.customer.Customer;
import switchfive.project.ddomain.factories.iFactories.ICustomerFactory;
import switchfive.project.ddomain.shared.valueObjects.CustomerName;

@Component
public class ImplCustomerAssemblerJPA implements ICustomerAssemblerJPA {

    ICustomerFactory customerFactory;

    @Autowired
    public ImplCustomerAssemblerJPA(ICustomerFactory customerFactory) {
        this.customerFactory = customerFactory;
    }

    public Customer customerJpaToCustomer(CustomerJPA customerJPA) {
        String jpaName = customerJPA.getName();
        CustomerName name = CustomerName.create(jpaName);

        return this.customerFactory.createCustomer(name);
    }

    public CustomerJPA customerToCustomerJPA(Customer customer) {
        String name = customer.getCustomerName();

        return CustomerJPA.createCustomerJpa(name);
    }
}
