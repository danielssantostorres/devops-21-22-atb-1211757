package switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers;

import switchfive.project.ainfrastructure.persistence.data.RequestJPA;
import switchfive.project.ddomain.aggregates.request.Request;

public interface IRequestAssemblerJPA {

    Request toDomain(RequestJPA requestJPA);

    RequestJPA toData(Request request);

}
