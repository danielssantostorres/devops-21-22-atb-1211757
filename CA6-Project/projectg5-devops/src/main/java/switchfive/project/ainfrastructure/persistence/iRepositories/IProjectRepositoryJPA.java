package switchfive.project.ainfrastructure.persistence.iRepositories;

import org.springframework.data.repository.CrudRepository;
import switchfive.project.ainfrastructure.persistence.data.ProjectJPA;

import java.util.List;
import java.util.Optional;

public interface IProjectRepositoryJPA extends CrudRepository<ProjectJPA, String> {

    boolean existsByCode(String code);

    Optional<ProjectJPA> findByCode(String code);

    List<ProjectJPA> findAll();

}
