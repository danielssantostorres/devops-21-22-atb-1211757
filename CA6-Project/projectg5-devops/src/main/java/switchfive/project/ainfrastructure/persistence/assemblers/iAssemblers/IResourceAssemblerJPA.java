package switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers;

import switchfive.project.ainfrastructure.persistence.data.ResourceJPA;
import switchfive.project.ddomain.aggregates.resource.Resource;

import java.text.ParseException;

public interface IResourceAssemblerJPA {

    Resource toDomain(ResourceJPA resourceJPA) throws ParseException;

    Resource toDomainWithEmail(ResourceJPA resourceJPA) throws ParseException;

    ResourceJPA toData(Resource resource);
}
