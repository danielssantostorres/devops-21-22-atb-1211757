package switchfive.project.ainfrastructure.persistence.assemblers.implAssemblers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import switchfive.project.ainfrastructure.persistence.assemblers.iAssemblers.ITypologyAssemblerJPA;
import switchfive.project.ainfrastructure.persistence.data.TypologyJPA;
import switchfive.project.ddomain.aggregates.typology.Typology;
import switchfive.project.ddomain.factories.iFactories.ITypologyFactory;
import switchfive.project.ddomain.shared.valueObjects.TypologyDescription;

@Service
public class ImplTypologyAssemblerJPA implements ITypologyAssemblerJPA {

    @Autowired
    private ITypologyFactory typologyFactory;

    public TypologyDescription getTypologyDescription(TypologyJPA typologyJPA) {
        String description = typologyJPA.getDescription();
        return TypologyDescription.create(description);
    }


    public Typology toDomain(TypologyJPA typologyJPA) {
        TypologyDescription description = this.getTypologyDescription(typologyJPA);
        return this.typologyFactory.createTypology(description);
    }


    public TypologyJPA toData(Typology typology) {
        String description = typology.getDescription();

        TypologyJPA typologyJPA = new TypologyJPA();
        typologyJPA.setDescription(description);

        return typologyJPA;
    }
}
